package org.schema.schine.network;

import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.StringTools;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.resource.FileExt;

public class DataStatsEntry {
   public final long time;
   public final Object2ObjectOpenHashMap entry;
   public final long volume;
   public boolean selected;

   public DataStatsEntry(long var1, Object2ObjectOpenHashMap var3) {
      this.time = var1;
      this.entry = var3;
      this.volume = this.getVolume(var3);
   }

   public static long calcVolume(Int2LongOpenHashMap var0) {
      long var1 = 0L;

      long var3;
      for(Iterator var5 = var0.values().iterator(); var5.hasNext(); var1 += var3) {
         var3 = (Long)var5.next();
      }

      return var1;
   }

   private long getVolume(Object2ObjectOpenHashMap var1) {
      long var2 = 0L;
      Iterator var6 = var1.entrySet().iterator();

      Long var5;
      while(var6.hasNext()) {
         for(Iterator var4 = ((Int2LongOpenHashMap)((Entry)var6.next()).getValue()).values().iterator(); var4.hasNext(); var2 += var5) {
            var5 = (Long)var4.next();
         }
      }

      return var2;
   }

   public int hashCode() {
      return (int)(this.time ^ this.time >>> 32);
   }

   public boolean equals(Object var1) {
      return ((DataStatsEntry)var1).time == this.time;
   }

   public String save(boolean var1) {
      (new FileExt("./savedNetworkStatistics")).mkdir();
      SimpleDateFormat var2 = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
      FileExt var19 = new FileExt("./savedNetworkStatistics/statistics_" + this.time + "_" + var2.format(this.time) + (var1 ? "_SENT" : "_RECEIVED") + ".txt");
      BufferedWriter var20 = null;
      boolean var13 = false;

      label165: {
         try {
            Iterator var3;
            try {
               var13 = true;
               var20 = new BufferedWriter(new FileWriter(var19));
               var3 = this.entry.entrySet().iterator();

               while(var3.hasNext()) {
                  Entry var4;
                  String var5 = ((Class)(var4 = (Entry)var3.next()).getKey()).toString();
                  var20.write(var5);

                  for(int var22 = var5.length(); var22 < 95; ++var22) {
                     var20.write("-");
                  }

                  var20.write(" ");
                  var20.write(StringTools.readableFileSize(calcVolume((Int2LongOpenHashMap)var4.getValue())));
                  String[] var23 = NetworkObject.getFieldNames((Class)var4.getKey());
                  var20.newLine();
                  Iterator var21 = ((Int2LongOpenHashMap)var4.getValue()).int2LongEntrySet().iterator();

                  while(var21.hasNext()) {
                     it.unimi.dsi.fastutil.ints.Int2LongMap.Entry var6 = (it.unimi.dsi.fastutil.ints.Int2LongMap.Entry)var21.next();
                     String var7 = "    " + var23[var6.getIntKey()];
                     var20.write(var7);

                     for(int var24 = var7.length(); var24 < 96; ++var24) {
                        var20.write(" ");
                     }

                     var20.write(StringTools.readableFileSize(var6.getLongValue()));
                     var20.newLine();
                  }
               }

               var13 = false;
               break label165;
            } catch (IOException var17) {
               var3 = null;
               var17.printStackTrace();
               var13 = false;
            }
         } finally {
            if (var13) {
               if (var20 != null) {
                  try {
                     var20.close();
                  } catch (IOException var14) {
                     var14.printStackTrace();
                  }
               }

            }
         }

         if (var20 != null) {
            try {
               var20.close();
            } catch (IOException var15) {
               var15.printStackTrace();
            }

            return var19.getAbsolutePath();
         }

         return var19.getAbsolutePath();
      }

      try {
         var20.close();
      } catch (IOException var16) {
         var16.printStackTrace();
      }

      return var19.getAbsolutePath();
   }
}
