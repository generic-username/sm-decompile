package org.schema.schine.network;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import org.schema.schine.network.server.ServerStateInterface;

public class MultiBufferedOutputStream extends BufferedOutputStream {
   private ServerStateInterface state;

   public MultiBufferedOutputStream(ServerStateInterface var1) {
      super((OutputStream)null);
      this.state = var1;
   }

   public void write(byte[] var1) throws IOException {
      Iterator var2 = this.state.getClients().values().iterator();

      while(var2.hasNext()) {
         ((RegisteredClientOnServer)var2.next()).getProcessor().getOut().write(var1);
      }

   }

   public synchronized void write(int var1) throws IOException {
      Iterator var2 = this.state.getClients().values().iterator();

      while(var2.hasNext()) {
         ((RegisteredClientOnServer)var2.next()).getProcessor().getOut().write(var1);
      }

   }

   public synchronized void write(byte[] var1, int var2, int var3) throws IOException {
      Iterator var4 = this.state.getClients().values().iterator();

      while(var4.hasNext()) {
         ((RegisteredClientOnServer)var4.next()).getProcessor().getOut().write(var1, var2, var3);
      }

   }

   public synchronized void flush() throws IOException {
      Iterator var1 = this.state.getClients().values().iterator();

      while(var1.hasNext()) {
         ((RegisteredClientOnServer)var1.next()).getProcessor().getOut().flush();
      }

   }
}
