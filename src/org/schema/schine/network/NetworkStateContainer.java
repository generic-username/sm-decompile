package org.schema.schine.network;

import it.unimi.dsi.fastutil.ints.Int2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.synchronization.GhostSendable;
import org.schema.schine.resource.UniqueInterface;
import org.schema.schine.resource.tag.TagSerializable;

public class NetworkStateContainer {
   public final ObjectArrayList updateSet = new ObjectArrayList();
   public final Int2BooleanOpenHashMap newStatesBeforeForce = new Int2BooleanOpenHashMap();
   private final Int2ObjectOpenHashMap localObjects = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap localUpdatableObjects = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap topLevelObjects = new Int2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap uidObjects = new Object2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap dbObjects = new Long2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap remoteObjects = new Int2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap persistentObjects = new Object2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap ghostObjects = new Int2ObjectOpenHashMap();
   private final boolean privateChannel;
   private final StateInterface state;
   public ObjectArrayList debugReceivedClasses = new ObjectArrayList();
   private final Int2ObjectOpenHashMap empty = new Int2ObjectOpenHashMap();

   public NetworkStateContainer(boolean var1, StateInterface var2) {
      this.state = var2;
      this.privateChannel = var1;
   }

   public void checkGhostObjects() {
      if (!this.getGhostObjects().isEmpty()) {
         long var1 = System.currentTimeMillis();
         synchronized(this.getGhostObjects()) {
            ObjectIterator var4 = this.getGhostObjects().values().iterator();

            while(var4.hasNext()) {
               GhostSendable var5 = (GhostSendable)var4.next();
               if (var1 - var5.timeDeleted > 20000L) {
                  var4.remove();
               }
            }

         }
      }
   }

   public Int2ObjectOpenHashMap getGhostObjects() {
      return this.ghostObjects;
   }

   public Int2ObjectOpenHashMap getLocalObjects() {
      return this.localObjects;
   }

   public Int2ObjectOpenHashMap getLocalUpdatableObjects() {
      return this.localUpdatableObjects;
   }

   public Int2ObjectOpenHashMap getRemoteObjects() {
      return this.remoteObjects;
   }

   public boolean isPrivateChannel() {
      return this.privateChannel;
   }

   public void putLocal(int var1, Sendable var2) {
      assert var2 != null;

      assert this.state.isSynched();

      if (var2 == null) {
         throw new NullPointerException();
      } else {
         this.localObjects.put(var1, var2);
         if (var2.isUpdatable()) {
            this.getLocalUpdatableObjects().put(var1, var2);
         }

         if (var2 instanceof TagSerializable && ((UniqueInterface)var2).getUniqueIdentifier() != null) {
            this.getUidObjectMap().put(((UniqueInterface)var2).getUniqueIdentifier(), var2);
         }

         if (var2 instanceof UniqueLongIDInterface && ((UniqueLongIDInterface)var2).getDbId() > 0L) {
            this.dbObjects.put(((UniqueLongIDInterface)var2).getDbId(), var2);
         }

         Int2ObjectOpenHashMap var3;
         if ((var3 = (Int2ObjectOpenHashMap)this.topLevelObjects.get(var2.getTopLevelType().ordinal())) == null) {
            var3 = new Int2ObjectOpenHashMap();
            this.topLevelObjects.put(var2.getTopLevelType().ordinal(), var3);
         }

         var3.put(var1, var2);
      }
   }

   public Sendable removeLocal(int var1) {
      Sendable var2;
      Int2ObjectOpenHashMap var3;
      if ((var2 = (Sendable)this.localObjects.remove(var1)) != null && (var3 = (Int2ObjectOpenHashMap)this.topLevelObjects.get(var2.getTopLevelType().ordinal())) != null) {
         var3.remove(var1);
      }

      System.err.println("[ENTITIES] removed object from loaded state " + var2 + "; " + var1);

      assert this.state.isSynched();

      if (var2.isUpdatable()) {
         this.getLocalUpdatableObjects().remove(var1);
      }

      if (var2 instanceof UniqueInterface && ((UniqueInterface)var2).getUniqueIdentifier() != null) {
         this.getUidObjectMap().remove(((UniqueInterface)var2).getUniqueIdentifier());
      }

      if (var2 instanceof UniqueLongIDInterface) {
         this.dbObjects.remove(((UniqueLongIDInterface)var2).getDbId());
      }

      return var2;
   }

   public String toString() {
      return "(Local/Remote: " + this.localObjects + "/" + this.remoteObjects + ")";
   }

   public Object2ObjectOpenHashMap getUidObjectMap() {
      return this.uidObjects;
   }

   public int getLocalObjectsSize() {
      return this.localObjects.size();
   }

   public Long2ObjectOpenHashMap getDbObjects() {
      return this.dbObjects;
   }

   public Int2ObjectOpenHashMap getLocalObjectsByTopLvlType(TopLevelType var1) {
      Int2ObjectOpenHashMap var2;
      return (var2 = (Int2ObjectOpenHashMap)this.topLevelObjects.get(var1.ordinal())) != null ? var2 : this.empty;
   }

   public Object2ObjectOpenHashMap getPersistentObjects() {
      return this.persistentObjects;
   }
}
