package org.schema.schine.network;

public class ClientIdNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public ClientIdNotFoundException(String var1) {
      super(var1);
   }
}
