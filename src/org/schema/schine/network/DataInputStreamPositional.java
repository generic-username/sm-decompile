package org.schema.schine.network;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import java.io.DataInputStream;

public class DataInputStreamPositional extends DataInputStream {
   public DataInputStreamPositional(FastByteArrayInputStream var1) {
      super(var1);
   }

   public long position() {
      return ((FastByteArrayInputStream)this.in).position();
   }

   public void position(long var1) {
      ((FastByteArrayInputStream)this.in).position(var1);
   }
}
