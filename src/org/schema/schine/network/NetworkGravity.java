package org.schema.schine.network;

import javax.vecmath.Vector3f;

public class NetworkGravity {
   public int gravityId = -1;
   public int gravityIdReceive = -1;
   public Vector3f gravity = new Vector3f();
   public Vector3f gravityReceive = new Vector3f();
   public boolean gravityReceived;
   public boolean forcedFromServer;
   public boolean central;

   public NetworkGravity() {
   }

   public NetworkGravity(NetworkGravity var1) {
      this.gravityId = var1.gravityId;
      this.gravityIdReceive = var1.gravityIdReceive;
      this.gravity = var1.gravity;
      this.central = var1.central;
      this.gravityReceive = var1.gravityReceive;
      this.gravityReceived = var1.gravityReceived;
      this.forcedFromServer = var1.forcedFromServer;
   }

   public String toString() {
      return "NetworkGravity [gravityId=" + this.gravityId + ", gravityIdReceive=" + this.gravityIdReceive + ", gravity=" + this.gravity + ", gravityReceive=" + this.gravityReceive + ", gravityReceived=" + this.gravityReceived + ", forcedFromServer=" + this.forcedFromServer + "]";
   }
}
