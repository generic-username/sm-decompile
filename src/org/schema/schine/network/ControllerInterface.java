package org.schema.schine.network;

import org.schema.schine.network.objects.Sendable;

public interface ControllerInterface {
   long getServerRunningTime();

   long calculateStartTime();

   long getUniverseDayInMs();

   void onRemoveEntity(Sendable var1);
}
