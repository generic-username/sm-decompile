package org.schema.schine.network.objects.container;

import javax.vecmath.Vector3f;

public interface CenterOfMassInterface {
   Vector3f getCenterOfMass();

   boolean isVirtual();

   float getTotalPhysicalMass();
}
