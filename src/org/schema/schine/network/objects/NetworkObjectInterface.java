package org.schema.schine.network.objects;

public interface NetworkObjectInterface {
   boolean isOnServer();
}
