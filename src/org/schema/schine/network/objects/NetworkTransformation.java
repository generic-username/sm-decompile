package org.schema.schine.network.objects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;

public class NetworkTransformation {
   public boolean received;
   public boolean sendVil;
   public boolean receivedVil;
   private Transform transform;
   private Transform transformReceive;
   private final Vector3f lin = new Vector3f();
   private final Vector3f ang = new Vector3f();
   private final Vector3f linReceive = new Vector3f();
   private final Vector3f angReceive = new Vector3f();
   private long timeStamp;
   private long timeStampReceive;
   private boolean playerAttached;
   private boolean playerAttachedReceive;
   public boolean prime;

   public NetworkTransformation() {
      this.transform = new Transform();
      this.transformReceive = new Transform();
   }

   public NetworkTransformation(Transform var1, long var2) {
      this.transform = var1;
      this.transformReceive = new Transform(var1);
      this.timeStamp = var2;
      this.timeStampReceive = var2;
   }

   public Vector3f getAng() {
      return this.ang;
   }

   public void setAng(Vector3f var1) {
      this.ang.set(var1);
   }

   public Vector3f getAngReceive() {
      return this.angReceive;
   }

   public Vector3f getLin() {
      return this.lin;
   }

   public void setLin(Vector3f var1) {
      this.lin.set(var1);
   }

   public Vector3f getLinReceive() {
      return this.linReceive;
   }

   public long getTimeStamp() {
      return this.timeStamp;
   }

   public void setTimeStamp(long var1) {
      this.timeStamp = var1;
   }

   public long getTimeStampReceive() {
      return this.timeStampReceive;
   }

   public void setTimeStampReceive(long var1) {
      this.timeStampReceive = var1;
   }

   public Transform getTransform() {
      return this.transform;
   }

   public void setTransform(Transform var1) {
      this.transform = var1;
   }

   public Transform getTransformReceive() {
      return this.transformReceive;
   }

   public void setTransformReceive(Transform var1) {
      this.transformReceive = var1;
   }

   public boolean isPlayerAttached() {
      return this.playerAttached;
   }

   public void setPlayerAttached(boolean var1) {
      this.playerAttached = var1;
   }

   public boolean isPlayerAttachedReceive() {
      return this.playerAttachedReceive;
   }

   public void setPlayerAttachedReceive(boolean var1) {
      this.playerAttachedReceive = var1;
   }
}
