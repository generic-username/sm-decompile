package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteBoolean extends RemoteComparable {
   public RemoteBoolean(boolean var1) {
      this(false, var1);
   }

   public RemoteBoolean(boolean var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteBoolean(boolean var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteBoolean(NetworkObject var1) {
      this(false, var1);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readBoolean());
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeBoolean((Boolean)this.get());
      return 1;
   }
}
