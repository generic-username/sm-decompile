package org.schema.schine.network.objects.remote;

public interface StreamableArray {
   int arrayLength();

   void cleanAtRelease();
}
