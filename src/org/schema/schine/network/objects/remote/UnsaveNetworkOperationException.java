package org.schema.schine.network.objects.remote;

public class UnsaveNetworkOperationException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public UnsaveNetworkOperationException() {
   }

   public UnsaveNetworkOperationException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public UnsaveNetworkOperationException(String var1) {
      super(var1);
   }

   public UnsaveNetworkOperationException(Throwable var1) {
      super(var1);
   }
}
