package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteLong extends RemoteComparable {
   public RemoteLong(boolean var1) {
      this(0L, var1);
   }

   public RemoteLong(long var1, boolean var3) {
      super(var1, var3);
   }

   public RemoteLong(Long var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteLong(NetworkObject var1) {
      this(0L, var1);
   }

   public int byteLength() {
      return 8;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readLong());
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong((Long)this.get());
      return this.byteLength();
   }
}
