package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteLongIntPair extends RemoteComparable {
   public RemoteLongIntPair(boolean var1) {
      super(new LongIntPair(), var1);
   }

   public RemoteLongIntPair(NetworkObject var1) {
      super(new LongIntPair(), var1);
   }

   public RemoteLongIntPair(LongIntPair var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteLongIntPair(LongIntPair var1, boolean var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 12;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((LongIntPair)this.get()).l = var1.readLong();
      ((LongIntPair)this.get()).i = var1.readInt();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(((LongIntPair)this.get()).l);
      var1.writeInt(((LongIntPair)this.get()).i);
      return this.byteLength();
   }
}
