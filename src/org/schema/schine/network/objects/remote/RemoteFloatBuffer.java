package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatCollection;
import it.unimi.dsi.fastutil.floats.FloatIterator;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.floats.FloatListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;

public class RemoteFloatBuffer extends RemoteField implements FloatList, RemoteBufferInterface {
   private final FloatArrayList receiveBuffer = new FloatArrayList();
   public int MAX_BATCH = 16;

   public RemoteFloatBuffer(boolean var1) {
      super(new FloatArrayList(), var1);
   }

   public RemoteFloatBuffer(boolean var1, int var2) {
      super(new FloatArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteFloatBuffer(NetworkObject var1) {
      super(new FloatArrayList(), var1);
   }

   public RemoteFloatBuffer(NetworkObject var1, int var2) {
      super(new FloatArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      if (this.MAX_BATCH < 127) {
         var2 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var2 = var1.readShort();
      } else {
         var2 = var1.readInt();
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         this.receiveBuffer.add(var1.readFloat());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((FloatArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         var1.writeFloat(((FloatArrayList)this.get()).getFloat(var3));
      }

      ((FloatArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((FloatArrayList)this.get()).isEmpty();
      return 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public FloatArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public FloatListIterator iterator() {
      return ((FloatArrayList)this.get()).iterator();
   }

   public FloatListIterator floatListIterator() {
      return ((FloatArrayList)this.get()).floatListIterator();
   }

   public FloatListIterator floatListIterator(int var1) {
      return ((FloatArrayList)this.get()).floatListIterator(var1);
   }

   public FloatListIterator listIterator() {
      return ((FloatArrayList)this.get()).listIterator();
   }

   public FloatListIterator listIterator(int var1) {
      return ((FloatArrayList)this.get()).listIterator(var1);
   }

   public FloatList floatSubList(int var1, int var2) {
      return ((FloatArrayList)this.get()).floatSubList(var1, var2);
   }

   public FloatList subList(int var1, int var2) {
      return ((FloatArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((FloatArrayList)this.get()).size();
   }

   public void getElements(int var1, float[] var2, int var3, int var4) {
      ((FloatArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((FloatArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, float[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((FloatArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, float[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((FloatArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean add(float var1) {
      boolean var2 = ((FloatArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public void add(int var1, float var2) {
      ((FloatArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, FloatCollection var2) {
      boolean var3;
      if (var3 = ((FloatArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, FloatList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((FloatArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(FloatList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((FloatArrayList)this.get()).addAll(var1);
   }

   public float getFloat(int var1) {
      return ((FloatArrayList)this.get()).getFloat(var1);
   }

   public int indexOf(float var1) {
      return ((FloatArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(float var1) {
      return ((FloatArrayList)this.get()).lastIndexOf(var1);
   }

   public float removeFloat(int var1) {
      return ((FloatArrayList)this.get()).removeFloat(var1);
   }

   public float set(int var1, float var2) {
      float var3 = ((FloatArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public int size() {
      return ((FloatArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((FloatArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((FloatArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((FloatArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((FloatArrayList)this.get()).toArray(var1);
   }

   public boolean add(Float var1) {
      boolean var2 = ((FloatArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((FloatArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((FloatArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((FloatArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((FloatArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((FloatArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((FloatArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((FloatArrayList)this.get()).clear();
   }

   public Float get(int var1) {
      return ((FloatArrayList)this.get()).get(var1);
   }

   public Float set(int var1, Float var2) {
      float var3 = ((FloatArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Float var2) {
      ((FloatArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Float remove(int var1) {
      return ((FloatArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((FloatArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((FloatArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((FloatArrayList)this.get()).compareTo(var1);
   }

   public FloatIterator floatIterator() {
      return ((FloatArrayList)this.get()).floatIterator();
   }

   public boolean contains(float var1) {
      return ((FloatArrayList)this.get()).contains(var1);
   }

   public float[] toFloatArray() {
      return ((FloatArrayList)this.get()).toFloatArray();
   }

   public float[] toFloatArray(float[] var1) {
      return ((FloatArrayList)this.get()).toFloatArray(var1);
   }

   public float[] toArray(float[] var1) {
      return ((FloatArrayList)this.get()).toArray(var1);
   }

   public boolean rem(float var1) {
      return ((FloatArrayList)this.get()).rem(var1);
   }

   public boolean addAll(FloatCollection var1) {
      boolean var2;
      if (var2 = ((FloatArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(FloatCollection var1) {
      return ((FloatArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(FloatCollection var1) {
      return ((FloatArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(FloatCollection var1) {
      return ((FloatArrayList)this.get()).retainAll(var1);
   }
}
