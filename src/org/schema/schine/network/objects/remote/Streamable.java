package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface Streamable extends NetworkChangeObservable {
   int byteLength();

   void cleanAtRelease();

   void fromByteStream(DataInputStream var1, int var2) throws IOException;

   Object get();

   void set(Object var1);

   void set(Object var1, boolean var2);

   int toByteStream(DataOutputStream var1) throws IOException;
}
