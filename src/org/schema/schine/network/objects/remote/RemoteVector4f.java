package org.schema.schine.network.objects.remote;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteVector4f extends RemoteFloatPrimitiveArray {
   public RemoteVector4f(boolean var1) {
      super(4, var1);
   }

   public RemoteVector4f(NetworkObject var1) {
      super(4, var1);
   }

   public RemoteVector4f(Vector4f var1, boolean var2) {
      super(4, var2);
      this.set(var1);
   }

   public RemoteVector4f(Vector4f var1, NetworkObject var2) {
      super(4, var2);
      this.set(var1);
   }

   public RemoteVector4f(Vector3f var1, float var2, boolean var3) {
      super(4, var3);
      this.set(var1.x, var1.y, var1.z, var2);
   }

   public RemoteVector4f(Vector3f var1, float var2, NetworkObject var3) {
      super(4, var3);
      this.set(var1.x, var1.y, var1.z, var2);
   }

   public Vector4f getVector() {
      return this.getVector(new Vector4f());
   }

   public float getX() {
      return this.getFloatArray()[0];
   }

   public float getY() {
      return this.getFloatArray()[1];
   }

   public float getZ() {
      return this.getFloatArray()[2];
   }

   public float getW() {
      return this.getFloatArray()[3];
   }

   public Quat4f getVector(Quat4f var1) {
      var1.set(super.getFloatArray()[0], super.getFloatArray()[1], super.getFloatArray()[2], super.getFloatArray()[3]);
      return var1;
   }

   public Vector4f getVector(Vector4f var1) {
      var1.set(super.getFloatArray()[0], super.getFloatArray()[1], super.getFloatArray()[2], super.getFloatArray()[3]);
      return var1;
   }

   public void set(Vector4f var1) {
      super.set(0, var1.x);
      super.set(1, var1.y);
      super.set(2, var1.z);
      super.set(3, var1.w);
   }

   public void set(Vector4f var1, boolean var2) {
      super.set(0, var1.x, var2);
      super.set(1, var1.y, var2);
      super.set(2, var1.z, var2);
      super.set(3, var1.w, var2);
   }

   public String toString() {
      return "(r" + this.getVector() + ")";
   }

   public void set(float var1, float var2, float var3, float var4) {
      super.set(0, var1);
      super.set(1, var2);
      super.set(2, var3);
      super.set(3, var4);
   }

   public void set(float var1, float var2, float var3, float var4, boolean var5) {
      super.set(0, var1, var5);
      super.set(1, var2, var5);
      super.set(2, var3, var5);
      super.set(3, var4, var5);
   }

   public void set(Vector3f var1, float var2) {
      super.set(0, var1.x);
      super.set(1, var1.y);
      super.set(2, var1.z);
      super.set(3, var2);
   }

   public void set(Quat4f var1) {
      super.set(0, var1.x);
      super.set(1, var1.y);
      super.set(2, var1.z);
      super.set(3, var1.w);
   }
}
