package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteLongPrimitive implements Streamable {
   private final boolean onServer;
   protected boolean keepChanged;
   private boolean changed;
   private NetworkChangeObserver observer;
   private long value;
   private boolean forcedClientSending;

   public RemoteLongPrimitive(long var1, boolean var3) {
      this(var1, false, var3);
   }

   public RemoteLongPrimitive(long var1, boolean var3, boolean var4) {
      this.value = var1;
      this.onServer = var4;
      this.changed = var3;
   }

   public RemoteLongPrimitive(long var1, boolean var3, NetworkObject var4) {
      this(var1, var3, var4.isOnServer());

      assert var4 != null;

   }

   public RemoteLongPrimitive(long var1, NetworkObject var3) {
      this(var1, false, var3);
   }

   public int byteLength() {
      return 4;
   }

   public void cleanAtRelease() {
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readLong());
   }

   public Long get() {
      return this.value;
   }

   public void set(Long var1) {
      this.set(var1);
   }

   public void set(Long var1, boolean var2) {
      this.set(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(this.value);
      return 4;
   }

   public void forceClientUpdates() {
      this.forcedClientSending = true;
   }

   public long getLong() {
      return this.value;
   }

   public boolean hasChanged() {
      return this.changed;
   }

   public boolean initialSynchUpdateOnly() {
      return false;
   }

   public boolean keepChanged() {
      return this.keepChanged;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void setObserver(NetworkChangeObserver var1) {
      this.observer = var1;
   }

   public void set(long var1) {
      this.set(var1, this.forcedClientSending);
   }

   public void set(long var1, boolean var3) {
      if (this.onServer || var3) {
         this.setChanged(this.hasChanged() || var1 != this.value);
      }

      this.value = var1;
      if (this.hasChanged() && this.observer != null) {
         this.observer.update(this);
      }

   }
}
