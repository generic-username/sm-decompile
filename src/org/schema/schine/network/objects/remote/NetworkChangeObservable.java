package org.schema.schine.network.objects.remote;

public interface NetworkChangeObservable {
   boolean hasChanged();

   boolean initialSynchUpdateOnly();

   boolean keepChanged();

   void setChanged(boolean var1);

   void setObserver(NetworkChangeObserver var1);
}
