package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteByte extends RemoteComparable {
   public RemoteByte(boolean var1) {
      this((byte)0, var1);
   }

   public RemoteByte(byte var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteByte(Byte var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteByte(NetworkObject var1) {
      this((byte)0, var1);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      byte var3 = var1.readByte();
      this.set(var3);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeByte((Byte)this.get());
      return 1;
   }
}
