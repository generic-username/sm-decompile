package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteLongGZipPackage extends RemoteField {
   public RemoteLongGZipPackage() {
      super((Object)null, (NetworkObject)null);
   }

   public int byteLength() {
      return 0;
   }

   @Deprecated
   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
   }

   @Deprecated
   public int toByteStream(DataOutputStream var1) throws IOException {
      assert false : "deprecated";

      return -1;
   }
}
