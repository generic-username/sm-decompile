package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortCollection;
import it.unimi.dsi.fastutil.shorts.ShortIterator;
import it.unimi.dsi.fastutil.shorts.ShortList;
import it.unimi.dsi.fastutil.shorts.ShortListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;

public class RemoteShortBuffer extends RemoteField implements ShortList, RemoteBufferInterface {
   private final ShortArrayList receiveBuffer = new ShortArrayList();
   public int MAX_BATCH = 16;

   public RemoteShortBuffer(boolean var1, int var2) {
      super(new ShortArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteShortBuffer(NetworkObject var1, int var2) {
      super(new ShortArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      if (this.MAX_BATCH < 127) {
         var2 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var2 = var1.readShort();
      } else {
         var2 = var1.readInt();
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         this.receiveBuffer.add(var1.readShort());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((ShortArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         short var4 = ((ShortArrayList)this.get()).getShort(var3);
         var1.writeShort(var4);
      }

      ((ShortArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((ShortArrayList)this.get()).isEmpty();
      return 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public ShortArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public ShortListIterator iterator() {
      return ((ShortArrayList)this.get()).iterator();
   }

   public ShortListIterator shortListIterator() {
      return ((ShortArrayList)this.get()).shortListIterator();
   }

   public ShortListIterator shortListIterator(int var1) {
      return ((ShortArrayList)this.get()).shortListIterator(var1);
   }

   public ShortListIterator listIterator() {
      return ((ShortArrayList)this.get()).listIterator();
   }

   public ShortListIterator listIterator(int var1) {
      return ((ShortArrayList)this.get()).listIterator(var1);
   }

   public ShortList shortSubList(int var1, int var2) {
      return ((ShortArrayList)this.get()).shortSubList(var1, var2);
   }

   public ShortList subList(int var1, int var2) {
      return ((ShortArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((ShortArrayList)this.get()).size();
   }

   public void getElements(int var1, short[] var2, int var3, int var4) {
      ((ShortArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((ShortArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, short[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((ShortArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, short[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((ShortArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean addCoord(short var1, short var2, short var3) {
      boolean var4 = ((ShortArrayList)this.get()).add(var1) | ((ShortArrayList)this.get()).add(var2) | ((ShortArrayList)this.get()).add(var3);
      this.setChanged(var4);
      this.observer.update(this);
      return var4;
   }

   public boolean addCoord(short var1, short var2, short var3, short var4) {
      boolean var5 = ((ShortArrayList)this.get()).add(var1) | ((ShortArrayList)this.get()).add(var2) | ((ShortArrayList)this.get()).add(var3) | ((ShortArrayList)this.get()).add(var4);
      this.setChanged(var5);
      this.observer.update(this);
      return var5;
   }

   public boolean add(short var1) {
      boolean var2 = ((ShortArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public void add(int var1, short var2) {
      ((ShortArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, ShortCollection var2) {
      boolean var3;
      if (var3 = ((ShortArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, ShortList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ShortArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(ShortList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ShortArrayList)this.get()).addAll(var1);
   }

   public short getShort(int var1) {
      return ((ShortArrayList)this.get()).getShort(var1);
   }

   public int indexOf(short var1) {
      return ((ShortArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(short var1) {
      return ((ShortArrayList)this.get()).lastIndexOf(var1);
   }

   public short removeShort(int var1) {
      return ((ShortArrayList)this.get()).removeShort(var1);
   }

   public short set(int var1, short var2) {
      short var3 = ((ShortArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public int size() {
      return ((ShortArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((ShortArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((ShortArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((ShortArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((ShortArrayList)this.get()).toArray(var1);
   }

   public boolean add(Short var1) {
      boolean var2 = ((ShortArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((ShortArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((ShortArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((ShortArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((ShortArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((ShortArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((ShortArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((ShortArrayList)this.get()).clear();
   }

   public Short get(int var1) {
      return ((ShortArrayList)this.get()).get(var1);
   }

   public Short set(int var1, Short var2) {
      short var3 = ((ShortArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Short var2) {
      ((ShortArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Short remove(int var1) {
      return ((ShortArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((ShortArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((ShortArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((ShortArrayList)this.get()).compareTo(var1);
   }

   public ShortIterator shortIterator() {
      return ((ShortArrayList)this.get()).shortIterator();
   }

   public boolean contains(short var1) {
      return ((ShortArrayList)this.get()).contains(var1);
   }

   public short[] toShortArray() {
      return ((ShortArrayList)this.get()).toShortArray();
   }

   public short[] toShortArray(short[] var1) {
      return ((ShortArrayList)this.get()).toShortArray(var1);
   }

   public short[] toArray(short[] var1) {
      return ((ShortArrayList)this.get()).toArray(var1);
   }

   public boolean rem(short var1) {
      return ((ShortArrayList)this.get()).rem(var1);
   }

   public boolean addAll(ShortCollection var1) {
      boolean var2;
      if (var2 = ((ShortArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(ShortCollection var1) {
      return ((ShortArrayList)this.get()).retainAll(var1);
   }
}
