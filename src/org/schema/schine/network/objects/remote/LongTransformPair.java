package org.schema.schine.network.objects.remote;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.schine.network.SerialializationInterface;

public class LongTransformPair implements SerialializationInterface {
   public long l;
   public Transform t;

   public LongTransformPair() {
   }

   public LongTransformPair(long var1, Transform var3) {
      this.l = var1;
      this.t = var3;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.l);
      TransformTools.serializeFully(var1, this.t);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.l = var1.readLong();
      this.t = TransformTools.deserializeFully(var1, new Transform());
   }
}
