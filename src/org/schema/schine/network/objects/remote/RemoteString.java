package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteString extends RemoteComparable {
   public RemoteString(boolean var1) {
      super("", var1);
   }

   public RemoteString(NetworkObject var1) {
      super("", var1);
   }

   public RemoteString(String var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteString(String var1, boolean var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return ((String)this.get()).length() + 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      byte[] var3 = new byte[var1.readInt()];
      var1.readFully(var3);
      this.set(new String(var3, "utf-8"));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte[] var2 = ((String)this.get()).getBytes("utf-8");
      var1.writeInt(var2.length);
      var1.write(var2);
      return this.byteLength();
   }
}
