package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteFloatPrimitiveArray implements Streamable, StreamableArray {
   private final boolean onServer;
   protected boolean keepChanged;
   private boolean changed;
   private NetworkChangeObserver observer;
   private boolean forcedClientSending;
   private float[] array;

   public RemoteFloatPrimitiveArray(int var1, boolean var2) {
      this(var1, false, var2);
   }

   public RemoteFloatPrimitiveArray(int var1, boolean var2, boolean var3) {
      this.array = new float[var1];
      this.onServer = var3;
      this.changed = var2;
   }

   public RemoteFloatPrimitiveArray(int var1, boolean var2, NetworkObject var3) {
      this(var1, var2, var3.isOnServer());

      assert var3 != null;

   }

   public RemoteFloatPrimitiveArray(int var1, NetworkObject var2) {
      this(var1, false, var2);
   }

   public int arrayLength() {
      return this.array.length;
   }

   public void cleanAtRelease() {
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      for(var2 = 0; var2 < this.array.length; ++var2) {
         this.set(var2, var1.readFloat(), this.forcedClientSending);
      }

   }

   public Float[] get() {
      assert false;

      Float[] var1 = new Float[this.array.length];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var1[var2] = this.array[var2];
      }

      return var1;
   }

   public void set(Float[] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.set(var2, var1[var2], this.forcedClientSending);
      }

   }

   public void set(Float[] var1, boolean var2) {
      for(int var3 = 0; var3 < var1.length; ++var3) {
         this.set(var3, var1[var3], var2);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      for(int var2 = 0; var2 < this.array.length; ++var2) {
         var1.writeFloat(this.array[var2]);
      }

      return 4;
   }

   public void forceClientUpdates() {
      this.forcedClientSending = true;
   }

   public float[] getFloatArray() {
      return this.array;
   }

   public boolean hasChanged() {
      return this.changed;
   }

   public boolean initialSynchUpdateOnly() {
      return false;
   }

   public boolean keepChanged() {
      return this.keepChanged;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void setObserver(NetworkChangeObserver var1) {
      this.observer = var1;
   }

   public void set(float[] var1) {
      this.set(var1, this.forcedClientSending);
   }

   public void set(float[] var1, boolean var2) {
      for(int var3 = 0; var3 < var1.length; ++var3) {
         this.set(var3, var1[var3], var2);
      }

   }

   public void set(int var1, float var2) {
      this.set(var1, var2, this.forcedClientSending);
   }

   public void set(int var1, float var2, boolean var3) {
      if (this.onServer || var3) {
         this.setChanged(this.hasChanged() || var2 != this.array[var1]);
      }

      this.array[var1] = var2;
      if (this.hasChanged() && this.observer != null) {
         this.observer.update(this);
      }

   }
}
