package org.schema.schine.network.objects.remote;

import javax.vecmath.Matrix3f;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteMatrix3f extends RemoteFloatPrimitiveArray {
   public RemoteMatrix3f(boolean var1) {
      super(9, var1);
   }

   public RemoteMatrix3f(NetworkObject var1) {
      super(9, var1);
   }

   public RemoteMatrix3f(Matrix3f var1, boolean var2) {
      super(9, var2);
      this.set(var1);
   }

   public RemoteMatrix3f(Matrix3f var1, NetworkObject var2) {
      super(9, var2);
      this.set(var1);
   }

   public Matrix3f getMatrix() {
      return this.getMatrix(new Matrix3f());
   }

   public Matrix3f getMatrix(Matrix3f var1) {
      var1.m00 = super.getFloatArray()[0];
      var1.m01 = super.getFloatArray()[1];
      var1.m02 = super.getFloatArray()[2];
      var1.m10 = super.getFloatArray()[3];
      var1.m11 = super.getFloatArray()[4];
      var1.m12 = super.getFloatArray()[5];
      var1.m20 = super.getFloatArray()[6];
      var1.m21 = super.getFloatArray()[7];
      var1.m22 = super.getFloatArray()[8];
      return var1;
   }

   public void set(Matrix3f var1, boolean var2) {
      super.set(0, var1.m00, var2);
      super.set(1, var1.m01, var2);
      super.set(2, var1.m02, var2);
      super.set(3, var1.m10, var2);
      super.set(4, var1.m11, var2);
      super.set(5, var1.m12, var2);
      super.set(6, var1.m20, var2);
      super.set(7, var1.m21, var2);
      super.set(8, var1.m22, var2);
   }

   public String toString() {
      return "(r" + this.getMatrix() + ")";
   }

   public void set(Matrix3f var1) {
      super.set(0, var1.m00);
      super.set(1, var1.m01);
      super.set(2, var1.m02);
      super.set(3, var1.m10);
      super.set(4, var1.m11);
      super.set(5, var1.m12);
      super.set(6, var1.m20);
      super.set(7, var1.m21);
      super.set(8, var1.m22);
   }

   public boolean equalsMatrix(Matrix3f var1) {
      return var1.m00 == super.getFloatArray()[0] && var1.m01 == super.getFloatArray()[1] && var1.m02 == super.getFloatArray()[2] && var1.m10 == super.getFloatArray()[3] && var1.m11 == super.getFloatArray()[4] && var1.m12 == super.getFloatArray()[5] && var1.m20 == super.getFloatArray()[6] && var1.m21 == super.getFloatArray()[7] && var1.m22 == super.getFloatArray()[8];
   }
}
