package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteInteger extends RemoteComparable {
   public RemoteInteger(boolean var1) {
      this(0, var1);
   }

   public RemoteInteger(Integer var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteInteger(Integer var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteInteger(NetworkObject var1) {
      this(0, var1);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readInt());
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt((Integer)this.get());
      return 1;
   }
}
