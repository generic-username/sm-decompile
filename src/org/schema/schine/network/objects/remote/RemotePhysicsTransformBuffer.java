package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.NetworkTransformation;

public class RemotePhysicsTransformBuffer extends RemoteBuffer {
   public RemotePhysicsTransformBuffer(NetworkObject var1) {
      super(RemotePhysicsTransform.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemotePhysicsTransform var5;
         (var5 = new RemotePhysicsTransform(new NetworkTransformation(), this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2 = 0;
      var1.writeInt(((ObjectArrayList)this.get()).size());
      int var5 = var2 + 4;

      RemotePhysicsTransform var4;
      for(Iterator var3 = ((ObjectArrayList)this.get()).iterator(); var3.hasNext(); var5 += var4.toByteStream(var1)) {
         var4 = (RemotePhysicsTransform)var3.next();
      }

      ((ObjectArrayList)this.get()).clear();
      return var5;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
