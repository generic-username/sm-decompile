package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public abstract class RemoteArray extends RemoteField implements NetworkChangeObserver, StreamableArray {
   public RemoteArray(RemoteField[] var1, boolean var2) {
      super(var1, var2);
      this.init(var1);
   }

   public RemoteArray(RemoteField[] var1, NetworkObject var2) {
      super(var1, var2);
      this.init(var1);
   }

   protected void addObservers() {
      for(int var1 = 0; var1 < ((RemoteField[])this.get()).length; ++var1) {
         ((RemoteField[])this.get())[var1].observer = this;
      }

   }

   public int arrayLength() {
      return ((RemoteField[])this.get()).length;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      for(int var3 = 0; var3 < ((RemoteField[])this.get()).length; ++var3) {
         ((RemoteField[])this.get())[var3].fromByteStream(var1, var2);
         this.set(var3, (Comparable)this.get(var3).get());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      for(int var2 = 0; var2 < ((RemoteField[])this.get()).length; ++var2) {
         ((RemoteField[])this.get())[var2].toByteStream(var1);
      }

      return this.byteLength();
   }

   public RemoteField get(int var1) {
      return ((RemoteField[])super.get())[var1];
   }

   protected abstract void init(RemoteField[] var1);

   public abstract void set(int var1, Comparable var2);

   public void update(Streamable var1) {
      this.setChanged(true);
      var1.setChanged(false);
      if (this.observer != null) {
         this.observer.update(this);
      }

   }

   public boolean isSynched() {
      return this.observer == null || this.observer.isSynched();
   }
}
