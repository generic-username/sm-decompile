package org.schema.schine.network.objects.remote;

import org.schema.common.util.linAlg.Vector4i;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteVector4i extends RemoteIntPrimitiveArray {
   public RemoteVector4i(boolean var1) {
      super(4, var1);
   }

   public RemoteVector4i(NetworkObject var1) {
      super(4, var1);
   }

   public RemoteVector4i(Vector4i var1, boolean var2) {
      super(4, var2);
      this.set(var1);
   }

   public RemoteVector4i(Vector4i var1, NetworkObject var2) {
      super(4, var2);
      this.set(var1);
   }

   public Vector4i getVector() {
      return this.getVector(new Vector4i());
   }

   public Vector4i getVector(Vector4i var1) {
      var1.set(super.getIntArray()[0], super.getIntArray()[1], super.getIntArray()[2], super.getIntArray()[3]);
      return var1;
   }

   public void set(Vector4i var1) {
      super.set(0, var1.x);
      super.set(1, var1.y);
      super.set(2, var1.z);
      super.set(3, var1.w);
   }

   public String toString() {
      return "(r" + this.getVector() + ")";
   }
}
