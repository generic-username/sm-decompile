package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.NetworkGravity;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteGravity extends RemoteField {
   public RemoteGravity(NetworkGravity var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteGravity(NetworkGravity var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((NetworkGravity)this.get()).gravityIdReceive = var1.readInt();
      ((NetworkGravity)this.get()).gravityReceive.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      ((NetworkGravity)this.get()).central = var1.readBoolean();
      ((NetworkGravity)this.get()).forcedFromServer = var1.readBoolean();
      ((NetworkGravity)this.get()).gravityReceived = true;
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((NetworkGravity)this.get()).gravityId);
      var1.writeFloat(((NetworkGravity)this.get()).gravity.x);
      var1.writeFloat(((NetworkGravity)this.get()).gravity.y);
      var1.writeFloat(((NetworkGravity)this.get()).gravity.z);
      var1.writeBoolean(((NetworkGravity)this.get()).central);
      var1.writeBoolean(((NetworkGravity)this.get()).forcedFromServer);
      return 1;
   }
}
