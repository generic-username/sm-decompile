package org.schema.schine.network.objects.remote;

public interface NetworkChangeObserver {
   void update(Streamable var1);

   boolean isSynched();
}
