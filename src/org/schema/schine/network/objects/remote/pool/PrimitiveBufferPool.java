package org.schema.schine.network.objects.remote.pool;

import com.bulletphysics.util.ObjectArrayList;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.schema.schine.network.objects.remote.Streamable;

public class PrimitiveBufferPool {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final Map initialValue() {
         return new HashMap();
      }
   };
   private ObjectArrayList list = new ObjectArrayList();
   private Constructor constructor;

   public PrimitiveBufferPool(Class var1) {
      try {
         this.constructor = var1.getConstructor(Boolean.TYPE);
      } catch (SecurityException var2) {
         var2.printStackTrace();

         assert false;

      } catch (NoSuchMethodException var3) {
         var3.printStackTrace();

         assert false : "Clazz " + var1;

      }
   }

   public static void cleanCurrentThread() {
      threadLocal.remove();
   }

   public static PrimitiveBufferPool get(Class var0) {
      Map var1;
      PrimitiveBufferPool var2;
      if ((var2 = (PrimitiveBufferPool)(var1 = (Map)threadLocal.get()).get(var0)) == null) {
         var2 = new PrimitiveBufferPool(var0);
         var1.put(var0, var2);
      }

      return var2;
   }

   private Streamable create(boolean var1) {
      try {
         return (Streamable)this.constructor.newInstance(var1);
      } catch (InstantiationException var2) {
         throw new IllegalStateException(var2);
      } catch (IllegalAccessException var3) {
         throw new IllegalStateException(var3);
      } catch (IllegalArgumentException var4) {
         throw new IllegalStateException(var4);
      } catch (InvocationTargetException var5) {
         throw new IllegalStateException(var5);
      }
   }

   public Streamable get(boolean var1) {
      return this.list.size() > 0 ? (Streamable)this.list.remove(this.list.size() - 1) : this.create(var1);
   }

   public void release(Streamable var1) {
      var1.cleanAtRelease();
      this.list.add(var1);
   }
}
