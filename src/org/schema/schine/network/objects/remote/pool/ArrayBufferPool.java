package org.schema.schine.network.objects.remote.pool;

import com.bulletphysics.util.ObjectArrayList;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.schema.schine.network.objects.remote.StreamableArray;

public class ArrayBufferPool {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final Map initialValue() {
         return new HashMap();
      }
   };
   private ObjectArrayList list = new ObjectArrayList();
   private Constructor constructor;
   private Integer size;

   public ArrayBufferPool(Class var1) {
      try {
         this.constructor = var1.getConstructor(Integer.TYPE, Boolean.TYPE);
      } catch (SecurityException var2) {
         var2.printStackTrace();

         assert false;

      } catch (NoSuchMethodException var3) {
         var3.printStackTrace();

         assert false;

      }
   }

   public static void cleanCurrentThread() {
      threadLocal.remove();
   }

   public static ArrayBufferPool get(Class var0, Integer var1) {
      Map var2;
      Object var3;
      if ((var3 = (Map)(var2 = (Map)threadLocal.get()).get(var0)) == null) {
         var3 = new HashMap();
         var2.put(var0, var3);
      }

      ArrayBufferPool var4;
      if ((var4 = (ArrayBufferPool)((Map)var3).get(var1)) == null) {
         (var4 = new ArrayBufferPool(var0)).size = var1;
         ((Map)var3).put(var1, var4);
      }

      return var4;
   }

   private StreamableArray create(Integer var1, boolean var2) {
      try {
         return (StreamableArray)this.constructor.newInstance(var1, var2);
      } catch (InstantiationException var3) {
         throw new IllegalStateException(var3);
      } catch (IllegalAccessException var4) {
         throw new IllegalStateException(var4);
      } catch (IllegalArgumentException var5) {
         throw new IllegalStateException(var5);
      } catch (InvocationTargetException var6) {
         throw new IllegalStateException(var6);
      }
   }

   public StreamableArray get(boolean var1) {
      return this.list.size() > 0 ? (StreamableArray)this.list.remove(this.list.size() - 1) : this.create(this.size, var1);
   }

   public void release(StreamableArray var1) {
      assert var1.arrayLength() == this.size : var1.arrayLength() + " / " + this.size;

      var1.cleanAtRelease();
      this.list.add(var1);
   }
}
