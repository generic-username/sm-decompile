package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteByteArrayDyn extends RemoteField {
   public RemoteByteArrayDyn(byte[] var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteByteArrayDyn(byte[] var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var1.readFully((byte[])this.get(), 0, ((byte[])this.get()).length);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((byte[])this.get()).length);
      var1.write((byte[])this.get(), 0, ((byte[])this.get()).length);
      return 1;
   }
}
