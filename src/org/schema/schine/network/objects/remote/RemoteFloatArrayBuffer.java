package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.pool.ArrayBufferPool;

public class RemoteFloatArrayBuffer extends RemoteBuffer {
   private final int arraySize;
   private ArrayBufferPool pool;

   public RemoteFloatArrayBuffer(int var1, boolean var2) {
      super(RemoteFloatPrimitiveArray.class, var2);
      this.arraySize = var1;
      this.cacheConstructor();
   }

   public RemoteFloatArrayBuffer(int var1, NetworkObject var2) {
      super(RemoteFloatPrimitiveArray.class, var2);
      this.arraySize = var1;
      this.cacheConstructor();
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteFloatPrimitiveArray var5 = (RemoteFloatPrimitiveArray)this.pool.get(this.onServer);

         assert var5.arrayLength() == this.arraySize : var5.byteLength() + " / " + this.arraySize;

         var5.fromByteStream(var1, var2);
         this.receiveBuffer.add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;
      if (!((ObjectArrayList)this.get()).isEmpty()) {
         for(int var4 = 0; var4 < var2; ++var4) {
            Streamable var5 = (Streamable)((ObjectArrayList)this.get()).remove(0);
            var3 += var5.toByteStream(var1);
            var5.setChanged(false);
         }
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 1;
   }

   protected void cacheConstructor() {
      this.pool = ArrayBufferPool.get(this.clazz, this.arraySize);
   }

   public void clearReceiveBuffer() {
      for(int var1 = 0; var1 < this.getReceiveBuffer().size(); ++var1) {
         this.pool.release((StreamableArray)this.getReceiveBuffer().get(var1));
      }

      this.getReceiveBuffer().clear();
   }

   public boolean add(RemoteFloatPrimitiveArray var1) {
      assert var1.getFloatArray().length == this.arraySize : "Invalid Array Size: " + var1.getFloatArray().length + " != " + this.arraySize;

      return super.add((Streamable)var1);
   }

   public int getArraySize() {
      return this.arraySize;
   }
}
