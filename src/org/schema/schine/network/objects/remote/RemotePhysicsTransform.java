package org.schema.schine.network.objects.remote;

import com.bulletphysics.linearmath.MatrixUtil;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.NetworkTransformation;

public class RemotePhysicsTransform extends RemoteField {
   private static final byte playerAttached = 1;
   private static final byte sendVelos = 2;
   private static final byte linVeloZero = 4;
   private static final byte angVeloZero = 8;
   private static final byte primed = 16;
   public static final byte LINEA_VELO = 1;
   public static final byte ANGULAR_VELO = 2;
   private static final float EPSILON = 1.0E-6F;
   private Quat4f qReceive = new Quat4f();
   private Quat4f qSend = new Quat4f();

   public RemotePhysicsTransform(NetworkTransformation var1, boolean var2) {
      super(var1, var2);
   }

   public RemotePhysicsTransform(NetworkTransformation var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readByte() & 255;
      this.qReceive.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
      MatrixUtil.setRotation(((NetworkTransformation)this.get()).getTransformReceive().basis, this.qReceive);
      ((NetworkTransformation)this.get()).getTransformReceive().origin.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      ((NetworkTransformation)this.get()).setPlayerAttachedReceive((var2 & 1) == 1);
      if ((var2 & 2) == 2) {
         ((NetworkTransformation)this.get()).receivedVil = true;
         if ((var2 & 4) == 4) {
            ((NetworkTransformation)this.get()).getLinReceive().set(0.0F, 0.0F, 0.0F);
         } else {
            ((NetworkTransformation)this.get()).getLinReceive().set(var1.readFloat(), var1.readFloat(), var1.readFloat());
         }

         if ((var2 & 8) == 8) {
            ((NetworkTransformation)this.get()).getAngReceive().set(0.0F, 0.0F, 0.0F);
         } else {
            ((NetworkTransformation)this.get()).getAngReceive().set(var1.readFloat(), var1.readFloat(), var1.readFloat());
         }
      }

      if (this.onServer) {
         ((NetworkTransformation)this.get()).setTimeStampReceive(var1.readLong());
      }

      ((NetworkTransformation)this.get()).received = (var2 & 16) == 16;
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = 0;
      if (((NetworkTransformation)this.get()).isPlayerAttached()) {
         var2 = 1;
      }

      if (((NetworkTransformation)this.get()).sendVil) {
         var2 |= 2;
      }

      if (((NetworkTransformation)this.get()).getLin().lengthSquared() <= 1.0E-6F) {
         var2 |= 4;
      }

      if (((NetworkTransformation)this.get()).getAng().lengthSquared() <= 1.0E-6F) {
         var2 |= 8;
      }

      if (((NetworkTransformation)this.get()).prime) {
         var2 |= 16;
         ((NetworkTransformation)this.get()).prime = false;
      }

      var1.writeByte((byte)var2);
      Quat4fTools.set(((NetworkTransformation)this.get()).getTransform().basis, this.qSend);
      var1.writeFloat(this.qSend.x);
      var1.writeFloat(this.qSend.y);
      var1.writeFloat(this.qSend.z);
      var1.writeFloat(this.qSend.w);
      var1.writeFloat(((NetworkTransformation)this.get()).getTransform().origin.x);
      var1.writeFloat(((NetworkTransformation)this.get()).getTransform().origin.y);
      var1.writeFloat(((NetworkTransformation)this.get()).getTransform().origin.z);
      if (((NetworkTransformation)this.get()).sendVil) {
         if ((var2 & 4) != 4) {
            var1.writeFloat(((NetworkTransformation)this.get()).getLin().x);
            var1.writeFloat(((NetworkTransformation)this.get()).getLin().y);
            var1.writeFloat(((NetworkTransformation)this.get()).getLin().z);
         }

         if ((var2 & 8) != 8) {
            var1.writeFloat(((NetworkTransformation)this.get()).getAng().x);
            var1.writeFloat(((NetworkTransformation)this.get()).getAng().y);
            var1.writeFloat(((NetworkTransformation)this.get()).getAng().z);
         }
      }

      if (!this.onServer) {
         var1.writeLong(((NetworkTransformation)this.get()).getTimeStamp());
      }

      return this.byteLength();
   }
}
