package org.schema.schine.network.objects;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;

public class LocalSectorTransition implements SerialializationInterface {
   public Vector3i oldPos = new Vector3i();
   public Vector3i newPos = new Vector3i();
   public float planetRotation;
   public boolean oldPosPlanet;
   public boolean newPosPlanet;
   public float sectorSize;

   public Transform getTransitionTransform(Transform var1) {
      Vector3i var2;
      (var2 = new Vector3i()).sub(this.newPos, this.oldPos);
      Vector3f var5 = new Vector3f((float)var2.x * this.sectorSize, (float)var2.y * this.sectorSize, (float)var2.z * this.sectorSize);
      var1 = new Transform(var1);
      Transform var3;
      (var3 = new Transform()).setIdentity();
      var3.origin.set(var5);
      Matrix3f var4;
      (var4 = new Matrix3f()).rotX(6.2831855F * this.planetRotation);
      if (this.oldPosPlanet) {
         var4.invert();
         TransformTools.rotateAroundPoint(new Vector3f(var5), var4, var3, new Transform());
      } else if (!this.oldPosPlanet && this.newPosPlanet) {
         TransformTools.rotateAroundPoint(new Vector3f(), var4, var3, new Transform());
      } else {
         var3.origin.set(var5);
      }

      var3.inverse();
      var3.mul(var1);
      var1.set(var3);
      return var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.oldPos.serialize(var1);
      this.newPos.serialize(var1);
      var1.writeFloat(this.planetRotation);
      var1.writeBoolean(this.oldPosPlanet);
      var1.writeBoolean(this.newPosPlanet);
      var1.writeFloat(this.sectorSize);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.oldPos.deserialize(var1);
      this.newPos.deserialize(var1);
      this.planetRotation = var1.readFloat();
      this.oldPosPlanet = var1.readBoolean();
      this.newPosPlanet = var1.readBoolean();
      this.sectorSize = var1.readFloat();
   }
}
