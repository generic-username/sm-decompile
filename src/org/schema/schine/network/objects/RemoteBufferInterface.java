package org.schema.schine.network.objects;

public interface RemoteBufferInterface {
   void clearReceiveBuffer();

   boolean isEmpty();
}
