package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class GetNextFreeObjectId extends Command {
   public GetNextFreeObjectId() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException {
      System.err.println("Client got new ID range " + var1[0]);
      var2.setIdStartRange((Integer)var1[0]);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException {
      int var5 = IdGen.getFreeObjectId(ClientState.NEW_ID_RANGE);
      System.err.println("SENDING new ID RANGE TO CLIENT " + var5 + "; packet " + var4);
      this.createReturnToClient(var3, var1, var4, new Object[]{var5});
   }
}
