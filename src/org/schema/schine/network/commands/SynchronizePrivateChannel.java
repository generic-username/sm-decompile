package org.schema.schine.network.commands;

import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.network.synchronization.SynchronizationReceiver;

public class SynchronizePrivateChannel extends Command {
   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws Exception {
      assert var2.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var2.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else if (var2.isNetworkSynchronized()) {
         SynchronizationReceiver.update(var2.getPrivateLocalAndRemoteObjectContainer(), 0, var2.getProcessor().getIn(), var2, var2.isNetworkSynchronized(), false, var3, var2.getProcessor().getLastReceived());
         var2.setSynchronized(true);
      } else {
         System.err.println("[SYNCHRONIZE] " + var2 + " IS WAITING TO SYNCH WITH SERVER - SKIPPING INPUT");

         while(var2.getProcessor().getIn().read() != -1) {
         }

         System.err.println("[SYNCHRONIZE] " + var2 + " IS WAITING TO SYNCH WITH SERVER - SKIPPED INPUT");
      }
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      if (var3.isReady()) {
         assert var3.isSynched();

         if (NetworkObject.CHECKUNSAVE && !var3.isSynched()) {
            throw new UnsaveNetworkOperationException();
         }

         SynchronizationReceiver.update(var1.getClient().getLocalAndRemoteObjectContainer(), var1.getClient().getId(), var1.getIn(), var3, true, false, var4, var1.getLastReceived());
      }

   }
}
