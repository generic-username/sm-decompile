package org.schema.schine.network.commands;

import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.server.AdminRemoteClient;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class ExecuteAdminCommand extends Command {
   public ExecuteAdminCommand() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      String var6 = (String)var2[0];
      String var5 = (String)var2[1];
      var3.executeAdminCommand(var6, var5, new AdminRemoteClient(var1));
   }
}
