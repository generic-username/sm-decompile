package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.common.util.StringTools;
import org.schema.schine.network.Command;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class MessageTo extends Command {
   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
      var2.message(new Object[]{"[" + var1[0] + "]: " + (var1[1] instanceof Object[] ? StringTools.getFormatedMessage((Object[])var1[1]) : var1[1])}, (Integer)var1[2]);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException {
      String var5 = ((RegisteredClientOnServer)var3.getClients().get(var1.getClient().getId())).getPlayerName();
      this.createReturnToClient(var3, var1, var4, new Object[]{var5, var2});
   }
}
