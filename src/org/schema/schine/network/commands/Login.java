package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class Login extends Command {
   public static final int NOT_LOGGED_IN = -4242;
   private long started;

   public Login() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
      int var9 = (Integer)var1[0];
      String var4 = var1[3].toString();
      String var5 = "";
      if (var1.length > 4) {
         var5 = (String)var1[4];
      }

      var2.setServerVersion(var4);
      var2.setExtraLoginFailReason(var5);
      if (var9 < 0) {
         Login.LoginCode var8 = Login.LoginCode.getById(var9);
         switch(var8) {
         case ERROR_ALREADY_LOGGED_IN:
            System.err.println("[Client] [LOGIN]: ERROR: Already logged in " + var5);
            var2.setId(var9);
            return;
         case ERROR_AUTHENTICATION_FAILED:
            System.err.println("[Client] [LOGIN]: ERROR: Authentication Failed " + var5);
            var2.setId(var9);
            return;
         case ERROR_ACCESS_DENIED:
            System.err.println("[Client] [LOGIN]: ERROR: Access Denied " + var5);
            var2.setId(var9);
            return;
         case ERROR_GENRAL_ERROR:
            System.err.println("[Client] [LOGIN]: ERROR: General Error " + var5);
            var2.setId(var9);
            return;
         case ERROR_WRONG_CLIENT_VERSION:
            System.err.println("[Client] [LOGIN]: ERROR: The version of your client is not equal to the server. Try updating with the StarMade-Starter. (client version: " + var2.getClientVersion() + "; server version: " + var4 + ") " + var5);
            var2.setId(var9);
            return;
         case ERROR_SERVER_FULL:
            System.err.println("[Client] [LOGIN]: ERROR: Server FULL Error " + var5);
            var2.setId(var9);
            return;
         case ERROR_YOU_ARE_BANNED:
            System.err.println("[Client] [LOGIN]: ERROR: You are banned from this server " + var5);
            var2.setId(var9);
            return;
         case ERROR_NOT_ON_WHITELIST:
            System.err.println("[Client] [LOGIN]: ERROR: You are not whitelisted on this server " + var5);
            var2.setId(var9);
            return;
         case ERROR_INVALID_USERNAME:
            System.err.println("[Client] [LOGIN]: ERROR: The username is not accepted " + var5);
            var2.setId(var9);
            return;
         case ERROR_AUTHENTICATION_FAILED_REQUIRED:
            System.err.println("[Client] [LOGIN]: ERROR: Authentication required but not delivered " + var5);
            var2.setId(var9);
            return;
         case ERROR_NOT_ADMIN:
            System.err.println("[Client] [LOGIN]: ERROR: Not admin " + var5);
            var2.setId(var9);
            return;
         default:
            assert false : "something went wrong: " + var9;

            var2.setId(var9);
            throw new IllegalArgumentException("Unknown login return code. Your client might be out of date. Please update!");
         }
      } else {
         var2.setId((Integer)var1[1]);
         long var6 = System.currentTimeMillis() - this.started;
         var2.setServerTimeOnLogin((Long)var1[2] + var6 / 2L);
         System.err.println("[Client] [LOGIN]: Client sucessfully registered with id: " + var2.getId() + "; Time Difference: " + var2.getServerTimeDifference() + " (W/O RTT " + var1[2] + "); (RTT: " + var6 + ")");
      }
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      LoginRequest var5 = new LoginRequest();
      String var6 = (String)var2[0];
      String var7 = var2.length > 1 ? var2[1].toString() : "0.0.0";
      String var8 = var2.length > 2 ? (String)var2[2] : "";
      String var9 = var2.length > 3 ? (String)var2[3] : "";
      byte var11 = var2.length > 4 ? (Byte)var2[4] : 0;
      int var10 = IdGen.getFreeStateId();
      System.err.println("[SERVER][LOGIN] new client connected. given id: " + var10 + ": description: " + var6);
      var5.state = var3;
      var5.playerName = var6;
      var5.version = var7;
      var5.uid = var8;
      var5.login_code = var9;
      var5.id = var10;
      var5.serverProcessor = var1;
      var5.packetid = var4;
      var5.login = this;
      var5.userAgent = var11;
      var3.addLoginRequest(var5);
      System.err.println("[SERVER][LOGIN] return code 0");
   }

   public void writeAndCommitParametriziedCommand(Object[] var1, int var2, int var3, short var4, NetworkProcessor var5) throws IOException {
      this.started = System.currentTimeMillis();
      super.writeAndCommitParametriziedCommand(var1, var2, var3, var4, var5);
   }

   public static enum LoginCode {
      SUCCESS_LOGGED_IN(0, ""),
      ERROR_GENRAL_ERROR(-1, "Server: general error"),
      ERROR_ALREADY_LOGGED_IN(-2, "Server: name already logged in on this server\n\n\n\n(If you are retrying after a socket exception,\nplease wait 3 minutes for the server to time-out your old connection)"),
      ERROR_ACCESS_DENIED(-3, "Server: access denied"),
      ERROR_SERVER_FULL(-4, "Server: this server is full. Please try again later."),
      ERROR_WRONG_CLIENT_VERSION(-5, ""),
      ERROR_YOU_ARE_BANNED(-6, "Server: you are banned from this server"),
      ERROR_AUTHENTICATION_FAILED(-7, "Server Reject: this login name is protected on this server!\n\nYou either aren't logged in via uplink,\nor the protected name belongs to another user!\n\nPlease use the \"Uplink\" menu to authenticate this name!"),
      ERROR_NOT_ON_WHITELIST(-8, "Server: you are not whitelisted on this server"),
      ERROR_INVALID_USERNAME(-9, "Server: your username is not allowed.\nMust be at least 3 characters.\nOnly letters, numbers, '-' and '_' are allowed."),
      ERROR_AUTHENTICATION_FAILED_REQUIRED(-10, "Server: this server requires authentication via the StarMade registry.\nThis requirement is usually turned on by a server admin to deal with trolling/griefing.\nPlease use the 'Uplink' button to enter your StarMade Registry credentials.\n\nIf you don't have StarMade credentials yet, create one for free on www.star-made.org.\nAnd if you are playing on steam you can upgrade your account via steam link."),
      ERROR_NOT_ADMIN(-11, "This login can only be made as admin");

      public final int code;
      private final String msg;

      private LoginCode(int var3, String var4) {
         this.code = var3;
         this.msg = var4;
      }

      public static Login.LoginCode getById(int var0) {
         Login.LoginCode[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            Login.LoginCode var4;
            if ((var4 = var1[var3]).code == var0) {
               return var4;
            }
         }

         throw new IllegalArgumentException("ID: " + var0);
      }

      public final String errorMessage() {
         return this.msg;
      }
   }
}
