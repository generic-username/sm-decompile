package org.schema.schine.network;

import java.util.Observable;

public class Pinger extends Observable {
   private static byte[] pingBytes = new byte[2];
   private static byte[] pongBytes = new byte[2];

   public static byte[] createPing() {
      return pingBytes;
   }

   public static byte[] createPong() {
      return pongBytes;
   }

   static {
      pingBytes[0] = 23;
      pingBytes[1] = -1;
      pongBytes[0] = 23;
      pongBytes[1] = -2;
   }
}
