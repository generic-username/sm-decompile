package org.schema.schine.network.udp;

public class UDPException extends RuntimeException {
   private static final long serialVersionUID = 1L;
   private Exception ioException;

   public UDPException(String var1) {
      super(var1);
   }

   public UDPException(String var1, Exception var2) {
      super(var1);
      this.ioException = var2;
   }

   public Exception getIoException() {
      return this.ioException;
   }
}
