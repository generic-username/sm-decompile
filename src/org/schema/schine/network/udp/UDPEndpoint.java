package org.schema.schine.network.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;

public class UDPEndpoint {
   private int id;
   private SocketAddress address;
   private DatagramSocket socket;
   private boolean connected = true;
   private UDPProcessor processor;

   public UDPEndpoint(UDPProcessor var1, int var2, SocketAddress var3, DatagramSocket var4) {
      this.id = var2;
      this.address = var3;
      this.socket = var4;
      this.processor = var1;
   }

   public void close() {
      this.close(false);
   }

   public void close(boolean var1) {
      try {
         this.processor.closeEndpoint(this);
         this.connected = false;
      } catch (IOException var2) {
         throw new UDPException("Error closing endpoint for socket:" + this.socket, var2);
      }
   }

   public String getAddress() {
      return String.valueOf(this.address);
   }

   public long getId() {
      return (long)this.id;
   }

   protected SocketAddress getRemoteAddress() {
      return this.address;
   }

   public boolean isConnected() {
      return this.connected;
   }

   public void send(byte[] var1, int var2, int var3) {
      if (!this.isConnected()) {
         throw new UDPException("Endpoint is not connected:" + this);
      } else {
         try {
            DatagramPacket var5 = new DatagramPacket(var1, var2, var3, this.address);
            this.processor.enqueueWrite(this, var5);
         } catch (IOException var4) {
            throw new UDPException("Error sending datagram to:" + this.address, var4);
         }
      }
   }

   public String toString() {
      return "UdpEndpoint[" + this.id + ", " + this.address + "]";
   }
}
