package org.schema.schine.network.udp;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class NamedThreadFactory implements ThreadFactory {
   private String name;
   private boolean daemon;
   private ThreadFactory delegate;

   public NamedThreadFactory(String var1) {
      this(var1, Executors.defaultThreadFactory());
   }

   public NamedThreadFactory(String var1, boolean var2) {
      this(var1, var2, Executors.defaultThreadFactory());
   }

   public NamedThreadFactory(String var1, boolean var2, ThreadFactory var3) {
      this.name = var1;
      this.daemon = var2;
      this.delegate = var3;
   }

   public NamedThreadFactory(String var1, ThreadFactory var2) {
      this(var1, false, var2);
   }

   public Thread newThread(Runnable var1) {
      Thread var3;
      String var2 = (var3 = this.delegate.newThread(var1)).getName();
      var3.setName(this.name + "[" + var2 + "]");
      var3.setDaemon(this.daemon);
      return var3;
   }
}
