package org.schema.schine.network.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.schema.schine.network.IdGen;

public class UDPProcessor {
   private Map socketEndpoints;
   private InetSocketAddress address;
   private UDPProcessor.HostThread thread;
   private ExecutorService writer;

   public UDPProcessor(InetAddress var1, int var2) {
      this(new InetSocketAddress(var1, var2));
   }

   public UDPProcessor(InetSocketAddress var1) {
      this.socketEndpoints = new ConcurrentHashMap();
      this.address = var1;
   }

   public UDPProcessor(int var1) throws IOException {
      this(new InetSocketAddress(var1));
   }

   public static void main(String[] var0) {
      (new Thread(new Runnable() {
         public final void run() {
            try {
               UDPProcessor var1;
               (var1 = new UDPProcessor(4242)).initialize();

               while(true) {
                  System.err.println("Broadcasting");
                  var1.broadcast(new byte[]{10}, 0, 1);

                  try {
                     Thread.sleep(1000L);
                  } catch (InterruptedException var2) {
                     var2.printStackTrace();
                  }
               }
            } catch (IOException var3) {
               var3.printStackTrace();
            }
         }
      })).start();
      (new Thread(new Runnable() {
         public final void run() {
            try {
               UDPProcessor var1;
               (var1 = new UDPProcessor(4244)).initialize();
               var1.getEndpoint(new InetSocketAddress(4242), true).send(new byte[13], 0, 1);
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }
      })).start();
   }

   public void broadcast(byte[] var1, int var2, int var3) {
      Iterator var4 = this.socketEndpoints.values().iterator();

      while(var4.hasNext()) {
         UDPEndpoint var5 = (UDPEndpoint)var4.next();
         System.err.println("SEDING TO ENDPOINT");
         var5.send(var1, var2, var3);
      }

   }

   protected void closeEndpoint(UDPEndpoint var1) throws IOException {
      if (this.socketEndpoints.remove(var1.getRemoteAddress()) != null) {
         ;
      }
   }

   protected UDPProcessor.HostThread createHostThread() {
      return new UDPProcessor.HostThread();
   }

   public void enqueueWrite(UDPEndpoint var1, DatagramPacket var2) throws IOException {
      this.writer.execute(new UDPProcessor.MessageWriter(var1, var2));
   }

   protected UDPEndpoint getEndpoint(SocketAddress var1, boolean var2) {
      UDPEndpoint var3;
      if ((var3 = (UDPEndpoint)this.socketEndpoints.get(var1)) == null && var2) {
         var3 = new UDPEndpoint(this, IdGen.getIndependentId(), var1, this.thread.getSocket());
         this.socketEndpoints.put(var1, var3);
      }

      return var3;
   }

   public void initialize() {
      if (this.thread != null) {
         throw new IllegalStateException("Kernel already initialized.");
      } else {
         this.writer = Executors.newFixedThreadPool(2, new NamedThreadFactory(this.toString() + "-writer"));
         this.thread = this.createHostThread();

         try {
            this.thread.connect();
            this.thread.start();
         } catch (IOException var2) {
            throw new UDPException("Error hosting:" + this.address, var2);
         }
      }
   }

   protected void newData(DatagramPacket var1) {
      this.getEndpoint(var1.getSocketAddress(), true);
      System.err.println("RECEIVED " + Arrays.toString(var1.getData()));
   }

   public void terminate() throws InterruptedException {
      if (this.thread == null) {
         throw new IllegalStateException("Kernel not initialized.");
      } else {
         try {
            this.thread.close();
            this.writer.shutdown();
            this.thread = null;
         } catch (IOException var2) {
            throw new UDPException("Error closing host connection:" + this.address, var2);
         }
      }
   }

   public class MessageWriter implements Runnable {
      private UDPEndpoint endpoint;
      private DatagramPacket packet;

      public MessageWriter(UDPEndpoint var2, DatagramPacket var3) {
         this.endpoint = var2;
         this.packet = var3;
      }

      public void run() {
         if (this.endpoint.isConnected()) {
            try {
               UDPProcessor.this.thread.getSocket().send(this.packet);
            } catch (Exception var2) {
               (new UDPException("Error sending datagram to:" + UDPProcessor.this.address, var2)).fillInStackTrace();
               var2.printStackTrace();
            }
         }
      }
   }

   public class HostThread extends Thread {
      private DatagramSocket socket;
      private boolean running = true;
      private byte[] buffer = new byte['\uffff'];

      public HostThread() {
         this.setName("UDP Host@" + UDPProcessor.this.address);
         this.setDaemon(true);
      }

      public void close() throws IOException, InterruptedException {
         this.running = false;
         this.socket.close();
         this.join();
      }

      public void connect() throws IOException {
         this.socket = new DatagramSocket(UDPProcessor.this.address);
      }

      protected DatagramSocket getSocket() {
         return this.socket;
      }

      private void reportError(IOException var1) {
      }

      public void run() {
         while(this.running) {
            try {
               DatagramPacket var1 = new DatagramPacket(this.buffer, this.buffer.length);
               this.socket.receive(var1);
               UDPProcessor.this.newData(var1);
            } catch (IOException var2) {
               if (!this.running) {
                  return;
               }

               this.reportError(var2);
            }
         }

      }
   }
}
