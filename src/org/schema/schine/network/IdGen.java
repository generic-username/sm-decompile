package org.schema.schine.network;

public class IdGen {
   public static final int SERVER_ID = 0;
   private static Integer idPool = new Integer(0);
   private static int independentIdPool;
   private static Short packetIdC = -32767;
   private static int NETWORK_ID_CREATOR = 1;

   public static int getFreeObjectId(int var0) {
      boolean var1 = false;
      synchronized(idPool) {
         int var4 = idPool;
         idPool = idPool + var0;
         return var4;
      }
   }

   public static synchronized int getFreeStateId() {
      return NETWORK_ID_CREATOR++;
   }

   public static synchronized int getIndependentId() {
      return independentIdPool++;
   }

   public static synchronized short getNewPacketId() {
      synchronized(packetIdC) {
         Short var10000;
         if (packetIdC == -32768) {
            var10000 = packetIdC;
            packetIdC = (short)(packetIdC + 1);
         }

         short var1 = packetIdC;
         var10000 = packetIdC;
         packetIdC = (short)(packetIdC + 1);
         return var1;
      }
   }
}
