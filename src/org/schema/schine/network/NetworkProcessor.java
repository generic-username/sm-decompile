package org.schema.schine.network;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public interface NetworkProcessor {
   void closeSocket() throws IOException;

   void flushDoubleOutBuffer() throws IOException;

   int getCurrentSize();

   DataInputStreamPositional getIn() throws IOException;

   InputStream getInRaw() throws IOException;

   DataOutputStreamPositional getOut();

   OutputStream getOutRaw() throws IOException;

   StateInterface getState();

   boolean isAlive();

   void notifyPacketReceived(short var1, Command var2);

   void resetDoubleOutBuffer() throws IOException;

   void updatePing() throws IOException;

   ReentrantReadWriteLock getBufferLock();

   ObjectArrayList getLastReceived();
}
