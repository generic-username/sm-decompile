package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;
import net.rudp.ReliableServerSocket;
import org.schema.common.LogUtil;
import org.schema.schine.network.exception.ServerPortNotAvailableException;

public class ServerListener extends Observable implements Runnable {
   private final int SPAM_PROTECT_TIME_MS;
   private final int SPAM_PROTECT_ATTEMPTS;
   private final int port;
   private final ServerSocket serverSocket;
   private final ServerStateInterface state;
   private final String host;
   private ObjectOpenHashSet spamExceptions = new ObjectOpenHashSet();
   private boolean listening;
   private Object2ObjectOpenHashMap connectionHistory = new Object2ObjectOpenHashMap();
   public int serverId;
   private static int ID;

   public ServerListener(String var1, int var2, ServerStateInterface var3) throws IOException, ServerPortNotAvailableException {
      this.host = var1;
      this.port = var2;
      this.state = var3;
      boolean var4 = false;
      boolean var5 = false;
      this.serverId = ID++;
      this.SPAM_PROTECT_TIME_MS = var3.getNTSpamProtectTimeMs();
      this.SPAM_PROTECT_ATTEMPTS = var3.getNTSpamProtectMaxAttempty();
      String[] var6;
      int var7 = (var6 = var3.getNTSpamProtectException().split(",")).length;

      for(int var8 = 0; var8 < var7; ++var8) {
         String var9 = var6[var8];
         this.spamExceptions.add(var9);
      }

      try {
         Socket var11;
         (var11 = new Socket(var1, this.port)).setSoTimeout(3000);
         DataOutputStream var13 = new DataOutputStream(new BufferedOutputStream(var11.getOutputStream()));
         DataInputStream var14 = new DataInputStream(new BufferedInputStream(var11.getInputStream()));
         var13.writeInt(1);
         var13.write(100);
         var13.flush();
         if ((byte)var14.read() == 100) {
            var5 = true;
            System.out.println("[SERVER] A schine server is already running");
         }

         var11.shutdownInput();
         var11.shutdownOutput();
         var11.close();
      } catch (Exception var10) {
         if (var10 instanceof SocketTimeoutException) {
            var10.printStackTrace();
         } else {
            var4 = true;
         }
      }

      if (var4 && !var5) {
         System.err.println("[SERVER] port " + var2 + " is open");
         if (var3.getAcceptingIP() != null && !var3.getAcceptingIP().equals("all")) {
            if (var3.useUDP()) {
               System.err.println("[SERVER] USING UDP... LISTENING ON " + var3.getAcceptingIP());
               this.serverSocket = new ReliableServerSocket(this.port, 0, InetAddress.getByName(var3.getAcceptingIP()));
            } else {
               System.err.println("[SERVER] USING TCP... LISTENING ON " + var3.getAcceptingIP());
               this.serverSocket = new ServerSocket(this.port, 0, InetAddress.getByName(var3.getAcceptingIP()));
            }
         } else if (var3.useUDP()) {
            System.err.println("[SERVER] USING UDP... ");
            this.serverSocket = new ReliableServerSocket(this.port, 0);
         } else {
            System.err.println("[SERVER] USING TCP... ");
            this.serverSocket = new ServerSocket(this.port, 0);
         }
      } else {
         System.err.println("THROWING EXCEPTION");
         ServerPortNotAvailableException var12;
         (var12 = new ServerPortNotAvailableException("localhost port " + this.port + " is closed or already in use")).setInstanceRunning(var5);
         throw var12;
      }
   }

   public String getHost() {
      return this.host;
   }

   public boolean isListening() {
      return this.listening;
   }

   public void stop() {
      System.err.println("[SERVER] Stopping Listener");

      try {
         this.listening = false;
         this.serverSocket.close();
      } catch (IOException var1) {
         var1.printStackTrace();
      }

      System.err.println("[SERVER] Stopped Listener");
   }

   public void run() {
      System.err.println("[ServerListener] Server initialization OK... now waiting for connections");
      if (this.state.announceServer() && this.state.announceHost() != null && this.state.announceHost().length() > 0) {
         new ServerSender(ServerSender.DEFAULT_HB, "files-origin.star-made.org", ServerSender.DEFAULT_PORT, this.state.announceHost(), this.port);
      }

      this.listening = true;

      while(this.listening) {
         try {
            if (this.serverSocket.isClosed()) {
               System.err.println("server socket is closed!");
            }

            this.serverSocket.setPerformancePreferences(0, 2, 1);
            this.serverSocket.setReceiveBufferSize(this.state.getSocketBufferSize());
            Socket var1 = this.serverSocket.accept();
            System.err.println("[SERVERSOCKET] Connection made. starting new processor " + var1.getPort() + ", " + var1.getInetAddress() + "; local: " + var1.getLocalPort() + ", " + var1.getLocalAddress() + ", keepalive " + var1.getKeepAlive());
            LogUtil.log().fine("[SERVERSOCKET] Incoming connection: " + var1.getPort() + ", " + var1.getInetAddress() + " -> " + var1.getLocalPort() + ", " + var1.getLocalAddress());
            if (this.state.isNTSpamCheckActive() && !this.checkSpam(var1.getInetAddress())) {
               System.err.println("[SERVERSOCKET] Closing connection to " + var1.getInetAddress() + " because of too many connection tries in a short period");
               var1.close();
            } else {
               var1.setKeepAlive(true);
               if (!this.state.useUDP()) {
                  var1.setTcpNoDelay(this.state.tcpNoDelay());
               }

               var1.setTrafficClass(24);
               var1.setSendBufferSize(this.state.getSocketBufferSize());
               var1.setSoTimeout(300000);
               ServerProcessor var4 = new ServerProcessor(var1, this.state);
               if (this.state.getController() instanceof Observer) {
                  var4.addObserver((Observer)this.state.getController());
               }

               Thread var2;
               (var2 = new Thread(var4)).setDaemon(true);
               var4.setThread(var2);
               var2.setName("SERVER-Listener Thread (unknownId)");
               var2.start();
               this.setChanged();
               this.notifyObservers(var4);
               System.out.println("[SERVER] ListenerId[" + this.serverId + "] connection registered (TRunning: " + ServerProcessor.PROCESSOR_THREADS_RUNNING + "/" + ServerProcessor.PING_THREADS_RUNNING + "/" + ServerProcessor.SENDING_THREADS_RUNNING + ") " + var4.id);
            }
         } catch (IOException var3) {
            if (this.listening) {
               var3.printStackTrace();
            }
         }
      }

   }

   private boolean checkSpam(InetAddress var1) {
      String var4 = var1.toString().replace("/", "");
      if (this.spamExceptions.contains(var4)) {
         return true;
      } else {
         ObjectIterator var2 = this.connectionHistory.entrySet().iterator();

         while(true) {
            LongArrayList var3;
            do {
               if (!var2.hasNext()) {
                  if ((var3 = (LongArrayList)this.connectionHistory.get(var4)) == null) {
                     var3 = new LongArrayList();
                     this.connectionHistory.put(var4, var3);
                  }

                  var3.add(System.currentTimeMillis());
                  if (var3.size() <= this.SPAM_PROTECT_ATTEMPTS) {
                     return true;
                  }

                  System.err.println("[SERVERSOCKET] WARNING: Spam Protect active against: " + var4 + ": " + var3.size() + " (reduced to 100 records in memory to avoid memory-leak attack) connection attepts in less then " + this.SPAM_PROTECT_TIME_MS / 1000 + "sec");

                  while(var3.size() > 100) {
                     var3.remove(0);
                  }

                  return false;
               }
            } while(!(var3 = (LongArrayList)((Entry)var2.next()).getValue()).isEmpty() && System.currentTimeMillis() - var3.get(var3.size() - 1) <= (long)this.SPAM_PROTECT_TIME_MS);

            var2.remove();
         }
      }
   }
}
