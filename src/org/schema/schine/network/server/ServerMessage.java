package org.schema.schine.network.server;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import org.schema.schine.network.Command;
import org.schema.schine.network.SerialializationInterface;

public class ServerMessage implements SerialializationInterface {
   public static final int MESSAGE_TYPE_SIMPLE = 0;
   public static final int MESSAGE_TYPE_INFO = 1;
   public static final int MESSAGE_TYPE_WARNING = 2;
   public static final int MESSAGE_TYPE_ERROR = 3;
   public static final int MESSAGE_TYPE_ERROR_BLOCK = 4;
   public static final int MESSAGE_TYPE_DIALOG = 5;
   private Object[] message;
   public int type;
   public int receiverPlayerId;
   public String prefix;
   public boolean adminOnly;
   public long block;

   public ServerMessage() {
   }

   public ServerMessage(Object[] var1, int var2) {
      this.message = var1;
      this.type = var2;
      this.receiverPlayerId = 0;
   }

   public ServerMessage(Object[] var1, int var2, boolean var3) {
      this(var1, var2);
      this.adminOnly = var3;
   }

   public ServerMessage(Object[] var1, int var2, int var3) {
      this.message = var1;
      this.type = var2;
      this.receiverPlayerId = var3;
   }

   public String toString() {
      return "[SERVERMSG (type " + this.type + "): " + Arrays.toString(this.message) + "]";
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.type);
      if (this.type == 4) {
         var1.writeLong(this.block);
      }

      var1.writeInt(this.receiverPlayerId);
      Command.serialize(this.message, var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.type = var1.readByte();
      if (this.type == 4) {
         this.block = var1.readLong();
      }

      this.receiverPlayerId = var1.readInt();
      this.message = Command.deserialize(var1);
   }

   public Object[] getMessage() {
      return this.message;
   }

   public void setMessage(Object[] var1) {
      this.message = var1;
   }
}
