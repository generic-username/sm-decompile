package org.schema.schine.network.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ServerSender extends Thread {
   public static final String DEFAULT_HOST = "files-origin.star-made.org";
   public static long DEFAULT_HB = 300000L;
   public static int DEFAULT_PORT = 19138;
   private final long heartbeat;
   private int port;
   private String host;
   private String accounceServerHost;
   private int annouceServerPort;

   public ServerSender(long var1, String var3, int var4, String var5, int var6) {
      this.setPriority(1);
      this.setDaemon(true);
      this.heartbeat = var1;
      this.host = var5;
      this.port = var6;
      this.accounceServerHost = var3;
      this.annouceServerPort = var4;
      this.start();
   }

   public static void main(String[] var0) {
      new ServerSender(DEFAULT_HB, "localhost", DEFAULT_PORT, "localhost", 4242);
   }

   public void run() {
      this.annouceServer();

      while(true) {
         try {
            Thread.sleep(this.heartbeat);
         } catch (InterruptedException var1) {
            var1.printStackTrace();
         }

         this.annouceServer();
      }
   }

   private void annouceServer() {
      try {
         Socket var1;
         (var1 = new Socket(this.accounceServerHost, this.annouceServerPort)).setSoTimeout(3000);
         DataOutputStream var2 = new DataOutputStream(new BufferedOutputStream(var1.getOutputStream()));
         new DataInputStream(new BufferedInputStream(var1.getInputStream()));
         var2.writeUTF(this.host + ":" + this.port);
         var2.flush();
         var1.shutdownInput();
         var1.shutdownOutput();
         var1.close();
      } catch (Exception var3) {
         var3.printStackTrace();
      }
   }
}
