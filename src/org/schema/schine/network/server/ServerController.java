package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.LogUtil;
import org.schema.common.ParseException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.DataOutputStreamPositional;
import org.schema.schine.network.Header;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientController;
import org.schema.schine.network.commands.Login;
import org.schema.schine.network.commands.LogoutClient;
import org.schema.schine.network.commands.RequestSynchronizeAll;
import org.schema.schine.network.commands.Synchronize;
import org.schema.schine.network.commands.SynchronizePrivateChannel;
import org.schema.schine.network.synchronization.SynchronizationReceiver;
import org.schema.schine.network.synchronization.SynchronizationSender;
import org.xml.sax.SAXException;

public abstract class ServerController extends Observable implements Runnable, Observer, ServerControllerInterface {
   public static int port = 4242;
   private final IntOpenHashSet delHelper = new IntOpenHashSet();
   private final ObjectArrayFIFOQueue systemInQueue = new ObjectArrayFIFOQueue();
   protected HashSet toRemoveClients = new HashSet();
   private ServerState serverState;
   private ServerListener serverListener;
   public static boolean debugLogoutOnShutdown;
   private Timer timer = new Timer();
   private DataOutputStreamPositional dataOut;
   private DataOutputStreamPositional dataOutPrivateToSend;
   private DataOutputStreamPositional dataOutPrivate;
   private FastByteArrayOutputStream byteOutput = new FastByteArrayOutputStream(1048576);
   private FastByteArrayOutputStream byteOutputPrivate = new FastByteArrayOutputStream(1048576);
   private FastByteArrayOutputStream byteOutputPrivateToSend = new FastByteArrayOutputStream(1048576);
   private long lastSetSnapshop;
   protected ServerController.ServerActiveCheck serverActiveChecker;
   protected static SystemInListener systemInListener = new SystemInListener();

   public ServerController(ServerState var1) {
      systemInListener.setState(var1);
      this.serverActiveChecker = new ServerController.ServerActiveCheck();
      this.serverActiveChecker.start();
      this.setServerState(var1);
      var1.setController(this);
      this.dataOut = new DataOutputStreamPositional(this.byteOutput);
      this.dataOutPrivate = new DataOutputStreamPositional(this.byteOutputPrivate);
      this.dataOutPrivateToSend = new DataOutputStreamPositional(this.byteOutputPrivateToSend);
      Runtime.getRuntime().addShutdownHook(new Thread() {
         public void run() {
            try {
               if (!ServerState.isShutdown()) {
                  System.err.println("WARNING: USING EMERGENCY SHUTDOWN HOOK");
                  ServerState.setShutdown(true);
                  System.err.println("WARNING: USING EMERGENCY SHUTDOWN HOOK: onShutDown");
                  ServerController.this.onShutDown(true);
                  System.err.println("WARNING: USING EMERGENCY SHUTDOWN HOOK: closing log");
                  System.err.println("WARNING: USING EMERGENCY SHUTDOWN HOOK DONE");
               }

            } catch (IOException var2) {
               System.err.println("[ERROR] SERVER SHUTDOWN. Failed to save ServerState!");
               var2.printStackTrace();
            }
         }
      });
   }

   public void broadcastMessage(Object[] var1, int var2) {
      synchronized(this.serverState.getToBroadCastMessages()) {
         this.serverState.getToBroadCastMessages().add(new ServerMessage(var1, var2));
      }
   }

   public boolean isListenting() {
      return this.serverListener.isListening();
   }

   public int registerClient(RegisteredClientOnServer var1, String var2, StringBuffer var3) throws Exception {
      if (!var2.trim().equals(this.getServerState().getVersion().trim())) {
         return Login.LoginCode.ERROR_WRONG_CLIENT_VERSION.code;
      } else {
         if (!this.isAdmin(var1)) {
            if (this.serverState.getClients().size() >= this.serverState.getMaxClients()) {
               var3.append("(currently " + this.serverState.getClients().size() + "/" + this.serverState.getMaxClients() + ")");
               return Login.LoginCode.ERROR_SERVER_FULL.code;
            }

            if (this.isBanned(var1, var3)) {
               System.err.println("[SERVER][LOGIN] Denying banned user: " + var1);
               return Login.LoginCode.ERROR_YOU_ARE_BANNED.code;
            }

            if (!this.isWhiteListed(var1)) {
               System.err.println("[SERVER][LOGIN] Denying not white listed user: " + var1);
               return Login.LoginCode.ERROR_NOT_ON_WHITELIST.code;
            }
         }

         var2 = var1.getPlayerName();
         System.err.println("[SERVER] Client register setup phase 1 completed. Name: " + var2 + "; checking already logged in");
         Iterator var4 = this.serverState.getClients().values().iterator();

         do {
            if (!var4.hasNext()) {
               int var5;
               if ((var5 = this.onLoggedIn(var1)) == Login.LoginCode.SUCCESS_LOGGED_IN.code) {
                  this.getServerState().getClients().put(var1.getId(), var1);
                  this.setChanged();
                  this.notifyObservers();
               }

               System.err.println("[SERVER] Client register setup phase 2 completed. Name: " + var2 + "; sending success to client");
               return var5;
            }
         } while(!((RegisteredClientOnServer)var4.next()).getPlayerName().toLowerCase(Locale.ENGLISH).equals(var2.toLowerCase(Locale.ENGLISH)));

         return Login.LoginCode.ERROR_ALREADY_LOGGED_IN.code;
      }
   }

   public void unregister(int var1) {
      synchronized(this.toRemoveClients) {
         this.toRemoveClients.add(var1);
      }
   }

   public void broadcastMessageAdmin(Object[] var1, int var2) {
      synchronized(this.serverState.getToBroadCastMessages()) {
         this.serverState.getToBroadCastMessages().add(new ServerMessage(var1, var2, true));
      }
   }

   public void checkIfClientsNeedResynch(Set var1) throws IOException {
      Iterator var6 = var1.iterator();

      while(var6.hasNext()) {
         RegisteredClientOnServer var2;
         if ((var2 = (RegisteredClientOnServer)var6.next()).getProcessor().isAlive()) {
            try {
               if (var2.needsSynch()) {
                  long var3 = System.currentTimeMillis();
                  System.err.println("[SERVER] (client needs synch) SENDING SYNCHALL TO " + var2);
                  RequestSynchronizeAll.executeSynchAll(this.serverState, var2.getProcessor());
                  var2.wasFullSynched = true;
                  var2.flagSynch((short)-32768);
                  System.err.println("[SERVER] SYNCHALL TO " + var2 + " took: " + (System.currentTimeMillis() - var3));
               }
            } catch (IOException var5) {
               var5.printStackTrace();
            }
         }
      }

   }

   public abstract void createThreadDump();

   protected abstract void displayError(Exception var1);

   public ServerState getServerState() {
      return this.serverState;
   }

   public void setServerState(ServerState var1) {
      this.serverState = var1;
   }

   public ObjectArrayFIFOQueue getSystemInQueue() {
      return this.systemInQueue;
   }

   public Timer getTimer() {
      return this.timer;
   }

   public void setTimer(Timer var1) {
      this.timer = var1;
   }

   public abstract void initializeServerState() throws IOException, SQLException, NoSuchAlgorithmException, ResourceException, ParseException, SAXException, ParserConfigurationException;

   protected abstract boolean isAdmin(RegisteredClientOnServer var1);

   public boolean isBanned(RegisteredClientOnServer var1, StringBuffer var2) {
      return false;
   }

   public boolean isWhiteListed(RegisteredClientOnServer var1) {
      return false;
   }

   public abstract int onLoggedIn(RegisteredClientOnServer var1) throws Exception;

   public abstract void onLoggedout(RegisteredClientOnServer var1);

   protected abstract void onShutDown(boolean var1) throws IOException;

   public void run() {
      System.out.println("[SERVERCONTROLLER][INIT] (listenerId[" + this.serverListener.serverId + "]) SERVER STARTED UPDATING");

      try {
         Thread.sleep(100L);
         this.timer.updateFPS(true);

         assert !ServerState.isShutdown() : "Shutdown before start";

         for(; !ServerState.isShutdown(); this.timer.updateFPS(true)) {
            if (!this.serverState.isPaused()) {
               long var1 = System.currentTimeMillis();
               this.update(this.timer);
               long var3 = Math.max(0L, System.currentTimeMillis() - var1);
               long var5;
               if ((var5 = 37L - var3) > 0L && var5 < 60000L) {
                  Thread.sleep(var5);
               }

               ServerState.entityCount = this.getServerState().getLocalAndRemoteObjectContainer().getLocalObjectsSize();
            } else {
               Thread.sleep(500L);
            }
         }
      } catch (RuntimeException var9) {
         System.err.println("Exiting (normal) because of exception " + var9);
         var9.printStackTrace();
         this.displayError(var9);
      } catch (Exception var10) {
         System.err.println("Exiting (normal) because of exception " + var10);
         var10.printStackTrace();
         this.displayError(var10);
      }

      if (!ServerState.isShutdown()) {
         try {
            this.onShutDown(false);
            LogUtil.closeAll();
            ServerState.serverIsOkToShutdown = true;
            if (!ClientController.isCreated()) {
               System.err.println("[SERVER][FATAL] Exception: KILLING SERVER BECAUSE OF ERROR");

               try {
                  throw new Exception("System.exit() called");
               } catch (Exception var7) {
                  var7.printStackTrace();
                  System.exit(0);
               }
            } else {
               System.err.println("[SERVER] Client detected: SET CLIENT FINISHED");
               GLFrame.setFinished(true);
            }
         } catch (IOException var8) {
            var8.printStackTrace();
         }
      } else {
         System.err.println("[SERVER][SHUTDOWN] Server shutdown update thread finished!");
      }
   }

   public void sendLogout(int var1, String var2) throws IOException {
      RegisteredClientOnServer var3;
      if ((var3 = (RegisteredClientOnServer)this.serverState.getClients().get(var1)) != null) {
         System.err.println("[SERVER] SENDING ACTIVE LOGOUT TO CLIENT " + var3);
         var3.getProcessor().serverCommand(NetUtil.commands.getByClass(LogoutClient.class).getId(), 0, var2);
         var3.getProcessor().disconnectAfterSent();
      }

   }

   protected void sendMessages(Set var1) {
      long var2 = System.currentTimeMillis();

      label37:
      while(this.getServerState().getToBroadCastMessages().size() > 0) {
         ServerMessage var4 = (ServerMessage)this.getServerState().getToBroadCastMessages().remove(0);

         try {
            Iterator var5 = var1.iterator();

            while(true) {
               RegisteredClientOnServer var6;
               do {
                  if (!var5.hasNext()) {
                     continue label37;
                  }

                  var6 = (RegisteredClientOnServer)var5.next();
               } while(var4.adminOnly && !this.isAdmin(var6));

               var6.serverMessage(var4);
            }
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      }

      long var8;
      if ((var8 = System.currentTimeMillis() - var2) > 10L) {
         System.err.println("[SERVER][UPDATE] WARNING: sendMessages update took " + var8);
      }

   }

   public void stopListening() {
      if (this.serverListener != null && this.serverListener.isListening()) {
         this.serverListener.stop();
      }

   }

   public void startServerAndListen() throws Exception {
      this.serverListener = new ServerListener("localhost", port, this.serverState);
      this.serverListener.addObserver(this);
      this.initializeServerState();
      Thread var1;
      (var1 = new Thread(this.serverListener, "ServerListener")).setDaemon(true);
      var1.start();
      (new Thread(this, "ServerController (" + this.serverListener.serverId + ")")).start();
   }

   public void synchronize(Set var1) throws IOException {
      this.checkIfClientsNeedResynch(var1);
      int var2;
      synchronized(this.serverState) {
         this.serverState.setSynched();
         var2 = SynchronizationSender.encodeNetworkObjects(this.getServerState().getLocalAndRemoteObjectContainer(), this.serverState, this.dataOut, false);
         this.serverState.setUnsynched();
      }

      Iterator var3 = var1.iterator();

      while(true) {
         while(var3.hasNext()) {
            RegisteredClientOnServer var15;
            if ((var15 = (RegisteredClientOnServer)var3.next()).wasFullSynched) {
               var15.wasFullSynched = false;
            }

            if (!var15.getProcessor().isAlive()) {
               System.err.println("[SERVER] Disconnect: Processor of " + var15 + " is dead: Removing client " + var15.getId());
               this.toRemoveClients.add(var15.getId());
            } else {
               this.byteOutputPrivate.reset();
               this.byteOutputPrivateToSend.reset();
               int var4 = this.synchronizePrivate(var15);
               if (var2 == 1 || var4 == 1) {
                  try {
                     var15.getProcessor().getBufferLock().writeLock().lock();
                     if (var2 == 1) {
                        (new Header(Synchronize.class, 0, var15.getId(), (short)-32768, (byte)123)).write(var15.getProcessor().getOut());
                        var15.getProcessor().getOut().write(this.byteOutput.array, 0, (int)this.byteOutput.position());
                     }

                     var15.getProcessor().getOut().write(this.byteOutputPrivateToSend.array, 0, (int)this.byteOutputPrivateToSend.position());
                     var4 = var15.getProcessor().getCurrentSize();
                     long var5 = System.currentTimeMillis();
                     var15.getProcessor().flushDoubleOutBuffer();
                     long var7;
                     if ((var7 = System.currentTimeMillis() - var5) > 100L) {
                        System.err.println("[WARNING][SERVER] WARNING: synchronized flush took " + var7 + " ms, size: " + var4 + " bytes");
                     }
                  } catch (IOException var12) {
                     var12.printStackTrace();
                     var15.getProcessor().resetDoubleOutBuffer();
                     System.err.println("[WARNING] SERVER CANNOT REACH " + var15 + " SKIPPING THIS CLIENT'S UPDATE");
                  } finally {
                     var15.getProcessor().getBufferLock().writeLock().unlock();
                  }
               }
            }
         }

         if (System.currentTimeMillis() - this.lastSetSnapshop > 1000L) {
            this.serverState.getDataStatsManager().snapshotUpload(this.serverState.getSentData());
            this.lastSetSnapshop = System.currentTimeMillis();
         }

         if (var2 == 1) {
            SynchronizationReceiver.handleDeleted(this.serverState.getLocalAndRemoteObjectContainer(), this.serverState, this.delHelper);
         }

         this.byteOutput.reset();
         this.getServerState().getLocalAndRemoteObjectContainer().checkGhostObjects();
         this.serverState.getDataStatsManager().update();
         return;
      }
   }

   public int synchronizePrivate(RegisteredClientOnServer var1) throws IOException {
      assert this.byteOutputPrivate.position() == 0L;

      int var2;
      synchronized(this.serverState) {
         this.serverState.setSynched();
         var2 = SynchronizationSender.encodeNetworkObjects(var1.getLocalAndRemoteObjectContainer(), this.serverState, this.dataOutPrivate, false);
         this.serverState.setUnsynched();
      }

      if (var2 == 1) {
         if (!var1.getProcessor().isAlive()) {
            return 0;
         }

         try {
            (new Header(SynchronizePrivateChannel.class, 0, var1.getId(), (short)-32768, (byte)123)).write(this.dataOutPrivateToSend);
            int var4 = (int)this.byteOutputPrivate.position();
            long var5 = System.currentTimeMillis();
            this.dataOutPrivateToSend.write(this.byteOutputPrivate.array, 0, (int)this.byteOutputPrivate.position());
            long var7;
            if ((var7 = System.currentTimeMillis() - var5) > 10L) {
               System.err.println("[WARNING][SERVER] Exception: private synchronized flush took " + var7 + " ms, size " + var4);
            }
         } catch (IOException var9) {
            var9.printStackTrace();
            this.byteOutputPrivateToSend.reset();
            System.err.println("[WARNING] SERVER CANNOT REACH " + var1 + " SKIPPING THIS CLIENT'S UPDATE");
         }
      }

      SynchronizationReceiver.handleDeleted(var1.getLocalAndRemoteObjectContainer(), this.serverState, this.delHelper);
      return var2;
   }

   public void update(Observable var1, Object var2) {
      this.setChanged();
      this.notifyObservers(var2);
   }

   public abstract boolean isUserProtectionAuthenticated(String var1, String var2);

   static {
      Thread var0;
      (var0 = new Thread(systemInListener, "SysInListener")).setDaemon(true);
      var0.start();
   }

   public class ServerActiveCheck extends Thread {
      private static final int waiting = 3000;
      private int serverOnUpdate = 0;
      private short lastUpdate = 0;
      private boolean shutdown;

      public ServerActiveCheck() {
         super("ServerActiveCheck");
      }

      public void run() {
         while(ServerController.this.serverState == null) {
            try {
               Thread.sleep(3000L);
            } catch (InterruptedException var1) {
               var1.printStackTrace();
            }
         }

         while(!this.shutdown) {
            if (this.lastUpdate != ServerController.this.serverState.getNumberOfUpdate()) {
               this.serverOnUpdate = 0;
               this.lastUpdate = ServerController.this.serverState.getNumberOfUpdate();
            } else {
               ++this.serverOnUpdate;
            }

            if (this.serverOnUpdate >= 5 && this.serverOnUpdate % 5 == 0) {
               try {
                  throw new ThreadDeath();
               } catch (Exception var3) {
                  var3.printStackTrace();
                  System.err.println("[SERVER] Exception Active checker: server not responding over " + this.serverOnUpdate * 3000 / 1000 + " seconds");
                  if (this.serverOnUpdate % 5 == 0) {
                     ServerController.this.createThreadDump();
                  }
               }
            }

            try {
               Thread.sleep(3000L);
            } catch (InterruptedException var2) {
            }
         }

      }

      public void shutdown() {
         this.shutdown = true;
         this.interrupt();
      }
   }
}
