package org.schema.schine.network;

public class ServerInfo extends AbstractServerInfo {
   public static long curtime;
   private final Byte infoVersion;
   private final String version;
   private final String name;
   private final String desc;
   private final Long startTime;
   private final Integer playerCount;
   private final Integer maxPlayers;
   private final long ping;
   private final String host;
   private final int port;
   private final String connType;
   public String ip;
   public boolean reachable;

   public ServerInfo(String var1, int var2, Object[] var3, long var4, String var6) {
      this.host = var1;
      this.port = var2;
      this.infoVersion = (Byte)var3[0];
      this.version = var3[1].toString();
      this.name = (String)var3[2];
      this.desc = (String)var3[3];
      this.startTime = (Long)var3[4];
      this.playerCount = (Integer)var3[5];
      this.maxPlayers = (Integer)var3[6];
      this.ping = var4;
      this.connType = var6;
   }

   public ServerInfo(String var1, int var2) {
      this.host = var1;
      this.port = var2;
      this.infoVersion = 0;
      this.version = "n/a";
      this.name = "n/a";
      this.desc = "n/a";
      this.startTime = 0L;
      this.playerCount = 0;
      this.maxPlayers = 0;
      this.ping = 99999L;
      this.connType = "n/a";
      this.reachable = false;
   }

   public Byte getInfoVersion() {
      return this.infoVersion;
   }

   public String getVersion() {
      return this.version;
   }

   public String getName() {
      return this.name;
   }

   public String getDesc() {
      return this.desc;
   }

   public Long getStartTime() {
      return this.startTime;
   }

   public Integer getPlayerCount() {
      return this.playerCount;
   }

   public Integer getMaxPlayers() {
      return this.maxPlayers;
   }

   public long getPing() {
      return this.ping;
   }

   public String getHost() {
      return this.host;
   }

   public int getPort() {
      return this.port;
   }

   public String getConnType() {
      return this.connType;
   }

   public boolean isResponsive() {
      return this.reachable;
   }
}
