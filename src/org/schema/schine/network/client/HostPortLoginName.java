package org.schema.schine.network.client;

public class HostPortLoginName {
   public static final byte STARMADE_CLIENT = 0;
   public static final byte STAR_MOTE = 1;
   public String host;
   public int port;
   public String loginName;
   public byte userAgent;

   public HostPortLoginName(String var1, int var2, byte var3, String var4) {
      this.host = var1;
      this.port = var2;
      this.loginName = var4;
      this.userAgent = var3;
   }

   public int hashCode() {
      return this.host.hashCode() + this.port;
   }

   public boolean equals(Object var1) {
      return this.host.equals(((HostPortLoginName)var1).host) && this.port == ((HostPortLoginName)var1).port;
   }

   public String toString() {
      return this.host + ":" + this.port;
   }
}
