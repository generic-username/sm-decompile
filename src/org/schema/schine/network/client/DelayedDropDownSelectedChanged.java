package org.schema.schine.network.client;

import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;

public class DelayedDropDownSelectedChanged {
   DropDownCallback dropDownCallback;
   GUIListElement guiListElement;

   public DelayedDropDownSelectedChanged(DropDownCallback var1, GUIListElement var2) {
      this.dropDownCallback = var1;
      this.guiListElement = var2;
   }

   public void execute() {
      this.dropDownCallback.onSelectionChanged(this.guiListElement);
   }
}
