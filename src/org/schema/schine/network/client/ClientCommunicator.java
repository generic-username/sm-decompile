package org.schema.schine.network.client;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientCommunicator {
   private ClientProcessor clientProcessor;
   private ClientStateInterface state;
   private boolean connected;
   private Socket connection;
   private Thread clientProcessorThread;

   public ClientCommunicator(ClientStateInterface var1, ClientToServerConnection var2) throws IOException {
      this.setState(var1);
      this.setConnection(var2.getConnection());

      while(!this.getConnection().isConnected() || !this.getConnection().isBound() || this.getConnection().isInputShutdown() || this.getConnection().isOutputShutdown()) {
         System.err.println("[CLIENT] WAITING FOR CONNECTION");
      }

      System.out.println("[CLIENT] SOCKET CONNECTED TO SERVER");
      var2.setOutput(new DataOutputStream(new BufferedOutputStream(this.getConnection().getOutputStream(), 8192)));
      this.clientProcessor = new ClientProcessor(var2, var1);
      this.clientProcessorThread = new Thread(this.clientProcessor);
      this.clientProcessorThread.setName("client Processor: " + var1.getId());
      this.clientProcessor.setThread(this.clientProcessorThread);
      this.clientProcessorThread.start();
      this.connected = true;
      var1.setClientConnection(this);
   }

   public ClientProcessor getClientProcessor() {
      return this.clientProcessor;
   }

   public Thread getClientProcessorThread() {
      return this.clientProcessorThread;
   }

   public void setClientProcessorThread(Thread var1) {
      this.clientProcessorThread = var1;
   }

   public Socket getConnection() {
      return this.connection;
   }

   public void setConnection(Socket var1) {
      this.connection = var1;
   }

   public ClientStateInterface getState() {
      return this.state;
   }

   public void setState(ClientStateInterface var1) {
      this.state = var1;
   }

   public boolean isConnected() {
      return this.connected;
   }
}
