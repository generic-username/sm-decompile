package org.schema.schine.network.client;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallbackBlocking;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.input.InputState;

public class GUICallbackController {
   private final List guiCallbacks = new ObjectArrayList();
   private final List callingGUIElements = new ObjectArrayList();
   private final List guiCallbacksBlocking = new ObjectArrayList();
   private final List callingGUIElementsBlocking = new ObjectArrayList();
   private final List insideGUIs = new ObjectArrayList();
   private final List toolTips = new ObjectArrayList();

   public void addCallback(GUICallback var1, GUIElement var2) {
      int var3;
      if ((var3 = this.guiCallbacks.indexOf(var1)) < 0 || var3 != this.callingGUIElements.indexOf(var2)) {
         this.guiCallbacks.add(var1);
         this.callingGUIElements.add(var2);
      }

   }

   boolean mayExecute(GUICallback var1, GUIElement var2, InputState var3) {
      boolean var4 = false;
      if ((var3.getController().getInputController().getCurrentActiveDropdown() == null || var3.getController().getInputController().getCurrentActiveDropdown() == var1) && (this.guiCallbacksBlocking.isEmpty() || (var4 = this.guiCallbacksBlocking.remove(var1)))) {
         if (var4) {
            ((GUICallbackBlocking)var1).onBlockedCallbackExecuted();
         }

         return true;
      } else {
         return false;
      }
   }

   public void execute(MouseEvent var1, InputState var2) {
      assert this.guiCallbacks.size() == this.callingGUIElements.size();

      int var3 = this.guiCallbacks.size();
      boolean var4 = false;
      int var5 = this.guiCallbacksBlocking.size();

      for(int var6 = 0; var6 < var3; ++var6) {
         GUICallback var7 = (GUICallback)this.guiCallbacks.get(var6);
         GUIElement var8 = (GUIElement)this.callingGUIElements.get(var6);
         if (var1.pressedLeftMouse() && var2.getController().getInputController().getCurrentActiveDropdown() != var7) {
            var4 = true;
         }

         if (this.mayExecute(var7, var8, var2)) {
            var7.callback(var8, var1);
         }
      }

      if (var5 > 0 && !this.guiCallbacksBlocking.isEmpty()) {
         Iterator var9 = this.guiCallbacksBlocking.iterator();

         while(var9.hasNext()) {
            ((GUICallbackBlocking)var9.next()).onBlockedCallbackNotExecuted(var5 > this.guiCallbacksBlocking.size());
         }
      }

      if (var2.getController().getInputController().getCurrentContextPane() != null && var2.getController().getInputController().getCurrentContextPane().drawnOnce && !var2.getController().getInputController().getCurrentContextPane().isInside() && var1.state) {
         var2.getController().getInputController().setCurrentContextPane((GUIContextPane)null);
      }

      if (var2.getController().getInputController().getCurrentActiveDropdown() != null && (var3 == 0 && var1.pressedLeftMouse() || var3 > 0 && var2.getController().getInputController().getCurrentActiveDropdown() != null && var4) && !var2.getController().getInputController().getCurrentActiveDropdown().isScrollBarInside()) {
         var2.getController().getInputController().getCurrentActiveDropdown().setExpanded(false);
      }

   }

   public void drawToolTips() {
      GUIElement.enableOrthogonal();
      Iterator var1 = this.toolTips.iterator();

      while(var1.hasNext()) {
         ((GUIToolTip)var1.next()).draw();
      }

      GUIElement.disableOrthogonal();
      this.toolTips.clear();
   }

   public void reset() {
      this.guiCallbacks.clear();
      this.callingGUIElements.clear();
      this.guiCallbacksBlocking.clear();
      this.callingGUIElementsBlocking.clear();
   }

   public List getGuiCallbacks() {
      return this.guiCallbacks;
   }

   public void addBlocking(GUICallbackBlocking var1, GUIElement var2) {
      this.guiCallbacksBlocking.add(var1);
      this.callingGUIElementsBlocking.add(var2);
   }

   public int blockingCount() {
      return this.guiCallbacksBlocking.size();
   }

   public void addToolTip(GUIToolTip var1) {
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         assert var1 != null;

         this.toolTips.add(var1);
      }

   }

   public void updateToolTips(Timer var1) {
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         GUIToolTip.tooltipGraphics.update(var1);
      }

   }

   public List getInsideGUIs() {
      return this.insideGUIs;
   }
}
