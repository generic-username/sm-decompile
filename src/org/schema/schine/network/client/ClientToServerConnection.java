package org.schema.schine.network.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import net.rudp.ReliableSocket;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.Command;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.Pinger;
import org.schema.schine.network.Recipient;
import org.schema.schine.network.Request;

public class ClientToServerConnection extends Pinger implements Recipient {
   private String host;
   private String name;
   private boolean updateStarted = false;
   private Socket connection;
   private int port;
   private ClientCommunicator communicator;
   private ClientStateInterface state;
   private DataOutputStream output;
   private Object waitingForPendingLock = new Object();

   public ClientToServerConnection(ClientStateInterface var1) {
      this.state = var1;
   }

   public void connect(String var1, int var2) throws IOException {
      this.setHost(var1);
      this.setPort(var2);
      if (this.connection == null) {
         System.out.println("[CLIENT] establishing new socket connection to " + var1 + ":" + var2);
         InetSocketAddress var6 = new InetSocketAddress(var1, var2);

         try {
            System.err.println("[CLIENT] Trying TCP...");
            this.connection = new Socket();
            if (EngineSettings.CLIENT_TRAFFIC_CLASS.isOn()) {
               this.connection.setTrafficClass(24);
            }

            this.connection.setSendBufferSize((Integer)EngineSettings.CLIENT_BUFFER_SIZE.getCurrentState());
            this.connection.setTcpNoDelay(true);
            this.connection.connect(var6);
         } catch (Exception var5) {
            if (this.connection != null) {
               try {
                  this.connection.close();
               } catch (Exception var4) {
                  var4.printStackTrace();
               }
            }

            System.err.println("[CLIENT] TCP connection failed: " + var5.getClass().getSimpleName() + "; " + var5.getMessage());
            System.err.println("[CLIENT] Trying UDP...");

            try {
               this.connection = new ReliableSocket();
               if (EngineSettings.CLIENT_TRAFFIC_CLASS.isOn()) {
                  this.connection.setTrafficClass(24);
               }

               this.connection.setSendBufferSize((Integer)EngineSettings.CLIENT_BUFFER_SIZE.getCurrentState());
               this.connection.connect(var6, 2500);
               System.err.println("[CLIENT] UDP CONNECTION SUCCESSFULL");
            } catch (SocketTimeoutException var3) {
               throw new SocketTimeoutException("Failed to connect!");
            }
         }
      }

      this.communicator = new ClientCommunicator(this.state, this);
   }

   public void disconnect() {
      try {
         this.connection.shutdownInput();
         this.connection.shutdownOutput();
         this.connection.close();
         System.out.println("Client Socket connection has been closed");
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public Socket getConnection() {
      return this.connection;
   }

   public String getHost() {
      return this.host;
   }

   public void setHost(String var1) {
      this.host = var1;
   }

   public int getId() {
      return this.state.getId();
   }

   public void sendCommand(int var1, short var2, Class var3, Object... var4) throws IOException {
      Command var5;
      if ((var5 = NetUtil.commands.getByClass(var3)).getMode() == 1) {
         Request var6 = new Request(var2);
         synchronized(this.waitingForPendingLock) {
            synchronized(var6) {
               synchronized(this.communicator.getClientProcessor().getPendingRequests()) {
                  assert !this.communicator.getClientProcessor().getPendingRequests().containsKey(var2) : this.communicator.getClientProcessor().getPendingRequests();

                  this.communicator.getClientProcessor().getPendingRequests().put(var2, var6);
                  var5.writeAndCommitParametriziedCommand(var4, this.getId(), var1, var2, this.state.getProcessor());
               }

               while(this.communicator.getClientProcessor().getPendingRequests().containsKey(var2)) {
                  try {
                     var6.wait(60000L);
                  } catch (InterruptedException var10) {
                     var10.printStackTrace();
                  }

                  if (this.communicator.getClientProcessor().getPendingRequests().containsKey(var2)) {
                     System.err.println("[WARNING] PACKET ID: #" + var2 + " IS STILL PENDING");
                  }
               }

               assert !this.communicator.getClientProcessor().getPendingRequests().containsKey(var2) : "waiting for packet " + var2 + " on command " + var3 + ": " + this.communicator.getClientProcessor().getPendingRequests();

            }
         }
      } else {
         var5.writeAndCommitParametriziedCommand(var4, this.getId(), var1, var2, this.state.getProcessor());
      }
   }

   public Object[] sendReturnedCommand(int var1, short var2, Class var3, Object... var4) throws IOException, InterruptedException {
      Command var5 = NetUtil.commands.getByClass(var3);

      assert var5.getMode() == 1;

      Request var6 = new Request(var2);
      synchronized(this.waitingForPendingLock) {
         Object[] var10000;
         synchronized(var6) {
            synchronized(this.communicator.getClientProcessor().getPendingRequests()) {
               assert !this.communicator.getClientProcessor().getPendingRequests().containsKey(var2) : this.communicator.getClientProcessor().getPendingRequests();

               this.communicator.getClientProcessor().getPendingRequests().put(var2, var6);
               var5.writeAndCommitParametriziedCommand(var4, this.getId(), var1, var2, this.state.getProcessor());
            }

            while(this.communicator.getClientProcessor().getPendingRequests().containsKey(var2)) {
               var6.wait(60000L);
               if (this.communicator.getClientProcessor().getPendingRequests().containsKey(var2)) {
                  System.err.println("[WARNING] PACKET ID: #" + var2 + " IS STILL PENDING");
               }
            }

            assert !this.communicator.getClientProcessor().getPendingRequests().containsKey(var2) : "waiting for packet " + var2 + " on command " + var3 + "; " + this.communicator.getClientProcessor().getPendingRequests();

            Object[] var9 = this.state.getReturn(var2);

            assert var9 != null;

            var10000 = var9;
         }

         return var10000;
      }
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public DataOutputStream getOutput() {
      return this.output;
   }

   public void setOutput(DataOutputStream var1) {
      this.output = var1;
   }

   public int getPort() {
      return this.port;
   }

   public void setPort(int var1) {
      this.port = var1;
   }

   public boolean isAlive() {
      return this.communicator.getClientProcessorThread().isAlive();
   }

   public boolean isUpdateStarted() {
      return this.updateStarted;
   }

   public void setUpdateStarted(boolean var1) {
      this.updateStarted = var1;
   }

   public void sendCommand(int var1, Class var2, Object... var3) throws IOException {
      short var4 = IdGen.getNewPacketId();
      this.sendCommand(var1, var4, var2, var3);
   }

   public int getReceiveQueue() {
      return this.communicator.getClientProcessor().getReceiveQueue();
   }
}
