package org.schema.schine.network.client;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import org.schema.schine.auth.Session;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputController;
import org.schema.schine.network.ControllerInterface;
import org.schema.schine.network.LoginFailedException;

public interface ClientControllerInterface extends InputController, ControllerInterface {
   void alertMessage(String var1);

   void aquireFreeIds() throws IOException, InterruptedException;

   void handleBrokeConnection();

   void kick(String var1);

   void login(String var1, String var2, byte var3, Session var4) throws IOException, InterruptedException, LoginFailedException;

   void logout(String var1);

   void onShutDown() throws IOException;

   void requestSynchronizeAll() throws IOException;

   void synchronize() throws IOException;

   void update(Timer var1);

   void updateStateInput(Timer var1);

   void queueTransformableAudio(String var1, Transform var2, float var3, float var4);

   void queueTransformableAudio(String var1, Transform var2, float var3);

   void queueUIAudio(String var1);
}
