package org.schema.schine.network;

public interface Identifiable {
   int getId();

   void setId(int var1);
}
