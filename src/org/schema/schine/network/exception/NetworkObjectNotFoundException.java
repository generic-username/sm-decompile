package org.schema.schine.network.exception;

import java.util.HashMap;

public class NetworkObjectNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public NetworkObjectNotFoundException(int var1, Class var2, String var3, HashMap var4) {
      super(var2.getSimpleName() + " with id " + var1 + " not found. updateString: " + var3 + ". available: " + var4);
   }
}
