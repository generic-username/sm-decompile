package org.schema.schine.network.exception;

public class DisconnectException extends Exception {
   private static final long serialVersionUID = 1L;

   public DisconnectException(String var1) {
      super(var1);
   }
}
