package org.schema.schine.network.synchronization;

import org.schema.schine.network.objects.Sendable;

public class GhostSendable {
   public final long timeDeleted;
   public final Sendable sendable;

   public GhostSendable(long var1, Sendable var3) {
      this.timeDeleted = var1;
      this.sendable = var3;
   }
}
