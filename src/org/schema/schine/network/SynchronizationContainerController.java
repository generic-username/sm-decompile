package org.schema.schine.network;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.physics.Physical;

public class SynchronizationContainerController {
   private final ObjectArrayList toAdd = new ObjectArrayList();
   private final NetworkStateContainer container;
   private final StateInterface state;
   private final boolean privateChannel;
   long delay = 0L;

   public SynchronizationContainerController(NetworkStateContainer var1, StateInterface var2, boolean var3) {
      this.container = var1;
      this.state = var2;
      this.privateChannel = var3;
   }

   public void addImmediateSynchronizedObject(Sendable var1) {
      assert var1 != null;

      if (var1.getId() < 0) {
         throw new IllegalArgumentException("[NT][CRITICAL] Tried to add " + var1 + " with illegal ID " + var1.getId());
      } else {
         assert this.state.isSynched();

         if (NetworkObject.CHECKUNSAVE && !this.state.isSynched()) {
            throw new UnsaveNetworkOperationException();
         } else if (!var1.isOkToAdd()) {
            System.err.println("Exception: CRITICAL ERROR: could not add object " + var1);
         } else {
            var1.newNetworkObject();

            assert var1.getNetworkObject() != null : var1.getClass();

            var1.getNetworkObject().init();
            var1.getNetworkObject().newObject = true;
            var1.getNetworkObject().addObserversForFields();
            if (var1 instanceof Physical) {
               ((Physical)var1).initPhysics();
            }

            if (this.getState() instanceof ServerState) {
               ((ServerState)this.getState()).doDatabaseInsert(var1);
            }

            this.container.putLocal(var1.getId(), var1);
            var1.updateToFullNetworkObject();
            var1.getNetworkObject().setAllFieldsChanged();
            var1.getNetworkObject().setChanged(true);
            if (var1.getNetworkObject().id.get() < 0) {
               throw new IllegalArgumentException("[NT][CRITICAL] Tried to add NetworkObject for " + var1 + ": " + var1.getNetworkObject() + " with illegal ID " + var1.getNetworkObject().id.get());
            } else {
               assert var1.getNetworkObject().id.get() >= 0;

               assert var1.getNetworkObject().newObject;

               this.container.getRemoteObjects().put(var1.getId(), var1.getNetworkObject());
               this.getState().notifyOfAddedObject(var1);
            }
         }
      }
   }

   public void addNewSynchronizedObjectQueued(Sendable var1) {
      if (var1.getId() < 0) {
         throw new IllegalArgumentException("[NT][CRITICAL] Tried to add " + var1 + " with illegal ID " + var1.getId());
      } else {
         assert !this.toAdd.contains(var1);

         synchronized(this.toAdd) {
            this.toAdd.add(var1);
         }
      }
   }

   public StateInterface getState() {
      return this.state;
   }

   public void handleQueuedSynchronizedObjects() {
      if (!this.toAdd.isEmpty()) {
         long var1 = System.currentTimeMillis();
         synchronized(this.toAdd) {
            int var4 = 0;

            while(true) {
               if (var4 >= this.toAdd.size()) {
                  this.toAdd.clear();
                  break;
               }

               this.addImmediateSynchronizedObject((Sendable)this.toAdd.get(var4));
               ++var4;
            }
         }

         long var3;
         if ((var3 = System.currentTimeMillis() - var1) > 10L) {
            System.err.println("[SERVER][UPDATE] WARNING: handleQueuedSynchronizedObjects update took " + var3 + " on " + this.state);
         }
      }

   }

   public boolean isPrivateChannel() {
      return this.privateChannel;
   }
}
