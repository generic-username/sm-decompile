package org.schema.schine.tools.rss;

import java.util.ArrayList;
import java.util.List;

public class Feed {
   final String title;
   final String link;
   final String description;
   final String language;
   final String copyright;
   final String pubDate;
   final List entries = new ArrayList();

   public Feed(String var1, String var2, String var3, String var4, String var5, String var6) {
      this.title = var1;
      this.link = var2;
      this.description = var3;
      this.language = var4;
      this.copyright = var5;
      this.pubDate = var6;
   }

   public List getMessages() {
      return this.entries;
   }

   public String getTitle() {
      return this.title;
   }

   public String getLink() {
      return this.link;
   }

   public String getDescription() {
      return this.description;
   }

   public String getLanguage() {
      return this.language;
   }

   public String getCopyright() {
      return this.copyright;
   }

   public String getPubDate() {
      return this.pubDate;
   }

   public String toString() {
      return "Feed [copyright=" + this.copyright + ", description=" + this.description + ", language=" + this.language + ", link=" + this.link + ", pubDate=" + this.pubDate + ", title=" + this.title + "]";
   }
}
