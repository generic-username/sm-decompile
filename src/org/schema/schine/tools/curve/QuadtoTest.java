package org.schema.schine.tools.curve;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D.Double;
import java.util.Hashtable;
import java.util.Locale;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class QuadtoTest extends JPanel implements ActionListener {
   private static final long serialVersionUID = 1L;
   Double[] points = new Double[0];
   Double[] ctrls = new Double[0];
   java.awt.geom.Line2D.Double[] tangents = new java.awt.geom.Line2D.Double[0];
   java.awt.geom.Path2D.Double path = new java.awt.geom.Path2D.Double();
   boolean addingPoints = false;
   boolean showLines = true;
   double skew = 0.5D;
   private MouseListener ml = new MouseAdapter() {
      public void mousePressed(MouseEvent var1) {
         if (QuadtoTest.this.addingPoints) {
            QuadtoTest.this.addPoint(var1.getPoint());
            QuadtoTest.this.repaint();
         } else {
            for(int var2 = 0; var2 < QuadtoTest.this.points.length; ++var2) {
               System.out.printf("points[%d] = (%5.1f, %5.1f)%n", var2, QuadtoTest.this.points[var2].x, QuadtoTest.this.points[var2].y);
            }

            System.out.println("--------------");
         }
      }
   };

   public QuadtoTest() {
      this.addMouseListener(this.ml);
   }

   public static void main(String[] var0) {
      QuadtoTest var2 = new QuadtoTest();
      JFrame var1;
      (var1 = new JFrame()).setDefaultCloseOperation(3);
      var1.add(var2);
      var1.add(var2.getControls(), "Last");
      var1.setSize(400, 400);
      var1.setLocation(100, 100);
      var1.setVisible(true);
   }

   public void actionPerformed(ActionEvent var1) {
      String var2;
      if ((var2 = var1.getActionCommand()).equals("ADD POINTS")) {
         this.addingPoints = ((AbstractButton)var1.getSource()).isSelected();
      }

      if (var2.equals("FIT CURVE")) {
         this.fitCurve();
      }

      if (var2.equals("RESET")) {
         this.points = new Double[0];
         this.ctrls = new Double[0];
         this.tangents = new java.awt.geom.Line2D.Double[0];
         this.path.reset();
         this.repaint();
      }

      if (var2.equals("SHOW LINES")) {
         this.showLines = ((AbstractButton)var1.getSource()).isSelected();
         this.repaint();
      }

   }

   protected void paintComponent(Graphics var1) {
      super.paintComponent(var1);
      Graphics2D var3;
      (var3 = (Graphics2D)var1).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      var3.setPaint(Color.blue);
      var3.draw(this.path);
      int var2;
      if (this.showLines) {
         var3.setPaint(Color.pink);

         for(var2 = 0; var2 < this.points.length - 1; ++var2) {
            var3.draw(new java.awt.geom.Line2D.Double(this.points[var2].x, this.points[var2].y, this.points[var2 + 1].x, this.points[var2 + 1].y));
         }

         var3.setPaint(Color.orange);

         for(var2 = 0; var2 < this.tangents.length; ++var2) {
            var3.draw(this.tangents[var2]);
         }
      }

      var3.setPaint(Color.red);

      for(var2 = 0; var2 < this.points.length; ++var2) {
         this.mark(this.points[var2], var3);
      }

      var3.setPaint(Color.green.darker());

      for(var2 = 0; var2 < this.ctrls.length; ++var2) {
         this.mark(this.ctrls[var2], var3);
      }

   }

   private void fitCurve() {
      if (this.points.length >= 3) {
         int var1 = this.points.length - 1;
         this.tangents = new java.awt.geom.Line2D.Double[var1 - 1];
         this.ctrls = new Double[var1];

         int var2;
         for(var2 = 0; var2 < this.tangents.length; ++var2) {
            this.tangents[var2] = new java.awt.geom.Line2D.Double();
         }

         for(var2 = 0; var2 < this.ctrls.length; ++var2) {
            this.ctrls[var2] = new Double();
         }

         this.path.reset();
         this.path.moveTo(this.points[0].x, this.points[0].y);

         for(var2 = 1; var2 < var1; ++var2) {
            double var4 = this.getAngularDifference(var2) * this.skew;
            this.tangents[var2 - 1] = this.getTangentLine(var2, var4);
         }

         for(var2 = 0; var2 < var1; ++var2) {
            Double var3 = this.getCtrlPoint(var2);
            this.ctrls[var2].setLocation(var3.x, var3.y);
            this.path.quadTo(var3.x, var3.y, this.points[var2 + 1].x, this.points[var2 + 1].y);
         }

         this.repaint();
      }
   }

   private double getAngularDifference(int var1) {
      double var2 = this.points[var1 + 1].y - this.points[var1].y;
      double var4 = this.points[var1 + 1].x - this.points[var1].x;
      double var6 = Math.atan2(var2, var4);
      var2 = this.points[var1].y - this.points[var1 - 1].y;
      var4 = this.points[var1].x - this.points[var1 - 1].x;
      double var8 = Math.atan2(var2, var4);
      return var6 - var8;
   }

   private java.awt.geom.Line2D.Double getTangentLine(int var1, double var2) {
      double var4 = this.points[var1].y - this.points[var1 - 1].y;
      double var6 = this.points[var1].x - this.points[var1 - 1].x;
      double var8 = Math.atan2(var4, var6) + var2;
      double var10 = this.points[var1].x + 200.0D * Math.cos(var8 - 3.141592653589793D);
      double var12 = this.points[var1].y + 200.0D * Math.sin(var8 - 3.141592653589793D);
      double var14 = this.points[var1].x + 200.0D * Math.cos(var8);
      double var16 = this.points[var1].y + 200.0D * Math.sin(var8);
      return new java.awt.geom.Line2D.Double(var10, var12, var14, var16);
   }

   private Double getCtrlPoint(int var1) {
      if (var1 != 0 && var1 != this.points.length - 2) {
         return this.getIntersection(this.tangents[var1 - 1], this.tangents[var1]);
      } else {
         int var2 = var1 == 0 ? 0 : var1 - 1;
         this.getTheta(this.tangents[var2]);
         double var3 = this.getTheta(this.points[var1], this.points[var1 + 1]);
         double var5 = this.points[var1].distance(this.points[var1 + 1]) / 2.0D;
         double var7 = this.points[var1].x + var5 * Math.cos(var3);
         double var9 = this.points[var1].y + var5 * Math.sin(var3);
         double var11 = var3 - 1.5707963267948966D;
         double var13 = var7 + 200.0D * Math.cos(var11);
         double var15 = var9 + 200.0D * Math.sin(var11);
         java.awt.geom.Line2D.Double var17 = new java.awt.geom.Line2D.Double(var7, var9, var13, var15);
         return this.getIntersection(this.tangents[var2], var17);
      }
   }

   private double getTheta(java.awt.geom.Line2D.Double var1) {
      Double var2 = new Double(var1.x1, var1.y1);
      Double var3 = new Double(var1.x2, var1.y2);
      return this.getTheta(var2, var3);
   }

   private double getTheta(Double var1, Double var2) {
      double var3 = var2.y - var1.y;
      double var5 = var2.x - var1.x;
      return Math.atan2(var3, var5);
   }

   private Double getIntersection(java.awt.geom.Line2D.Double var1, java.awt.geom.Line2D.Double var2) {
      double var3 = var1.x1;
      double var5 = var1.y1;
      double var7 = var1.x2;
      double var9 = var1.y2;
      double var11 = var2.x1;
      double var13 = var2.y1;
      double var15 = var2.x2;
      double var17 = var2.y2;
      Double var25 = new Double();
      double var19 = this.getDeterminant(var3 - var7, var5 - var9, var11 - var15, var13 - var17);
      double var21 = this.getDeterminant(var3, var5, var7, var9);
      double var23 = this.getDeterminant(var11, var13, var15, var17);
      var25.x = this.getDeterminant(var21, var3 - var7, var23, var11 - var15) / var19;
      var21 = this.getDeterminant(var3, var5, var7, var9);
      var23 = this.getDeterminant(var11, var13, var15, var17);
      var25.y = this.getDeterminant(var21, var5 - var9, var23, var13 - var17) / var19;
      return var25;
   }

   private double getDeterminant(double var1, double var3, double var5, double var7) {
      return var1 * var7 - var3 * var5;
   }

   private void mark(Double var1, Graphics2D var2) {
      var2.fill(new java.awt.geom.Ellipse2D.Double(var1.x - 2.0D, var1.y - 2.0D, 4.0D, 4.0D));
   }

   private void addPoint(Point var1) {
      int var2;
      Double[] var3 = new Double[(var2 = this.points.length) + 1];
      System.arraycopy(this.points, 0, var3, 0, var2);
      var3[var2] = new Double((double)var1.x, (double)var1.y);
      this.points = var3;
   }

   private JPanel getControls() {
      JPanel var1 = new JPanel(new GridBagLayout());
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).insets = new Insets(1, 1, 1, 1);
      var2.weightx = 1.0D;
      String[] var3 = new String[]{"add points", "fit curve", "reset", "show lines"};

      for(int var4 = 0; var4 < var3.length; ++var4) {
         Object var5;
         switch(var4) {
         case 0:
            var5 = new JToggleButton(var3[var4]);
            break;
         case 3:
            var5 = new JCheckBox(var3[var4], this.showLines);
            break;
         default:
            var5 = new JButton(var3[var4]);
         }

         ((AbstractButton)var5).setActionCommand(var3[var4].toUpperCase(Locale.ENGLISH));
         ((AbstractButton)var5).addActionListener(this);
         var1.add((Component)var5, var2);
      }

      var2.gridy = 1;
      var2.gridwidth = var3.length;
      var2.fill = 2;
      var1.add(this.getSlider(), var2);
      return var1;
   }

   private JSlider getSlider() {
      int var1 = (int)(this.skew * 100.0D);
      final JSlider var2;
      (var2 = new JSlider(40, 60, var1)).setMajorTickSpacing(10);
      var2.setMinorTickSpacing(2);
      var2.setPaintTicks(true);
      var2.setLabelTable(this.getLabelTable(40, 60, 2));
      var2.setPaintLabels(true);
      var2.addChangeListener(new ChangeListener() {
         public void stateChanged(ChangeEvent var1) {
            int var2x = var2.getValue();
            QuadtoTest.this.skew = (double)var2x / 100.0D;
            QuadtoTest.this.fitCurve();
         }
      });
      var2.setBorder(BorderFactory.createTitledBorder("skew"));
      return var2;
   }

   private Hashtable getLabelTable(int var1, int var2, int var3) {
      Hashtable var4;
      for(var4 = new Hashtable(); var1 <= var2; var1 += var3) {
         String var5 = String.format("%.2f", (double)var1 / 100.0D);
         var4.put(var1, new JLabel(var5));
      }

      return var4;
   }
}
