package org.schema.schine.tools.curve;

interface Equation {
   double compute(double var1);
}
