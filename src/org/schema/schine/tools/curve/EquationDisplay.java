package org.schema.schine.tools.curve;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.GeneralPath;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JComponent;

class EquationDisplay extends JComponent implements PropertyChangeListener {
   private static final long serialVersionUID = 1L;
   private static final Color COLOR_BACKGROUND;
   private static final Color COLOR_MAJOR_GRID;
   private static final Color COLOR_MINOR_GRID;
   private static final Color COLOR_AXIS;
   private static final float STROKE_AXIS = 1.2F;
   private static final float STROKE_GRID = 1.0F;
   private static final float COEFF_ZOOM = 1.1F;
   protected double minX;
   protected double maxX;
   protected double minY;
   protected double maxY;
   private List equations;
   private double originX;
   private double originY;
   private double majorX;
   private int minorX;
   private double majorY;
   private int minorY;
   private boolean drawText = true;
   private Point dragStart;
   private NumberFormat formatter;
   private EquationDisplay.ZoomHandler zoomHandler;
   private EquationDisplay.PanMotionHandler panMotionHandler;
   private EquationDisplay.PanHandler panHandler;

   public EquationDisplay(double var1, double var3, double var5, double var7, double var9, double var11, double var13, int var15, double var16, int var18) {
      if (var5 >= var7) {
         throw new IllegalArgumentException("minX must be < to maxX");
      } else if (var1 >= var5 && var1 <= var7) {
         if (var9 >= var11) {
            throw new IllegalArgumentException("minY must be < to maxY");
         } else if (var3 >= var9 && var3 <= var11) {
            if (var15 <= 0) {
               throw new IllegalArgumentException("minorX must be > 0");
            } else if (var18 <= 0) {
               throw new IllegalArgumentException("minorY must be > 0");
            } else if (var13 <= 0.0D) {
               throw new IllegalArgumentException("majorX must be > 0.0");
            } else if (var16 <= 0.0D) {
               throw new IllegalArgumentException("majorY must be > 0.0");
            } else {
               this.originX = var1;
               this.originY = var3;
               this.minX = var5;
               this.maxX = var7;
               this.minY = var9;
               this.maxY = var11;
               this.majorX = var13;
               this.minorX = var15;
               this.majorY = var16;
               this.minorY = var18;
               this.equations = new LinkedList();
               this.formatter = NumberFormat.getInstance();
               this.formatter.setMaximumFractionDigits(2);
               this.panHandler = new EquationDisplay.PanHandler();
               this.addMouseListener(this.panHandler);
               this.panMotionHandler = new EquationDisplay.PanMotionHandler();
               this.addMouseMotionListener(this.panMotionHandler);
               this.zoomHandler = new EquationDisplay.ZoomHandler();
               this.addMouseWheelListener(this.zoomHandler);
            }
         } else {
            throw new IllegalArgumentException("originY must be between minY and maxY");
         }
      } else {
         throw new IllegalArgumentException("originX must be between minX and maxX");
      }
   }

   public boolean isDrawText() {
      return this.drawText;
   }

   public void setDrawText(boolean var1) {
      this.drawText = var1;
   }

   public void addEquation(AbstractEquation var1, Color var2) {
      if (var1 != null) {
         var1.addPropertyChangeListener(this);
         this.equations.add(new EquationDisplay.DrawableEquation(var1, var2));
         this.repaint();
      }

   }

   public void removeEquation(AbstractEquation var1) {
      if (var1 != null) {
         EquationDisplay.DrawableEquation var2 = null;
         Iterator var3 = this.equations.iterator();

         while(var3.hasNext()) {
            EquationDisplay.DrawableEquation var4;
            if ((var4 = (EquationDisplay.DrawableEquation)var3.next()).getEquation() == var1) {
               var2 = var4;
               break;
            }
         }

         if (var2 != null) {
            var1.removePropertyChangeListener(this);
            this.equations.remove(var2);
            this.repaint();
         }
      }

   }

   public void propertyChange(PropertyChangeEvent var1) {
      this.repaint();
   }

   protected double yPositionToPixel(double var1) {
      double var3;
      return (var3 = (double)this.getHeight()) - (var1 - this.minY) * var3 / (this.maxY - this.minY);
   }

   protected double xPositionToPixel(double var1) {
      return (var1 - this.minX) * (double)this.getWidth() / (this.maxX - this.minX);
   }

   protected double xPixelToPosition(double var1) {
      double var3 = this.xPositionToPixel(this.originX);
      return (var1 - var3) * (this.maxX - this.minX) / (double)this.getWidth();
   }

   protected double yPixelToPosition(double var1) {
      double var3 = this.yPositionToPixel(this.originY);
      return ((double)this.getHeight() - var1 - var3) * (this.maxY - this.minY) / (double)this.getHeight();
   }

   protected void paintComponent(Graphics var1) {
      if (this.isVisible()) {
         Graphics2D var2 = (Graphics2D)var1;
         this.setupGraphics(var2);
         this.paintBackground(var2);
         this.drawGrid(var2);
         this.drawAxis(var2);
         this.drawEquations(var2);
         this.paintInformation(var2);
      }
   }

   public Dimension getPreferredSize() {
      return new Dimension(400, 100);
   }

   public void setEnabled(boolean var1) {
      if (this.isEnabled() != var1) {
         if (var1) {
            this.addMouseListener(this.panHandler);
            this.addMouseMotionListener(this.panMotionHandler);
            this.addMouseWheelListener(this.zoomHandler);
            return;
         }

         this.removeMouseListener(this.panHandler);
         this.removeMouseMotionListener(this.panMotionHandler);
         this.removeMouseWheelListener(this.zoomHandler);
      }

   }

   protected void paintInformation(Graphics2D var1) {
   }

   private void drawEquations(Graphics2D var1) {
      Iterator var2 = this.equations.iterator();

      while(var2.hasNext()) {
         EquationDisplay.DrawableEquation var3 = (EquationDisplay.DrawableEquation)var2.next();
         var1.setColor(var3.getColor());
         this.drawEquation(var1, var3.getEquation());
      }

   }

   private void drawEquation(Graphics2D var1, AbstractEquation var2) {
      float var4 = (float)this.yPositionToPixel(var2.compute(this.xPixelToPosition(0.0D)));
      GeneralPath var5;
      (var5 = new GeneralPath()).moveTo(0.0F, var4);

      for(float var3 = 0.0F; var3 < (float)this.getWidth(); ++var3) {
         double var6 = this.xPixelToPosition((double)var3);
         var4 = (float)this.yPositionToPixel(var2.compute(var6));
         var5.lineTo(var3, var4);
      }

      var1.draw(var5);
   }

   private void drawGrid(Graphics2D var1) {
      Stroke var2 = var1.getStroke();
      this.drawVerticalGrid(var1);
      this.drawHorizontalGrid(var1);
      if (this.drawText) {
         this.drawVerticalLabels(var1);
         this.drawHorizontalLabels(var1);
      }

      var1.setStroke(var2);
   }

   private void drawHorizontalLabels(Graphics2D var1) {
      double var2 = this.xPositionToPixel(this.originX);
      var1.setColor(COLOR_AXIS);

      double var4;
      int var6;
      for(var4 = this.originY + this.majorY; var4 < this.maxY + this.majorY; var4 += this.majorY) {
         var6 = (int)this.yPositionToPixel(var4);
         var1.drawString(this.formatter.format(var4), (int)var2 + 5, var6);
      }

      for(var4 = this.originY - this.majorY; var4 > this.minY - this.majorY; var4 -= this.majorY) {
         var6 = (int)this.yPositionToPixel(var4);
         var1.drawString(this.formatter.format(var4), (int)var2 + 5, var6);
      }

   }

   private void drawHorizontalGrid(Graphics2D var1) {
      double var2 = this.majorY / (double)this.minorY;
      double var4 = this.xPositionToPixel(this.originX);
      BasicStroke var6 = new BasicStroke(1.0F);
      BasicStroke var7 = new BasicStroke(1.2F);

      double var8;
      int var10;
      int var11;
      for(var8 = this.originY + this.majorY; var8 < this.maxY + this.majorY; var8 += this.majorY) {
         var1.setStroke(var6);
         var1.setColor(COLOR_MINOR_GRID);

         for(var10 = 0; var10 < this.minorY; ++var10) {
            var11 = (int)this.yPositionToPixel(var8 - (double)var10 * var2);
            var1.drawLine(0, var11, this.getWidth(), var11);
         }

         var10 = (int)this.yPositionToPixel(var8);
         var1.setColor(COLOR_MAJOR_GRID);
         var1.drawLine(0, var10, this.getWidth(), var10);
         var1.setStroke(var7);
         var1.setColor(COLOR_AXIS);
         var1.drawLine((int)var4 - 3, var10, (int)var4 + 3, var10);
      }

      for(var8 = this.originY - this.majorY; var8 > this.minY - this.majorY; var8 -= this.majorY) {
         var1.setStroke(var6);
         var1.setColor(COLOR_MINOR_GRID);

         for(var10 = 0; var10 < this.minorY; ++var10) {
            var11 = (int)this.yPositionToPixel(var8 + (double)var10 * var2);
            var1.drawLine(0, var11, this.getWidth(), var11);
         }

         var10 = (int)this.yPositionToPixel(var8);
         var1.setColor(COLOR_MAJOR_GRID);
         var1.drawLine(0, var10, this.getWidth(), var10);
         var1.setStroke(var7);
         var1.setColor(COLOR_AXIS);
         var1.drawLine((int)var4 - 3, var10, (int)var4 + 3, var10);
      }

   }

   private void drawVerticalLabels(Graphics2D var1) {
      double var2 = this.yPositionToPixel(this.originY);
      FontMetrics var4 = var1.getFontMetrics();
      var1.setColor(COLOR_AXIS);

      double var5;
      int var7;
      for(var5 = this.originX + this.majorX; var5 < this.maxX + this.majorX; var5 += this.majorX) {
         var7 = (int)this.xPositionToPixel(var5);
         var1.drawString(this.formatter.format(var5), var7, (int)var2 + var4.getHeight());
      }

      for(var5 = this.originX - this.majorX; var5 > this.minX - this.majorX; var5 -= this.majorX) {
         var7 = (int)this.xPositionToPixel(var5);
         var1.drawString(this.formatter.format(var5), var7, (int)var2 + var4.getHeight());
      }

   }

   private void drawVerticalGrid(Graphics2D var1) {
      double var2 = this.majorX / (double)this.minorX;
      double var4 = this.yPositionToPixel(this.originY);
      BasicStroke var6 = new BasicStroke(1.0F);
      BasicStroke var7 = new BasicStroke(1.2F);

      double var8;
      int var10;
      int var11;
      for(var8 = this.originX + this.majorX; var8 < this.maxX + this.majorX; var8 += this.majorX) {
         var1.setStroke(var6);
         var1.setColor(COLOR_MINOR_GRID);

         for(var10 = 0; var10 < this.minorX; ++var10) {
            var11 = (int)this.xPositionToPixel(var8 - (double)var10 * var2);
            var1.drawLine(var11, 0, var11, this.getHeight());
         }

         var10 = (int)this.xPositionToPixel(var8);
         var1.setColor(COLOR_MAJOR_GRID);
         var1.drawLine(var10, 0, var10, this.getHeight());
         var1.setStroke(var7);
         var1.setColor(COLOR_AXIS);
         var1.drawLine(var10, (int)var4 - 3, var10, (int)var4 + 3);
      }

      for(var8 = this.originX - this.majorX; var8 > this.minX - this.majorX; var8 -= this.majorX) {
         var1.setStroke(var6);
         var1.setColor(COLOR_MINOR_GRID);

         for(var10 = 0; var10 < this.minorX; ++var10) {
            var11 = (int)this.xPositionToPixel(var8 + (double)var10 * var2);
            var1.drawLine(var11, 0, var11, this.getHeight());
         }

         var10 = (int)this.xPositionToPixel(var8);
         var1.setColor(COLOR_MAJOR_GRID);
         var1.drawLine(var10, 0, var10, this.getHeight());
         var1.setStroke(var7);
         var1.setColor(COLOR_AXIS);
         var1.drawLine(var10, (int)var4 - 3, var10, (int)var4 + 3);
      }

   }

   private void drawAxis(Graphics2D var1) {
      double var2 = this.yPositionToPixel(this.originY);
      double var4 = this.xPositionToPixel(this.originX);
      var1.setColor(COLOR_AXIS);
      Stroke var6 = var1.getStroke();
      var1.setStroke(new BasicStroke(1.2F));
      var1.drawLine(0, (int)var2, this.getWidth(), (int)var2);
      var1.drawLine((int)var4, 0, (int)var4, this.getHeight());
      FontMetrics var7 = var1.getFontMetrics();
      var1.drawString(this.formatter.format(0.0D), (int)var4 + 5, (int)var2 + var7.getHeight());
      var1.setStroke(var6);
   }

   protected void setupGraphics(Graphics2D var1) {
      var1.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
   }

   protected void paintBackground(Graphics2D var1) {
      var1.setColor(COLOR_BACKGROUND);
      var1.fill(var1.getClipBounds());
   }

   static {
      COLOR_BACKGROUND = Color.WHITE;
      COLOR_MAJOR_GRID = Color.GRAY.brighter();
      COLOR_MINOR_GRID = new Color(220, 220, 220);
      COLOR_AXIS = Color.BLACK;
   }

   class PanMotionHandler extends MouseMotionAdapter {
      private PanMotionHandler() {
      }

      public void mouseDragged(MouseEvent var1) {
         Point var4 = var1.getPoint();
         double var2 = EquationDisplay.this.xPixelToPosition(var4.getX()) - EquationDisplay.this.xPixelToPosition(EquationDisplay.this.dragStart.getX());
         EquationDisplay var10000 = EquationDisplay.this;
         var10000.minX -= var2;
         var10000 = EquationDisplay.this;
         var10000.maxX -= var2;
         var2 = EquationDisplay.this.yPixelToPosition(var4.getY()) - EquationDisplay.this.yPixelToPosition(EquationDisplay.this.dragStart.getY());
         var10000 = EquationDisplay.this;
         var10000.minY -= var2;
         var10000 = EquationDisplay.this;
         var10000.maxY -= var2;
         EquationDisplay.this.repaint();
         EquationDisplay.this.dragStart = var4;
      }

      // $FF: synthetic method
      PanMotionHandler(Object var2) {
         this();
      }
   }

   class PanHandler extends MouseAdapter {
      private PanHandler() {
      }

      public void mousePressed(MouseEvent var1) {
         EquationDisplay.this.dragStart = var1.getPoint();
      }

      // $FF: synthetic method
      PanHandler(Object var2) {
         this();
      }
   }

   class ZoomHandler implements MouseWheelListener {
      private ZoomHandler() {
      }

      public void mouseWheelMoved(MouseWheelEvent var1) {
         double var2 = EquationDisplay.this.maxX - EquationDisplay.this.minX;
         double var4 = EquationDisplay.this.maxY - EquationDisplay.this.minY;
         double var6 = EquationDisplay.this.minX + var2 / 2.0D;
         double var8 = EquationDisplay.this.minY + var4 / 2.0D;
         if (var1.getWheelRotation() < 0) {
            var2 /= 1.100000023841858D;
            var4 /= 1.100000023841858D;
         } else {
            var2 *= 1.100000023841858D;
            var4 *= 1.100000023841858D;
         }

         EquationDisplay.this.minX = var6 - var2 / 2.0D;
         EquationDisplay.this.maxX = var6 + var2 / 2.0D;
         EquationDisplay.this.minY = var8 - var4 / 2.0D;
         EquationDisplay.this.maxY = var8 + var4 / 2.0D;
         EquationDisplay.this.repaint();
      }

      // $FF: synthetic method
      ZoomHandler(Object var2) {
         this();
      }
   }

   class DrawableEquation {
      private AbstractEquation equation;
      private Color color;

      DrawableEquation(AbstractEquation var2, Color var3) {
         this.equation = var2;
         this.color = var3;
      }

      AbstractEquation getEquation() {
         return this.equation;
      }

      Color getColor() {
         return this.color;
      }
   }
}
