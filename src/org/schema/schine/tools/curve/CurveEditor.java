package org.schema.schine.tools.curve;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;

public class CurveEditor extends JPanel {
   private static final long serialVersionUID = 1L;

   public CurveEditor(JFrame var1, PSCurveVariable var2) {
      this.setLayout(new GridBagLayout());
      SplineDisplay var3 = new SplineDisplay(new PSCurveVariable[]{var2});
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var3, var4);
   }

   public static void main(String[] var0) {
      SwingUtilities.invokeLater(new Runnable() {
         public final void run() {
            try {
               UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException var2) {
            } catch (InstantiationException var3) {
            } catch (IllegalAccessException var4) {
            } catch (UnsupportedLookAndFeelException var5) {
            }

            JFrame var1;
            (var1 = new JFrame("CurveEditor")).setContentPane(new CurveEditor(var1, new PSCurveVariable() {
               public String getName() {
                  return "curve";
               }

               public Color getColor() {
                  return Color.BLUE;
               }
            }));
            var1.pack();
            var1.setLocationRelativeTo((Component)null);
            var1.setResizable(true);
            var1.setDefaultCloseOperation(3);
            var1.setVisible(true);
         }
      });
   }
}
