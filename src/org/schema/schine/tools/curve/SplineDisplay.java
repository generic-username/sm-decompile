package org.schema.schine.tools.curve;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;

public class SplineDisplay extends EquationDisplay {
   private static final long serialVersionUID = 1L;
   public ArrayList splines = new ArrayList();
   public MouseEvent lastEvent;
   Point2D selected = null;

   public SplineDisplay(PSCurveVariable[] var1) {
      super(0.0D, 0.0D, 0.0D, 1.0D, -1.0D, 1.0D, 0.5D, 2, 0.5D, 2);

      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.splines.add(new SplineCurve(this, var1[var2]));
      }

   }

   protected void paintInformation(Graphics2D var1) {
      super.paintInformation(var1);

      for(int var2 = 0; var2 < this.splines.size(); ++var2) {
         ((SplineCurve)this.splines.get(var2)).paint(var1);
      }

   }
}
