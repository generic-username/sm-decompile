package org.schema.schine.tools.gradient;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;

public class ColorAdd extends JPanel {
   private static final long serialVersionUID = 1L;
   private JSlider slider;
   private float lastColorValue;

   public ColorAdd(final LinearGradientChooser var1, final PSGradientVariable var2, float var3) {
      this.setLayout(new GridBagLayout());
      final JButton var4;
      (var4 = new JButton("Pick")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            final JColorChooser var3 = new JColorChooser(Color.RED);
            JDialog var2;
            (var2 = new JDialog()).setContentPane(var3);
            var2.setAlwaysOnTop(true);
            var2.setLocationRelativeTo((Component)null);
            var2.setVisible(true);
            var2.pack();
            var3.getSelectionModel().addChangeListener(new ChangeListener() {
               public void stateChanged(ChangeEvent var1x) {
                  Color var2 = var3.getColor();
                  var4.setBackground(var2);
                  var4.setContentAreaFilled(false);
                  var4.setOpaque(true);
                  var1.update(ColorAdd.this.lastColorValue, ColorAdd.this.lastColorValue, var4.getBackground());
               }
            });
         }
      });
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      this.add(var4, var5);
      this.slider = new JSlider();
      this.slider.setValue((int)(var3 * 100.0F));
      this.slider.addChangeListener(new ChangeListener() {
         public void stateChanged(ChangeEvent var1x) {
            var1.update(ColorAdd.this.lastColorValue, ColorAdd.this.getColorValue(), var4.getBackground());
            ColorAdd.this.lastColorValue = ColorAdd.this.getColorValue();
         }
      });
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).fill = 1;
      var7.weightx = 1.0D;
      var7.insets = new Insets(0, 0, 0, 5);
      var7.gridx = 1;
      var7.gridy = 0;
      this.add(this.slider, var7);
      this.lastColorValue = this.getColorValue();
      JButton var8;
      (var8 = new JButton("del")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.removeColor(ColorAdd.this, var2);
         }
      });
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).anchor = 13;
      var6.gridx = 2;
      var6.gridy = 0;
      this.add(var8, var6);
   }

   public float getColorValue() {
      return (float)this.slider.getValue() / 100.0F;
   }
}
