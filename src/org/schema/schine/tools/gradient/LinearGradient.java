package org.schema.schine.tools.gradient;

import it.unimi.dsi.fastutil.floats.Float2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.floats.Float2ObjectMap.Entry;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D.Float;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;

public class LinearGradient extends JPanel {
   private static final long serialVersionUID = 1L;
   private PSGradientVariable var;

   public LinearGradient(PSGradientVariable var1) {
      this.var = var1;
   }

   public void paint(Graphics var1) {
      Graphics2D var9 = (Graphics2D)var1;
      int var2 = this.getWidth();
      int var3 = this.getHeight();

      ObjectBidirectionalIterator var4;
      Entry var6;
      for(Entry var5 = (Entry)(var4 = this.var.color.float2ObjectEntrySet().iterator()).next(); var4.hasNext(); var5 = var6) {
         var6 = (Entry)var4.next();
         Float var7 = new Float(var5.getFloatKey() * (float)var2, 0.0F, var6.getFloatKey() * (float)var2, (float)var3);
         if (((Color)var6.getValue()).getAlpha() < 255) {
            var9.setPaint(Color.WHITE);
            var9.fill(var7);
         }

         GradientPaint var8 = new GradientPaint(var5.getFloatKey() * (float)var2, 0.0F, (Color)var5.getValue(), var6.getFloatKey() * (float)var2, 0.0F, (Color)var6.getValue());
         var9.setPaint(var8);
         var9.fill(var7);
      }

   }

   public Dimension getPreferredSize() {
      return new Dimension(350, 30);
   }

   public void update(float var1, float var2, Color var3) {
      if (var1 == 0.0F) {
         var1 += 0.1F;
      }

      if (var1 == 1.0F) {
         var1 -= 0.1F;
      }

      if (var2 == 0.0F) {
         var2 += 0.1F;
      }

      if (var2 == 1.0F) {
         var2 -= 0.1F;
      }

      Float2ObjectRBTreeMap var4;
      (var4 = new Float2ObjectRBTreeMap(this.var.color)).remove(var1);
      var4.put(var2, var3);
      this.var.color = var4;
      this.repaint();
   }
}
