package org.schema.schine.tools.gradient;

import java.util.ArrayList;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class ColorListModel implements ListModel {
   public ArrayList colors = new ArrayList();

   public int getSize() {
      return this.colors.size();
   }

   public ColorAdd getElementAt(int var1) {
      return (ColorAdd)this.colors.get(var1);
   }

   public void addListDataListener(ListDataListener var1) {
   }

   public void removeListDataListener(ListDataListener var1) {
   }
}
