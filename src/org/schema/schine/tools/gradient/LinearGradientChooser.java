package org.schema.schine.tools.gradient;

import it.unimi.dsi.fastutil.floats.Float2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.floats.Float2ObjectMap.Entry;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;

public class LinearGradientChooser extends JPanel {
   private static final long serialVersionUID = 1L;
   private LinearGradient linearGradient;
   private JPanel panel;

   public LinearGradientChooser(final PSGradientVariable var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).rowWeights = new double[]{1.0D, 0.0D, 1.0D};
      var2.columnWeights = new double[]{0.0D, 1.0D, 0.0D};
      this.setLayout(var2);
      final JButton var4;
      (var4 = new JButton("Pick")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            final JColorChooser var3 = new JColorChooser(Color.RED);
            JDialog var2;
            (var2 = new JDialog()).setContentPane(var3);
            var2.setLocationRelativeTo((Component)null);
            var2.setAlwaysOnTop(true);
            var2.setVisible(true);
            var2.pack();
            var3.getSelectionModel().addChangeListener(new ChangeListener() {
               public void stateChanged(ChangeEvent var1x) {
                  Color var2 = var3.getColor();
                  var4.setBackground(var2);
                  var4.setContentAreaFilled(false);
                  var4.setOpaque(true);
                  var1.color.put(0.0F, var2);
                  LinearGradientChooser.this.linearGradient.repaint();
               }
            });
         }
      });
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).fill = 1;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var4, var3);
      this.linearGradient = new LinearGradient(var1);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var5.fill = 1;
      var5.gridx = 1;
      var5.gridy = 0;
      this.add(this.linearGradient, var5);
      (var2 = new GridBagLayout()).columnWidths = new int[]{0};
      var2.rowHeights = new int[]{0};
      var2.columnWeights = new double[]{Double.MIN_VALUE};
      var2.rowWeights = new double[]{Double.MIN_VALUE};
      this.linearGradient.setLayout(var2);
      (var4 = new JButton("Pick")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 2;
      var3.gridy = 0;
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            final JColorChooser var3 = new JColorChooser(Color.RED);
            JDialog var2;
            (var2 = new JDialog()).setContentPane(var3);
            var2.setAlwaysOnTop(true);
            var2.setLocationRelativeTo((Component)null);
            var2.setVisible(true);
            var2.pack();
            var3.getSelectionModel().addChangeListener(new ChangeListener() {
               public void stateChanged(ChangeEvent var1x) {
                  Color var2 = var3.getColor();
                  var4.setBackground(var2);
                  var4.setContentAreaFilled(false);
                  var4.setOpaque(true);
                  var1.color.put(1.0F, var2);
                  LinearGradientChooser.this.linearGradient.repaint();
               }
            });
         }
      });
      this.add(var4, var3);
      var4 = new JButton("Add");
      (var3 = new GridBagConstraints()).fill = 3;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 1;
      var3.gridy = 1;
      this.add(var4, var3);
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            LinearGradientChooser.this.panel.add(new ColorAdd(LinearGradientChooser.this, var1, 0.5F));
            LinearGradientChooser.this.panel.revalidate();
         }
      });
      this.panel = new JPanel();
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.gridwidth = 3;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 2;
      this.add(this.panel, var5);
      this.panel.setLayout(new BoxLayout(this.panel, 1));
      Iterator var7 = var1.color.float2ObjectEntrySet().iterator();

      while(var7.hasNext()) {
         Entry var6;
         if ((var6 = (Entry)var7.next()).getFloatKey() > 0.0F && var6.getFloatKey() < 1.0F) {
            ColorAdd var8 = new ColorAdd(this, var1, var6.getFloatKey());
            this.panel.add(var8);
         }
      }

   }

   public void removeColor(ColorAdd var1, PSGradientVariable var2) {
      if (var1.getColorValue() > 0.0F && var1.getColorValue() < 1.0F) {
         Float2ObjectRBTreeMap var3;
         (var3 = new Float2ObjectRBTreeMap(var2.color)).remove(var1.getColorValue());
         var2.color = var3;
      }

      this.panel.remove(var1);
      this.panel.revalidate();
      this.panel.repaint();
   }

   public void update(float var1, float var2, Color var3) {
      this.linearGradient.update(var1, var2, var3);
   }
}
