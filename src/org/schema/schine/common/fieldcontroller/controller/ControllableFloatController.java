package org.schema.schine.common.fieldcontroller.controller;

import org.schema.schine.common.fieldcontroller.controllable.Controllable;
import org.schema.schine.common.fieldcontroller.controllable.ControllableField;
import org.schema.schine.common.fieldcontroller.controllable.ControllableFieldFloat;

public class ControllableFloatController extends ControllableFieldController {
   private float increment;
   private Float oldVal;

   public ControllableFloatController(Controllable var1, ClientClassController var2, int var3, float var4) {
      super(var1, var2, 0, var3);
      this.increment = var4;
   }

   public void execute(boolean var1) {
      ControllableField[] var2;
      ControllableFieldFloat var10000 = (ControllableFieldFloat)(var2 = this.getControllable().getControls())[this.index];
      var10000.setValue(var10000.getValue() + (var1 ? this.increment : -this.increment));
      this.getControllable().setControls(var2);
   }

   public String getName() {
      return this.getControllable().getControls()[this.index].getName();
   }

   public Float getValue() {
      Float var1;
      if (!(var1 = ((ControllableFieldFloat)this.getControllable().getControls()[this.index]).getValue()).equals(this.oldVal)) {
         this.setChanged(true);
         this.oldVal = var1;
      }

      return var1;
   }

   public void set(Float var1) {
      ControllableField[] var2;
      ((ControllableFieldFloat)(var2 = this.getControllable().getControls())[this.index]).setValue(var1);
      this.getControllable().setControls(var2);
      this.setChanged(true);
   }

   public void up() {
   }
}
