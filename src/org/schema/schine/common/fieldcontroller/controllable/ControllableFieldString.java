package org.schema.schine.common.fieldcontroller.controllable;

public class ControllableFieldString implements ControllableField {
   String name;
   private String[] values;
   private int index;

   public ControllableFieldString(String var1, String[] var2) {
      this.name = var1;
      this.setValues(var2);
   }

   public int getIndex() {
      return this.index;
   }

   public void setIndex(int var1) {
      this.index = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public String getValue() {
      return this.getValues()[this.index];
   }

   public void setValue(String var1) {
      this.getValues()[this.index] = var1;
   }

   public String[] getValues() {
      return this.values;
   }

   public void setValues(String[] var1) {
      this.values = var1;
   }
}
