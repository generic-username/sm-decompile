package org.schema.schine.common.fieldcontroller.controllable;

public interface ControllableField {
   String getName();

   void setName(String var1);

   Object getValue();

   void setValue(Object var1);
}
