package org.schema.schine.common.fieldcontroller.controllable;

import org.schema.schine.common.fieldcontroller.controller.ClientClassController;
import org.schema.schine.physics.PhysicsState;

public interface Controllable {
   void buildClassController();

   void executeCommand(ControllableCommand var1);

   ClientClassController getClientClassController();

   ControllableField[] getControls();

   void setControls(ControllableField[] var1);

   int getEntityID();

   String getName();

   PhysicsState getState();
}
