package org.schema.schine.common.fieldcontroller.controllable;

public class ControllableFieldFloat implements ControllableField {
   private float value;
   private String name;

   public ControllableFieldFloat() {
   }

   public ControllableFieldFloat(String var1, float var2) {
      this.value = var2;
      this.name = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public Float getValue() {
      return this.value;
   }

   public void setValue(Float var1) {
      this.value = var1;
   }
}
