package org.schema.schine.common.xlsx;

public class Cell {
   public Integer id = null;
   public Integer rowIndex = null;
   public Integer colIndex = null;
   public String cellName = null;
   public String text = null;
   public String formula = null;
   public String comment = null;
   public Style style = null;
   public Object value = null;
   public Cell tmpCell = null;
}
