package org.schema.schine.common;

public interface InputChecker {
   boolean check(String var1, TextCallback var2);
}
