package org.schema.schine.common;

import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;

public interface TextCallback {
   String[] getCommandPrefixes();

   String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException;

   void onFailedTextCheck(String var1);

   void onTextEnter(String var1, boolean var2, boolean var3);

   void newLine();
}
