package org.schema.schine.common.language.editor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatsAndToolsPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private LanguageEditor l;
   private JLabel lblTotal;
   private JLabel lblTranslated;
   private JLabel lblMissing;

   public StatsAndToolsPanel(JFrame var1, LanguageEditor var2) {
      var2.addObserver(this);
      this.setLayout(new GridBagLayout());
      this.l = var2;
      this.lblTotal = new JLabel("Total:");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.anchor = 17;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(this.lblTotal, var3);
      this.lblTranslated = new JLabel("Translated:");
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 1;
      var3.gridy = 0;
      this.add(this.lblTranslated, var3);
      this.lblMissing = new JLabel("Missing:");
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.anchor = 13;
      var3.gridx = 2;
      var3.gridy = 0;
      this.add(this.lblMissing, var3);
   }

   public void update(Observable var1, Object var2) {
      if (var2 == null || var2 == this.getClass()) {
         this.lblTotal.setText("Total: " + this.l.total);
         this.lblTranslated.setText("Translated: " + this.l.translated);
         this.lblMissing.setText("Missing: " + this.l.missing);
      }

   }
}
