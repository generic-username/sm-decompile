package org.schema.schine.common.language.editor;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.schine.common.language.LanguageReader;
import org.schema.schine.common.language.Translation;
import org.schema.schine.resource.FileExt;
import org.xml.sax.SAXException;

public class LanguageEditor extends Observable {
   private static int close;
   public boolean changesPending;
   public List list;
   public int total;
   public int missing;
   public int translated;
   public int selectionIndex;
   private String languageName;
   public JList translationList;
   protected boolean autofillDupe;
   private JMenuBar menuBar;
   private JMenu mnFile;
   private JMenu mnEdit;
   private JMenuItem mntmNew;
   private JMenuItem mntmSave;
   private JMenuItem mntmSaveAs;
   private JCheckBoxMenuItem mntmAutoSave;
   private JMenuItem mntmLoad;
   private boolean init;
   private JMenuItem mntmExit;
   private File lastFile;
   private SearchDialog searchDiag;
   private boolean autosave = true;
   private JFileChooser fc;
   File defaultLanguage;
   private JFrame f;
   Integer autosaveCount;
   private ObjectArrayFIFOQueue cc;
   private JMenuItem mntmSearch;
   private JMenuItem mntmImportFromCrowdin;

   public LanguageEditor() {
      this.defaultLanguage = new FileExt("." + File.separator + "data" + File.separator + "language" + File.separator + "defaultPack.xml");
      this.autosaveCount = 0;
      this.cc = new ObjectArrayFIFOQueue();
   }

   public void init() throws IOException, SAXException, ParserConfigurationException {
      this.f = new JFrame("StarMade Language Editor");
      KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new LanguageEditor.DB());
      this.buildMenu(this.f);
      this.f.requestFocus();
      this.f.setSize(1000, 600);
      Dimension var1;
      int var2 = (int)(((var1 = Toolkit.getDefaultToolkit().getScreenSize()).getWidth() - (double)this.f.getWidth()) / 2.0D);
      int var3 = (int)((var1.getHeight() - (double)this.f.getHeight()) / 2.0D);
      this.f.setLocation(var2, var3);
      LanguageEditorMainPanel var4 = new LanguageEditorMainPanel(this.f, this);
      this.f.setContentPane(var4);
      this.f.setVisible(true);
      this.f.setDefaultCloseOperation(close);
   }

   public List get() {
      synchronized(this.cc) {
         if (!this.cc.isEmpty()) {
            return (List)this.cc.dequeue();
         } else {
            ObjectArrayList var2 = new ObjectArrayList(this.list.size());

            for(int var3 = 0; var3 < this.list.size(); ++var3) {
               var2.add(new Translation());
            }

            return var2;
         }
      }
   }

   public void put(List var1) {
      synchronized(this.cc) {
         this.cc.enqueue(var1);
      }
   }

   public void autosave(final JFrame var1) {
      final List var2 = this.get();

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         ((Translation)var2.get(var3)).set((Translation)this.list.get(var3));
      }

      (new Thread(new Runnable() {
         public void run() {
            FileExt var1x = new FileExt("." + File.separator + "language_autosave_" + LanguageEditor.this.autosaveCount + ".xml");
            System.err.println("AUTOSAVE: " + var1x);
            LanguageEditor.this.save(var1, var1x, var2);
            synchronized(LanguageEditor.this.autosaveCount) {
               LanguageEditor.this.autosaveCount = (LanguageEditor.this.autosaveCount + 1) % 10;
            }

            LanguageEditor.this.put(var2);
         }
      })).start();
   }

   private void save(JFrame var1, File var2, List var3) {
      try {
         LanguageReader.save(var2, this.languageName, (String)null, var3);
         this.changesPending = false;
      } catch (Exception var4) {
         var4.printStackTrace();
         JOptionPane.showMessageDialog(var1, "Error loading file. Check logs", "error", 0);
      }
   }

   private void load(Frame var1, File var2) {
      try {
         this.calculateInital(var2, this.defaultLanguage);
         EventQueue.invokeLater(new Runnable() {
            public void run() {
               try {
                  LanguageEditor.this.setChanged();
                  LanguageEditor.this.notifyObservers();
                  LanguageEditor.this.translationList.setSelectedIndex(0);
               } catch (Exception var1) {
                  var1.printStackTrace();
               }
            }
         });
      } catch (Exception var3) {
         var3.printStackTrace();
         JOptionPane.showMessageDialog(var1, "Error loading file. Check logs", "error", 0);
      }

      this.changesPending = false;
   }

   private void buildMenu(final JFrame var1) {
      this.menuBar = new JMenuBar();
      var1.setJMenuBar(this.menuBar);
      var1.addKeyListener(new KeyListener() {
         public void keyTyped(KeyEvent var1) {
         }

         public void keyReleased(KeyEvent var1) {
         }

         public void keyPressed(KeyEvent var1) {
            if (var1.isControlDown() && var1.getKeyCode() == 70 && var1.isShiftDown()) {
               LanguageEditor.this.openSearch();
            }

         }
      });
      this.mnFile = new JMenu("File");
      this.mnEdit = new JMenu("Edit");
      this.menuBar.add(this.mnFile);
      this.menuBar.add(this.mnEdit);
      this.mntmSearch = new JMenuItem("Search");
      this.mntmSearch.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LanguageEditor.this.openSearch();
         }
      });
      this.mnEdit.add(this.mntmSearch);
      this.mntmNew = new JMenuItem("New Translation");
      this.mntmNew.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Iterator var5;
            if (!LanguageEditor.this.init) {
               LanguageEditor.this.languageName = (String)JOptionPane.showInputDialog(var1, "Enter Language Name", "New Language", -1, (Icon)null, (Object[])null, "");

               try {
                  LanguageEditor.this.calculateInital(LanguageEditor.this.defaultLanguage, LanguageEditor.this.defaultLanguage);
               } catch (Exception var3) {
                  JOptionPane.showMessageDialog(var1, "Error loading default file. Check logs", "error", 0);
                  var3.printStackTrace();
               }

               var1.setTitle("StarMade Language Editor (" + LanguageEditor.this.languageName + ")");
               LanguageEditor.this.missing = 0;
               LanguageEditor.this.translated = 0;
               LanguageEditor.this.total = 0;

               for(var5 = LanguageEditor.this.list.iterator(); var5.hasNext(); ++LanguageEditor.this.total) {
                  if (((Translation)var5.next()).translator.equals("default")) {
                     ++LanguageEditor.this.missing;
                  } else {
                     ++LanguageEditor.this.translated;
                  }
               }

               EventQueue.invokeLater(new Runnable() {
                  public void run() {
                     try {
                        LanguageEditor.this.setChanged();
                        LanguageEditor.this.notifyObservers();
                        LanguageEditor.this.translationList.setSelectedIndex(0);
                     } catch (Exception var1x) {
                        var1x.printStackTrace();
                     }
                  }
               });
               LanguageEditor.this.init = true;
            } else {
               Object[] var4 = new Object[]{"Ok", "Cancel"};
               switch(JOptionPane.showOptionDialog(var1, "Do you want to start a new translation?\nAll unsaved progress will be lost!", "Warning", 2, 2, (Icon)null, var4, var4[1])) {
               case 0:
                  LanguageEditor.this.languageName = (String)JOptionPane.showInputDialog(var1, "Enter Language Name", "New Language", -1, (Icon)null, (Object[])null, "");
                  var1.setTitle("StarMade Language Editor (" + LanguageEditor.this.languageName + ")");
                  LanguageEditor.this.lastFile = null;

                  Translation var2;
                  for(var5 = LanguageEditor.this.list.iterator(); var5.hasNext(); var2.translator = "default") {
                     Translation var10000 = var2 = (Translation)var5.next();
                     var10000.translation = var10000.original;
                  }

                  LanguageEditor.this.missing = 0;
                  LanguageEditor.this.translated = 0;
                  LanguageEditor.this.total = 0;

                  for(var5 = LanguageEditor.this.list.iterator(); var5.hasNext(); ++LanguageEditor.this.total) {
                     if (((Translation)var5.next()).translator.equals("default")) {
                        ++LanguageEditor.this.missing;
                     } else {
                        ++LanguageEditor.this.translated;
                     }
                  }

                  EventQueue.invokeLater(new Runnable() {
                     public void run() {
                        try {
                           LanguageEditor.this.setChanged();
                           LanguageEditor.this.notifyObservers();
                        } catch (Exception var1x) {
                           var1x.printStackTrace();
                        }
                     }
                  });
               default:
               }
            }
         }
      });
      this.mntmSave = new JMenuItem("Save");
      this.mntmSave.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            File var2;
            if (LanguageEditor.this.lastFile == null && (var2 = LanguageEditor.this.chooseFile(var1, "Save As...")) != null) {
               LanguageEditor.this.lastFile = var2;
            }

            if (LanguageEditor.this.lastFile != null) {
               LanguageEditor.this.save(var1, LanguageEditor.this.lastFile, LanguageEditor.this.list);
            }

         }
      });
      this.mntmSaveAs = new JMenuItem("Save as...");
      this.mntmSaveAs.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            File var2;
            if ((var2 = LanguageEditor.this.chooseFile(var1, "Save As...")) != null) {
               LanguageEditor.this.lastFile = var2;
            }

            if (LanguageEditor.this.lastFile != null) {
               LanguageEditor.this.save(var1, LanguageEditor.this.lastFile, LanguageEditor.this.list);
            }

         }
      });
      this.mntmAutoSave = new JCheckBoxMenuItem("Auto-Save");
      this.mntmAutoSave.setSelected(this.autosave);
      this.mntmAutoSave.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            LanguageEditor.this.autosave = LanguageEditor.this.mntmAutoSave.isSelected();
         }
      });
      this.mntmLoad = new JMenuItem("Load");
      this.mntmLoad.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            File var2;
            if ((var2 = LanguageEditor.this.chooseFile(var1, "Load...")) != null) {
               LanguageEditor.this.lastFile = var2;
            }

            if (LanguageEditor.this.lastFile != null) {
               LanguageEditor.this.load(var1, LanguageEditor.this.lastFile);
            }

         }
      });
      this.mntmExit = new JMenuItem("Exit");
      this.mntmExit.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (!LanguageEditor.this.changesPending) {
               if (LanguageEditor.close == 2) {
                  var1.dispose();
               } else {
                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var2) {
                     var2.printStackTrace();
                     System.exit(0);
                  }
               }
            } else {
               Object[] var4 = new Object[]{"Ok", "Cancel"};
               switch(JOptionPane.showOptionDialog(var1, "You have unsaved changes. Do you really want to exit?", "Warning", 2, 2, (Icon)null, var4, var4[1])) {
               case 0:
                  if (LanguageEditor.close == 2) {
                     var1.dispose();
                     return;
                  } else {
                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var3) {
                        var3.printStackTrace();
                        System.exit(0);
                     }
                  }
               default:
               }
            }
         }
      });
      this.mnFile.add(this.mntmNew);
      this.mnFile.add(this.mntmSave);
      this.mnFile.add(this.mntmSaveAs);
      this.mnFile.add(this.mntmAutoSave);
      this.mnFile.add(this.mntmLoad);
      this.mntmImportFromCrowdin = new JMenuItem("Import from Crowdin XML");
      this.mntmImportFromCrowdin.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (!LanguageEditor.this.init) {
               LanguageEditor.this.languageName = (String)JOptionPane.showInputDialog(var1, "Enter Language Name", "Import Language", -1, (Icon)null, (Object[])null, "");

               try {
                  LanguageEditor.this.calculateInital(LanguageEditor.this.defaultLanguage, LanguageEditor.this.defaultLanguage);
                  LanguageEditor.this.handleImport(var1);
               } catch (Exception var3) {
                  JOptionPane.showMessageDialog(var1, "Error loading default file. Check logs", "error", 0);
                  var3.printStackTrace();
               }

               var1.setTitle("StarMade Language Editor (" + LanguageEditor.this.languageName + ")");
               EventQueue.invokeLater(new Runnable() {
                  public void run() {
                     try {
                        LanguageEditor.this.setChanged();
                        LanguageEditor.this.notifyObservers();
                        LanguageEditor.this.translationList.setSelectedIndex(0);
                     } catch (Exception var1x) {
                        var1x.printStackTrace();
                     }
                  }
               });
               LanguageEditor.this.init = true;
            } else {
               Object[] var4 = new Object[]{"Ok", "Cancel"};
               switch(JOptionPane.showOptionDialog(var1, "Do you want to start a new translation?\nAll unsaved progress will be lost!", "Warning", 2, 2, (Icon)null, var4, var4[1])) {
               case 0:
                  LanguageEditor.this.languageName = (String)JOptionPane.showInputDialog(var1, "Enter Language Name", "Import", -1, (Icon)null, (Object[])null, "");
                  var1.setTitle("StarMade Language Editor (" + LanguageEditor.this.languageName + ")");
                  LanguageEditor.this.lastFile = null;

                  Translation var2;
                  for(Iterator var5 = LanguageEditor.this.list.iterator(); var5.hasNext(); var2.translator = "default") {
                     Translation var10000 = var2 = (Translation)var5.next();
                     var10000.translation = var10000.original;
                  }

                  LanguageEditor.this.handleImport(var1);
                  EventQueue.invokeLater(new Runnable() {
                     public void run() {
                        try {
                           LanguageEditor.this.setChanged();
                           LanguageEditor.this.notifyObservers();
                        } catch (Exception var1x) {
                           var1x.printStackTrace();
                        }
                     }
                  });
               default:
               }
            }
         }
      });
      this.mnFile.add(this.mntmImportFromCrowdin);
      this.mnFile.add(this.mntmExit);
   }

   private void handleImport(JFrame var1) {
      File var5 = this.chooseImportFile(var1, "choose import file");

      try {
         LanguageReader.importFileXML(var5, this.list);
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (SAXException var3) {
         var3.printStackTrace();
      } catch (ParserConfigurationException var4) {
         var4.printStackTrace();
      }

      this.missing = 0;
      this.translated = 0;
      this.total = 0;

      for(Iterator var6 = this.list.iterator(); var6.hasNext(); ++this.total) {
         if (((Translation)var6.next()).translator.equals("default")) {
            ++this.missing;
         } else {
            ++this.translated;
         }
      }

      EventQueue.invokeLater(new Runnable() {
         public void run() {
            try {
               ((TranslationListModel)LanguageEditor.this.translationList.getModel()).allChanged();
               LanguageEditor.this.setChanged();
               LanguageEditor.this.notifyObservers();
            } catch (Exception var1) {
               var1.printStackTrace();
            }
         }
      });
   }

   protected void openSearch() {
      if (this.searchDiag == null) {
         this.searchDiag = new SearchDialog();
         this.searchDiag.editor = this;
      }

      this.searchDiag.setVisible(true);
   }

   private File chooseFile(JFrame var1, String var2) {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         FileFilter var3 = new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".xml");
               }
            }

            public String getDescription() {
               return "StarMade Language (.xml)";
            }
         };
         this.fc.addChoosableFileFilter(var3);
         this.fc.setFileFilter(var3);
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      return this.fc.showDialog(var1, var2) == 0 ? this.fc.getSelectedFile() : null;
   }

   private File chooseImportFile(JFrame var1, String var2) {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         FileFilter var3 = new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".zip");
               }
            }

            public String getDescription() {
               return "Crowdin(Android) archive (.zip)";
            }
         };
         FileFilter var4 = new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".xml");
               }
            }

            public String getDescription() {
               return "Crowdin(Android) (.xml)";
            }
         };
         this.fc.addChoosableFileFilter(var3);
         this.fc.addChoosableFileFilter(var4);
         this.fc.setFileFilter(var3);
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      return this.fc.showDialog(var1, var2) == 0 ? this.fc.getSelectedFile() : null;
   }

   private void calculateInital(File var1, File var2) throws IOException, SAXException, ParserConfigurationException {
      Object2ObjectAVLTreeMap var3 = new Object2ObjectAVLTreeMap();
      LanguageReader.loadLangFile(var1, var2, var3);
      this.list = new ObjectArrayList(var3.size());
      Iterator var5 = var3.values().iterator();

      Translation var6;
      while(var5.hasNext()) {
         var6 = (Translation)var5.next();
         this.list.add(var6);
      }

      var5 = this.list.iterator();

      while(var5.hasNext()) {
         (var6 = (Translation)var5.next()).dupl = new it.unimi.dsi.fastutil.objects.ObjectArrayList();
         Iterator var7 = this.list.iterator();

         while(var7.hasNext()) {
            Translation var4 = (Translation)var7.next();
            if (var6 != var4 && var6.original.equals(var4.original)) {
               var6.dupl.add(var4);
            }
         }
      }

      this.missing = 0;
      this.translated = 0;
      this.total = 0;

      for(var5 = this.list.iterator(); var5.hasNext(); ++this.total) {
         if (((Translation)var5.next()).translator.equals("default")) {
            ++this.missing;
         } else {
            ++this.translated;
         }
      }

   }

   public static void main(String[] var0) {
      LanguageEditor var1 = new LanguageEditor();
      if (var0.length > 0 && var0[0].length() > 0) {
         close = 2;
      } else {
         close = 3;
      }

      try {
         var1.init();
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (SAXException var3) {
         var3.printStackTrace();
      } catch (ParserConfigurationException var4) {
         var4.printStackTrace();
      }
   }

   public void translationListSelectionChanged(final int var1) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            try {
               LanguageEditor.this.selectionIndex = var1;
               LanguageEditor.this.setChanged();
               LanguageEditor.this.notifyObservers();
            } catch (Exception var1x) {
               var1x.printStackTrace();
            }
         }
      });
   }

   public void fillDupl(Translation var1) {
      Iterator var2 = var1.dupl.iterator();

      while(var2.hasNext()) {
         final Translation var3;
         if (!(var3 = (Translation)var2.next()).translation.equals(var1.translation)) {
            this.changesPending = true;
            ++this.translated;
            --this.missing;
            var3.translator = "modified";
            var3.translation = new String(var1.translation);
            EventQueue.invokeLater(new Runnable() {
               public void run() {
                  if (LanguageEditor.this.selectionIndex >= 0 && LanguageEditor.this.selectionIndex < LanguageEditor.this.list.size()) {
                     ((TranslationListModel)LanguageEditor.this.translationList.getModel()).changed(LanguageEditor.this.list.indexOf(var3));
                     LanguageEditor.this.setChanged();
                     LanguageEditor.this.notifyObservers();
                  }

               }
            });
         }
      }

   }

   public void onChangeSelection(boolean var1) {
      if (this.selectionIndex >= 0 && this.selectionIndex < this.list.size()) {
         Translation var2 = (Translation)this.list.get(this.selectionIndex);
         if (var1) {
            this.fillDupl(var2);
         }

         if (!var2.original.equals(var2.translation)) {
            System.err.println("TT\n\n\"" + var2.original + "\"\n\n\"" + var2.translation + "\n");
            if (var2.translator.equals("default")) {
               ++this.translated;
               --this.missing;
            }

            this.changesPending = true;
            var2.translator = "modified";
         } else {
            this.changesPending = true;
         }

         if (var2.changed) {
            if (this.autosave) {
               this.autosave(this.f);
            }

            var2.changed = false;
         }
      }

      EventQueue.invokeLater(new Runnable() {
         public void run() {
            if (LanguageEditor.this.selectionIndex >= 0 && LanguageEditor.this.selectionIndex < LanguageEditor.this.list.size()) {
               ((TranslationListModel)LanguageEditor.this.translationList.getModel()).changed(LanguageEditor.this.selectionIndex);
               LanguageEditor.this.setChanged();
               LanguageEditor.this.notifyObservers();
            }

         }
      });
   }

   public void search(String var1, boolean var2, boolean var3) {
      if (this.list != null) {
         for(int var6 = 0; var6 < this.list.size(); ++var6) {
            int var4 = (var6 + this.selectionIndex + 1) % this.list.size();
            Translation var5 = (Translation)this.list.get(var4);
            if (var2 && var5.translation.contains(var1)) {
               this.selectionIndex = var4;
               this.onChangeSelection(this.autofillDupe);
               this.translationList.setSelectedIndex(this.selectionIndex);
               this.translationList.ensureIndexIsVisible(this.translationList.getSelectedIndex());
               return;
            }

            if (!var2 && var5.original.contains(var1)) {
               this.selectionIndex = var4;
               this.onChangeSelection(this.autofillDupe);
               this.translationList.setSelectedIndex(this.selectionIndex);
               this.translationList.ensureIndexIsVisible(this.translationList.getSelectedIndex());
               return;
            }
         }

      }
   }

   class DB implements KeyEventDispatcher {
      private DB() {
      }

      public boolean dispatchKeyEvent(KeyEvent var1) {
         if (var1.isControlDown() && var1.getID() == 401 && var1.getKeyCode() == 70 && var1.isControlDown()) {
            LanguageEditor.this.openSearch();
         }

         return false;
      }

      // $FF: synthetic method
      DB(Object var2) {
         this();
      }
   }
}
