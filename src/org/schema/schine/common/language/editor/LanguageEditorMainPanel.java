package org.schema.schine.common.language.editor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class LanguageEditorMainPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;

   public LanguageEditorMainPanel(JFrame var1, LanguageEditor var2) {
      var2.addObserver(this);
      GridBagLayout var3;
      (var3 = new GridBagLayout()).rowWeights = new double[]{0.0D, 1.0D};
      var3.columnWeights = new double[]{0.0D, 1.0D};
      this.setLayout(var3);
      JPanel var9 = new JPanel();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var9, var4);
      GridBagLayout var10;
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.setLayout(var10);
      StatsAndToolsPanel var11 = new StatsAndToolsPanel(var1, var2);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var9.add(var11, var5);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0};
      var3.rowHeights = new int[]{0};
      var3.columnWeights = new double[]{Double.MIN_VALUE};
      var3.rowWeights = new double[]{Double.MIN_VALUE};
      var9 = new JPanel();
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.add(var9, var4);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.setLayout(var10);
      JSplitPane var12;
      (var12 = new JSplitPane()).setOrientation(0);
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      var9.add(var12, var5);
      var9 = new JPanel();
      var12.setLeftComponent(var9);
      GridBagLayout var13;
      (var13 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var13.rowHeights = new int[]{0, 0};
      var13.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var13.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.setLayout(var13);
      TranslationListPanel var14 = new TranslationListPanel(var1, var2);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.weightx = 1.0D;
      var6.fill = 1;
      var6.gridx = 0;
      var6.gridy = 0;
      var9.add(var14, var6);
      var9 = new JPanel();
      var12.setRightComponent(var9);
      (var10 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var10.rowHeights = new int[]{0, 0};
      var10.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.setLayout(var10);
      TranslationDetailPanel var7 = new TranslationDetailPanel(var1, var2);
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).fill = 1;
      var8.gridx = 0;
      var8.gridy = 0;
      var9.add(var7, var8);
   }

   public void update(Observable var1, Object var2) {
   }
}
