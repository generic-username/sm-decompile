package org.schema.schine.common.language.editor;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import org.schema.schine.common.language.Translation;

public class TranslationCellRenderer extends DefaultListCellRenderer {
   private static final long serialVersionUID = 1L;
   Color lRed0 = new Color(255, 120, 120);
   Color lRed1 = new Color(255, 140, 140);
   Color lGreen0 = new Color(120, 255, 120);
   Color lGreen1 = new Color(140, 255, 140);
   Color lBlue0 = new Color(235, 140, 200);
   Color lBlue1 = new Color(140, 235, 200);
   DefaultListCellRenderer d = new DefaultListCellRenderer();
   DecimalFormat f = new DecimalFormat("0000");

   public Component getListCellRendererComponent(JList var1, Object var2, int var3, boolean var4, boolean var5) {
      JLabel var6 = (JLabel)this.d.getListCellRendererComponent(var1, this.f.format((long)var3) + "  \n" + var2, var3, var4, var5);
      if (var4) {
         if (var2 != null && var2 instanceof Translation) {
            if (((Translation)var2).translator.equals("default")) {
               var6.setBackground(this.lBlue0);
            } else {
               var6.setBackground(this.lBlue1);
            }

            return var6;
         } else {
            return super.getListCellRendererComponent(var1, this.f.format((long)var3) + "  \n" + var2, var3, var4, var5);
         }
      } else {
         if (var2 != null && var2 instanceof Translation) {
            if (((Translation)var2).translator.equals("default")) {
               var6.setBackground(var3 % 2 == 0 ? this.lRed0 : this.lRed1);
            } else {
               var6.setBackground(var3 % 2 == 0 ? this.lGreen0 : this.lGreen1);
            }
         }

         return var6;
      }
   }
}
