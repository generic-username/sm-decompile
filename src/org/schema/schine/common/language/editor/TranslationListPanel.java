package org.schema.schine.common.language.editor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class TranslationListPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private JList ls;

   public TranslationListPanel(JFrame var1, final LanguageEditor var2) {
      GridBagLayout var3;
      (var3 = new GridBagLayout()).rowWeights = new double[]{1.0D};
      var3.columnWeights = new double[]{1.0D};
      this.setLayout(var3);
      this.ls = new JList(new TranslationListModel(var2));
      var2.addObserver(this);
      var2.translationList = this.ls;
      this.ls.addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
            var2.onChangeSelection(var2.autofillDupe);
            var2.translationListSelectionChanged(TranslationListPanel.this.ls.getSelectedIndex());
         }
      });
      this.ls.setSelectionMode(0);
      this.ls.setCellRenderer(new TranslationCellRenderer());
      JScrollPane var4 = new JScrollPane(this.ls);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.weighty = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      this.add(var4, var5);
   }

   public void update(Observable var1, Object var2) {
   }
}
