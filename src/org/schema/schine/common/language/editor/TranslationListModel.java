package org.schema.schine.common.language.editor;

import java.util.Observable;
import java.util.Observer;
import javax.swing.AbstractListModel;
import org.schema.schine.common.language.Translation;

public class TranslationListModel extends AbstractListModel implements Observer {
   private static final long serialVersionUID = 1L;
   public LanguageEditor l;

   public TranslationListModel(LanguageEditor var1) {
      this.l = var1;
      var1.addObserver(this);
   }

   public int getSize() {
      return this.l.list == null ? 0 : this.l.list.size();
   }

   public Translation getElementAt(int var1) {
      return (Translation)this.l.list.get(var1);
   }

   public void update(Observable var1, Object var2) {
      this.fireContentsChanged(this.l, 0, this.l.list.size() - 1);
   }

   public void changed(int var1) {
      this.fireContentsChanged(this, var1, var1);
   }

   public void allChanged() {
      this.fireContentsChanged(this, 0, this.l.list.size() - 1);
   }
}
