package org.schema.schine.common.language;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.schema.common.XMLTools;
import org.schema.schine.common.util.XLSX2CSV;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.forms.font.FontPath;
import org.schema.schine.network.StateInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LanguageReader {
   public static void copyFile(File var0, File var1) throws IOException {
      BufferedInputStream var4 = new BufferedInputStream(new FileInputStream(var0));
      BufferedOutputStream var5 = new BufferedOutputStream(new FileOutputStream(var1));
      byte[] var2 = new byte[1024];

      int var3;
      while((var3 = var4.read(var2)) > 0) {
         var5.write(var2, 0, var3);
      }

      var5.flush();
      var4.close();
      var5.close();
   }

   public static void loadLangFile(File var0, Map var1) throws IOException, SAXException, ParserConfigurationException {
      loadLangFile(var0, var0, var1);
   }

   public static void save(File var0, String var1, String var2, List var3) throws ParserConfigurationException, TransformerException {
      Document var4;
      Element var5;
      (var5 = (var4 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()).createElement("Language")).setAttribute("Language", var1);
      if (var2 != null) {
         var5.setAttribute("fontnameorpath", var2);
      }

      var5.setAttribute("Version", "0");
      var4.appendChild(var5);
      Iterator var13 = var3.iterator();

      while(var13.hasNext()) {
         Translation var14 = (Translation)var13.next();
         Element var17;
         (var17 = var4.createElement("Element")).setAttribute("var", var14.var);
         var17.setAttribute("translator", var14.translator);
         Element var6 = var4.createElement("Original");
         Element var7 = var4.createElement("Translation");
         Element var8 = var4.createElement("Arguments");
         var6.setTextContent(var14.original);
         var7.setTextContent(var14.translation);
         String[] var15;
         int var9 = (var15 = var14.args).length;

         for(int var10 = 0; var10 < var9; ++var10) {
            String var11 = var15[var10];
            Element var12;
            (var12 = var4.createElement("Argument")).setTextContent(var11);
            var8.appendChild(var12);
         }

         var17.appendChild(var7);
         var17.appendChild(var6);
         var17.appendChild(var8);
         var5.appendChild(var17);
      }

      var0.getParentFile().mkdirs();
      Transformer var16;
      (var16 = TransformerFactory.newInstance().newTransformer()).setOutputProperty("omit-xml-declaration", "yes");
      var16.setOutputProperty("indent", "yes");
      new StringWriter();
      StreamResult var18 = new StreamResult(var0);
      DOMSource var19 = new DOMSource(var4);
      var16.transform(var19, var18);
   }

   public static void importFileTutorial(File var0, String var1) throws IOException, SAXException, ParserConfigurationException {
      Object var8;
      if (var0.getName().endsWith(".zip")) {
         var8 = new ZipInputStream(new BufferedInputStream(new FileInputStream(var0)), StandardCharsets.UTF_8);
         boolean var3 = false;

         ZipEntry var2;
         while((var2 = ((ZipInputStream)var8).getNextEntry()) != null) {
            System.err.println("ZIP ENTRY: " + var2.getName());
            if (var2.getName().toLowerCase(Locale.ENGLISH).equals("Tutorial Subtitles.xlsx".toLowerCase(Locale.ENGLISH))) {
               System.err.println("USING: " + var2.getName());
               var3 = true;
               break;
            }
         }

         if (!var3) {
            ((InputStream)var8).close();
            throw new IOException("Tutorial File not found in Zip");
         }
      } else {
         var8 = new BufferedInputStream(new FileInputStream(var0));
      }

      try {
         OPCPackage var11 = OPCPackage.open((InputStream)var8);
         ByteArrayOutputStream var9 = new ByteArrayOutputStream();
         PrintStream var10 = new PrintStream(var9);
         (new XLSX2CSV(var11, var10, -1)).process();
         var11.close();
         processTutorialFile(new String(var9.toByteArray(), "UTF-8"), var1);
      } catch (Exception var6) {
         throw new IOException("Tutorial Parsing Failed (XLSX)", var6);
      } finally {
         ;
      }
   }

   private static void processTutorialFile(String var0, String var1) throws IOException {
      Object2ObjectOpenHashMap var2 = new Object2ObjectOpenHashMap();
      BufferedReader var12 = new BufferedReader(new StringReader(var0));
      LanguageReader.Index var3 = new LanguageReader.Index("CONTEXT");
      LanguageReader.Index var4 = new LanguageReader.Index("S. TIMESTAMP");
      LanguageReader.Index var5 = new LanguageReader.Index("E. TIMESTAMP");
      LanguageReader.Index var6 = new LanguageReader.Index("TRANSLATED");
      LanguageReader.Index var7 = new LanguageReader.Index("ORIGINAL");

      while(true) {
         while(true) {
            String var8;
            do {
               if ((var8 = var12.readLine()) == null) {
                  var12.close();
                  writeSubtitles(var2, var1);
                  return;
               }
            } while(var8.trim().length() == 0);

            if (var8.startsWith("\"ID\"")) {
               var3.find(var8);
               var4.find(var8);
               var5.find(var8);
               var6.find(var8);
               var7.find(var8);
            } else if (var8.contains(",")) {
               LanguageReader.SubtitleElem var9 = new LanguageReader.SubtitleElem();
               StringBuffer var10;
               if (var8.startsWith("\"key_")) {
                  (var10 = new StringBuffer()).append(var8);

                  while(!var8.trim().endsWith("\"")) {
                     var8 = var12.readLine();
                     var10.append("\n" + var8);
                  }

                  var8 = var10.toString();
                  var9.keybind = true;
                  var9.context = var3.get(var8).split("\n")[0].trim();
                  var9.translated = var6.get(var8);
                  var9.original = var7.get(var8);
               } else {
                  (var10 = new StringBuffer()).append(var8);

                  while(!var8.trim().endsWith("\"")) {
                     var8 = var12.readLine();
                     var10.append("\n" + var8);
                  }

                  var8 = var10.toString();
                  var9.context = var3.get(var8);
                  var9.start = var4.get(var8);
                  var9.end = var5.get(var8);
                  var9.translated = var6.get(var8);
                  var9.original = var7.get(var8);
               }

               Object var11;
               if ((var11 = (List)var2.get(var9.context.toLowerCase(Locale.ENGLISH))) == null) {
                  var11 = new ObjectArrayList();
                  var2.put(var9.context.toLowerCase(Locale.ENGLISH), var11);
               }

               ((List)var11).add(var9);
            }
         }
      }
   }

   private static void writeSubtitles(Map var0, String var1) throws IOException {
      Iterator var12 = var0.values().iterator();

      while(var12.hasNext()) {
         List var2;
         String var3 = ((LanguageReader.SubtitleElem)(var2 = (List)var12.next()).get(0)).context;
         System.err.println("PROCESSING: " + var3);
         File var4 = new File("./data/video/tutorial/");
         String var5 = null;
         String var6 = null;
         File[] var7;
         int var8 = (var7 = var4.listFiles()).length;

         for(int var9 = 0; var9 < var8; ++var9) {
            File var10;
            if ((var10 = var7[var9]).getName().toLowerCase(Locale.ENGLISH).contains(var3.toLowerCase(Locale.ENGLISH))) {
               if (var10.getName().toLowerCase(Locale.ENGLISH).endsWith(".sbv")) {
                  var5 = var10.getName();
               }

               if (var10.getName().toLowerCase(Locale.ENGLISH).endsWith(".xml")) {
                  var6 = var10.getName();
               }
            }
         }

         if (var5 == null || var6 == null) {
            throw new IOException("FILES NOT FOUND " + var3 + ": " + Arrays.toString(var4.listFiles()));
         }

         File var18 = new File(var1 + var5);
         File var19 = new File(var1 + var6);
         var18.delete();
         var19.delete();
         BufferedWriter var20 = new BufferedWriter(new FileWriter(var18));
         Iterator var21 = var2.iterator();

         LanguageReader.SubtitleElem var14;
         while(var21.hasNext()) {
            if (!(var14 = (LanguageReader.SubtitleElem)var21.next()).keybind) {
               var20.write(var14.start + "," + var14.end + "\n");
               var20.write(var14.translated + "\n");
               var20.write("\n");
            }
         }

         var20.close();

         try {
            var21 = var2.iterator();

            while(var21.hasNext()) {
               if ((var14 = (LanguageReader.SubtitleElem)var21.next()).keybind) {
                  Document var13;
                  Element var15 = (var13 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()).createElement("Meta");
                  Element var16 = var13.createElement("Keybinds");
                  Element var17 = var13.createElement("Description");
                  var16.setTextContent(var14.translated);
                  var17.setTextContent(var14.context);
                  var15.appendChild(var16);
                  var15.appendChild(var17);
                  var13.appendChild(var15);
                  XMLTools.writeDocument(var19, var13);
                  break;
               }
            }
         } catch (Exception var11) {
            throw new IOException(var11);
         }
      }

   }

   public static void importFileXML(File var0, List var1) throws IOException, SAXException, ParserConfigurationException {
      Object var5;
      if (var0.getName().endsWith(".zip")) {
         var5 = new ZipInputStream(new BufferedInputStream(new FileInputStream(var0)));

         ZipEntry var2;
         while((var2 = ((ZipInputStream)var5).getNextEntry()) != null) {
            System.err.println("ZIP ENTRY: " + var2.getName());
            if (var2.getName().toLowerCase(Locale.ENGLISH).equals("pack-crowdin.xml".toLowerCase(Locale.ENGLISH))) {
               break;
            }
         }
      } else {
         var5 = new BufferedInputStream(new FileInputStream(var0));
      }

      Object2ObjectOpenHashMap var10 = new Object2ObjectOpenHashMap(var1.size());
      Iterator var6 = var1.iterator();

      Translation var3;
      while(var6.hasNext()) {
         var3 = (Translation)var6.next();
         var10.put(var3.var, var3);
      }

      DocumentBuilder var10000 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      var3 = null;
      Document var8 = var10000.parse((InputStream)var5);
      ((InputStream)var5).close();
      NodeList var7 = var8.getDocumentElement().getChildNodes();

      for(int var9 = 0; var9 < var7.getLength(); ++var9) {
         Node var11;
         if ((var11 = var7.item(var9)).getNodeType() == 1 && var11.getNodeName().equals("string")) {
            String var4 = var11.getAttributes().getNamedItem("name").getNodeValue();
            String var12 = var11.getTextContent();
            Translation var13;
            if ((var13 = (Translation)var10.get(var4)) != null) {
               var13.translation = var12;
               var13.translator = "CrowdinCommunity";
               var13.changed = true;
            }
         }
      }

   }

   public static void loadLangFile(File var0, File var1, Map var2) throws IOException, SAXException, ParserConfigurationException {
      DocumentBuilder var3;
      Element var4;
      String var5 = (var4 = (var3 = DocumentBuilderFactory.newInstance().newDocumentBuilder()).parse(var1).getDocumentElement()).getAttributes().getNamedItem("Language").getNodeValue();
      String var6 = var4.getAttributes().getNamedItem("Version").getNodeValue();
      System.err.println("[LANGUAGE] Loading " + var5 + "; Version: " + var6);

      Node var7;
      try {
         Node var10 = var4.getAttributes().getNamedItem("fontnameorpath");
         Node var12 = var4.getAttributes().getNamedItem("yOffsetStartSize");
         var7 = var4.getAttributes().getNamedItem("yOffsetDividedBy");
         Node var8 = var4.getAttributes().getNamedItem("yOffsetFixed");
         if (var10 != null) {
            FontPath.font_Path = var10.getNodeValue();
            System.err.println("[LANGUAGE] LOADED FONT PATH/NAME: " + FontPath.font_Path);
         }

         if (var12 != null) {
            FontPath.offsetStartSize = Integer.parseInt(var12.getNodeValue());
         }

         if (var7 != null) {
            FontPath.offsetDividedBy = Integer.parseInt(var7.getNodeValue());
         }

         if (var8 != null) {
            FontPath.offsetFixed = Integer.parseInt(var8.getNodeValue());
         }
      } catch (Exception var9) {
         var9.printStackTrace();
         GLFrame.processErrorDialogExceptionWithoutReport(var9, (StateInterface)null);
      }

      NodeList var11 = var4.getChildNodes();

      int var13;
      for(var13 = 0; var13 < var11.getLength(); ++var13) {
         if ((var7 = var11.item(var13)).getNodeType() == 1 && var7.getNodeName().equals("Element")) {
            parseElement(var7, var2, false);
         }
      }

      if (var0 != var1) {
         var5 = (var4 = var3.parse(var0).getDocumentElement()).getAttributes().getNamedItem("Language").getNodeValue();
         var6 = var4.getAttributes().getNamedItem("Version").getNodeValue();
         System.err.println("[LANGUAGE] Loading " + var5 + "; Version: " + var6);
         var11 = var4.getChildNodes();

         for(var13 = 0; var13 < var11.getLength(); ++var13) {
            if ((var7 = var11.item(var13)).getNodeType() == 1 && var7.getNodeName().equals("Element")) {
               parseElement(var7, var2, true);
            }
         }
      }

   }

   private static void parseElement(Node var0, Map var1, boolean var2) {
      String var3 = var0.getAttributes().getNamedItem("var").getNodeValue();
      String var4 = var0.getAttributes().getNamedItem("translator").getNodeValue();
      NodeList var12 = var0.getChildNodes();
      ArrayList var5 = null;
      String var6 = null;
      String var7 = null;

      for(int var8 = 0; var8 < var12.getLength(); ++var8) {
         Node var9;
         if ((var9 = var12.item(var8)).getNodeType() == 1) {
            if (var9.getNodeName().equals("Original")) {
               var7 = var9.getTextContent();
            } else if (var9.getNodeName().equals("Translation")) {
               var6 = var9.getTextContent();
            } else if (var9.getNodeName().equals("Arguments")) {
               NodeList var14 = var9.getChildNodes();

               for(int var10 = 0; var10 < var14.getLength(); ++var10) {
                  Node var11;
                  if ((var11 = var14.item(var10)).getNodeType() == 1) {
                     if (var5 == null) {
                        var5 = new ArrayList();
                     }

                     var5.add(var11.getTextContent());
                  }
               }
            }
         }
      }

      String[] var13 = new String[var5 != null ? var5.size() : 0];

      for(int var15 = 0; var15 < var13.length; ++var15) {
         var13[var15] = (String)var5.get(var15);
      }

      if (var6.length() == 0) {
         var6 = var7;
      }

      Translation var16;
      if (var2) {
         if ((var16 = (Translation)var1.get(var3)) != null) {
            if (var16.original.equals(var7)) {
               var16.translation = var6;
               var16.translator = var4;
               return;
            }

            var16.oldTranslation = var6;
         }

      } else {
         var16 = new Translation(var3, var7, var6, (String)null, var4, var13);
         var1.put(var3, var16);
      }
   }

   static class Index {
      String name;
      int index = -1;

      public Index(String var1) {
         this.name = var1;
      }

      public String get(String var1) {
         String[] var2 = var1.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
         if (this.index >= 0 && this.index <= var2.length - 1) {
            if ((var1 = var2[this.index].trim()).startsWith("\"")) {
               var1 = var1.substring(1, var1.length()).trim();
            }

            if (var1.endsWith("\"")) {
               var1 = var1.substring(0, var1.length() - 1).trim();
            }

            return var1;
         } else {
            throw new IndexOutOfBoundsException("INVALID INDEX : " + this.index + "::: " + var1 + "; " + Arrays.toString(var2) + "; len " + var2.length);
         }
      }

      public void find(String var1) {
         String[] var3 = var1.split(",");

         for(int var2 = 0; var2 < var3.length; ++var2) {
            if (var3[var2].toLowerCase(Locale.ENGLISH).equals("\"" + this.name.toLowerCase(Locale.ENGLISH) + "\"")) {
               this.index = var2;
               return;
            }
         }

      }
   }

   static class SubtitleElem {
      public String context;
      public String start;
      public String end;
      public String translated;
      public String original;
      public boolean keybind;

      private SubtitleElem() {
      }

      public String toString() {
         return "SubtitleElem [context=" + this.context + ", start=" + this.start + ", end=" + this.end + ", translated=" + this.translated + ", original=" + this.original + ", keybind=" + this.keybind + "]";
      }

      // $FF: synthetic method
      SubtitleElem(Object var1) {
         this();
      }
   }
}
