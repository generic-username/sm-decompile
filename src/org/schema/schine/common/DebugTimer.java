package org.schema.schine.common;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.Map;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.StateInterface;

public class DebugTimer {
   private DebugTimer.Update update;

   public static boolean isOn() {
      return Keyboard.isCreated() && Keyboard.isKeyDown(42) && Keyboard.isKeyDown(201);
   }

   public void start(StateInterface var1) {
      if (isOn()) {
         this.update = new DebugTimer.Update();
         this.update.updateTime = var1.getUpdateTime();
      }

   }

   public void end() {
      if (this.update != null) {
         StringBuffer var1 = new StringBuffer();
         this.update.printTo(var1);
         System.err.println(var1);
         this.update = null;
      }

   }

   public void start(String var1) {
      if (this.update != null) {
         this.update.start(var1);
      }

   }

   public void end(String var1) {
      if (this.update != null) {
         this.update.end(var1);
      }

   }

   public void start(Object var1) {
      if (this.update != null) {
         this.update.start(var1);
      }

   }

   public void end(Object var1) {
      if (this.update != null) {
         this.update.end(var1);
      }

   }

   public void start(Object var1, String var2) {
      if (this.update != null) {
         this.update.start(var1, var2);
      }

   }

   public void end(Object var1, String var2) {
      if (this.update != null) {
         this.update.end(var1, var2);
      }

   }

   public void start(Object var1, String var2, String var3) {
      if (this.update != null) {
         this.update.start(var1, var2, var3);
      }

   }

   public void end(Object var1, String var2, String var3) {
      if (this.update != null) {
         this.update.end(var1, var2, var3);
      }

   }

   public void start(Object var1, String var2, String var3, String var4) {
      if (this.update != null) {
         this.update.start(var1, var2, var3, var4);
      }

   }

   public void start(Object var1, String var2, String var3, String var4, String var5) {
      if (this.update != null) {
         this.update.start(var1, var2, var3, var4, var5);
      }

   }

   public void start(Object var1, String var2, String var3, String var4, String var5, String var6) {
      if (this.update != null) {
         this.update.start(var1, var2, var3, var4, var5, var6);
      }

   }

   public void end(Object var1, String var2, String var3, String var4, String var5) {
      if (this.update != null) {
         this.update.end(var1, var2, var3, var4, var5);
      }

   }

   public void end(Object var1, String var2, String var3, String var4, String var5, String var6) {
      if (this.update != null) {
         this.update.end(var1, var2, var3, var4, var5, var6);
      }

   }

   public void setMeta(Object var1, String var2, String var3, String var4, String var5) {
      if (this.update != null) {
         this.update.setMeta(var1, var2, var3, var4, var5);
      }

   }

   public void end(Object var1, String var2, String var3, String var4) {
      if (this.update != null) {
         this.update.end(var1, var2, var3, var4);
      }

   }

   static class Update {
      public long updateTime;
      public Map elements;

      private Update() {
         this.elements = new Object2ObjectOpenHashMap();
      }

      public void start(String var1) {
         if (DebugTimer.isOn()) {
            DebugTimer.DebugElement var2;
            (var2 = new DebugTimer.DebugElement()).id = var1;
            this.elements.put(var2.id, var2);
            var2.start();
         }

      }

      public void end(String var1) {
         if (DebugTimer.isOn()) {
            ((DebugTimer.DebugElement)this.elements.get(var1)).end();
         }

      }

      public void start(Object var1) {
         this.start(var1.toString());
      }

      public void end(Object var1) {
         this.end(var1.toString());
      }

      public void start(Object var1, String var2) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).start(var2);
      }

      public void end(Object var1, String var2) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).end(var2);
      }

      public void printTo(StringBuffer var1) {
         int var2 = 0;
         Iterator var3 = this.elements.values().iterator();

         while(var3.hasNext()) {
            DebugTimer.DebugElement var4;
            if ((var4 = (DebugTimer.DebugElement)var3.next()).withinThreshold()) {
               var1.append("-------ELEMENT " + var2 + ": " + var4.id + (var4.meta != null ? " [META: " + var4.meta + "]" : "") + "---------\n");
               var4.printTo(var1, 1);
               ++var2;
            }
         }

         if (var2 > 0) {
            var1.append("::::::::::PRINT DEBUG INFO END::::::::::");
         }

      }

      public void start(Object var1, String var2, String var3) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).start(var2, var3);
      }

      public void end(Object var1, String var2, String var3) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).end(var2, var3);
      }

      public void setMeta(Object var1, String var2, String var3, String var4, String var5) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).setMeta(var2, var3, var4, var5);
      }

      public void start(Object var1, String var2, String var3, String var4) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).start(var2, var3, var4);
      }

      public void end(Object var1, String var2, String var3, String var4) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).end(var2, var3, var4);
      }

      public void start(Object var1, String var2, String var3, String var4, String var5) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).start(var2, var3, var4, var5);
      }

      public void end(Object var1, String var2, String var3, String var4, String var5) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).end(var2, var3, var4, var5);
      }

      public void start(Object var1, String var2, String var3, String var4, String var5, String var6) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).start(var2, var3, var4, var5, var6);
      }

      public void end(Object var1, String var2, String var3, String var4, String var5, String var6) {
         ((DebugTimer.DebugElement)this.elements.get(var1.toString())).end(var2, var3, var4, var5, var6);
      }

      // $FF: synthetic method
      Update(Object var1) {
         this();
      }
   }

   public static class DebugElement {
      public String meta;
      public String id;
      public long taken = Long.MIN_VALUE;
      public long started;
      public Map children = new Object2ObjectOpenHashMap();

      public void start() {
         this.started = System.currentTimeMillis();
      }

      public void end() {
         this.taken = System.currentTimeMillis() - this.started;
      }

      public void start(String var1) {
         DebugTimer.DebugElement var2;
         (var2 = new DebugTimer.DebugElement()).id = var1;
         this.children.put(var2.id, var2);
         var2.start();
      }

      public void end(String var1) {
         assert this.children.get(var1) != null : var1 + "; ;;; " + this.children;

         ((DebugTimer.DebugElement)this.children.get(var1)).end();
      }

      public void printTo(StringBuffer var1, int var2) {
         for(int var3 = 0; var3 < var2; ++var3) {
            var1.append("   ");
         }

         var1.append(this.taken + " ms [" + this.id + "]" + (this.meta != null ? " [META: " + this.meta + "]" : "") + "\n");
         Iterator var4 = this.children.values().iterator();

         while(var4.hasNext()) {
            ((DebugTimer.DebugElement)var4.next()).printTo(var1, var2 + 1);
         }

      }

      public boolean withinThreshold() {
         return this.taken >= (long)(Integer)EngineSettings.PROFILER_MIN_MS.getCurrentState();
      }

      public void start(String var1, String var2) {
         ((DebugTimer.DebugElement)this.children.get(var1)).start(var2);
      }

      public void end(String var1, String var2) {
         ((DebugTimer.DebugElement)this.children.get(var1)).end(var2);
      }

      public void setMeta(String var1, String var2, String var3, String var4) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).children.get(var3)).meta = var4;
      }

      public void start(String var1, String var2, String var3) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).start(var3);
      }

      public void end(String var1, String var2, String var3) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).end(var3);
      }

      public void start(String var1, String var2, String var3, String var4) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).children.get(var3)).start(var4);
      }

      public void end(String var1, String var2, String var3, String var4) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).children.get(var3)).end(var4);
      }

      public void start(String var1, String var2, String var3, String var4, String var5) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).children.get(var3)).children.get(var4)).start(var5);
      }

      public void end(String var1, String var2, String var3, String var4, String var5) {
         ((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)((DebugTimer.DebugElement)this.children.get(var1)).children.get(var2)).children.get(var3)).children.get(var4)).end(var5);
      }
   }
}
