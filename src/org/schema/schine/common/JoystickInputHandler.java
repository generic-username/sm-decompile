package org.schema.schine.common;

import org.schema.schine.input.JoystickEvent;

public interface JoystickInputHandler {
   void handleJoystickEvent(JoystickEvent var1);
}
