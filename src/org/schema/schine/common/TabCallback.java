package org.schema.schine.common;

public interface TabCallback {
   boolean catchTab(TextAreaInput var1);

   void onEnter();
}
