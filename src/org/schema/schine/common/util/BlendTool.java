package org.schema.schine.common.util;

import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.Keyboard;

public class BlendTool {
   private static int[] sfactors = new int[]{0, 1, 768, 769, 774, 775, 770, 771, 772, 773, 776};
   private static String[] sfactorsString = new String[]{"GL_ZERO", "GL_ONE", "GL_SRC_COLOR", "GL_ONE_MINUS_SRC_COLOR", "GL_DST_COLOR", "GL_ONE_MINUS_DST_COLOR", "GL_SRC_ALPHA", "GL_ONE_MINUS_SRC_ALPHA", "GL_DST_ALPHA", "GL_ONE_MINUS_DST_ALPHA", "GL_SRC_ALPHA_SATURATE"};
   private static int[] dfactors = new int[]{0, 1, 768, 769, 774, 775, 770, 771, 772, 773};
   private static String[] dfactorsString = new String[]{"GL_ZERO", "GL_ONE", "GL_SRC_COLOR", "GL_ONE_MINUS_SRC_COLOR", "GL_DST_COLOR", "GL_ONE_MINUS_DST_COLOR", "GL_SRC_ALPHA", "GL_ONE_MINUS_SRC_ALPHA", "GL_DST_ALPHA", "GL_ONE_MINUS_DST_ALPHA", "GL_CONSTANT_COLOR", "GL_ONE_MINUS_CONSTANT_COLOR", "GL_CONSTANT_ALPHA", "GL_ONE_MINUS_CONSTANT_ALPHA"};
   private int source = 1;
   private int dest = 1;
   private boolean kLast;

   public void apply() {
      GlUtil.glBlendFunc(sfactors[this.source], dfactors[this.dest]);
   }

   public void checkKeyboard() {
      if (!this.kLast) {
         if (Keyboard.isKeyDown(205)) {
            this.switchSource(1);
            this.kLast = true;
         }

         if (Keyboard.isKeyDown(203)) {
            this.switchSource(-1);
            this.kLast = true;
         }

         if (Keyboard.isKeyDown(200)) {
            this.switchDest(1);
            this.kLast = true;
         }

         if (Keyboard.isKeyDown(208)) {
            this.switchDest(-1);
            this.kLast = true;
         }
      }

      if (!Keyboard.isKeyDown(205) && !Keyboard.isKeyDown(203) && !Keyboard.isKeyDown(200) && !Keyboard.isKeyDown(208)) {
         this.kLast = false;
      }

   }

   public void switchDest(int var1) {
      this.dest = FastMath.cyclicModulo(this.dest + var1, dfactors.length);
      System.err.println("SWITCHED " + this);
   }

   public void switchSource(int var1) {
      this.source = FastMath.cyclicModulo(this.source + var1, sfactors.length);
      System.err.println("SWITCHED " + this);
   }

   public String toString() {
      return "[ " + sfactorsString[this.source] + ", " + dfactorsString[this.dest] + " ]";
   }
}
