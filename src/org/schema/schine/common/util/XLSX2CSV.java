package org.schema.schine.common.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.eventusermodel.XSSFReader.SheetIterator;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XLSX2CSV {
   private final OPCPackage xlsxPackage;
   private final int minColumns;
   private final PrintStream output;

   public XLSX2CSV(OPCPackage var1, PrintStream var2, int var3) {
      this.xlsxPackage = var1;
      this.output = var2;
      this.minColumns = var3;
   }

   public void processSheet(StylesTable var1, ReadOnlySharedStringsTable var2, SheetContentsHandler var3, InputStream var4) throws IOException, SAXException {
      DataFormatter var5 = new DataFormatter();
      InputSource var9 = new InputSource(var4);

      try {
         XMLReader var6 = SAXHelper.newXMLReader();
         XSSFSheetXMLHandler var8 = new XSSFSheetXMLHandler(var1, (CommentsTable)null, var2, var3, var5, false);
         var6.setContentHandler(var8);
         var6.parse(var9);
      } catch (ParserConfigurationException var7) {
         throw new RuntimeException("SAX parser appears to be broken - " + var7.getMessage());
      }
   }

   public void process() throws IOException, OpenXML4JException, SAXException {
      ReadOnlySharedStringsTable var1 = new ReadOnlySharedStringsTable(this.xlsxPackage);
      XSSFReader var2;
      StylesTable var3 = (var2 = new XSSFReader(this.xlsxPackage)).getStylesTable();
      SheetIterator var5 = (SheetIterator)var2.getSheetsData();

      while(var5.hasNext()) {
         InputStream var4 = var5.next();
         var5.getSheetName();
         this.output.println();
         this.processSheet(var3, var1, new XLSX2CSV.SheetToCSV(), var4);
         var4.close();
      }

   }

   public static void main(String[] var0) throws Exception {
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      PrintStream var2 = new PrintStream(var1);
      File var3;
      if (!(var3 = new File("tj.xlsx")).exists()) {
         System.err.println("Not found or not a file: " + var3.getPath());
      } else {
         int var4 = -1;
         if (var0.length >= 2) {
            var4 = Integer.parseInt(var0[1]);
         }

         OPCPackage var5 = OPCPackage.open(var3.getPath(), PackageAccess.READ);
         (new XLSX2CSV(var5, var2, var4)).process();
         var5.close();
         String var6 = new String(var1.toByteArray(), StandardCharsets.UTF_8);
         FileWriter var7 = new FileWriter("tt.txt");
         BufferedWriter var8;
         (var8 = new BufferedWriter(var7)).write(var6);
         var8.close();
         System.err.println(var6);
      }
   }

   public static void test() throws IOException {
      File var0 = new File("tj.xlsx");
      FileInputStream var5 = new FileInputStream(var0);
      XSSFWorkbook var6;
      Iterator var1 = (var6 = new XSSFWorkbook(var5)).getSheetAt(0).iterator();
      BufferedWriter var2 = new BufferedWriter(new FileWriter(new File("ttj.txt")));

      while(var1.hasNext()) {
         Iterator var3 = ((Row)var1.next()).cellIterator();

         while(var3.hasNext()) {
            Cell var4;
            switch((var4 = (Cell)var3.next()).getCellType()) {
            case 0:
               System.out.print(var4.getNumericCellValue() + "\t");
               break;
            case 1:
               System.out.print(var4.getRichStringCellValue().getString() + "\t");
               var2.write(var4.getRichStringCellValue().getString() + "\t");
            case 2:
            case 3:
            default:
               break;
            case 4:
               System.out.print(var4.getBooleanCellValue() + "\t");
            }
         }

         var2.write("\n");
         System.out.println("");
      }

      var6.close();
      var2.close();
   }

   class SheetToCSV implements SheetContentsHandler {
      private boolean firstCellOfRow;
      private int currentRow;
      private int currentCol;

      private SheetToCSV() {
         this.firstCellOfRow = false;
         this.currentRow = -1;
         this.currentCol = -1;
      }

      private void outputMissingRows(int var1) {
         for(int var2 = 0; var2 < var1; ++var2) {
            for(int var3 = 0; var3 < XLSX2CSV.this.minColumns; ++var3) {
               XLSX2CSV.this.output.append(',');
            }

            XLSX2CSV.this.output.append('\n');
         }

      }

      public void startRow(int var1) {
         this.outputMissingRows(var1 - this.currentRow - 1);
         this.firstCellOfRow = true;
         this.currentRow = var1;
         this.currentCol = -1;
      }

      public void endRow(int var1) {
         for(var1 = this.currentCol; var1 < XLSX2CSV.this.minColumns; ++var1) {
            XLSX2CSV.this.output.append(',');
         }

         XLSX2CSV.this.output.append('\n');
      }

      public void cell(String var1, String var2, XSSFComment var3) {
         if (this.firstCellOfRow) {
            this.firstCellOfRow = false;
         } else {
            XLSX2CSV.this.output.append(',');
         }

         if (var1 == null) {
            var1 = (new CellAddress(this.currentRow, this.currentCol)).formatAsString();
         }

         short var6;
         int var7 = (var6 = (new CellReference(var1)).getCol()) - this.currentCol - 1;

         for(int var4 = 0; var4 < var7; ++var4) {
            XLSX2CSV.this.output.append(',');
         }

         this.currentCol = var6;

         try {
            Double.parseDouble(var2);
            XLSX2CSV.this.output.append(var2);
         } catch (NumberFormatException var5) {
            XLSX2CSV.this.output.append('"');
            XLSX2CSV.this.output.append(var2);
            XLSX2CSV.this.output.append('"');
         }
      }

      public void headerFooter(String var1, boolean var2, String var3) {
      }

      // $FF: synthetic method
      SheetToCSV(Object var2) {
         this();
      }
   }
}
