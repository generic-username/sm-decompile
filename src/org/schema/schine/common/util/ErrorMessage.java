package org.schema.schine.common.util;

public class ErrorMessage {
   private String msg;

   public ErrorMessage(String var1) {
      this.msg = var1;
   }

   public String toString() {
      return this.msg;
   }
}
