package org.schema.schine.common.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.resource.FileExt;

public class JoglLibLoader {
   private static void loadJoglLibs(JFrame var0) {
      String var1 = System.getProperty("user.dir");
      System.out.println("current working dir: " + var1);
      System.loadLibrary("nativewindow_awt");
      System.out.println("[JoglLibLoader] nativeWindow_AWT OK");
      System.loadLibrary("gluegen-rt");
      System.out.println("[JoglLibLoader] gluegen          OK");
      System.loadLibrary("newt");
      System.out.println("[JoglLibLoader] newt             OK");
      System.loadLibrary("jogl_desktop");
      System.out.println("[JoglLibLoader] jogl_desktop     OK");
   }

   public static void loadLibs() {
      String var0 = System.getProperty("os.name");
      String var1 = System.getProperties().getProperty("os.arch");
      String var2 = System.getProperties().getProperty("java.library.path");
      System.out.println("[JoglLibLoader] java library path: " + var2);
      JFrame var14 = new JFrame();

      try {
         String var15 = File.separator;
         String var3;
         if (var1.contains("64")) {
            var3 = "x64";
         } else {
            var3 = "x86";
         }

         if (var0.toLowerCase(Locale.ENGLISH).contains("win")) {
            var3 = "lib" + var15 + "win-" + var3 + var15;
         } else if (!var0.toLowerCase(Locale.ENGLISH).contains("nux") && !var0.contains("nix")) {
            if (!var0.toLowerCase(Locale.ENGLISH).contains("mac")) {
               throw new UnsupportedOperationException("[ClientStarter] OS '" + var0 + "' not yet supported on architecture " + var1);
            }

            var3 = "lib" + var15 + "mac-" + var3 + var15;
         } else {
            var3 = "lib" + var15 + "linux-" + var3 + var15;
         }

         System.out.println("[JoglLibLoader][SYSTEM] loading openGL libraries for " + var0 + "  " + var1);

         try {
            loadJoglLibs(var14);
         } catch (UnsatisfiedLinkError var9) {
            var9.printStackTrace();
            FileExt var11 = new FileExt("");
            System.out.println("[JoglLibLoader][SYSTEM] PLATFORM NOT YET INSTALLED.");
            System.out.println("[JoglLibLoader][SYSTEM] NOW INSTALLING REQUIRED LIBRARIES");
            FileExt var12;
            if ((var12 = new FileExt(var11.getAbsolutePath() + var15 + var3)).exists() && var12.isDirectory()) {
               System.out.println("Lib directory found: " + var12.getAbsolutePath());
               File[] var13;
               int var16 = (var13 = var12.listFiles(new FilenameFilter() {
                  public final boolean accept(File var1, String var2) {
                     return var2.endsWith(".dll") || var2.endsWith(".so");
                  }
               })).length;

               for(int var5 = 0; var5 < var16; ++var5) {
                  File var6 = var13[var5];
                  System.out.print("[JoglLibLoader] installing " + var6.getName() + " ... ");
                  FileExt var7 = new FileExt(var11.getAbsolutePath() + var15 + var6.getName());
                  DataUtil.copy(var6, var7);
                  System.out.println(" installed to " + var7.getAbsolutePath());
               }

               loadJoglLibs(var14);
            } else {
               throw new IllegalAccessError("library dir " + var12.getAbsolutePath() + " not found");
            }
         }
      } catch (Exception var10) {
         System.err.println("EXEPTION WHILE LOADING LIBS");
         Object[] var4 = new Object[]{"terminate"};
         JOptionPane.showOptionDialog(var14, var10.getMessage(), "Critical ERROR", 2, 0, (Icon)null, var4, var4[0]);
         var10.printStackTrace();
         System.err.println("Exiting because of log lib exception");

         try {
            throw new Exception("System.exit() called");
         } catch (Exception var8) {
            var8.printStackTrace();
            System.exit(-1);
         }
      }
   }
}
