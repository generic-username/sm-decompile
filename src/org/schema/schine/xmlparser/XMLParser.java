package org.schema.schine.xmlparser;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.resource.ResourceLoader;
import org.schema.schine.xmlparser.Types.Typecreator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLParser extends DefaultHandler {
   static final String sNEWLINE = System.getProperty("line.separator");
   private static XMLContainer root;
   XMLContainer current;
   XMLContainer parent;
   private Typecreator tCreator;
   private int level;
   private Object unitName;

   public static XMLContainer getRoot() {
      return root;
   }

   public static void setRoot(XMLContainer var0) {
      root = var0;
   }

   public Typecreator getAttributes(String var1, String var2, String var3) {
      if (this.tCreator == null || !var2.equals(this.unitName)) {
         if (getRoot() == null) {
            this.parseXML(var1);
         }

         this.unitName = var2;
         XMLContainer var4 = XMLContainer.rekursiveSearchContainer(var2, getRoot(), (XMLContainer)null);
         if (!var2.equals(var4.name)) {
            throw new NullPointerException("\nERROR: recursive Search of: " + var2 + " failed, found: " + var4.name);
         }

         this.tCreator = var4.tCreator;
         if (var4.tCreator == null) {
            throw new IllegalArgumentException();
         }
      }

      return this.tCreator;
   }

   public int getLevel() {
      return this.level;
   }

   public void setLevel(int var1) {
      this.level = var1;
   }

   public void parseXML(String var1) {
      try {
         long var3 = System.currentTimeMillis();
         System.out.println("[XMLParser] Parsing main configuration XML File: " + DataUtil.dataPath + var1);
         SAXParser var2 = SAXParserFactory.newInstance().newSAXParser();

         try {
            var2.parse(ResourceLoader.resourceUtil.getResourceAsInputStream(DataUtil.dataPath + var1), this);
         } catch (ResourceException var5) {
            var5.printStackTrace();
         }

         System.out.println("[XMLParser] DONE Parsing main configuration. TIME: " + (System.currentTimeMillis() - var3) + "ms");
      } catch (ParserConfigurationException var6) {
         var6.printStackTrace();
      } catch (SAXException var7) {
         var7.printStackTrace();
      } catch (IOException var8) {
         var8.printStackTrace();
      }
   }

   public void startDocument() throws SAXException {
   }

   public void endDocument() throws SAXException {
   }

   public void startElement(String var1, String var2, String var3, Attributes var4) throws SAXException {
      var1 = "".equals(var2) ? var3 : var2;
      if (getRoot() == null) {
         root = new XMLContainer();
         getRoot().name = var1;
         this.current = getRoot();
      } else {
         XMLContainer var6;
         (var6 = new XMLContainer()).name = var1;
         if (var4 != null) {
            var6.tCreator = new Typecreator(var6.name);

            for(int var5 = 0; var5 < var4.getLength(); ++var5) {
               var3 = var4.getLocalName(var5);
               if ("".equals(var3)) {
                  var3 = var4.getQName(var5);
               }

               var6.tCreator.addType(var4.getValue(var5), var3);
            }
         }

         this.current.childs.add(var6);
         var6.parent = this.current;
         if (!this.current.ended) {
            this.current = var6;
         }

      }
   }

   public void endElement(String var1, String var2, String var3) throws SAXException {
      if (("".equals(var2) ? var3 : var2).equals(this.current.name)) {
         this.current.ended = true;
         if (this.current.parent != null) {
            this.current = this.current.parent;
         }
      }

   }

   public void characters(char[] var1, int var2, int var3) throws SAXException {
   }
}
