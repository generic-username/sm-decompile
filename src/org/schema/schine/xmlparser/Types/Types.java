package org.schema.schine.xmlparser.Types;

public enum Types {
   SPRITEPATH("path"),
   SPRITENAME("filename"),
   SPAWNABLE("spawnable"),
   SOUNDPATH("soundpath"),
   CULLING("culling");

   private String name;

   private Types(String var3) {
      this.name = var3;
   }

   public final String toString() {
      return this.name;
   }
}
