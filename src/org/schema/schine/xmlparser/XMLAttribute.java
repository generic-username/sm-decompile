package org.schema.schine.xmlparser;

public class XMLAttribute {
   public String name;
   public String value;

   public XMLAttribute(String var1, String var2) {
      this.name = var1;
      this.value = var2;
   }

   public String toString() {
      return this.name;
   }
}
