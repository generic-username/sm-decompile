package org.schema.schine.xmlparser;

import java.util.Iterator;
import java.util.LinkedList;
import org.schema.schine.xmlparser.Types.Typecreator;

public class XMLContainer {
   public String name;
   public int d = 0;
   public boolean ended = false;
   public Typecreator tCreator;
   public LinkedList childs = new LinkedList();
   public XMLContainer parent;

   public static XMLContainer rekursiveSearchContainer(String var0, XMLContainer var1, XMLContainer var2) {
      if (var0.equals(var1.name)) {
         return var1;
      } else {
         Iterator var3 = var1.childs.iterator();

         do {
            if (!var3.hasNext()) {
               return var2;
            }

            var2 = (XMLContainer)var3.next();
         } while(!(var2 = rekursiveSearchContainer(var0, var2, var1)).name.equals(var0));

         return var2;
      }
   }

   public String toString() {
      String var1 = "";
      var1 = var1 + "<" + this.name + ">\n";

      XMLContainer var3;
      for(Iterator var2 = this.childs.iterator(); var2.hasNext(); var1 = var1 + var3.toString()) {
         var3 = (XMLContainer)var2.next();

         for(int var4 = 0; var4 < var3.d; ++var4) {
            var1 = var1 + " ";
         }
      }

      return var1 + "</" + this.name + ">\n";
   }
}
