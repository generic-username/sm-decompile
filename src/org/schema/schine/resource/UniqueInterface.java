package org.schema.schine.resource;

public interface UniqueInterface {
   String getUniqueIdentifier();

   boolean isVolatile();
}
