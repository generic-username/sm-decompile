package org.schema.schine.resource;

import java.io.File;
import java.net.URI;
import org.schema.schine.network.server.ServerController;

public class FileExt extends File {
   private static final long serialVersionUID = 1L;

   public FileExt(File var1, String var2) {
      super(var1, var2);
   }

   public FileExt(String var1, String var2) {
      super(var1, var2);
   }

   public FileExt(String var1) {
      super(var1);
   }

   public FileExt(URI var1) {
      super(var1);
   }

   public boolean delete() {
      if (ServerController.debugLogoutOnShutdown) {
         try {
            throw new Exception("##DEBUG## DELETING FILE: " + this.getAbsolutePath());
         } catch (Exception var1) {
            var1.printStackTrace();
         }
      }

      return super.delete();
   }

   public void deleteOnExit() {
      if (ServerController.debugLogoutOnShutdown) {
         try {
            throw new Exception("##DEBUG## DELETING FILE ON EXIT: " + this.getAbsolutePath());
         } catch (Exception var1) {
            var1.printStackTrace();
         }
      }

      super.deleteOnExit();
   }

   public boolean renameTo(File var1) {
      if (ServerController.debugLogoutOnShutdown) {
         try {
            throw new Exception("##DEBUG## RENAME TO: " + this.getAbsolutePath() + " -> " + var1.getAbsolutePath());
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

      return super.renameTo(var1);
   }
}
