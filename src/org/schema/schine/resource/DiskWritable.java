package org.schema.schine.resource;

import org.schema.schine.resource.tag.TagSerializable;

public interface DiskWritable extends UniqueInterface, TagSerializable {
}
