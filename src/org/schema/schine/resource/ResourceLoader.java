package org.schema.schine.resource;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.ParseException;
import org.schema.common.util.StringTools;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.data.ResourceUtil;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructSet;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.ImageProbs;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.DDSLoader;
import org.schema.schine.graphicsengine.texture.textureImp.Texture3D;
import org.schema.schine.network.StateInterface;
import org.schema.schine.xmlparser.XMLParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class ResourceLoader {
   public static final int MODE_DEFAULT = 0;
   public static final int MODE_WITH_SPRITES = 1;
   public static final int MODE_FAST_LOAD = 2;
   public static ResourceUtil resourceUtil = new ResourceUtil();
   public static Texture3D explosionVolume;
   public static boolean dedicatedServer;
   public final List loadQueue = new ObjectArrayList();
   private final MeshLoader meshLoader;
   private final ImageLoader imageLoader;
   private final AudioFileLoader audioLoader;
   private final ResourceMap resourceMap;
   float onePerc;
   private XMLParser parser;
   private int mode;
   private LinkedList loadList = new LinkedList();
   private int loadCount = 0;
   private String loadString = "";
   private float loadStatus;
   private String defaultGroundName;
   private String defaultGroundPath;
   private String defaultGroundObjectName;
   private boolean loaded = false;
   protected Set loadedDataEntries = new ObjectOpenHashSet();

   public ResourceLoader(int var1) {
      this.setMode(var1);
      this.resourceMap = new ResourceMap();
      this.imageLoader = new ImageLoader();
      this.meshLoader = new MeshLoader(resourceUtil);
      this.audioLoader = new AudioFileLoader(resourceUtil);
      this.parser = new XMLParser();
   }

   public static void checkFor(String var0, NamedNodeMap var1, Node var2, Node var3) {
      if (var2 == null) {
         System.err.println("Attributes Count: " + var1.getLength());

         for(int var4 = 0; var4 < var1.getLength(); ++var4) {
            System.err.println("Attribute: " + var1.item(var4).getNodeName() + " = " + var1.item(var4).getNodeValue());
         }

         throw new NullPointerException("Attribute not found in " + var3.getNodeName() + " from " + var1.getLength() + " available");
      }
   }

   public void loadModelDirectly(String var1, String var2, String var3) throws ResourceException, IOException {
      ResourceLoadEntry var4;
      (var4 = new ResourceLoadEntry(var1, 1)).fileName = var3;
      var4.path = var2;
      var4.load(this);
      this.loadedDataEntries.add(var4);
   }

   static void loadModel(Node var0, List var1) throws ResourceException, ParseException {
      NodeList var14 = var0.getChildNodes();

      for(int var2 = 0; var2 < var14.getLength(); ++var2) {
         Node var3;
         if ((var3 = var14.item(var2)).getNodeType() == 1) {
            String var4 = var3.getNodeName();
            NamedNodeMap var5;
            Node var6 = (var5 = var3.getAttributes()).getNamedItem("path");
            checkFor("path", var5, var6, var3);
            String var15 = var6.getNodeValue();
            NodeList var16 = var3.getChildNodes();

            for(int var7 = 0; var7 < var16.getLength(); ++var7) {
               Node var8;
               if ((var8 = var16.item(var7)).getNodeType() == 1) {
                  NamedNodeMap var9;
                  Node var10 = (var9 = var8.getAttributes()).getNamedItem("filename");
                  Node var11 = var9.getNamedItem("relpath");
                  checkFor("filename", var9, var10, var3);
                  Node var18 = var9.getNamedItem("physicsmesh");
                  String var12 = null;
                  if (var18 != null) {
                     var12 = var18.getNodeValue();
                  } else if (var4.toLowerCase(Locale.ENGLISH).equals("collision")) {
                     var12 = "dedicated";
                  }

                  String var19 = var10.getNodeValue();
                  ResourceLoadEntry var21 = new ResourceLoadEntry(var8.getNodeName(), 1);
                  String var13 = var15;
                  if (var11 != null) {
                     var13 = var15 + var11.getNodeValue();
                  }

                  if (!var13.endsWith("/")) {
                     var13 = var13 + "/";
                  }

                  var21.path = var13;
                  var21.fileName = var19;
                  var21.physicsMesh = var12;
                  NodeList var17 = var8.getChildNodes();

                  for(int var20 = 0; var20 < var17.getLength(); ++var20) {
                     if ((var11 = var17.item(var20)).getNodeType() == 1) {
                        if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("animation")) {
                           var11 = var17.item(var20).getAttributes().getNamedItem("default");
                           var12 = "STATIC_DEFAULT";
                           if (var11 != null) {
                              var12 = var11.getNodeValue();
                           }

                           var21.animation = new AnimationStructure();
                           var21.animation.parse(var17.item(var20), var12, (AnimationStructSet)null);
                        } else if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("texture")) {
                           var21.texture = TextureStructure.parse(var17.item(var20));
                        } else if (var11.getNodeName().toLowerCase(Locale.ENGLISH).equals("creature")) {
                           var21.creature = CreatureStructure.parse(var17.item(var20));
                        }
                     }
                  }

                  boolean var10000 = $assertionsDisabled;
                  var1.add(var21);
               }
            }
         }
      }

   }

   public static void loadModelConfig(List var0) throws ResourceException, ParseException, FileNotFoundException, SAXException, IOException, ParserConfigurationException {
      NodeList var1 = parseXML().getChildNodes();

      for(int var2 = 0; var2 < var1.getLength(); ++var2) {
         loadModel(var1.item(var2), var0);
      }

   }

   public static Document parseXML() throws FileNotFoundException, SAXException, IOException, ParserConfigurationException {
      DocumentBuilder var0 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      BufferedInputStream var1 = new BufferedInputStream(new FileInputStream(DataUtil.dataPath + File.separator + DataUtil.configPath));
      Document var2 = var0.parse(var1);
      var1.close();
      return var2;
   }

   public AudioFileLoader getAudioLoader() {
      return this.audioLoader;
   }

   public ImageProbs getBufferedImage(String var1) throws ResourceException {
      ImageProbs var2;
      if ((var2 = (ImageProbs)this.imageLoader.getImageMap().get(var1)) == null) {
         throw new ResourceException("Could not find sprites: " + var1);
      } else {
         return var2;
      }
   }

   public ImageLoader getImageLoader() {
      return this.imageLoader;
   }

   public LinkedList getLoadList() {
      return this.loadList;
   }

   public void setLoadList(LinkedList var1) {
      this.loadList = var1;
   }

   public float getLoadStatus() {
      return this.loadStatus;
   }

   public void setLoadStatus(int var1) {
      this.loadStatus = (float)var1;
   }

   public String getLoadString() {
      return this.loadString;
   }

   public void setLoadString(String var1) {
      this.loadString = var1;
   }

   public Mesh getMesh(String var1) {
      return (Mesh)this.meshLoader.getMeshMap().get(var1);
   }

   public int getMode() {
      return this.mode;
   }

   public void setMode(int var1) {
      this.mode = var1;
   }

   public String getRandomTipp() {
      return "UNDEFINED";
   }

   public Sprite getSprite(String var1) {
      Sprite var2 = (Sprite)this.imageLoader.getSpriteMap().get(var1);

      assert var2 != null : "Could not find sprites: " + var1;

      return var2;
   }

   public boolean isLoaded() {
      return this.loaded;
   }

   public void setLoaded(boolean var1) {
      this.loaded = var1;
   }

   public void enqueueFont() {
      ResourceLoadEntry var1;
      (var1 = new ResourceLoadEntry("FONT", 3)).fileName = "FONT";
      var1.path = "FONT";
      var1.fileType = "FONT";
      boolean var10000 = $assertionsDisabled;
      synchronized(this.loadQueue) {
         this.loadQueue.add(var1);
         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   public void enqueueAudio() {
      try {
         this.loadAudioResources();
         this.resetLoadCounts();
         this.setLoaded(false);
      } catch (ResourceException var3) {
         ResourceException var1 = var3;

         try {
            GLFrame.processErrorDialogException((Exception)var1, (StateInterface)null);
         } catch (Exception var2) {
            var2.printStackTrace();
         }

         System.err.println("COULD NOT LOAD AUDIO PATH!!!!!!");
      }
   }

   public void enqueueModels() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
      int var1 = this.loadQueue.size();
      synchronized(this.loadQueue) {
         loadModelConfig(this.loadQueue);
         this.resetLoadCounts();
         this.setLoaded(false);
      }

      System.err.println("[RESOURCE] ENQUEING 3D MODELS " + (var1 - this.loadQueue.size()));
   }

   public void enqueueCusom() {
      synchronized(this.loadQueue) {
         try {
            this.loadQueue.addAll(this.loadCustom());
         } catch (ResourceException var3) {
         }

         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   public void loadAll() throws ResourceException, ParseException, FileNotFoundException, SAXException, IOException, ParserConfigurationException {
      System.err.println("[RESOURCE] EQUEUING ALL RESOURCES!");
      this.enqueueFont();

      assert this.check(this.loadQueue);

      this.enqueueImageResources();

      assert this.check(this.loadQueue);

      this.enqueueConfigResources("GuiConfig.xml", false);

      assert this.check(this.loadQueue);

      this.enqueueAudio();

      assert this.check(this.loadQueue);

      this.enqueueModels();

      assert this.check(this.loadQueue);

      this.enqueueCusom();

      assert this.check(this.loadQueue);

      this.resetLoadCounts();
   }

   public abstract void enqueueConfigResources(String var1, boolean var2);

   public void enqueueImageResources() {
      this.loadImageResources();

      assert this.check(this.loadQueue);

      this.resetLoadCounts();
      this.setLoaded(false);
   }

   public void enqueueWithResetForced(ResourceLoadEntry... var1) {
      synchronized(this.loadQueue) {
         int var3 = (var1 = var1).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            ResourceLoadEntry var5 = var1[var4];
            this.loadedDataEntries.remove(var5);
            this.loadQueue.add(var5);
         }

         assert this.check(this.loadQueue);

         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   public void enqueueWithResetForced(Collection var1) {
      synchronized(this.loadQueue) {
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            ResourceLoadEntry var3 = (ResourceLoadEntry)var5.next();
            this.loadedDataEntries.remove(var3);
            this.loadQueue.add(var3);
         }

         assert this.check(this.loadQueue);

         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   public void enqueueWithReset(ResourceLoadEntry... var1) {
      synchronized(this.loadQueue) {
         int var3 = (var1 = var1).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            ResourceLoadEntry var5 = var1[var4];
            this.loadQueue.add(var5);
         }

         assert this.check(this.loadQueue);

         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   private boolean check(Collection var1) {
      synchronized(var1){}

      Throwable var10000;
      label98: {
         boolean var10001;
         Iterator var9;
         try {
            var9 = var1.iterator();
         } catch (Throwable var8) {
            var10000 = var8;
            var10001 = false;
            break label98;
         }

         while(true) {
            try {
               if (var9.hasNext()) {
                  if ((ResourceLoadEntry)var9.next() != null) {
                     continue;
                  }

                  return false;
               }
            } catch (Throwable var7) {
               var10000 = var7;
               var10001 = false;
               break;
            }

            return true;
         }
      }

      Throwable var10 = var10000;
      throw var10;
   }

   public void enqueueWithReset(List var1) {
      synchronized(this.loadQueue) {
         assert this.check(var1);

         this.loadQueue.addAll(var1);
         this.resetLoadCounts();
         this.setLoaded(false);
      }
   }

   void loadAudio(ResourceLoadEntry var1) throws ResourceException {
      try {
         this.audioLoader.loadSound(var1.path + var1.fileName, var1.name, var1.fileType);
         this.loadList.add(0, var1.name + ": " + var1.path + var1.fileName);
      } catch (Exception var2) {
         var2.printStackTrace();
         throw new ResourceException(var1.toString());
      }
   }

   void loadAudioResources() throws ResourceException {
      String var1 = DataUtil.dataPath + "/audio-resource/";
      FileExt var2;
      if (!(var2 = new FileExt(var1)).exists()) {
         var2.mkdirs();
      }

      this.loadAudioResourcesRecusively(var2);
   }

   private void loadAudioResourcesRecusively(File var1) throws ResourceException {
      if (var1.exists()) {
         File[] var2 = var1.listFiles();

         for(int var3 = 0; var3 < var2.length; ++var3) {
            if (var2[var3].isDirectory()) {
               this.loadAudioResourcesRecusively(var2[var3]);
            } else {
               String var4;
               int var5;
               String var8;
               ResourceLoadEntry var9;
               if (var2[var3].getName().endsWith(".ogg")) {
                  var5 = (var5 = (var4 = var2[var3].getName()).lastIndexOf("/")) < 0 ? var4.lastIndexOf("\\") : var5;
                  var8 = var4.substring(var5 + 1, var4.lastIndexOf(".ogg"));
                  (var9 = new ResourceLoadEntry(var8, 2)).path = var1.getPath() + File.separator;
                  var9.fileName = var4;
                  var9.fileType = "OGG";
                  synchronized(this.loadQueue) {
                     this.loadQueue.add(var9);
                  }
               } else if (var2[var3].getName().endsWith(".wav")) {
                  var5 = (var5 = (var4 = var2[var3].getName()).lastIndexOf("/")) < 0 ? var4.lastIndexOf("\\") : var5;
                  var8 = var4.substring(var5 + 1, var4.lastIndexOf(".wav"));
                  (var9 = new ResourceLoadEntry(var8, 2)).path = var1.getPath() + File.separator;
                  var9.fileName = var4;
                  var9.fileType = "WAV";
                  synchronized(this.loadQueue) {
                     this.loadQueue.add(var9);
                  }
               }
            }
         }

      } else {
         throw new ResourceException("Audiopath not found");
      }
   }

   public List loadCustom() throws ResourceException {
      ArrayList var1;
      (var1 = new ArrayList()).add(new ResourceLoadEntry("Explision", 5) {
         public void load(ResourceLoader var1) throws ResourceException {
            try {
               ResourceLoader.explosionVolume = new Texture3D();
               DDSLoader.load(new FileExt(DataUtil.dataPath + "effects/explosionMaps/explode.dds"), ResourceLoader.explosionVolume, false);
            } catch (IOException var2) {
               var2.printStackTrace();
               throw new ResourceException("Explosion volume");
            }
         }
      });
      return var1;
   }

   public void resetLoadCounts() {
      synchronized(this.loadQueue) {
         this.onePerc = 100.0F / (float)Math.max(1, this.loadQueue.size());
         this.loadCount = 0;
      }
   }

   public void loadQueuedDataEntry() throws ResourceException, IOException {
      synchronized(this.loadQueue) {
         if (!this.loadQueue.isEmpty()) {
            while(true) {
               if (this.loadQueue.isEmpty()) {
                  this.setLoaded(true);
                  return;
               }

               ResourceLoadEntry var2;
               if ((var2 = (ResourceLoadEntry)this.loadQueue.remove(0)) == null) {
                  System.err.println("ERROR! NULL load entry detected!");
               }

               if (var2 != null && var2.canLoad() && !this.loadedDataEntries.contains(var2)) {
                  System.err.println("############## LOADING RESOURCE " + var2.name);
                  System.currentTimeMillis();
                  var2.load(this);
                  System.currentTimeMillis();
                  this.loadedDataEntries.add(var2);
                  ++this.loadCount;
                  this.loadStatus = (float)this.loadCount * this.onePerc;
                  this.loadString = StringTools.format(Lng.ORG_SCHEMA_SCHINE_RESOURCE_RESOURCELOADER_0, (int)this.loadStatus);
                  break;
               }
            }
         } else {
            this.setLoaded(true);
         }

      }
   }

   private void loadImage(File var1, String var2, String var3) {
      String var4;
      if ((var4 = var1.getName()).endsWith(".png")) {
         int var8 = (var8 = var4.lastIndexOf("/")) < 0 ? var4.lastIndexOf("\\") : var8;
         String var10 = var3 + var4.substring(var8 + 1, var4.lastIndexOf(".png"));
         ResourceLoadEntry var9;
         (var9 = new ResourceLoadEntry(var10, 0)).path = var2;
         var9.fileName = var4;
         synchronized(this.loadQueue) {
            this.loadQueue.add(var9);
         }
      } else {
         if (var1.isDirectory() && !var4.equals("unused")) {
            File[] var7 = var1.listFiles();

            for(int var5 = 0; var5 < var7.length; ++var5) {
               this.loadImage(var7[var5], var2 + var4 + "/", var3 + var4 + "/");
            }
         }

      }
   }

   private void loadImageResources() {
      String var1 = DataUtil.dataPath + "/image-resource/";
      FileExt var2;
      if (!(var2 = new FileExt(var1)).exists()) {
         List var8;
         Iterator var9 = (var8 = resourceUtil.getFileNamesInPackage("client.jar", "data.image-resource")).iterator();

         while(var9.hasNext()) {
            String var4;
            if ((var4 = (String)var9.next()).endsWith(".png")) {
               int var5 = (var5 = var4.lastIndexOf("/")) < 0 ? var4.lastIndexOf("\\") : var5;
               String var10 = var4.substring(var5 + 1, var4.lastIndexOf(".png"));
               ResourceLoadEntry var11;
               (var11 = new ResourceLoadEntry(var10, 0)).path = var1;
               var11.fileName = var4;
               synchronized(this.loadQueue) {
                  this.loadQueue.add(var11);
               }
            }
         }

         System.err.println("Icons from jar added: " + var8);
      } else {
         File[] var7 = var2.listFiles();

         for(int var3 = 0; var3 < var7.length; ++var3) {
            this.loadImage(var7[var3], var1, "");
         }

      }
   }

   void loadMesh(ResourceLoadEntry var1) throws ResourceException {
      if (this.mode != 2 && this.meshLoader.loadMesh(var1.name, var1.fileName, var1.path, var1.animation, var1.texture, var1.creature, var1.physicsMesh)) {
         this.loadList.add(0, var1.name + ": " + var1.path + var1.fileName);
      } else {
         if (this.mode == 2) {
            this.loadString = this.loadString + " FAST MODE";
         }

         if (!this.meshLoader.getMeshMap().containsKey(this.defaultGroundObjectName)) {
            if (this.mode == 2) {
               this.loadList.add(0, "DEFAULT: " + this.defaultGroundObjectName);
            }

            this.meshLoader.loadMesh(this.defaultGroundObjectName, this.defaultGroundName, this.defaultGroundPath, (AnimationStructure)null, (TextureStructure)null, (CreatureStructure)null, (String)null);
         }

         this.loadList.add(0, "**SKIPPED: " + var1.name + "... added default: " + this.defaultGroundObjectName);
         this.meshLoader.getMeshMap().put(var1.name, this.meshLoader.getMeshMap().get(this.defaultGroundObjectName));
      }
   }

   void loadSprite(ResourceLoadEntry var1) throws IOException {
      this.imageLoader.loadImage(var1.path + var1.fileName, var1.name);
      this.loadList.add(0, var1.name + ": " + var1.path + var1.fileName);
   }

   public ResourceMap getResourceMap() {
      return this.resourceMap;
   }

   public void removeLoaded(ResourceLoadEntry var1) {
      this.loadedDataEntries.remove(var1);
   }

   public void onStopClient() {
   }

   public void forceLoadAll() {
      try {
         while(!this.loadQueue.isEmpty()) {
            this.loadQueuedDataEntry();
         }

      } catch (ResourceException var1) {
         var1.printStackTrace();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public void loadServer() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
   }

   public void loadClient() {
   }
}
