package org.schema.schine.resource.tag;

import java.io.DataInput;
import java.io.IOException;

public class TagSerializableLong2TransformMapFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      TagSerializableLong2TransformMap var3 = new TagSerializableLong2TransformMap(var2);
      TagSerializableLong2TransformMap.deserializeBody(var1, var3, var2);
      return var3;
   }
}
