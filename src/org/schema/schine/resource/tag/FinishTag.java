package org.schema.schine.resource.tag;

public class FinishTag extends Tag {
   public static final FinishTag INST = new FinishTag();

   public FinishTag() {
      super(Tag.Type.FINISH, (String)null, (Tag[])null);
   }
}
