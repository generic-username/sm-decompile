package org.schema.schine.resource.tag;

import it.unimi.dsi.fastutil.longs.Long2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectSortedMap.FastSortedEntrySet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.network.SerialializationInterface;

public class TagSerializableLong2Vector3fMap extends Long2ObjectLinkedOpenHashMap implements SerialializationInterface, SerializableTagElement {
   private static final long serialVersionUID = -1556336980069443312L;
   public boolean ignoreZeroLenVectors = false;

   public TagSerializableLong2Vector3fMap() {
   }

   public TagSerializableLong2Vector3fMap(int var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2Vector3fMap(int var1) {
      super(var1);
   }

   public TagSerializableLong2Vector3fMap(long[] var1, Vector3f[] var2, float var3) {
      super(var1, var2, var3);
   }

   public TagSerializableLong2Vector3fMap(long[] var1, Vector3f[] var2) {
      super(var1, var2);
   }

   public TagSerializableLong2Vector3fMap(Long2ObjectMap var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2Vector3fMap(Long2ObjectMap var1) {
      super(var1);
   }

   public TagSerializableLong2Vector3fMap(Map var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2Vector3fMap(Map var1) {
      super(var1);
   }

   public byte getFactoryId() {
      return 5;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      serialize(var1, this, this.ignoreZeroLenVectors);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      serialize(var1, this, this.ignoreZeroLenVectors);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      deserialize(var1, this);
   }

   private static void serialize(DataOutput var0, Long2ObjectLinkedOpenHashMap var1, boolean var2) throws IOException {
      FastSortedEntrySet var6 = var1.long2ObjectEntrySet();
      int var3 = 0;
      Iterator var4 = var6.iterator();

      while(true) {
         Entry var5;
         do {
            if (!var4.hasNext()) {
               var0.writeInt(var3);
               if (var3 > 0) {
                  var4 = var6.iterator();

                  while(true) {
                     do {
                        if (!var4.hasNext()) {
                           return;
                        }

                        var5 = (Entry)var4.next();
                     } while(var2 && ((Vector3f)var5.getValue()).x == 0.0F && ((Vector3f)var5.getValue()).y == 0.0F && ((Vector3f)var5.getValue()).z == 0.0F);

                     var0.writeLong(var5.getLongKey());
                     Vector3fTools.serialize((Vector3f)var5.getValue(), var0);
                  }
               }

               return;
            }

            var5 = (Entry)var4.next();
         } while(var2 && ((Vector3f)var5.getValue()).x == 0.0F && ((Vector3f)var5.getValue()).y == 0.0F && ((Vector3f)var5.getValue()).z == 0.0F);

         ++var3;
      }
   }

   public static void deserialize(DataInput var0, Long2ObjectLinkedOpenHashMap var1) throws IOException {
      int var2 = var0.readInt();
      deserializeBody(var0, var1, var2);
   }

   public static void deserializeBody(DataInput var0, Long2ObjectLinkedOpenHashMap var1, int var2) throws IOException {
      for(int var3 = 0; var3 < var2; ++var3) {
         var1.put(var0.readLong(), Vector3fTools.deserialize(var0));
      }

   }
}
