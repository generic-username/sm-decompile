package org.schema.schine.resource.tag;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.longs.Long2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectSortedMap.FastSortedEntrySet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.schine.network.SerialializationInterface;

public class TagSerializableLong2TransformMap extends Long2ObjectLinkedOpenHashMap implements SerialializationInterface, SerializableTagElement {
   public boolean ignoreIdentTransforms = false;
   private static final long serialVersionUID = -1556336980069443312L;

   public TagSerializableLong2TransformMap() {
   }

   public TagSerializableLong2TransformMap(int var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2TransformMap(int var1) {
      super(var1);
   }

   public TagSerializableLong2TransformMap(long[] var1, Transform[] var2, float var3) {
      super(var1, var2, var3);
   }

   public TagSerializableLong2TransformMap(long[] var1, Transform[] var2) {
      super(var1, var2);
   }

   public TagSerializableLong2TransformMap(Long2ObjectMap var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2TransformMap(Long2ObjectMap var1) {
      super(var1);
   }

   public TagSerializableLong2TransformMap(Map var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLong2TransformMap(Map var1) {
      super(var1);
   }

   public byte getFactoryId() {
      return 6;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      serialize(var1, this, this.ignoreIdentTransforms);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      serialize(var1, this, this.ignoreIdentTransforms);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      deserialize(var1, this);
   }

   private static void serialize(DataOutput var0, Long2ObjectLinkedOpenHashMap var1, boolean var2) throws IOException {
      FastSortedEntrySet var3 = var1.long2ObjectEntrySet();
      ObjectArrayList var5 = new ObjectArrayList(var1.size());
      Iterator var6 = var3.iterator();

      while(true) {
         Entry var4;
         do {
            if (!var6.hasNext()) {
               var0.writeInt(var5.size());

               for(int var7 = 0; var7 < var5.size(); ++var7) {
                  var4 = (Entry)var5.get(var7);
                  var0.writeLong(var4.getLongKey());
                  TransformTools.serializeFully(var0, (Transform)var4.getValue());
               }

               return;
            }

            var4 = (Entry)var6.next();
         } while(var2 && ((Transform)var4.getValue()).equals(TransformTools.ident));

         var5.add(var4);
      }
   }

   public static void deserialize(DataInput var0, Long2ObjectLinkedOpenHashMap var1) throws IOException {
      int var2 = var0.readInt();
      deserializeBody(var0, var1, var2);
   }

   public static void deserializeBody(DataInput var0, Long2ObjectLinkedOpenHashMap var1, int var2) throws IOException {
      for(int var3 = 0; var3 < var2; ++var3) {
         long var4 = var0.readLong();
         Transform var6 = TransformTools.deserializeFully(var0, new Transform());
         var1.put(var4, var6);
      }

   }
}
