package org.schema.schine.resource.tag;

import java.io.DataInput;
import java.io.IOException;

public class TagSerializableLong2Vector3fMapFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      TagSerializableLong2Vector3fMap var3 = new TagSerializableLong2Vector3fMap(var2);
      TagSerializableLong2Vector3fMap.deserializeBody(var1, var3, var2);
      return var3;
   }
}
