package org.schema.schine.resource.tag;

import java.io.DataOutput;
import java.io.IOException;

public interface SerializableTagElement {
   byte CONTROL_ELEMENT_MAPPER = 0;
   byte ELEMENT_COUNT_MAP = 1;
   byte NPC_FACTION_NEWS_EVENT = 2;
   byte LONG_SET = 3;
   byte BLOCK_BUFFER = 4;
   byte LONG_2_VECTOR3f_MAP = 5;
   byte LONG_2_TRANSFORM_MAP = 6;

   byte getFactoryId();

   void writeToTag(DataOutput var1) throws IOException;
}
