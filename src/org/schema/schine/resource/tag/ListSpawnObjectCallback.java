package org.schema.schine.resource.tag;

public interface ListSpawnObjectCallback {
   Object get(Object var1);
}
