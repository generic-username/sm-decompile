package org.schema.schine.resource.tag;

import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import java.io.BufferedInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPInputStream;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;

public class Tag {
   private static TagTools tools;
   private static ObjectArrayList tagArrayPool = new ObjectArrayList(128);
   private long size;
   private Tag.Type type;
   private Tag.Type listType;
   private String name;
   private Object value;
   private short version;
   public static final String NULL_STRING = "null";

   public Tag(String var1, Tag.Type var2) {
      this(Tag.Type.LIST, var1, (Object)var2);
   }

   public Tag(String var1, boolean var2) {
      this(Tag.Type.BYTE, var1, (Object)Byte.valueOf((byte)(var2 ? 1 : 0)));
   }

   public Tag(Tag.Type var1, String var2, Object var3) {
      this.listType = null;
      switch(var1) {
      case FINISH:
         if (var3 != null) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case BYTE:
         if (!(var3 instanceof Byte)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case SHORT:
         if (!(var3 instanceof Short)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case INT:
         if (!(var3 instanceof Integer)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case LONG:
         if (!(var3 instanceof Long)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case FLOAT:
         if (!(var3 instanceof Float)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case DOUBLE:
         if (!(var3 instanceof Double)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case BYTE_ARRAY:
         if (!(var3 instanceof byte[])) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case STRING:
         if (!(var3 instanceof String)) {
            throw new IllegalArgumentException(var3 + "; " + (var3 != null ? var3.getClass().toString() : ""));
         }
         break;
      case VECTOR3f:
         if (!(var3 instanceof Vector3f)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case VECTOR3i:
         if (!(var3 instanceof Vector3i)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case VECTOR3b:
         if (!(var3 instanceof Vector3b)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case LIST:
         if (var3 instanceof Tag.Type) {
            this.listType = (Tag.Type)var3;
            var3 = new Tag[0];
         } else {
            if (!(var3 instanceof Tag[])) {
               throw new IllegalArgumentException();
            }

            this.listType = ((Tag[])var3)[0].getType();
         }
         break;
      case STRUCT:
         if (!(var3 instanceof Tag[])) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }

         assert this.notNull((Tag[])var3) : var3;
         break;
      case SERIALIZABLE:
         if (!(var3 instanceof SerializableTagElement)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case VECTOR4f:
         if (!(var3 instanceof Vector4f)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case MATRIX4f:
         if (!(var3 instanceof Matrix4f)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
         break;
      case MATRIX3f:
         if (!(var3 instanceof Matrix3f)) {
            throw new IllegalArgumentException(var3.getClass().getSimpleName());
         }
      case NOTHING:
         break;
      default:
         throw new IllegalArgumentException();
      }

      this.type = var1;
      this.name = var2;
      this.value = var3;
   }

   public Tag(Tag.Type var1, String var2, Tag[] var3) {
      this(var1, var2, (Object)var3);

      assert var1 == Tag.Type.FINISH || var1 == Tag.Type.NOTHING || var3[var3.length - 1].getType() == Tag.Type.FINISH;

   }

   private static String getTypeString(Tag.Type var0) {
      switch(var0) {
      case FINISH:
         return "TAG_End";
      case BYTE:
         return "TAG_Byte";
      case SHORT:
         return "TAG_Short";
      case INT:
         return "TAG_Int";
      case LONG:
         return "TAG_Long";
      case FLOAT:
         return "TAG_Float";
      case DOUBLE:
         return "TAG_Double";
      case BYTE_ARRAY:
         return "TAG_Byte_Array";
      case STRING:
         return "TAG_String";
      case VECTOR3f:
         return "TAG_Vector3f";
      case VECTOR3i:
         return "TAG_Vector3i";
      case VECTOR3b:
         return "TAG_Vector3b";
      case LIST:
         return "TAG_List";
      case STRUCT:
         return "TAG_Compound";
      case SERIALIZABLE:
         return "TAG_Serializable";
      case VECTOR4f:
         return "TAG_Vector4f";
      case MATRIX4f:
         return "TAG_Matrix4f";
      case MATRIX3f:
         return "TAG_Matrix3f";
      case NOTHING:
         return "TAG_NOTHING";
      default:
         return null;
      }
   }

   public static short[] shortArrayFromTagStruct(Tag var0) {
      Tag[] var3 = (Tag[])var0.getValue();

      assert var3[var3.length - 1].getType() == Tag.Type.FINISH;

      short[] var1 = new short[var3.length - 1];

      for(int var2 = 0; var2 < var3.length - 1; ++var2) {
         var1[var2] = (Short)var3[var2].getValue();
      }

      return var1;
   }

   public static void listFromTagStruct(Collection var0, Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      listFromTagStruct(var0, var2);
   }

   public static void listFromTagStruct(Collection var0, Tag var1, ListSpawnObjectCallback var2) {
      Tag[] var3 = (Tag[])var1.getValue();
      listFromTagStruct(var0, var3, var2);
   }

   public static void listFromTagStructSP(Collection var0, Tag var1, ListSpawnObjectCallback var2) {
      Tag[] var3 = (Tag[])var1.getValue();
      listFromTagStructSP(var0, var3, var2);
   }

   public static void listFromTagStructSP(Collection var0, Tag[] var1, ListSpawnObjectCallback var2) {
      assert var1[var1.length - 1].getType() == Tag.Type.FINISH;

      for(int var3 = 0; var3 < var1.length - 1; ++var3) {
         var0.add(var2.get(var1[var3]));
      }

   }

   public static void listFromTagStructSPElimDouble(Collection var0, Tag var1, ListSpawnObjectCallback var2) {
      Tag[] var3 = (Tag[])var1.getValue();
      listFromTagStructSP(var0, var3, var2);
   }

   public static void listFromTagStructSPElimDouble(Collection var0, Tag[] var1, ListSpawnObjectCallback var2) {
      assert var1[var1.length - 1].getType() == Tag.Type.FINISH;

      for(int var3 = 0; var3 < var1.length - 1; ++var3) {
         Object var4 = var2.get(var1[var3]);
         if (!var0.contains(var4)) {
            var0.add(var4);
         }
      }

   }

   public static void listFromTagStruct(Collection var0, Tag[] var1, ListSpawnObjectCallback var2) {
      assert var1[var1.length - 1].getType() == Tag.Type.FINISH;

      for(int var3 = 0; var3 < var1.length - 1; ++var3) {
         var0.add(var2.get(var1[var3].getValue()));
      }

   }

   public static void listFromTagStruct(Collection var0, Tag[] var1) {
      assert var1[var1.length - 1].getType() == Tag.Type.FINISH;

      for(int var2 = 0; var2 < var1.length - 1; ++var2) {
         var0.add(var1[var2].getValue());
      }

   }

   public static void listFromTagStruct(Constructor var0, Collection var1, Tag[] var2) {
      assert var2[var2.length - 1].getType() == Tag.Type.FINISH;

      for(int var3 = 0; var3 < var2.length - 1; ++var3) {
         TagSerializable var4;
         try {
            (var4 = (TagSerializable)var0.newInstance()).fromTagStructure(var2[var3]);
            var1.add(var4);
         } catch (IllegalArgumentException var5) {
            var4 = null;
            var5.printStackTrace();
         } catch (InstantiationException var6) {
            var4 = null;
            var6.printStackTrace();
         } catch (IllegalAccessException var7) {
            var4 = null;
            var7.printStackTrace();
         } catch (InvocationTargetException var8) {
            var4 = null;
            var8.printStackTrace();
         }
      }

   }

   public static Tag listToTagStruct(Collection var0, Tag.Type var1, String var2) {
      Tag[] var3;
      (var3 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;
      int var4 = 0;

      for(Iterator var6 = var0.iterator(); var6.hasNext(); ++var4) {
         Object var5 = var6.next();
         var3[var4] = new Tag(var1, (String)null, var5);
      }

      return new Tag(Tag.Type.STRUCT, var2, var3);
   }

   public static Tag listToTagStruct(short[] var0, String var1) {
      Tag[] var2;
      (var2 = new Tag[var0.length + 1])[var0.length] = FinishTag.INST;
      int var3 = 0;
      int var4 = (var0 = var0).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         Short var6 = var0[var5];
         var2[var3] = new Tag(Tag.Type.SHORT, (String)null, var6);
         ++var3;
      }

      return new Tag(Tag.Type.STRUCT, var1, var2);
   }

   public static void fromString2IntMapToTagStruct(Tag var0, Object2IntMap var1) {
      Tag[] var4 = var0.getStruct();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         Tag[] var3 = var4[var2].getStruct();
         var1.put(var3[0].getString(), var3[1].getInt());
      }

   }

   public static Tag string2IntMapToTagStruct(Object2IntMap var0) {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[var0.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var3;
      for(Iterator var4 = var0.entrySet().iterator(); var4.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var3.getKey()), new Tag(Tag.Type.INT, (String)null, var3.getValue()), FinishTag.INST})) {
         var3 = (Entry)var4.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public static void fromShort2StringMapToTagStruct(Tag var0, Short2ObjectMap var1) {
      Tag[] var4 = var0.getStruct();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         Tag[] var3 = var4[var2].getStruct();
         var1.put(var3[0].getShort(), var3[1].getString());
      }

   }

   public static Tag short2StringMapToTagStruct(Short2ObjectMap var0) {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[var0.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      Entry var3;
      for(Iterator var4 = var0.entrySet().iterator(); var4.hasNext(); var1[var2++] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, var3.getKey()), new Tag(Tag.Type.STRING, (String)null, var3.getValue()), FinishTag.INST})) {
         var3 = (Entry)var4.next();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public static Tag listToTagStruct(Object[] var0, Tag.Type var1, String var2) {
      Tag[] var3;
      (var3 = new Tag[var0.length + 1])[var0.length] = FinishTag.INST;
      int var4 = 0;
      int var5 = (var0 = var0).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         Object var7 = var0[var6];
         var3[var4] = new Tag(var1, (String)null, var7);
         ++var4;
      }

      return new Tag(Tag.Type.STRUCT, var2, var3);
   }

   public static Tag listToTagStruct(Collection var0, String var1) {
      Tag[] var2;
      (var2 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;
      int var3 = 0;

      for(Iterator var5 = var0.iterator(); var5.hasNext(); ++var3) {
         TagSerializable var4 = (TagSerializable)var5.next();
         var2[var3] = var4.toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, var1, var2);
   }

   public static Tag listToTagStruct(Long2ObjectOpenHashMap var0, String var1) {
      Tag[] var2;
      (var2 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;
      int var3 = 0;

      for(Iterator var6 = var0.entrySet().iterator(); var6.hasNext(); ++var3) {
         Entry var4 = (Entry)var6.next();
         Tag[] var5;
         (var5 = new Tag[3])[0] = new Tag(Tag.Type.LONG, (String)null, var4.getKey());
         var5[1] = new Tag(Tag.Type.STRING, (String)null, var4.getValue());
         var5[2] = FinishTag.INST;
         var2[var3] = new Tag(Tag.Type.STRUCT, (String)null, var5);
      }

      return new Tag(Tag.Type.STRUCT, var1, var2);
   }

   public static Tag listToTagStruct(Long2LongOpenHashMap var0, String var1) {
      Tag[] var2;
      (var2 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;
      int var3 = 0;

      for(Iterator var6 = var0.entrySet().iterator(); var6.hasNext(); ++var3) {
         Entry var4 = (Entry)var6.next();
         Tag[] var5;
         (var5 = new Tag[3])[0] = new Tag(Tag.Type.LONG, (String)null, var4.getKey());
         var5[1] = new Tag(Tag.Type.LONG, (String)null, var4.getValue());
         var5[2] = FinishTag.INST;
         var2[var3] = new Tag(Tag.Type.STRUCT, (String)null, var5);
      }

      return new Tag(Tag.Type.STRUCT, var1, var2);
   }

   public static Tag listToTagStruct(List var0, Tag.Type var1, String var2) {
      Tag[] var3;
      (var3 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;

      for(int var4 = 0; var4 < var0.size(); ++var4) {
         var3[var4] = new Tag(var1, (String)null, var0.get(var4));
      }

      return new Tag(Tag.Type.STRUCT, var2, var3);
   }

   public static Tag listToTagStructUsing(List var0, Tag.Type var1, String var2, ListObjectCallback var3) {
      Tag[] var4;
      (var4 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;

      for(int var5 = 0; var5 < var0.size(); ++var5) {
         var4[var5] = new Tag(var1, (String)null, var3.get(var0.get(var5)));
      }

      return new Tag(Tag.Type.STRUCT, var2, var4);
   }

   public static Tag listToTagStruct(List var0, String var1) {
      Tag[] var2;
      (var2 = new Tag[var0.size() + 1])[var0.size()] = FinishTag.INST;

      for(int var3 = 0; var3 < var0.size(); ++var3) {
         var2[var3] = ((TagSerializable)var0.get(var3)).toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, var1, var2);
   }

   public static Tag readFrom(InputStream var0, boolean var1, boolean var2) throws IOException {
      ProfiledDataInputStream var3 = null;
      PushbackInputStream var15 = new PushbackInputStream(var0, 2);
      byte[] var4 = new byte[2];
      var15.read(var4);
      var15.unread(var4);
      Object var16;
      DataInput var17;
      if (var4[0] == 31 && var4[1] == -117) {
         var17 = (DataInput)(var16 = new DataInputStream(new GZIPInputStream(var15, 4096)));
      } else {
         if (var2) {
            System.err.println("RECORDING SIZE!!!");
            var3 = (ProfiledDataInputStream)(var16 = new ProfiledDataInputStream(new DataInputStream(new BufferedInputStream(var15, 65536))));
            var17 = (DataInput)var16;
         } else {
            var17 = (DataInput)(var16 = new DataInputStream(new BufferedInputStream(var15, 65536)));
         }

         var17.readShort();
      }

      byte var18;
      byte var5 = (byte)Math.abs(var18 = var17.readByte());
      boolean var19 = var18 > 0;
      Object var6 = null;
      if (var5 == 0) {
         var6 = FinishTag.INST;
      } else {
         String var7 = null;
         if (var19) {
            var7 = var17.readUTF();
         }

         try {
            long var8 = System.currentTimeMillis();
            long var10 = 0L;
            if (var3 != null) {
               var10 = var3.getReadSize();
            }

            var6 = new Tag(Tag.Type.values()[var5], var7, readPayload(var17, var5, var3));
            if (var3 != null) {
               ((Tag)var6).size = var3.getReadSize() - var10;
            }

            long var12;
            if ((var12 = System.currentTimeMillis() - var8) > 100L) {
               System.err.println("[TAG] warning: reading tag: " + var7 + " took: " + var12);
            }
         } catch (IOException var14) {
            System.err.println("EXCEPTION WHILE READING TAG " + var7);
            throw var14;
         }
      }

      if (var1) {
         ((InputStream)var16).close();
      }

      return (Tag)var6;
   }

   public static Tag readFromZipped(InputStream var0, boolean var1) throws IOException {
      DataInputStream var9;
      long var3 = (var9 = new DataInputStream(new GZIPInputStream(var0))).readLong();
      int var2 = var9.readInt();
      if (tools == null) {
         tools = new TagTools();
      }

      synchronized(tools.inflateBuffer) {
         var9.readFully(tools.inflateBuffer, 0, var2);
         tools.inflater.setInput(tools.inflateBuffer, 0, var2);

         try {
            tools.inflater.inflate(tools.inputBuffer, 0, (int)var3);
         } catch (DataFormatException var7) {
            var7.printStackTrace();
         }

         byte var10;
         byte var13 = (byte)Math.abs(var10 = (var9 = new DataInputStream(tools.input)).readByte());
         boolean var11 = var10 > 0;
         Object var12;
         if (var13 == 0) {
            var12 = FinishTag.INST;
         } else {
            String var4 = null;
            if (var11) {
               var4 = var9.readUTF();
            }

            try {
               var12 = new Tag(Tag.Type.values()[var13], var4, readPayload(var9, var13, (ProfiledDataInputStream)null));
            } catch (IOException var6) {
               System.err.println("EXCEPTION WHILE READING TAG " + var4);
               throw var6;
            }
         }

         if (var1) {
            var9.close();
         }

         return (Tag)var12;
      }
   }

   public static Tag[] getTagArray() {
      synchronized(tagArrayPool) {
         return tagArrayPool.isEmpty() ? new Tag[128] : (Tag[])tagArrayPool.remove(tagArrayPool.size() - 1);
      }
   }

   public static void freeTagArray(Tag[] var0, int var1) {
      synchronized(tagArrayPool) {
         Arrays.fill(var0, 0, var1, (Object)null);
         tagArrayPool.add(var0);
      }
   }

   private static void cleanTagArray() {
      synchronized(tagArrayPool) {
         while(tagArrayPool.size() > 32) {
            tagArrayPool.remove(tagArrayPool.size() - 1);
         }

      }
   }

   private static Object readPayload(DataInput var0, byte var1, ProfiledDataInputStream var2) throws IOException {
      cleanTagArray();
      int var3;
      long var8;
      Tag[] var22;
      switch(var1) {
      case 0:
         return null;
      case 1:
         return var0.readByte();
      case 2:
         return var0.readShort();
      case 3:
         return var0.readInt();
      case 4:
         return var0.readLong();
      case 5:
         return var0.readFloat();
      case 6:
         return var0.readDouble();
      case 7:
         long var21 = System.currentTimeMillis();
         byte[] var20 = new byte[var0.readInt()];
         var0.readFully(var20);
         long var6;
         if ((var6 = System.currentTimeMillis() - var21) > 30L) {
            System.err.println("[TAG] WARNING Byte DESERIALIZATION took too long: " + var6 + "; ");
         }

         return var20;
      case 8:
         return var0.readUTF();
      case 9:
         return new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat());
      case 10:
         return new Vector3i(var0.readInt(), var0.readInt(), var0.readInt());
      case 11:
         return new Vector3b(var0.readByte(), var0.readByte(), var0.readByte());
      case 12:
         var1 = var0.readByte();
         var22 = new Tag[var3 = var0.readInt()];

         for(int var23 = 0; var23 < var3; ++var23) {
            var8 = 0L;
            if (var2 != null) {
               var8 = var2.getReadSize();
            }

            var22[var23] = new Tag(Tag.Type.values()[var1], (String)null, readPayload(var0, var1, var2));
            if (var2 != null) {
               var22[var23].size = var2.getReadSize() - var8;
            }
         }

         if (var22.length == 0) {
            return Tag.Type.values()[var1];
         }

         return var22;
      case 13:
         var8 = System.currentTimeMillis();
         Tag[] var19 = getTagArray();
         var3 = 0;

         byte var5;
         do {
            if (var3 == var19.length) {
               Tag[] var10 = new Tag[var3 << 1];
               System.arraycopy(var19, 0, var10, 0, var3);
               freeTagArray(var19, var3);
               var19 = var10;
            }

            byte var4;
            var5 = (byte)Math.abs(var4 = var0.readByte());
            boolean var24 = var4 > 0;
            String var11 = null;
            if (var24) {
               var11 = var0.readUTF();
            }

            long var26 = 0L;
            if (var2 != null) {
               var26 = var2.getReadSize();
            }

            long var27 = System.currentTimeMillis();

            try {
               var19[var3] = new Tag(Tag.Type.values()[var5], var11, readPayload(var0, var5, var2));
            } catch (IOException var18) {
               System.err.println("EXCEPTION IN " + var11 + "; array index: " + var3);
               throw var18;
            }

            assert var19[var3] != null;

            if (var2 != null) {
               var19[var3].size = var2.getReadSize() - var26;
            }

            long var16;
            if ((var16 = System.currentTimeMillis() - var27) > 100L) {
               System.err.println("[TAG] WARNING: Struct read time of " + var11 + " took: " + var16);
            }

            ++var3;
         } while(var5 != 0);

         var22 = new Tag[var3];
         System.arraycopy(var19, 0, var22, 0, var3);
         freeTagArray(var19, var3);
         long var25;
         if ((var25 = System.currentTimeMillis() - var8) > 30L) {
            System.err.println("[TAG] WARNING STRUCT DESERIALIZATION took too long: " + var25 + "; ");
         }

         return var22;
      case 14:
         byte var12 = var0.readByte();
         return SerializableTagRegister.register[var12].create(var0);
      case 15:
         return new Vector4f(var0.readFloat(), var0.readFloat(), var0.readFloat(), var0.readFloat());
      case 16:
         Matrix4f var13;
         (var13 = new Matrix4f()).m00 = var0.readFloat();
         var13.m01 = var0.readFloat();
         var13.m02 = var0.readFloat();
         var13.m03 = var0.readFloat();
         var13.m10 = var0.readFloat();
         var13.m11 = var0.readFloat();
         var13.m12 = var0.readFloat();
         var13.m13 = var0.readFloat();
         var13.m20 = var0.readFloat();
         var13.m21 = var0.readFloat();
         var13.m22 = var0.readFloat();
         var13.m23 = var0.readFloat();
         var13.m30 = var0.readFloat();
         var13.m31 = var0.readFloat();
         var13.m32 = var0.readFloat();
         var13.m33 = var0.readFloat();
         return var13;
      case 17:
         return null;
      case 18:
         Matrix3f var14;
         (var14 = new Matrix3f()).m00 = var0.readFloat();
         var14.m01 = var0.readFloat();
         var14.m02 = var0.readFloat();
         var14.m10 = var0.readFloat();
         var14.m11 = var0.readFloat();
         var14.m12 = var0.readFloat();
         var14.m20 = var0.readFloat();
         var14.m21 = var0.readFloat();
         var14.m22 = var0.readFloat();
         return var14;
      default:
         return null;
      }
   }

   public static boolean check(File var0) {
      try {
         readFrom(new BufferedInputStream(new FileInputStream(var0)), true, false);
         return true;
      } catch (IOException var1) {
         var1.printStackTrace();
         return false;
      }
   }

   private boolean notNull(Tag[] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         if (var1[var2] == null) {
            System.err.println("FATAL Exception: INDEX " + var2 + " IS NULL");
            return false;
         }
      }

      return true;
   }

   public void addTag(Tag var1) {
      if (this.type != Tag.Type.LIST && this.type != Tag.Type.STRUCT) {
         throw new RuntimeException();
      } else {
         int var2 = ((Tag[])this.value).length;
         if (this.type == Tag.Type.STRUCT) {
            --var2;
         }

         this.insertTag(var1, var2);
      }
   }

   public Tag findNextTagByName(String var1, Tag var2) {
      if (this.type != Tag.Type.LIST && this.type != Tag.Type.STRUCT) {
         return null;
      } else {
         Tag[] var3;
         int var4 = (var3 = (Tag[])this.value).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            Tag var6;
            if ((var6 = var3[var5]).name == null && var1 == null || var6.name != null && var6.name.equals(var1)) {
               return var6;
            }

            if ((var6 = var6.findTagByName(var1)) != null && var6 != var2) {
               return var6;
            }
         }

         return null;
      }
   }

   public Tag findTagByName(String var1) {
      return this.findNextTagByName(var1, (Tag)null);
   }

   public Tag.Type getListType() {
      return this.listType;
   }

   public String getName() {
      return this.name;
   }

   public Tag.Type getType() {
      return this.type;
   }

   public Object getValue() {
      return this.value;
   }

   public void setValue(Object var1) {
      switch(this.type) {
      case FINISH:
         if (var1 != null) {
            throw new IllegalArgumentException();
         }
         break;
      case BYTE:
         if (!(var1 instanceof Byte)) {
            throw new IllegalArgumentException();
         }
         break;
      case SHORT:
         if (!(var1 instanceof Short)) {
            throw new IllegalArgumentException();
         }
         break;
      case INT:
         if (!(var1 instanceof Integer)) {
            throw new IllegalArgumentException();
         }
         break;
      case LONG:
         if (!(var1 instanceof Long)) {
            throw new IllegalArgumentException();
         }
         break;
      case FLOAT:
         if (!(var1 instanceof Float)) {
            throw new IllegalArgumentException();
         }
         break;
      case DOUBLE:
         if (!(var1 instanceof Double)) {
            throw new IllegalArgumentException();
         }
         break;
      case BYTE_ARRAY:
         if (!(var1 instanceof byte[])) {
            throw new IllegalArgumentException();
         }
         break;
      case STRING:
         if (!(var1 instanceof String)) {
            throw new IllegalArgumentException();
         }
         break;
      case VECTOR3f:
         if (!(var1 instanceof Vector3f)) {
            throw new IllegalArgumentException();
         }
         break;
      case VECTOR3i:
         if (!(var1 instanceof Vector3i)) {
            throw new IllegalArgumentException();
         }
         break;
      case VECTOR3b:
         if (!(var1 instanceof Vector3b)) {
            throw new IllegalArgumentException();
         }
         break;
      case LIST:
         if (var1 instanceof Tag.Type) {
            this.listType = (Tag.Type)var1;
            var1 = new Tag[0];
         } else {
            if (!(var1 instanceof Tag[])) {
               throw new IllegalArgumentException();
            }

            this.listType = ((Tag[])var1)[0].getType();
         }
         break;
      case STRUCT:
         if (!(var1 instanceof Tag[])) {
            throw new IllegalArgumentException();
         }
         break;
      case SERIALIZABLE:
         if (!(var1 instanceof SerializableTagElement)) {
            throw new IllegalArgumentException();
         }
         break;
      case VECTOR4f:
         if (!(var1 instanceof Vector4f)) {
            throw new IllegalArgumentException();
         }
         break;
      case MATRIX4f:
         if (!(var1 instanceof Matrix4f)) {
            throw new IllegalArgumentException();
         }
         break;
      case MATRIX3f:
         if (!(var1 instanceof Matrix3f)) {
            throw new IllegalArgumentException();
         }
      case NOTHING:
         break;
      default:
         throw new IllegalArgumentException();
      }

      this.value = var1;
   }

   private void indent(int var1) {
      for(int var2 = 0; var2 < var1; ++var2) {
         System.out.print("   ");
      }

   }

   public void insertTag(Tag var1, int var2) {
      if (this.type != Tag.Type.LIST && this.type != Tag.Type.STRUCT) {
         throw new RuntimeException();
      } else {
         Tag[] var3;
         if ((var3 = (Tag[])this.value).length > 0 && this.type == Tag.Type.LIST && var1.getType() != this.getListType()) {
            throw new IllegalArgumentException();
         } else if (var2 > var3.length) {
            throw new IndexOutOfBoundsException();
         } else {
            Tag[] var4 = new Tag[var3.length + 1];
            System.arraycopy(var3, 0, var4, 0, var2);
            var4[var2] = var1;
            System.arraycopy(var3, var2, var4, var2 + 1, var3.length - var2);
            this.value = var4;
         }
      }
   }

   public void print() {
      this.print(this, 0);
   }

   private void print(Tag var1, int var2) {
      Tag.Type var3;
      if ((var3 = var1.getType()) != Tag.Type.FINISH) {
         String var4 = var1.getName();
         this.indent(var2);
         System.out.print(getTypeString(var1.getType()));
         if (var4 != null) {
            System.out.print("(\"" + var1.getName() + "\")");
         }

         if (var3 == Tag.Type.BYTE_ARRAY) {
            byte[] var10 = (byte[])var1.getValue();
            System.out.println(": [" + var10.length + " bytes]");
         } else {
            Tag var5;
            Tag[] var6;
            Tag[] var7;
            int var8;
            int var9;
            if (var3 == Tag.Type.LIST) {
               var7 = (Tag[])var1.getValue();
               System.out.println(": " + var7.length + " entries of type " + getTypeString(var1.getListType()));
               var6 = var7;
               var8 = var7.length;

               for(var9 = 0; var9 < var8; ++var9) {
                  var5 = var6[var9];
                  this.print(var5, var2 + 1);
               }

               this.indent(var2);
               System.out.println("}");
            } else if (var3 != Tag.Type.STRUCT) {
               System.out.println(": " + var1.getValue());
            } else {
               var7 = (Tag[])var1.getValue();
               System.out.println(": " + (var7.length - 1) + " entries");
               this.indent(var2);
               System.out.println("{");
               var6 = var7;
               var8 = var7.length;

               for(var9 = 0; var9 < var8; ++var9) {
                  var5 = var6[var9];
                  this.print(var5, var2 + 1);
               }

               this.indent(var2);
               System.out.println("}");
            }
         }
      }
   }

   public void removeSubTag(Tag var1) {
      if (this.type != Tag.Type.LIST && this.type != Tag.Type.STRUCT) {
         throw new RuntimeException();
      } else if (var1 != null) {
         Tag[] var2 = (Tag[])this.value;

         for(int var3 = 0; var3 < var2.length; ++var3) {
            if (var2[var3] == var1) {
               this.removeTag(var3);
               return;
            }

            if (var2[var3].type == Tag.Type.LIST || var2[var3].type == Tag.Type.STRUCT) {
               var2[var3].removeSubTag(var1);
            }
         }

      }
   }

   public Tag removeTag(int var1) {
      if (this.type != Tag.Type.LIST && this.type != Tag.Type.STRUCT) {
         throw new RuntimeException();
      } else {
         Tag[] var2;
         Tag var3 = (var2 = (Tag[])this.value)[var1];
         Tag[] var4 = new Tag[var2.length - 1];
         System.arraycopy(var2, 0, var4, 0, var1);
         ++var1;
         System.arraycopy(var2, var1, var4, var1 - 1, var2.length - var1);
         this.value = var4;
         return var3;
      }
   }

   public String toString() {
      return this.type.name() + ": " + this.name + "->" + (this.getValue() == null ? "null" : (this.getValue() instanceof Tag[] ? "STRUCT" : this.getValue())) + "(size: " + this.size + ")";
   }

   private void writePayload(DataOutput var1) throws IOException {
      int var11;
      switch(this.type) {
      case FINISH:
         return;
      case BYTE:
         var1.writeByte((Byte)this.value);
         return;
      case SHORT:
         var1.writeShort((Short)this.value);
         return;
      case INT:
         var1.writeInt((Integer)this.value);
         return;
      case LONG:
         var1.writeLong((Long)this.value);
         return;
      case FLOAT:
         var1.writeFloat((Float)this.value);
         return;
      case DOUBLE:
         var1.writeDouble((Double)this.value);
         return;
      case BYTE_ARRAY:
         byte[] var8 = (byte[])this.value;
         var1.writeInt(var8.length);
         var1.write(var8);
         return;
      case STRING:
         assert !((String)this.value).equals("null");

         var1.writeUTF((String)this.value);
         return;
      case VECTOR3f:
         var1.writeFloat(((Vector3f)this.value).x);
         var1.writeFloat(((Vector3f)this.value).y);
         var1.writeFloat(((Vector3f)this.value).z);
         return;
      case VECTOR3i:
         var1.writeInt(((Vector3i)this.value).x);
         var1.writeInt(((Vector3i)this.value).y);
         var1.writeInt(((Vector3i)this.value).z);
         return;
      case VECTOR3b:
         var1.write(((Vector3b)this.value).x);
         var1.write(((Vector3b)this.value).y);
         var1.write(((Vector3b)this.value).z);
         return;
      case LIST:
         Tag[] var7 = (Tag[])this.value;
         var1.writeByte(this.getListType().ordinal());
         var1.writeInt(var7.length);
         int var10 = (var7 = var7).length;

         for(var11 = 0; var11 < var10; ++var11) {
            var7[var11].writePayload(var1);
         }

         return;
      case STRUCT:
         Tag[] var10000 = (Tag[])this.value;
         Tag var2 = null;
         Tag[] var9 = var10000;
         var11 = var10000.length;

         for(int var5 = 0; var5 < var11; ++var5) {
            Tag.Type var6 = (var2 = var9[var5]).getType();
            var1.writeByte(var2.getName() != null && !var2.getName().equals("null") ? var6.ordinal() : -var6.ordinal());
            if (var6 != Tag.Type.FINISH) {
               if (var2.getName() != null && !var2.getName().equals("null")) {
                  var1.writeUTF(var2.getName());
               }

               var2.writePayload(var1);
            }
         }

         return;
      case SERIALIZABLE:
         var1.writeByte(((SerializableTagElement)this.value).getFactoryId());
         ((SerializableTagElement)this.value).writeToTag(var1);
         return;
      case VECTOR4f:
         var1.writeFloat(((Vector4f)this.value).x);
         var1.writeFloat(((Vector4f)this.value).y);
         var1.writeFloat(((Vector4f)this.value).z);
         var1.writeFloat(((Vector4f)this.value).w);
         return;
      case MATRIX4f:
         Matrix4f var3 = (Matrix4f)this.value;
         var1.writeFloat(var3.m00);
         var1.writeFloat(var3.m01);
         var1.writeFloat(var3.m02);
         var1.writeFloat(var3.m03);
         var1.writeFloat(var3.m10);
         var1.writeFloat(var3.m11);
         var1.writeFloat(var3.m12);
         var1.writeFloat(var3.m13);
         var1.writeFloat(var3.m20);
         var1.writeFloat(var3.m21);
         var1.writeFloat(var3.m22);
         var1.writeFloat(var3.m23);
         var1.writeFloat(var3.m30);
         var1.writeFloat(var3.m31);
         var1.writeFloat(var3.m32);
         var1.writeFloat(var3.m33);
         return;
      case MATRIX3f:
         Matrix3f var4 = (Matrix3f)this.value;
         var1.writeFloat(var4.m00);
         var1.writeFloat(var4.m01);
         var1.writeFloat(var4.m02);
         var1.writeFloat(var4.m10);
         var1.writeFloat(var4.m11);
         var1.writeFloat(var4.m12);
         var1.writeFloat(var4.m20);
         var1.writeFloat(var4.m21);
         var1.writeFloat(var4.m22);
      default:
         return;
      case NOTHING:
      }
   }

   public static Tag deserializeNT(DataInput var0) throws IOException {
      Tag.Type var1 = Tag.Type.values()[var0.readByte()];
      return new Tag(var1, (String)null, readPayload(var0, (byte)var1.ordinal(), (ProfiledDataInputStream)null));
   }

   public void serializeNT(DataOutput var1) throws IOException {
      var1.writeByte(this.type.ordinal());
      this.writePayload(var1);
   }

   public void writeTo(OutputStream var1, boolean var2) throws IOException {
      DataOutputStream var3;
      if (var1 instanceof DataOutputStream) {
         var3 = (DataOutputStream)var1;
      } else {
         var3 = new DataOutputStream(var1);
      }

      var3.writeShort(this.version);
      var3.writeByte(this.name != null && !this.name.equals("null") ? this.type.ordinal() : -this.type.ordinal());
      if (this.type != Tag.Type.FINISH) {
         if (this.name != null && !this.name.equals("null")) {
            var3.writeUTF(this.name);
         }

         this.writePayload(var3);
      }

      if (var2) {
         var1.close();
      }

   }

   public void writeToZipped(OutputStream var1, boolean var2) throws IOException {
      if (tools == null) {
         tools = new TagTools();
      }

      DataOutputStream var3 = new DataOutputStream(tools.output);
      synchronized(tools.output) {
         tools.output.reset();
         DataOutputStream var5 = new DataOutputStream(tools.output);
         var3.writeByte(this.name != null && !this.name.equals("null") ? this.type.ordinal() : -this.type.ordinal());
         if (this.type != Tag.Type.FINISH) {
            if (this.name != null && !this.name.equals("null")) {
               var3.writeUTF(this.name);
            }

            this.writePayload(var5);
         }

         var3.writeLong(tools.output.position());
         tools.deflater.setInput(tools.output.array, 0, (int)tools.output.position());
         int var7 = tools.deflater.deflate(tools.deflateBuffer);
         var3.writeInt(var7);
         var3.write(tools.deflateBuffer, 0, var7);
      }

      var1.flush();
      if (var2) {
         var1.close();
      }

   }

   public long getSize() {
      return this.size;
   }

   public byte getByte() {
      assert this.type == Tag.Type.BYTE : this.type;

      return (Byte)this.getValue();
   }

   public short getShort() {
      assert this.type == Tag.Type.SHORT : this.type;

      return (Short)this.getValue();
   }

   public int getInt() {
      assert this.type == Tag.Type.INT : this.type;

      return (Integer)this.getValue();
   }

   public long getLong() {
      assert this.type == Tag.Type.LONG : this.type;

      return (Long)this.getValue();
   }

   public float getFloat() {
      assert this.type == Tag.Type.FLOAT : this.type;

      return (Float)this.getValue();
   }

   public double getDouble() {
      assert this.type == Tag.Type.DOUBLE : this.type;

      return (Double)this.getValue();
   }

   public String getString() {
      assert this.type == Tag.Type.STRING : this.type;

      return (String)this.getValue();
   }

   public Tag[] getStruct() {
      assert this.type == Tag.Type.STRUCT : this.type;

      return (Tag[])this.getValue();
   }

   public Vector3i getVector3i() {
      assert this.type == Tag.Type.VECTOR3i : this.type;

      return (Vector3i)this.getValue();
   }

   public Vector3f getVector3f() {
      assert this.type == Tag.Type.VECTOR3f : this.type;

      return (Vector3f)this.getValue();
   }

   public byte[] getByteArray() {
      assert this.type == Tag.Type.BYTE_ARRAY : this.type;

      return (byte[])this.getValue();
   }

   public Matrix4f getMatrix4f() {
      assert this.type == Tag.Type.MATRIX4f : this.type;

      return (Matrix4f)this.getValue();
   }

   public Matrix3f getMatrix3f() {
      assert this.type == Tag.Type.MATRIX3f : this.type;

      return (Matrix3f)this.getValue();
   }

   public boolean getBoolean() {
      return this.getByte() != 0;
   }

   public static Tag getStringTag(String var0) {
      return new Tag(Tag.Type.STRING, (String)null, var0);
   }

   public static Tag getFloatTag(float var0) {
      return new Tag(Tag.Type.FLOAT, (String)null, var0);
   }

   public static Tag getLongTag(long var0) {
      return new Tag(Tag.Type.LONG, (String)null, var0);
   }

   public static Tag getIntTag(int var0) {
      return new Tag(Tag.Type.INT, (String)null, var0);
   }

   public static Tag getByteTag(byte var0) {
      return new Tag(Tag.Type.BYTE, (String)null, var0);
   }

   public static Tag getBooleanTag(boolean var0) {
      return new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(var0 ? 1 : 0)));
   }

   public static enum Type {
      FINISH,
      BYTE,
      SHORT,
      INT,
      LONG,
      FLOAT,
      DOUBLE,
      BYTE_ARRAY,
      STRING,
      VECTOR3f,
      VECTOR3i,
      VECTOR3b,
      LIST,
      STRUCT,
      SERIALIZABLE,
      VECTOR4f,
      MATRIX4f,
      NOTHING,
      MATRIX3f;
   }
}
