package org.schema.schine.resource.tag;

public interface TagSerializable {
   void fromTagStructure(Tag var1);

   Tag toTagStructure();
}
