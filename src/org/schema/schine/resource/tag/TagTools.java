package org.schema.schine.resource.tag;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class TagTools {
   public byte[] inflateBuffer = new byte[5242880];
   public byte[] inputBuffer = new byte[5242880];
   public FastByteArrayInputStream input;
   public byte[] deflateBuffer;
   public byte[] outputBuffer;
   public FastByteArrayOutputStream output;
   public Deflater deflater;
   public Inflater inflater;

   public TagTools() {
      this.input = new FastByteArrayInputStream(this.inputBuffer);
      this.deflateBuffer = new byte[5242880];
      this.outputBuffer = new byte[5242880];
      this.output = new FastByteArrayOutputStream(this.outputBuffer);
      this.deflater = new Deflater();
      this.inflater = new Inflater();
   }
}
