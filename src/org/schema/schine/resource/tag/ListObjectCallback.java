package org.schema.schine.resource.tag;

public interface ListObjectCallback {
   Object get(Object var1);
}
