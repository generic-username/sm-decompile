package org.schema.schine.resource.tag;

import java.io.DataInput;
import java.io.IOException;

public interface SerializableTagFactory {
   Object create(DataInput var1) throws IOException;
}
