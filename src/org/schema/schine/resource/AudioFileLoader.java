package org.schema.schine.resource;

import java.io.IOException;
import java.util.Locale;
import org.schema.common.util.data.ResourceUtil;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class AudioFileLoader {
   public AudioFileLoader(ResourceUtil var1) {
   }

   public void loadSound(String var1, String var2, String var3) throws IOException, ResourceException {
      if (EngineSettings.S_SOUND_SYS_ENABLED.isOn()) {
         System.err.println("LOADING SOUND: " + var1);
         if (var3.toLowerCase(Locale.ENGLISH).equals("ogg") || var3.toLowerCase(Locale.ENGLISH).equals("wav")) {
            Controller.getAudioManager().addSound(var2, new FileExt(var1));
         }

      }
   }
}
