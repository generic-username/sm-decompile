package org.schema.schine.auth;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.schema.schine.resource.FileExt;

public class MIMEMultipart {
   static String CRLF = "\r\n";
   StringBuilder text = new StringBuilder();
   String boundary = Long.toHexString(System.currentTimeMillis());

   public String getContent() {
      return this.text.toString();
   }

   public String getBoundary() {
      return this.boundary;
   }

   public int getLength() {
      return this.text.length();
   }

   public void putStandardParam(String var1, String var2, String var3) {
      StringBuilder var4;
      (var4 = new StringBuilder()).append("--" + this.boundary).append(CRLF);
      var4.append("Content-Disposition: form-data; name=\"" + var1 + "\"");
      var4.append(CRLF);
      var4.append("Content-Type: text/plain; charset=" + var3);
      var4.append(CRLF);
      var4.append(CRLF);
      var4.append(var2);
      var4.append(CRLF);
      this.text.append(var4.toString());
   }

   public void putBinaryFileParam(String var1, File var2, String var3, String var4) throws IOException {
      StringBuilder var5;
      (var5 = new StringBuilder()).append("--" + this.boundary);
      var5.append(CRLF);
      var5.append("Content-Disposition: form-data; name=\"");
      var5.append(var1);
      var5.append("\";  filename=\"");
      var5.append(var2.getName());
      var5.append("\"");
      var5.append(CRLF);
      var5.append("Content-Type: " + var3);
      var5.append(CRLF);
      var5.append("Content-Transfer-Encoding: binary");
      var5.append(CRLF);
      var5.append(CRLF);
      this.text.append(var5.toString());
      FileInputStream var6 = new FileInputStream(var2);
      byte[] var7 = new byte[(int)var2.length()];
      var6.read(var7);
      var6.close();
      this.text.append(new String(var7, var4));
      this.text.append(CRLF);
   }

   public void putBinaryFileParam(String var1, String var2, String var3, String var4) throws Exception {
      StringBuilder var5;
      (var5 = new StringBuilder()).append("--" + this.boundary);
      var5.append(CRLF);
      var5.append("Content-Disposition: form-data; name=\"");
      var5.append(var1);
      var5.append("\";  filename=\"");
      var5.append(var2);
      var5.append("\"");
      var5.append(CRLF);
      var5.append("Content-Type: " + var3);
      var5.append(CRLF);
      var5.append("Content-Transfer-Encoding: binary");
      var5.append(CRLF);
      var5.append(CRLF);
      this.text.append(var5.toString());
      FileExt var6 = new FileExt(var2);
      FileInputStream var8 = new FileInputStream(var6);
      byte[] var7 = new byte[(int)var6.length()];
      var8.read(var7);
      var8.close();
      this.text.append(new String(var7, var4));
      this.text.append(CRLF);
   }

   public void finish() {
      this.text.append("--");
      this.text.append(this.boundary);
      this.text.append("--");
      this.text.append(CRLF);
   }

   public String toString() {
      return this.text.toString();
   }
}
