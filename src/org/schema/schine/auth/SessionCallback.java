package org.schema.schine.auth;

import org.schema.schine.network.server.AuthenticationRequiredException;
import org.schema.schine.network.server.ServerController;

public interface SessionCallback {
   String getStarMadeUserName();

   boolean isUpgraded();

   boolean authorize(String var1, ServerController var2, boolean var3, boolean var4, boolean var5) throws AuthenticationRequiredException;
}
