package org.schema.schine.ai.aStar;

import org.schema.schine.network.Identifiable;

public abstract class ANode implements Node {
   private int x;
   private int z;
   private int costToThis;
   private int costToGoal;
   private int weight;
   private ANode[] neighbors = new ANode[8];
   private ANode parent;
   private String closeReason;
   private int neighborCount = 0;

   public ANode(int var1, int var2) {
      this.setPos(var1, var2);
   }

   public void addNeighbor(ANode var1) {
      this.getNeighbors()[this.neighborCount++] = var1;
   }

   public boolean equals(Object var1) {
      ANode var2;
      return (var2 = (ANode)var1).x == this.x && var2.z == this.z;
   }

   public String toString() {
      return "[" + this.x + "," + this.z + " / " + (this.getCostToGoal() + this.getCostToThis()) + (this.getCloseReason() != null ? "(" + this.getCloseReason() + ")" : "") + "]";
   }

   public String getCloseReason() {
      return this.closeReason;
   }

   public void setCloseReason(String var1) {
      this.closeReason = var1;
   }

   public int getCostToGoal() {
      return this.costToGoal;
   }

   public void setCostToGoal(int var1) {
      this.costToGoal = var1;
   }

   public int getCostToThis() {
      return this.costToThis;
   }

   public void setCostToThis(int var1) {
      this.costToThis = var1;
   }

   public int getNeighborCount() {
      return this.neighborCount;
   }

   public void setNeighborCount(int var1) {
      this.neighborCount = var1;
   }

   public ANode[] getNeighbors() {
      return this.neighbors;
   }

   public int getTraverseCost(ANode var1) {
      int var2 = Math.abs(var1.getX() - this.getX());
      int var3 = Math.abs(var1.getZ() - this.getZ());
      return 10 * (var2 + var3);
   }

   public void setNeighbors(ANode[] var1) {
      this.neighbors = var1;
   }

   public ANode getParent() {
      return this.parent;
   }

   public void setParent(ANode var1) {
      this.parent = var1;
   }

   public int getWeight() {
      return this.weight;
   }

   public void setWeight(int var1) {
      this.weight = var1;
   }

   public int getX() {
      return this.x;
   }

   public int getZ() {
      return this.z;
   }

   public abstract boolean isOccupiedFor(Identifiable var1);

   public void removeNeighbor(ANode var1) {
      for(int var2 = 0; var2 < this.getNeighborCount(); ++var2) {
         if (this.getNeighbors()[var2].equals(var1)) {
            this.getNeighbors()[var2] = this.getNeighbors()[this.getNeighborCount() - 1];
            this.getNeighbors()[this.getNeighborCount() - 1] = null;
            this.setNeighborCount(this.getNeighborCount() - 1);
         }
      }

   }

   public void setPos(int var1, int var2) {
      this.x = var1;
      this.z = var2;
   }
}
