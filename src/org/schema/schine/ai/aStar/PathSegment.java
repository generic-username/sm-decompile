package org.schema.schine.ai.aStar;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3D;

public class PathSegment {
   public Vector3D start;
   public Vector3D end;
   public Vector3D startPixel;
   public Vector3D endPixel;
   public Vector3D dir;
   public float length;

   public PathSegment(Vector3f var1, Vector3f var2, int var3) {
      this.start = new Vector3D(var1);
      this.end = new Vector3D(var2);
      this.startPixel = new Vector3D(var1.x * (float)var3 + (float)(var3 / 2), var1.y * (float)var3, var1.z * (float)var3 + (float)(var3 / 2));
      this.endPixel = new Vector3D(var2.x * (float)var3 + (float)(var3 / 2), var2.y * (float)var3, var2.z * (float)var3 + (float)(var3 / 2));
      this.dir = Vector3D.sub(this.endPixel, this.startPixel);
      this.length = this.dir.length();
   }

   public Field getEndField(Map var1) {
      return var1.getField((int)this.end.getX(), (int)this.end.getZ());
   }

   public Field getStartField(Map var1) {
      return var1.getField((int)this.start.getX(), (int)this.start.getZ());
   }

   public void resetDirection() {
      this.dir = Vector3D.sub(this.endPixel, this.startPixel);
   }

   public String toString() {
      return "[(" + this.start.getX() + "," + this.start.getZ() + ")-(" + this.end.getX() + "," + this.end.getZ() + ")]";
   }
}
