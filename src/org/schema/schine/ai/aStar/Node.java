package org.schema.schine.ai.aStar;

public interface Node {
   ANode[] getNeighbors();

   int getTraverseCost(ANode var1);
}
