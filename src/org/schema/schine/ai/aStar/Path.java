package org.schema.schine.ai.aStar;

import java.util.Iterator;
import java.util.Vector;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3D;

public class Path {
   int lastMark = -1;
   private Vector segs = new Vector();
   private int pathMark = -1;

   public void addAll(Path var1) {
      Iterator var3 = var1.getSegs().iterator();

      while(var3.hasNext()) {
         PathSegment var2 = (PathSegment)var3.next();
         this.addSeg(var2);
      }

   }

   public void addSeg(PathSegment var1) {
      this.segs.add(var1);
   }

   public void clear() {
      this.segs.clear();
   }

   public PathSegment getNearestSegment(Vector3f var1, boolean var2, float var3) {
      Vector3D var9 = new Vector3D(var1);
      float var4 = Float.MAX_VALUE;
      if (this.getPathMark() == -1) {
         this.setPathMark(this.size() - 1);
      }

      PathSegment var5 = this.getSeg(this.pathMark);
      int var6 = this.getPathMark();

      for(int var10 = var2 ? this.size() - 1 : var6; var10 >= 0; --var10) {
         PathSegment var7 = this.getSeg(var10);
         float var8;
         if ((var8 = Vector3D.sub(Vector3D.pointToSegmentDistance(var9, var7.endPixel, var7.startPixel), var9).length()) <= var4) {
            var4 = var8;
            this.setPathMark(var10);
            var5 = var7;
         }

         Vector3D var11;
         (var11 = var7.startPixel.clone()).sub(var9);
         if (var11.length() < var3) {
            this.setPathMark(var10);
            var5 = var7;
            break;
         }
      }

      this.lastMark = var6;
      return var5;
   }

   public int getPathMark() {
      return this.pathMark;
   }

   public void setPathMark(int var1) {
      this.pathMark = var1;
   }

   public PathSegment getSeg(int var1) {
      return (PathSegment)this.segs.get(var1);
   }

   public Vector getSegs() {
      return this.segs;
   }

   public void setSegs(Vector var1) {
      this.segs = var1;
   }

   public boolean isEmpty() {
      return this.size() <= 0;
   }

   public boolean isLast(PathSegment var1) {
      return this.segs.firstElement() == var1;
   }

   public void remove(int var1) {
      this.segs.remove(var1);
   }

   public void removeSeg(PathSegment var1) {
      this.segs.remove(var1);
   }

   public void resetPath() {
      this.setPathMark(-1);
   }

   public int size() {
      return this.segs.size();
   }

   public void smooth(Map var1) {
      PathSegment var2 = (PathSegment)this.segs.get(this.size() - 1);
      int var3 = this.size() - 2;
      Vector var4 = new Vector();

      for(int var5 = var3; var5 >= 0; --var5) {
         Vector3D var6;
         float var7 = (var6 = Vector3D.sub(((PathSegment)this.segs.get(var5)).startPixel, var2.startPixel)).length();
         Vector3D var8 = var2.startPixel.clone();
         var6.normalize();
         var6.scalarMult(6.0F);

         for(int var9 = 0; (float)var9 < var7; var9 += 6) {
            var8.add(var6);
            int var10 = Math.round(var8.getX() / 24.0F);
            int var11 = Math.round(var8.getZ() / 24.0F);
            if (!var1.checkField(var10, var11) || !var1.isWalkable(var10, var11) || var5 == 0 || var1.getField(var10, var11).getEntities().size() > 0) {
               while(var3 > var5) {
                  System.err.println("removing not needed segment " + this.segs.get(var3));
                  this.segs.remove(var3);
                  --var3;
               }

               var2.end = ((PathSegment)this.segs.get(var5)).start;
               var2.endPixel = ((PathSegment)this.segs.get(var5)).startPixel;
               var2 = (PathSegment)this.segs.get(var5);
               --var5;
               var3 = var5;
               break;
            }
         }
      }

      this.segs.removeAll(var4);
      Iterator var12 = this.segs.iterator();

      while(var12.hasNext()) {
         ((PathSegment)var12.next()).resetDirection();
      }

      System.err.println("Smoothing Path! now: " + this.segs);
   }

   public String toString() {
      String var1 = "Path: ";

      PathSegment var3;
      for(Iterator var2 = this.segs.iterator(); var2.hasNext(); var1 = var1 + " -> " + var3) {
         var3 = (PathSegment)var2.next();
      }

      return var1;
   }
}
