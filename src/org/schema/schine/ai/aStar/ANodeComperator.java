package org.schema.schine.ai.aStar;

import java.util.Comparator;

public class ANodeComperator implements Comparator {
   public int compare(ANode var1, ANode var2) {
      if (var1.getCostToGoal() + var1.getCostToThis() == var2.getCostToGoal() + var2.getCostToThis()) {
         return 0;
      } else {
         return var1.getCostToGoal() + var1.getCostToThis() < var2.getCostToGoal() + var2.getCostToThis() ? -1 : 1;
      }
   }
}
