package org.schema.schine.ai;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public abstract class MachineProgram {
   private final AiEntityState entityState;
   protected HashMap machines = new HashMap();
   private FiniteStateMachine currentMachine;
   private boolean suspended;

   public MachineProgram(AiEntityState var1, boolean var2) {
      this.entityState = var1;
      this.initializeMachines(this.machines);
      this.currentMachine = (FiniteStateMachine)this.machines.get(this.getStartMachine());

      assert this.currentMachine != null;

      this.suspended = var2;
   }

   public MachineProgram(AiEntityState var1, boolean var2, HashMap var3) {
      this.entityState = var1;
      if (var3 != null) {
         this.reinit(var3);
      }

      this.suspended = var2;
   }

   public void reinit(HashMap var1) {
      this.machines = var1;
      this.currentMachine = (FiniteStateMachine)var1.get(this.getStartMachine());

      assert this.machines.isEmpty() || this.currentMachine != null : var1 + "; " + this.getStartMachine();

   }

   public void setCurrentMachine(String var1) {
      this.currentMachine = (FiniteStateMachine)this.machines.get(var1);
   }

   public FiniteStateMachine getOtherMachine(String var1) {
      return (FiniteStateMachine)this.machines.get(var1);
   }

   public Set getMachineNames() {
      return this.machines.keySet();
   }

   public abstract void onAISettingChanged(AIConfiguationElementsInterface var1) throws FSMException;

   public AiEntityState getEntityState() {
      return this.entityState;
   }

   public FiniteStateMachine getMachine() {
      return this.currentMachine;
   }

   protected abstract String getStartMachine();

   public StateInterface getState() {
      return this.entityState.getState();
   }

   protected abstract void initializeMachines(HashMap var1);

   public boolean isSuspended() {
      return this.suspended;
   }

   public void onStateChanged(State var1, State var2) {
   }

   public void onSuspended() {
   }

   public void onUnSuspended() {
   }

   public void suspend(boolean var1) {
      if (this.isAlwaysOn()) {
         this.suspended = false;
      } else {
         if (var1 != this.suspended) {
            if (var1) {
               this.onSuspended();
            } else {
               this.onUnSuspended();
            }
         }

         this.suspended = var1;
      }
   }

   public boolean isAlwaysOn() {
      return false;
   }

   public void update(Timer var1) throws FSMException, Exception {
      this.getEntityState().updateOnActive(var1);
   }

   public void updateOtherMachines() throws FSMException {
   }

   public Collection getMachines() {
      return this.machines.values();
   }

   public boolean isInStartMachine() {
      return this.currentMachine == this.machines.get(this.getStartMachine());
   }
}
