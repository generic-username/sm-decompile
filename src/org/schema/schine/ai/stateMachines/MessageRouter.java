package org.schema.schine.ai.stateMachines;

import java.util.ArrayList;
import java.util.Iterator;
import org.schema.schine.ai.AiEntityStateInterface;

public class MessageRouter {
   public ArrayList storedMsg = new ArrayList();

   public boolean removeDelayedMessage(Message var1) {
      return this.storedMsg.remove(var1);
   }

   public void routeMessage(Message var1) {
      if (var1.receiver != null) {
         if (var1.deliveryTime > (float)System.currentTimeMillis()) {
            this.storeDelayedMsg(var1);
         } else {
            if (this.routeMessageChecker(var1.receiver, var1, var1.receiver.getStateCurrent())) {
               System.err.println("~~ MESSAGE: routed to " + var1.receiver + ": " + var1.getContent());
            }

         }
      }
   }

   private boolean routeMessageChecker(AiEntityStateInterface var1, Message var2, State var3) {
      return var1.processStateMachine(var3, var2);
   }

   public void sendDelayedMessages() {
      Iterator var1 = this.storedMsg.iterator();

      while(var1.hasNext()) {
         Message var2;
         if ((var2 = (Message)var1.next()).deliveryTime <= (float)System.currentTimeMillis()) {
            var2.sendMsg(this);
            this.removeDelayedMessage(var2);
         }
      }

   }

   private void storeDelayedMsg(Message var1) {
      this.storedMsg.add(var1);
   }

   public void update() {
      this.sendDelayedMessages();
   }
}
