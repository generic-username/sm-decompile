package org.schema.schine.ai.stateMachines;

import org.schema.schine.network.StateInterface;

public interface AiInterface {
   AIConfigurationInterface getAiConfiguration();

   String getRealName();

   String getUniqueIdentifier();

   StateInterface getState();
}
