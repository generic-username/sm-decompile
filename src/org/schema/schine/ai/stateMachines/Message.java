package org.schema.schine.ai.stateMachines;

import org.schema.schine.ai.AIEntityNotFoundException;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.network.objects.Sendable;

public abstract class Message {
   protected AiEntityStateInterface sender;
   protected AiEntityStateInterface receiver;
   protected float deliveryTime;
   private String content;

   public Message(String var1, AiEntityStateInterface var2, int var3) {
      this.sender = var2;

      try {
         Sendable var5;
         if ((var5 = (Sendable)var2.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var3)) == null || !(var5 instanceof AiInterface)) {
            throw new AIEntityNotFoundException(var3);
         }

         this.receiver = ((AiInterface)var5).getAiConfiguration().getAiEntityState();
      } catch (AIEntityNotFoundException var4) {
         var4.printStackTrace();
      }

      this.setContent(var1);
   }

   public abstract void execute(FiniteStateMachine var1);

   public String getContent() {
      return this.content;
   }

   public void setContent(String var1) {
      this.content = var1;
   }

   public AiEntityStateInterface getReceiver() {
      return this.receiver;
   }

   public AiEntityStateInterface getSender() {
      return this.sender;
   }

   public void sendDelayedMsg(MessageRouter var1, float var2) {
      this.deliveryTime = (float)System.currentTimeMillis() + var2;
      var1.routeMessage(this);
   }

   public void sendMsg(MessageRouter var1) {
      this.deliveryTime = (float)System.currentTimeMillis();
      var1.routeMessage(this);
   }

   public String toString() {
      return "[Msg <" + this.sender + "> to <" + this.receiver + ">]";
   }
}
