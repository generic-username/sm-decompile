package org.schema.schine.ai.stateMachines;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;

public class AiEntityState implements AiEntityStateInterface {
   private final MessageRouter messageRouter = new MessageRouter();
   protected final StateInterface state;
   private final boolean onServer;
   public String lastEngage = "";
   protected String name;
   protected MachineProgram program;

   public AiEntityState(String var1, StateInterface var2) {
      this.name = var1;
      this.state = var2;
      this.onServer = this.state instanceof ServerStateInterface;
   }

   public void sendMessage(Message var1) {
      this.messageRouter.routeMessage(var1);
   }

   public MachineProgram getCurrentProgram() {
      return this.program;
   }

   public void setCurrentProgram(MachineProgram var1) {
      this.program = var1;
   }

   public FiniteStateMachine getMachine() {
      return this.program.getMachine();
   }

   public StateInterface getState() {
      return this.state;
   }

   public State getStateCurrent() {
      return this.program.getMachine().getFsm().getCurrentState();
   }

   public boolean isActive() {
      return this.program != null && !this.program.isSuspended();
   }

   public boolean processStateMachine(State var1, Message var2) {
      if (this.program.getMachine() != null) {
         this.program.getMachine().onMsg(var2);
         return true;
      } else {
         throw new RuntimeException(this.name + ": Message " + var2.getContent() + " could not be sent from \"" + var2.getSender() + "\" to \"" + var2.getReceiver() + "\". REASON: machine null");
      }
   }

   public void updateOnActive(Timer var1) throws FSMException {
      if (this.program != null && !this.program.isSuspended()) {
         this.program.getMachine().update();
         this.program.updateOtherMachines();
      }

   }

   public void afterUpdate(Timer var1) {
   }

   public void updateGeneral(Timer var1) {
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public String toString() {
      String var1 = this.name;
      if (this.program == null) {
         return var1 + "[NULL_PROGRAM]\n" + this.lastEngage;
      } else {
         return this.program.getMachine().getFsm().getCurrentState() == null ? var1 + "\n->[" + this.program.getClass().getSimpleName() + "->NULL_STATE]\n" + this.lastEngage : var1 + "\n->[" + this.program.getClass().getSimpleName() + "->" + this.program.getMachine().getFsm().getCurrentState().getClass().getSimpleName() + "]\n" + this.lastEngage;
      }
   }

   public boolean isStateSet() {
      return this.getCurrentProgram() != null && this.getCurrentProgram().getMachine() != null && this.getCurrentProgram().getMachine().getFsm() != null && this.getCurrentProgram().getMachine().getFsm().getCurrentState() != null;
   }
}
