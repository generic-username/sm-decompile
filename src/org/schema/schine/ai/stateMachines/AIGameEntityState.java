package org.schema.schine.ai.stateMachines;

import java.util.Random;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public abstract class AIGameEntityState extends AiEntityState {
   private final Sendable sendable;
   public Random random;
   public long seed;
   private long seedTime;

   public AIGameEntityState(String var1, Sendable var2) {
      super(var1, var2.getState());
      this.sendable = var2;
      this.random = new Random();
      this.seed = this.random.nextLong();
   }

   public Sendable getEntity() {
      return this.sendable;
   }

   public float getShootingRange() {
      return 64.0F;
   }

   public float getSalvageRange() {
      return 64.0F;
   }

   public boolean isActive() {
      return super.isActive() || !this.isOnServer() && ((AiInterface)this.sendable).getAiConfiguration().isAIActiveClient();
   }

   public AIConfigurationInterface getAIConfig() {
      return ((AiInterface)this.getEntity()).getAiConfiguration();
   }

   public void updateOnActive(Timer var1) throws FSMException {
      super.updateOnActive(var1);
      if (this.isActive()) {
         if (this.isOnServer()) {
            this.updateAIServer(var1);
            if (System.currentTimeMillis() > this.seedTime) {
               this.seedTime = System.currentTimeMillis() + 10000L;
               this.seed = this.random.nextLong();
               return;
            }
         } else {
            this.updateAIClient(var1);
         }
      }

   }

   public abstract void updateAIClient(Timer var1);

   public abstract void updateAIServer(Timer var1) throws FSMException;

   public float getAntiMissileShootingSpeed() {
      return 1.0F;
   }

   public boolean canSalvage() {
      return false;
   }
}
