package org.schema.schine.ai.stateMachines;

public class MaximumTransitionsException extends Exception {
   private static final long serialVersionUID = 1L;

   public MaximumTransitionsException(int var1, State var2) {
      super("the maximum of " + var1 + " transitons for state " + var2 + " has been exeeded ");
   }
}
