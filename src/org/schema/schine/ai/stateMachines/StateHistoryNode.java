package org.schema.schine.ai.stateMachines;

import java.text.DecimalFormat;
import org.schema.common.util.StringTools;

public class StateHistoryNode {
   public State state;
   public Transition trans;
   public StateHistoryNode lastState;
   private int counter;

   public StateHistoryNode(State var1, Transition var2, StateHistoryNode var3) {
      this.state = var1;
      this.trans = var2;
      this.lastState = var3;
      if (var3 != null) {
         this.counter = var3.counter + 1;
      }

   }

   public String toString() {
      DecimalFormat var1 = new DecimalFormat("000");
      return this.lastState != null && this.trans != null ? this.lastState + "<" + var1.format((long)this.counter) + "> [" + StringTools.fill(this.lastState.state.toString() + "]", 18) + " -> {" + StringTools.fill(this.trans.toString() + "}", 18) + " -> [" + this.state.toString() + "]\n" : "";
   }
}
