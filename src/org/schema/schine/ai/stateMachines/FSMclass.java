package org.schema.schine.ai.stateMachines;

import java.util.HashMap;

public class FSMclass {
   HashMap statesMap = new HashMap();
   FiniteStateMachine machine;
   private StateHistoryNode history;

   public FSMclass(State var1, FiniteStateMachine var2) {
      var2.setState(var1);
      this.machine = var2;
      this.setHistory(new StateHistoryNode(var1, (Transition)null, (StateHistoryNode)null));
   }

   public State getCurrentState() {
      return this.getMachine().currentState;
   }

   public StateHistoryNode getHistory() {
      return this.history;
   }

   public void setHistory(StateHistoryNode var1) {
      this.history = var1;
   }

   public FiniteStateMachine getMachine() {
      return this.machine;
   }

   public State stateTransition(Transition var1) throws FSMException {
      return this.stateTransition(var1, 0);
   }

   public State stateTransition(Transition var1, int var2) throws FSMException {
      if (this.getCurrentState() == null) {
         throw new FSMException("ERROR (FSMclass): CURRENT STATE NOT FOUND " + this.getCurrentState());
      } else {
         FSMStateData var3;
         State var4;
         if ((var4 = (var3 = this.getCurrentState().getStateData()).getOutput(var1, var2)) == null) {
            System.err.println("could not set state: discarding");
            throw new FSMException(this.getCurrentState(), var1, var2);
         } else if (var4 == this.getCurrentState()) {
            this.getCurrentState().onExit();
            this.getCurrentState().setNewState(true);
            return this.getCurrentState();
         } else {
            this.getCurrentState().onExit();
            this.machine.setState(var4);
            this.getCurrentState().setNewState(true);
            this.machine.getMachineProgram().onStateChanged(var3.getState(), this.getCurrentState());
            return this.getCurrentState();
         }
      }
   }
}
