package org.schema.schine.ai.stateMachines;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.MachineProgram;

public abstract class FiniteStateMachine {
   State currentState;
   private FSMclass fsm;
   private AiEntityStateInterface obj;
   private MachineProgram robotProgram;
   private State startState;
   private Object2ObjectOpenHashMap directStates;

   public FiniteStateMachine(AiEntityStateInterface var1, MachineProgram var2, Object var3, State var4) {
      this.obj = var1;
      this.setMachineProgram(var2);
      this.createFSM(var3);
      this.setStartingState(var4);
   }

   public FiniteStateMachine(AiEntityStateInterface var1, MachineProgram var2, Object var3) {
      assert var1 != null;

      this.obj = var1;
      this.setMachineProgram(var2);
      this.createFSM(var3);

      assert this.getStartState() != null;

   }

   public abstract void createFSM(Object var1);

   public FSMclass getFsm() {
      return this.fsm;
   }

   public MachineProgram getMachineProgram() {
      return this.robotProgram;
   }

   public void setMachineProgram(MachineProgram var1) {
      this.robotProgram = var1;
   }

   public AiEntityStateInterface getObj() {
      return this.obj;
   }

   public abstract void onMsg(Message var1);

   public void setStartingState(State var1) {
      var1.initRecusively(this);
      var1.setMachineRecusively(this);
      this.setState(var1);
      this.startState = var1;
      this.fsm = new FSMclass(var1, this);
   }

   public void setState(State var1) {
      assert this.obj != null;

      this.currentState = var1;
   }

   public String toString() {
      return this.getClass().getSimpleName();
   }

   public void update() throws FSMException {
      if (this.currentState == null) {
         throw new FSMException("[CRITICAL] no state set! please set the FiniteStateMachine.setStartState(State state) Method in createFSM()");
      } else {
         State var1;
         if ((var1 = this.currentState).isNewState()) {
            var1.onEnter();
            var1.setNewState(false);
         } else {
            var1.onUpdate();
         }
      }
   }

   public void init(State var1) {
      var1.init(this);
   }

   public void reset() {
      this.setState(this.startState);
      this.startState.setNewState(true);
   }

   public Object2ObjectOpenHashMap getDirectStates() {
      return this.directStates;
   }

   public void setDirectStates(Object2ObjectOpenHashMap var1) {
      this.directStates = var1;
   }

   public State getStartState() {
      return this.startState;
   }
}
