package org.schema.schine.physics;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.HashedOverlappingPairCache;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.dispatch.PairCachingGhostObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.ConvexResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalConvexResult;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.BvhTriangleMeshShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ShapeHull;
import com.bulletphysics.collision.shapes.TriangleIndexVertexArray;
import com.bulletphysics.dynamics.ActionInterface;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;
import com.bulletphysics.dynamics.vehicle.RaycastVehicle;
import com.bulletphysics.extras.gimpact.GImpactCollisionAlgorithm;
import com.bulletphysics.extras.gimpact.GImpactMeshShape;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL15;
import org.schema.schine.graphicsengine.core.GLDebugDrawer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.DebugBoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.network.objects.container.TransformTimed;

public abstract class Physics {
   public static final float PHYSICS_GRAVITY = 0.0F;
   public static PhysicsHelperVars helpervars;
   protected static ThreadLocal threadLocal = new ThreadLocal() {
      protected final PhysicsHelperVars initialValue() {
         return new PhysicsHelperVars();
      }
   };
   private final Transform tempTrans = new Transform();
   public HashMap dataConstraintMap = new HashMap();
   protected DynamicsWorld dynamicsWorld;
   float counter;
   private Vector vehicles = new Vector();
   private BroadphaseInterface overlappingPairCache;
   private CollisionDispatcher dispatcher;
   private ConstraintSolver solver;
   private CollisionConfiguration collisionConfiguration;
   private PhysicsState state;
   private HashMap shapeEntityMap = new HashMap();
   private float iterations = 25.0F;
   private boolean hitIndicator;
   private long time_physics_curr = System.currentTimeMillis();
   private long time_physics_prev = System.currentTimeMillis();

   public Physics(PhysicsState var1) {
      this.setState(var1);
      this.initPhysics();
   }

   public void addObject(CollisionObject var1) {
      this.addObject(var1, (short)-1, (short)-1);
   }

   public void addObject(CollisionObject var1, short var2, short var3) {
      if (!this.containsObject(var1)) {
         if (var1 instanceof RigidBody) {
            ((DiscreteDynamicsWorld)this.dynamicsWorld).addRigidBody((RigidBody)var1, var2, var3);
            return;
         }

         ((DiscreteDynamicsWorld)this.dynamicsWorld).addCollisionObject(var1, var2, var3);
      }

   }

   public void cleanUp() {
      this.dynamicsWorld.destroy();
   }

   public boolean containsAction(ActionInterface var1) {
      for(int var2 = 0; var2 < this.getDynamicsWorld().getNumActions(); ++var2) {
         if (this.getDynamicsWorld().getAction(var2) == var1) {
            return true;
         }
      }

      return false;
   }

   public boolean containsObject(CollisionObject var1) {
      return ((DiscreteDynamicsWorld)this.dynamicsWorld).getCollisionObjectArray().contains(var1);
   }

   public CollisionShape createConvexHullShapeExt(Vector3f[] var1, Vector3f var2, boolean var3) {
      ObjectArrayList var4 = new ObjectArrayList(var1.length);
      int var5 = (var1 = var1).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         Vector3f var7;
         (var7 = var1[var6]).add(var2);
         var4.add(var7);
      }

      ConvexHullShape var8 = new ConvexHullShape(var4);
      if (!var3) {
         return var8;
      } else {
         ShapeHull var9 = new ShapeHull(var8);
         float var10 = var8.getMargin();
         var9.buildHull(var10);
         ConvexHullShape var11;
         (var11 = new ConvexHullShape(var9.getVertexPointer())).recalcLocalAabb();
         return var11;
      }
   }

   public CollisionShape createMeshShapeFromIndexBuffer(ByteBuffer var1, int var2, ByteBuffer var3, int var4, boolean var5) {
      TriangleIndexVertexArray var6 = new TriangleIndexVertexArray(var4, var3, 12, var2, var1, 12);
      Object var7;
      if (var5) {
         var7 = new BvhTriangleMeshShape(var6, true, true);
      } else {
         ((GImpactMeshShape)(var7 = new GImpactMeshShape(var6))).updateBound();
      }

      return (CollisionShape)var7;
   }

   public CollisionShape createMeshShapeFromIndexBuffer(int var1, int var2, int var3, int var4, boolean var5) {
      System.err.println("binding vertexBuffer to " + var1);
      GlUtil.glBindBuffer(34962, var1);
      GL15.glMapBuffer(34962, 35000, (ByteBuffer)null);
      System.err.println("binding index to " + var2);
      GlUtil.glBindBuffer(34963, var2);
      GL15.glMapBuffer(34963, 35000, (ByteBuffer)null);
      TriangleIndexVertexArray var6 = new TriangleIndexVertexArray(var3, (ByteBuffer)null, 12, var4, (ByteBuffer)null, 12);
      Object var7;
      if (var5) {
         var7 = new BvhTriangleMeshShape(var6, true, true);
      } else {
         ((GImpactMeshShape)(var7 = new GImpactMeshShape(var6))).updateBound();
      }

      GlUtil.glBindBuffer(34962, var1);
      GL15.glUnmapBuffer(34962);
      GlUtil.glBindBuffer(34963, var2);
      GL15.glUnmapBuffer(34963);
      return (CollisionShape)var7;
   }

   public void drawDebugObjects() {
      assert false;

      if (this.getDynamicsWorld() != null && this.getDynamicsWorld().getDebugDrawer() != null) {
         DiscreteDynamicsWorld var1;
         int var2 = (var1 = (DiscreteDynamicsWorld)this.getDynamicsWorld()).getNumCollisionObjects();

         try {
            int var3;
            for(var3 = 0; var3 < var2; ++var3) {
               CollisionObject var4;
               if ((var4 = (CollisionObject)var1.getCollisionObjectArray().get(var3)) != null) {
                  Transform var5 = new Transform();
                  var4.getWorldTransform(var5);
                  RigidBody var9;
                  if ((var9 = RigidBody.upcast(var4)) == null) {
                     DebugPoint var6 = new DebugPoint(var5.origin, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), 5.0F);
                     DebugDrawer.points.add(var6);
                  } else {
                     Vector3f var13 = new Vector3f();
                     Vector3f var7 = new Vector3f();
                     var9.getAabb(var13, var7);
                     var9.getCenterOfMassTransform(var5);
                     DebugBoundingBox var10 = new DebugBoundingBox(var13, var7, 1.0F, 1.0F, 0.0F, 1.0F);
                     DebugDrawer.boundingBoxes.add(var10);
                     DebugPoint var11 = new DebugPoint(var5.origin, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), 5.0F);
                     DebugDrawer.points.add(var11);
                  }
               }
            }

            var3 = var1.getNumConstraints();

            for(int var12 = 0; var12 < var3; ++var12) {
               GLDebugDrawer.drawOpenGL(var1.getConstraint(var12), new Vector3f(1.0F, 0.0F, 1.0F), this.getDynamicsWorld().getDebugDrawer().getDebugMode());
            }

         } catch (Exception var8) {
            var8.printStackTrace();
         }
      }
   }

   public abstract RigidBody getBodyFromShape(CollisionShape var1, float var2, Transform var3);

   public CollisionConfiguration getCollisionConfiguration() {
      return this.collisionConfiguration;
   }

   public void setCollisionConfiguration(CollisionConfiguration var1) {
      this.collisionConfiguration = var1;
   }

   public Vector3f getCollisionPoint(Vector3f var1, Vector3f var2, boolean var3) {
      return this.testRayCollisionPoint(var1, var2, var3).hitPointWorld;
   }

   public CollisionDispatcher getDispatcher() {
      return this.dispatcher;
   }

   public void setDispatcher(CollisionDispatcher var1) {
      this.dispatcher = var1;
   }

   public DynamicsWorld getDynamicsWorld() {
      return this.dynamicsWorld;
   }

   public void setDynamicsWorld(DynamicsWorld var1) {
      this.dynamicsWorld = var1;
   }

   public float getIterations() {
      return this.iterations;
   }

   public void setIterations(float var1) {
      this.iterations = var1;
   }

   public BroadphaseInterface getOverlappingPairCache() {
      return this.overlappingPairCache;
   }

   public void setOverlappingPairCache(BroadphaseInterface var1) {
      this.overlappingPairCache = var1;
   }

   public ConstraintSolver getSolver() {
      return this.solver;
   }

   public void setSolver(ConstraintSolver var1) {
      this.solver = var1;
   }

   public PhysicsState getState() {
      return this.state;
   }

   public void setState(PhysicsState var1) {
      this.state = var1;
   }

   public Vector getVehicles() {
      return this.vehicles;
   }

   public void setVehicles(Vector var1) {
      this.vehicles = var1;
   }

   public void initPhysics() {
      this.setCollisionConfiguration(new DefaultCollisionConfiguration());
      this.setDispatcher(new CollisionDispatcher(this.getCollisionConfiguration()));
      Vector3f var1 = new Vector3f(-10000.0F, -10000.0F, -10000.0F);
      Vector3f var2 = new Vector3f(10000.0F, 10000.0F, 10000.0F);
      this.setOverlappingPairCache(new AxisSweep3(var1, var2, 1024, new HashedOverlappingPairCache()));
      SequentialImpulseConstraintSolver var3 = new SequentialImpulseConstraintSolver();
      this.setSolver(var3);
      this.setDynamicsWorld(new DiscreteDynamicsWorld(this.getDispatcher(), this.getOverlappingPairCache(), this.getSolver(), this.getCollisionConfiguration()));
      GImpactCollisionAlgorithm.registerAlgorithm((CollisionDispatcher)this.dynamicsWorld.getDispatcher());
      this.getDynamicsWorld().setGravity(new Vector3f(0.0F, -0.0F, 0.0F));
   }

   public void orientate(Physical var1, Vector3f var2, Vector3f var3, Vector3f var4, float var5, float var6, float var7, float var8) {
      RigidBody var11;
      if ((var11 = (RigidBody)var1.getPhysicsDataContainer().getObject()).getCollisionFlags() != 1 && var1.getMass() > 0.0F) {
         if (var2 != null && var2.lengthSquared() > 0.0F && var3 != null && var3.lengthSquared() > 0.0F) {
            PhysicsHelperVars var12 = (PhysicsHelperVars)threadLocal.get();
            TransformTimed var9 = var1.getPhysicsDataContainer().getCurrentPhysicsTransform();
            GlUtil.getForwardVector(var12.currentForward, (Transform)var9);
            GlUtil.getUpVector(var12.currentUp, (Transform)var9);
            GlUtil.getRightVector(var12.currentRight, (Transform)var9);
            var12.toRight.set(var4);
            var12.toRight.normalize();
            var12.toForward.set(var2);
            var12.toForward.normalize();
            var12.toUp.set(var3);
            var12.toUp.normalize();
            Vector3f var10 = new Vector3f();
            var2 = new Vector3f();
            var3 = new Vector3f();
            var10.sub(var12.currentForward, var12.toForward);
            var2.sub(var12.currentUp, var12.toUp);
            var3.sub(var12.currentRight, var12.toRight);
            var12.axisYaw.cross(var12.currentForward, var12.toForward);
            var12.axisYaw.normalize();
            var12.axisRoll.cross(var12.currentUp, var12.toUp);
            var12.axisRoll.normalize();
            var12.axisPitch.cross(var12.currentRight, var12.toRight);
            var12.axisPitch.normalize();
            if (var10.length() < 1.1920929E-7F && var2.length() < 1.1920929E-7F && var3.length() < 1.1920929E-7F) {
               return;
            }

            var12.axisYaw.scale(var10.length());
            var12.axisRoll.scale(var2.length());
            var12.axisPitch.scale(var3.length());
            var12.axis.add(var12.axisYaw, var12.axisRoll);
            var12.axis.add(var12.axisPitch);
            if (var12.axis.lengthSquared() > 0.0F && var12.axis.length() > 5.0E-5F && !var11.isActive()) {
               var11.activate();
            }

            var10 = new Vector3f();
            var11.getAngularVelocity(var10);
            var12.axis.scale(var5);
            if (!Float.isNaN(var12.axis.x) && !Float.isNaN(var12.axis.y) && !Float.isNaN(var12.axis.z)) {
               var11.setAngularVelocity(var12.axis);
            }

            var12.lastAxis.set(var12.axis);
         }

      }
   }

   public String physicsSlowMsg() {
      return this.getState().getPhysicsSlowMsg();
   }

   public void removeObject(CollisionObject var1) {
      if (this.dynamicsWorld.getCollisionObjectArray().contains(var1)) {
         if (var1 instanceof RigidBody) {
            this.dynamicsWorld.removeRigidBody((RigidBody)var1);
            return;
         }

         this.dynamicsWorld.removeCollisionObject(var1);
      }

   }

   public void removeShapeOfEntity(Physical var1) {
      if (var1.getPhysicsDataContainer().getObject() != null) {
         CollisionObject var2;
         if ((var2 = var1.getPhysicsDataContainer().getObject()) instanceof RigidBody) {
            this.dynamicsWorld.removeRigidBody((RigidBody)var2);
         } else {
            this.dynamicsWorld.removeCollisionObject(var2);
         }

         this.shapeEntityMap.remove(var2);

         for(int var3 = 0; var3 < this.dynamicsWorld.getNumConstraints(); ++var3) {
            TypedConstraint var4;
            if ((var4 = this.dynamicsWorld.getConstraint(var3)).getRigidBodyA() == var2 || var4.getRigidBodyB() == var2) {
               this.dynamicsWorld.removeConstraint(var4);
            }
         }

         var1.getPhysicsDataContainer().clearPhysicsInfo();
      }
   }

   public void resetAll() {
      System.err.println("[PHYSICS] TOTAL RESET!");

      int var1;
      for(var1 = 0; var1 < this.dynamicsWorld.getCollisionObjectArray().size(); ++var1) {
         this.dynamicsWorld.removeCollisionObject((CollisionObject)this.dynamicsWorld.getCollisionObjectArray().get(var1));
      }

      for(var1 = 0; var1 < this.dynamicsWorld.getNumConstraints(); ++var1) {
         this.dynamicsWorld.removeConstraint(this.dynamicsWorld.getConstraint(var1));
      }

      Iterator var3 = this.vehicles.iterator();

      while(var3.hasNext()) {
         RaycastVehicle var2 = (RaycastVehicle)var3.next();
         this.dynamicsWorld.removeVehicle(var2);
      }

      this.getVehicles().clear();
      this.shapeEntityMap.clear();
      this.dataConstraintMap.clear();
      this.initPhysics();
   }

   public void shoot(Physical var1, float var2, Vector3f var3, boolean var4) {
      RigidBody var5 = (RigidBody)var1.getPhysicsDataContainer().getObject();
      this.shoot(var5, var2, var3, var4);
   }

   public void shoot(RigidBody var1, float var2, Vector3f var3, boolean var4) {
      if (var3 != null && var3.length() > 0.0F) {
         (var3 = new Vector3f(var3)).normalize();
         var3.scale(var2);
         var1.setLinearVelocity(var3);
      }

   }

   public void softClean() {
   }

   public void stopAngular(Physical var1) {
      ((RigidBody)var1.getPhysicsDataContainer().getObject()).setAngularVelocity(new Vector3f(0.0F, 0.0F, 0.0F));
   }

   public void stopForces(Physical var1) {
      RigidBody var2 = (RigidBody)var1.getPhysicsDataContainer().getObject();

      assert var2 != null;

      if (!var2.isActive()) {
         var2.activate();
      }

      var2.clearForces();
      var2.setLinearVelocity(new Vector3f(0.0F, 0.0F, 0.0F));
      var2.setAngularVelocity(new Vector3f(0.0F, 0.0F, 0.0F));
   }

   public void testOverlapAABB(PairCachingGhostObject var1) {
      System.err.println("overlapping: " + var1.getNumOverlappingObjects() + ", " + var1.getActivationState());
      ObjectArrayList var2 = var1.getOverlappingPairCache().getOverlappingPairArray();

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         BroadphasePair var4;
         CollisionObject var5 = (CollisionObject)((var4 = (BroadphasePair)var2.get(var3)).pProxy0.clientObject != var1 ? var4.pProxy0 : var4.pProxy1).clientObject;
         System.err.println("overlapping: " + var5);
      }

   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, boolean var3) {
      ClosestRayResultCallback var4 = new ClosestRayResultCallback(var1, var2);
      this.dynamicsWorld.rayTest(var1, var2, var4);
      RigidBody var5;
      return var4.collisionObject != null && (var5 = RigidBody.upcast(var4.collisionObject)) != null && var3 && !var5.isStaticObject() ? null : var4;
   }

   public void testSweepBoundingBox(Vector3f var1, Vector3f var2, Vector3f var3) {
      BoxShape var7 = new BoxShape(var3);
      Transform var4;
      (var4 = new Transform()).setIdentity();
      var4.origin.set(var1);
      Transform var5;
      (var5 = new Transform()).setIdentity();
      var5.origin.set(var2);
      this.hitIndicator = false;
      ConvexResultCallback var6 = new ConvexResultCallback() {
         public float addSingleResult(LocalConvexResult var1, boolean var2) {
            Physics.this.hitIndicator = true;
            System.err.println("hitIndicator: " + Physics.this.hitIndicator + " " + var1.hitCollisionObject);
            return 0.0F;
         }
      };
      var7.recalcLocalAabb();
      this.dynamicsWorld.convexSweepTest(var7, var4, var5, var6);
      System.err.println("tested bb: " + this.hitIndicator + ", callback: has hit " + var6.hasHit());
   }

   public void update(Timer var1, float var2) {
      assert this.getState() != null;

      this.counter = 0.0F;

      try {
         this.time_physics_curr = System.currentTimeMillis();
         this.dynamicsWorld.stepSimulation((float)(this.time_physics_curr - this.time_physics_prev) / 1000.0F, 14);
         this.time_physics_prev = this.time_physics_curr;
      } catch (Exception var5) {
         var1 = null;
         var5.printStackTrace();

         assert this.getState() != null;

         System.err.println("Exception: PHYSICS EXCEPTION HAS BEEN CATCHED! " + this.getState().toStringDebug());
      }

      int var6 = this.dynamicsWorld.getNumConstraints();

      for(int var7 = 0; var7 < var6; ++var7) {
         TypedConstraint var3 = this.dynamicsWorld.getConstraint(var7);
         PhysicsData var4;
         if ((var4 = (PhysicsData)this.dataConstraintMap.get(var3)) != null) {
            switch(var3.getConstraintType()) {
            case HINGE_CONSTRAINT_TYPE:
               ((HingeConstraintData)var4).update();
               break;
            case SLIDER_CONSTRAINT_TYPE:
               ((SliderConstraintData)var4).update();
            }
         }
      }

   }

   public void onOrientateOnly(Physical var1, float var2) {
   }
}
