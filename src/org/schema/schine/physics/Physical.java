package org.schema.schine.physics;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public interface Physical {
   void createConstraint(Physical var1, Physical var2, Object var3);

   Transform getInitialTransform();

   float getMass();

   PhysicsDataContainer getPhysicsDataContainer();

   void setPhysicsDataContainer(PhysicsDataContainer var1);

   StateInterface getState();

   void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6);

   void initPhysics();
}
