package org.schema.schine.physics;

import org.schema.schine.network.objects.container.ControllerDataContainer;

public interface ControllerInterface {
   ControllerDataContainer getControllerDataContainer();

   void setControllerDataContainer(ControllerDataContainer var1);
}
