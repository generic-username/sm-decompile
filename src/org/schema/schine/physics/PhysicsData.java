package org.schema.schine.physics;

import org.schema.schine.common.fieldcontroller.controllable.Controllable;
import org.schema.schine.common.fieldcontroller.controller.ClientClassController;
import org.schema.schine.network.Identifiable;

public abstract class PhysicsData implements Controllable {
   private Physical entity;
   private PhysicsState state;
   private ClientClassController clientClassController;

   public PhysicsData(Physical var1, PhysicsState var2) {
      this.setEntity(var1);
      this.setState(var2);
   }

   public ClientClassController getClientClassController() {
      return this.clientClassController;
   }

   public int getEntityID() {
      return ((Identifiable)this.entity).getId();
   }

   public PhysicsState getState() {
      return this.state;
   }

   public void setState(PhysicsState var1) {
      this.state = var1;
   }

   public void setClientClassController(ClientClassController var1) {
      this.clientClassController = var1;
   }

   public Physical getEntity() {
      return this.entity;
   }

   public void setEntity(Physical var1) {
      this.entity = var1;
   }

   public String toString() {
      return "Entity: " + this.entity.toString() + "\n" + this.getName();
   }
}
