package org.schema.schine.physics;

import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

public class ExtMotionState extends MotionState {
   public final Transform graphicsWorldTrans = new Transform();
   public final Transform centerOfMassOffset = new Transform();
   public final Transform centerOfMassOffsetInv = new Transform();
   public final Transform startWorldTrans = new Transform();
   private boolean centerDefault = true;

   public ExtMotionState() {
      this.graphicsWorldTrans.setIdentity();
      this.centerOfMassOffset.setIdentity();
      this.startWorldTrans.setIdentity();
   }

   public ExtMotionState(Transform var1) {
      this.graphicsWorldTrans.set(var1);
      this.centerOfMassOffset.setIdentity();
      this.startWorldTrans.set(var1);
   }

   public ExtMotionState(Transform var1, Transform var2) {
      this.graphicsWorldTrans.set(var1);
      this.centerOfMassOffset.set(var2);
      this.startWorldTrans.set(var1);
      this.centerOfMassOffsetInv.set(var2);
      this.centerOfMassOffsetInv.inverse();
      this.centerDefault = false;
   }

   public Transform getWorldTransform(Transform var1) {
      if (!this.centerDefault) {
         var1.set(this.centerOfMassOffsetInv);
         var1.mul(this.graphicsWorldTrans);
         return var1;
      } else {
         var1.set(this.graphicsWorldTrans);
         return var1;
      }
   }

   public void setWorldTransform(Transform var1) {
      this.graphicsWorldTrans.set(var1);
      if (!this.centerDefault) {
         this.graphicsWorldTrans.mul(this.centerOfMassOffset);
      }

   }
}
