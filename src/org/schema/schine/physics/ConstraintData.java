package org.schema.schine.physics;

import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;

public abstract class ConstraintData extends PhysicsData {
   public static final float LIFT_EPS = 0.001F;
   protected String nameA;
   protected String nameB;
   protected String constraintName;
   private TypedConstraint constraint;

   public ConstraintData(TypedConstraint var1, Physical var2, PhysicsState var3, String var4, String var5, String var6) {
      super(var2, var3);
      this.constraint = var1;
      this.nameA = var5;
      this.nameB = var6;
      this.constraintName = var4;
   }

   public TypedConstraint getConstraint() {
      return this.constraint;
   }

   public void setConstraint(TypedConstraint var1) {
      this.constraint = var1;
   }

   public String getName() {
      return this.constraintName;
   }
}
