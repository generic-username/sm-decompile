package org.schema.common;

import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.schema.schine.resource.FileExt;

public class LogUtil {
   private static PrintStream debugLogger;
   private static LoggingOutputStream losErr;
   private static LoggingOutputStream losOut;
   private static FileHandler fileHandler;
   private static Logger logger;
   private static Logger serverLog;
   private static Logger shipyardLog;
   private static boolean serverSetup;
   private static boolean shipyardSetup;
   private static int logFileCount = 20;

   public static void closeAll() throws IOException {
      if (losOut != null || fileHandler != null) {
         try {
            throw new Exception("[LOGUTIL] shutdown logs");
         } catch (Exception var0) {
            var0.printStackTrace();
         }
      }

      if (losOut != null) {
         losOut.close();
      }

      if (losErr != null) {
         losErr.close();
      }

      if (fileHandler != null) {
         if (logger != null) {
            logger.removeHandler(fileHandler);
         }

         fileHandler.close();
      }

   }

   public static PrintStream debug() {
      return debugLogger;
   }

   public static Logger log() {
      return serverLog;
   }

   public static Logger sy() {
      return shipyardLog;
   }

   public static void setUp(int var0, CallInterace var1) throws SecurityException, IOException {
      logFileCount = var0;
      closeAll();
      LogManager var2;
      (var2 = LogManager.getLogManager()).reset();
      if (!serverSetup) {
         setUpServerLog();
      }

      if (!shipyardSetup) {
         setUpShipyardLog();
      }

      FileExt var3;
      if (!(var3 = new FileExt("./logs/")).exists()) {
         var3.mkdirs();
      }

      var1.call();
      (fileHandler = new FileHandler("./logs/logstarmade.%g.log", 4194304, var0)).setFormatter(new CustomFormatter());
      PrintStream var4 = System.out;
      PrintStream var5 = System.err;
      (logger = Logger.getLogger("stdout")).addHandler(fileHandler);
      losOut = new LoggingOutputStream(var4, logger, StdOutErrLevel.STDOUT, "OUT");
      System.setOut(new PrintStream(losOut, true));
      var2.addLogger(logger);
      (logger = Logger.getLogger("stderr")).addHandler(fileHandler);
      losErr = new LoggingOutputStream(var5, logger, StdOutErrLevel.STDERR, "ERR");
      System.setErr(new PrintStream(losErr, true));
      var2.addLogger(logger);
   }

   public static void setUpServerLog() throws SecurityException, IOException {
      serverSetup = true;
      LogManager var0 = LogManager.getLogManager();
      FileExt var1;
      if (!(var1 = new FileExt("./logs/")).exists()) {
         var1.mkdirs();
      }

      FileHandler var3;
      (var3 = new FileHandler("./logs/serverlog.%g.log", 2097152, logFileCount)).setLevel(Level.ALL);
      CustomFormatter var2 = new CustomFormatter();
      var3.setFormatter(var2);
      (serverLog = Logger.getLogger("server")).addHandler(var3);
      serverLog.addHandler(new Handler() {
         public final void publish(LogRecord var1) {
            System.err.println("[SERVERLOG] " + var1.getMessage());
         }

         public final void flush() {
         }

         public final void close() throws SecurityException {
         }
      });
      var0.addLogger(serverLog);
      serverLog.setLevel(Level.ALL);
   }

   public static void setUpShipyardLog() throws SecurityException, IOException {
      shipyardSetup = true;
      LogManager var0 = LogManager.getLogManager();
      FileExt var1;
      if (!(var1 = new FileExt("./logs/")).exists()) {
         var1.mkdirs();
      }

      FileHandler var2;
      (var2 = new FileHandler("./logs/shipyardlog.%g.log", 2097152, logFileCount)).setLevel(Level.ALL);
      var2.setFormatter(new CustomFormatter());
      (shipyardLog = Logger.getLogger("shipyard")).addHandler(var2);
      var0.addLogger(shipyardLog);
      shipyardLog.setLevel(Level.ALL);
      shipyardLog.fine("------------------------------- STARTING NEW LOG ----------------------------");
   }
}
