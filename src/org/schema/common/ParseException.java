package org.schema.common;

public class ParseException extends Exception {
   private static final long serialVersionUID = 1L;

   public ParseException(String var1) {
      super(var1);
   }

   public ParseException(Exception var1) {
      super(var1);
   }
}
