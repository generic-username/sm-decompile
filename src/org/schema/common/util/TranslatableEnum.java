package org.schema.common.util;

public interface TranslatableEnum {
   String getName();
}
