package org.schema.common.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.schema.schine.common.language.Lng;

public class StringTools {
   public static final Pattern p = Pattern.compile("\n|\r");
   static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrsuvwxyz";
   private static final NumberFormat m;
   private static final boolean WITH_DASH = false;
   private static final Pattern parameterRegex;
   private static DecimalFormat threeZero;
   private static DecimalFormat fourZero;
   private static DecimalFormat twoZero;
   private static DecimalFormatSymbols otherSymbols;
   private static DecimalFormat point0;
   private static DecimalFormat point1;
   private static DecimalFormat point2;
   private static DecimalFormat massFormat;
   private static String[] suffix;
   private static int MAX_LENGTH;
   private static Random random;
   private static final DecimalFormat ms;
   private static final String[] units;

   public static String readableFileSize(long var0) {
      if (var0 <= 0L) {
         return "0";
      } else {
         int var2 = (int)(Math.log10((double)var0) / Math.log10(1024.0D));
         return ms.format((double)var0 / Math.pow(1024.0D, (double)var2)) + " " + units[var2];
      }
   }

   public static String cpt(String var0) {
      String var1 = "";

      for(int var2 = 0; var2 < var0.length(); ++var2) {
         char var3;
         if (Character.isUpperCase(var3 = var0.charAt(var2))) {
            var1 = var1 + " " + Character.toLowerCase(var3);
         } else {
            var1 = var1 + var3;
         }
      }

      return var1;
   }

   public static String escapeRegexp(String var0) {
      String var1 = "\\$.*+?|()[]{}^";

      for(int var2 = 0; var2 < var1.length(); ++var2) {
         Character var3 = var1.charAt(var2);
         var0 = var0.replaceAll("\\" + var3, "\\\\" + (var2 < 2 ? "\\" : "") + var3);
      }

      return var0;
   }

   public static List findGroup(String var0, String var1, int var2) {
      Matcher var3 = Pattern.compile(var1).matcher(var0);
      ArrayList var4 = new ArrayList();

      while(var3.find()) {
         var4.add(var3.group(var2));
      }

      return var4;
   }

   public static String formatSeperated(int var0) {
      return m.format((long)var0);
   }

   public static String formatSeperated(double var0) {
      return m.format(var0);
   }

   public static String formatSeperated(long var0) {
      return m.format(var0);
   }

   public static List tokenize(String var0, String var1, String var2) {
      var1 = var2.length() > 1 ? escapeRegexp(var1) + "(.*?)" + escapeRegexp(var2) : escapeRegexp(var1) + "([^" + var2 + "]*)" + escapeRegexp(var2);
      return findGroup(var0, var1, 1);
   }

   public static String[] splitTotokens(String var0, String var1) {
      String var2 = var0;

      int var3;
      for(var3 = 0; var2.contains(var1); ++var3) {
         var2 = var2.substring(var2.indexOf(var1) + var1.length());
      }

      String[] var4 = new String[var3];

      for(int var5 = 0; var5 < var3; ++var5) {
         var2 = var0.substring(0, var0.indexOf(var1));
         var4[var5] = var2;
         var0 = var0.substring(var0.indexOf(var1) + var1.length());
      }

      return var4;
   }

   public static String massFormat(long var0) {
      if (var0 < 1000L) {
         return String.valueOf(var0);
      } else {
         String var2;
         for(var2 = (var2 = massFormat.format(var0)).replaceAll("E[0-9]", suffix[Character.getNumericValue(var2.charAt(var2.length() - 1)) / 3]); var2.length() > MAX_LENGTH || var2.matches("[0-9]+\\.[a-z]"); var2 = var2.substring(0, var2.length() - 2) + var2.substring(var2.length() - 1)) {
         }

         return var2;
      }
   }

   public static String massFormat(int var0) {
      if (var0 < 1000) {
         return String.valueOf(var0);
      } else {
         String var1;
         for(var1 = (var1 = massFormat.format((long)var0)).replaceAll("E[0-9]", suffix[Character.getNumericValue(var1.charAt(var1.length() - 1)) / 3]); var1.length() > MAX_LENGTH || var1.matches("[0-9]+\\.[a-z]"); var1 = var1.substring(0, var1.length() - 2) + var1.substring(var1.length() - 1)) {
         }

         return var1;
      }
   }

   public static String massFormat(float var0) {
      if (var0 < 1000.0F) {
         return formatPointZero(var0);
      } else {
         String var1;
         for(var1 = (var1 = massFormat.format((double)var0)).replaceAll("E[0-9]", suffix[Character.getNumericValue(var1.charAt(var1.length() - 1)) / 3]); var1.length() > MAX_LENGTH || var1.matches("[0-9]+\\.[a-z]"); var1 = var1.substring(0, var1.length() - 2) + var1.substring(var1.length() - 1)) {
         }

         return var1;
      }
   }

   public static String massFormat(double var0) {
      if (var0 < 1000.0D) {
         return formatPointZero(var0);
      } else {
         String var2;
         for(var2 = (var2 = massFormat.format(var0)).replaceAll("E[0-9]", suffix[Character.getNumericValue(var2.charAt(var2.length() - 1)) / 3]); var2.length() > MAX_LENGTH || var2.matches("[0-9]+\\.[a-z]"); var2 = var2.substring(0, var2.length() - 2) + var2.substring(var2.length() - 1)) {
         }

         return var2;
      }
   }

   public static int coundNewLines(String var0) {
      Matcher var2 = p.matcher(var0);

      int var1;
      for(var1 = 1; var2.find(); ++var1) {
      }

      return var1;
   }

   public static String fill(String var0, int var1) {
      var1 -= var0.length();
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.append(" ");
      }

      return var0 + var2.toString();
   }

   public static String formatFourZero(int var0) {
      return fourZero.format((long)var0);
   }

   public static String formatThreeZero(int var0) {
      return threeZero.format((long)var0);
   }

   public static String formatPointZero(double var0) {
      return point0.format(var0);
   }

   public static String formatPointZero(float var0) {
      return point0.format((double)var0);
   }

   public static String formatPointZeroZero(double var0) {
      return point1.format(var0);
   }

   public static String formatPointZeroZero(float var0) {
      return point1.format((double)var0);
   }

   public static String formatPointZeroZeroZero(double var0) {
      return point2.format(var0);
   }

   public static String formatPointZeroZeroZero(float var0) {
      return point2.format((double)var0);
   }

   public static String autoComplete(String var0, Collection var1, boolean var2, StringInterface var3) {
      var0 = (new String(var0.toLowerCase(Locale.ENGLISH))).trim();
      ArrayList var4 = new ArrayList();
      Iterator var8 = var1.iterator();

      while(var8.hasNext()) {
         Object var5 = var8.next();
         if (var2) {
            if (var3.get(var5).toLowerCase(Locale.ENGLISH).startsWith(var0)) {
               var4.add(var3.get(var5).toLowerCase(Locale.ENGLISH));
            }
         } else if (var3.get(var5).toLowerCase(Locale.ENGLISH).startsWith(var0)) {
            var4.add(var3.get(var5));
         }
      }

      String var9 = new String(var0);
      boolean var12 = true;
      Iterator var7 = var4.iterator();

      while(var7.hasNext()) {
         String var10 = (String)var7.next();
         String var11 = "";

         for(Iterator var6 = var4.iterator(); var6.hasNext(); var11 = new String(LongestCommonSubsequence(var11, var10))) {
            var11 = (String)var6.next();
         }

         if (var12) {
            var9 = new String(var11);
            var12 = false;
         } else {
            var9 = LongestCommonSubsequence(var9, var11);
         }
      }

      return var9;
   }

   public static String formatTwoZero(int var0) {
      return twoZero.format((long)var0);
   }

   public static List LongestCommonSubsequence(Object[] var0, Object[] var1) {
      int[][] var2 = new int[var0.length + 1][var1.length + 1];

      int var3;
      int var4;
      for(var3 = 1; var3 <= var0.length; ++var3) {
         for(var4 = 1; var4 <= var1.length; ++var4) {
            if (var0[var3 - 1].equals(var1[var4 - 1])) {
               var2[var3][var4] = 1 + var2[var3 - 1][var4 - 1];
            } else {
               var2[var3][var4] = Math.max(var2[var3 - 1][var4], var2[var3][var4 - 1]);
            }
         }
      }

      var3 = var0.length;
      var4 = var1.length;
      LinkedList var5 = new LinkedList();

      while(var3 != 0 && var4 != 0) {
         if (var0[var3 - 1].equals(var1[var4 - 1])) {
            var5.add(var0[var3 - 1]);
            --var3;
            --var4;
         } else if (var2[var3][var4 - 1] >= var2[var3 - 1][var4]) {
            --var4;
         } else {
            --var3;
         }
      }

      Collections.reverse(var5);
      return var5;
   }

   public static String LongestCommonSubsequence(String var0, String var1) {
      char[] var5 = var0.toCharArray();
      char[] var7 = var1.toCharArray();
      Character[] var2 = new Character[var5.length];

      for(int var3 = 0; var3 < var5.length; ++var3) {
         var2[var3] = var5[var3];
      }

      Character[] var10 = new Character[var7.length];

      for(int var6 = 0; var6 < var7.length; ++var6) {
         var10[var6] = var7[var6];
      }

      StringBuffer var8 = new StringBuffer();
      List var9 = LongestCommonSubsequence((Object[])var2, (Object[])var10);

      for(int var4 = 0; var4 < var9.size(); ++var4) {
         if (var2[var4].equals(var9.get(var4)) && var10[var4].equals(var9.get(var4))) {
            var8.append(var9.get(var4));
         }
      }

      return var8.toString();
   }

   public static String pathToString(ArrayList var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = var0.size() - 1; var2 >= 0; --var2) {
         var1.append("[" + ((int[])var0.get(var2))[0] + "," + ((int[])var0.get(var2))[1] + "]->");
      }

      return var1.toString();
   }

   public static String randomString(int var0) {
      StringBuilder var1 = new StringBuilder(var0);

      for(int var2 = 0; var2 < var0; ++var2) {
         var1.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrsuvwxyz".charAt(random.nextInt(61)));
      }

      return var1.toString();
   }

   public static String[] splitParameters(String var0) {
      ArrayList var1 = new ArrayList();
      Matcher var3 = parameterRegex.matcher(var0);

      while(var3.find()) {
         if (var3.group(1) != null) {
            var1.add(var3.group(1));
         } else if (var3.group(2) != null) {
            var1.add(var3.group(2));
         } else {
            var1.add(var3.group());
         }
      }

      String[] var4 = new String[var1.size()];

      for(int var2 = 0; var2 < var4.length; ++var2) {
         System.err.println("[CLIENT] Admin parameter " + var2 + ": " + (String)var1.get(var2));
         var4[var2] = (String)var1.get(var2);
      }

      return var4;
   }

   public static String wrap(String var0, int var1) {
      if ((var0 = var0.trim()).length() < var1) {
         return var0;
      } else if (var0.substring(0, var1).contains("\n")) {
         return var0.substring(0, var0.indexOf("\n")).trim() + "\n\n" + wrap(var0.substring(var0.indexOf("\n") + 1), var1);
      } else {
         int var2;
         return (var2 = Math.max(var0.lastIndexOf(" ", var1), var0.lastIndexOf("\t", var1))) < 0 ? var0 : var0.substring(0, var2).trim() + "\n" + wrap(var0.substring(var2), var1);
      }
   }

   public static String formatDistance(float var0) {
      return var0 > 1000.0F ? formatPointZero(var0 / 1000.0F) + "km" : formatPointZero(var0) + "m";
   }

   public static String formatDistance(double var0) {
      return var0 > 1000.0D ? formatPointZero(var0 / 1000.0D) + "km" : formatPointZero(var0) + "m";
   }

   public static String getCommaSeperated(double[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String getCommaSeperated(long[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String getCommaSeperated(float[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String getCommaSeperated(byte[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String getCommaSeperated(int[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String getCommaSeperated(short[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2]);
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String formatTimeFromMS(long var0) {
      if (var0 < 60000L) {
         return formatPointZero((double)var0 / 1000.0D) + " secs";
      } else {
         int var1;
         int var2;
         int var4;
         int var3 = (var2 = (var1 = (var4 = (int)(var0 / 1000L)) / 60) / 60) / 24;
         var4 %= 60;
         var1 %= 60;
         var2 %= 24;
         if (var3 > 0) {
            return var3 + " d, " + var2 + " h, " + var1 + " min";
         } else if (var2 > 0) {
            return var2 + " h, " + var1 + " min";
         } else {
            return var1 > 0 ? var1 + " min, " + var4 + " secs" : var4 + " secs";
         }
      }
   }

   public static String formatSmallAndBig(int var0) {
      return formatSeperated(var0);
   }

   public static String formatSmallAndBig(long var0) {
      return formatSeperated(var0);
   }

   public static String formatSmallAndBig(float var0) {
      boolean var1;
      if ((var1 = var0 % 1.0F != 0.0F) && Math.abs(var0) < 1.0F) {
         String var2;
         return (var2 = formatPointZeroZero(var0)).endsWith("0") ? formatPointZero(var0) : var2;
      } else {
         return var1 && Math.abs(var0) < 100.0F ? formatPointZero(var0) : formatSeperated((double)((float)((int)(var0 * 10.0F)) / 10.0F));
      }
   }

   public static String formatSmallAndBig(double var0) {
      boolean var2;
      if ((var2 = var0 % 1.0D != 0.0D) && Math.abs(var0) < 1.0D) {
         return formatPointZeroZero(var0);
      } else {
         return var2 && Math.abs(var0) < 100.0D ? formatPointZero(var0) : formatSeperated((double)((long)(var0 * 10.0D)) / 10.0D);
      }
   }

   public static String getFormatedMessage(Object[] var0) {
      try {
         if (var0[0] instanceof Integer) {
            int var1 = (Integer)var0[0];
            var0[0] = Lng.getByIndex(var1);
            if (var0[0] == null) {
               var0[0] = "srv msg translation missing for client (" + var1 + ")";
            }

            System.err.println("[CLIENT] Server message: " + var0[0]);
         }

         if (var0.length == 1) {
            return var0[0].toString();
         } else {
            switch(var0.length) {
            case 2:
               return String.format(var0[0].toString(), var0[1]);
            case 3:
               return String.format(var0[0].toString(), var0[1], var0[2]);
            case 4:
               return String.format(var0[0].toString(), var0[1], var0[2], var0[3]);
            case 5:
               return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4]);
            case 6:
               return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5]);
            case 7:
               return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5]);
            default:
               return String.format(var0[0].toString(), var0[1], var0[2], var0[3], var0[4], var0[5], var0[6]);
            }
         }
      } catch (Exception var2) {
         var2.printStackTrace();
         return "TRANSLATION ERROR ON '" + (var0 != null && var0.length > 0 ? var0[0] : "{INVALID MESSAGE ARRAY}") + "'; PLEASE SEND IN REPORT FOR THAT LINE.";
      }
   }

   public static String formatRaceTime(long var0) {
      long var2 = TimeUnit.MILLISECONDS.toHours(var0);
      long var4 = TimeUnit.MILLISECONDS.toMinutes(var0 - TimeUnit.HOURS.toMillis(var2));
      long var6 = TimeUnit.MILLISECONDS.toSeconds(var0 - TimeUnit.HOURS.toMillis(var2) - TimeUnit.MINUTES.toMillis(var4));
      long var8 = TimeUnit.MILLISECONDS.toMillis(var0 - TimeUnit.HOURS.toMillis(var2) - TimeUnit.MINUTES.toMillis(var4) - TimeUnit.SECONDS.toMillis(var6));
      return String.format("%02d:%02d.%03d", var4, var6, var8);
   }

   public static String formatCountdown(int var0) {
      long var1 = TimeUnit.SECONDS.toHours((long)var0);
      long var3 = TimeUnit.SECONDS.toMinutes((long)var0 - TimeUnit.HOURS.toSeconds(var1));
      long var5 = TimeUnit.SECONDS.toSeconds((long)var0 - TimeUnit.HOURS.toSeconds(var1) - TimeUnit.MINUTES.toSeconds(var3));
      return String.format("%02d:%02d:%02d", var1, var3, var5);
   }

   public static String format(String var0, Object... var1) {
      try {
         return String.format(var0, var1);
      } catch (Exception var2) {
         var2.printStackTrace();
         System.err.println("Exception String: " + var0);
         System.err.println("Exception Args: " + Arrays.toString(var1));
         return "TRANSLATION ERROR ON '" + var0 + "'; PLEASE SEND IN REPORT FOR THAT LINE.";
      }
   }

   public static SimpleDateFormat getSimpleDateFormat(String var0, String var1) {
      try {
         return new SimpleDateFormat(var0);
      } catch (Exception var3) {
         System.err.println("Exception: INVALID DATE TIME: " + var0);
         var3.printStackTrace();
         return new SimpleDateFormat(var1);
      }
   }

   public static String limit(String var0, int var1, int var2) {
      return limitLines(limit(var0, var1), var2);
   }

   public static String limit(String var0, int var1) {
      return var0.length() > var1 ? var0.substring(0, var1) : var0;
   }

   public static String limitLines(String var0, int var1) {
      while(getOccurencesCoutn(var0, "\n") > var1) {
         var0 = var0.substring(0, var0.lastIndexOf("\n"));
      }

      return var0;
   }

   public static int getOccurencesCoutn(String var0, String var1) {
      int var2 = 0;
      int var3 = 0;

      while(var2 != -1) {
         if ((var2 = var0.indexOf(var1, var2)) != -1) {
            ++var3;
            var2 += var1.length();
         }
      }

      return var3;
   }

   public static String listEnum(Enum[] var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.append(var0[var2].name());
         if (var2 < var0.length - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public static String seperate(Object[] var0, String var1) {
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < var0.length; ++var3) {
         var2.append(var0[var3]);
         if (var3 < var0.length - 1) {
            var2.append(var1);
         }
      }

      return var2.toString();
   }

   public static String seperate(Collection var0, String var1) {
      StringBuffer var2 = new StringBuffer();
      Iterator var3 = var0.iterator();

      while(var3.hasNext()) {
         var2.append(var3.next());
         if (var3.hasNext()) {
            var2.append(var1);
         }
      }

      return var2.toString();
   }

   public static String formatBytes(long var0) {
      long var2;
      if ((var2 = Math.abs(var0)) < 1024L) {
         return var0 + "bytes";
      } else if (var2 < 1048576L) {
         return var0 / 1024L + "kb";
      } else {
         return var2 < 536870912L ? var0 / 1048576L + "mb" : formatPointZeroZero((double)var0 / 1.073741824E9D) + "gb";
      }
   }

   public static String getOnOff(boolean var0) {
      return var0 ? Lng.ORG_SCHEMA_COMMON_UTIL_STRINGTOOLS_0 : Lng.ORG_SCHEMA_COMMON_UTIL_STRINGTOOLS_1;
   }

   static {
      m = NumberFormat.getNumberInstance(Locale.US);
      parameterRegex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
      threeZero = new DecimalFormat("000");
      fourZero = new DecimalFormat("0000");
      twoZero = new DecimalFormat("00");
      otherSymbols = new DecimalFormatSymbols(Locale.US);
      point0 = new DecimalFormat("#0.0", otherSymbols);
      point1 = new DecimalFormat("#0.00", otherSymbols);
      point2 = new DecimalFormat("#0.000", otherSymbols);
      massFormat = new DecimalFormat("##0E0");
      suffix = new String[]{"", "k", "m", "b", "t"};
      MAX_LENGTH = 4;
      random = new Random();
      ms = new DecimalFormat("#,##0.#");
      units = new String[]{"B", "kB", "MB", "GB", "TB"};
   }

   public class StringLengthComparator implements Comparator {
      public int compare(String var1, String var2) {
         if (var1.length() < var2.length()) {
            return -1;
         } else {
            return var1.length() > var2.length() ? 1 : 0;
         }
      }
   }
}
