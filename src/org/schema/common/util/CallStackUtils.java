package org.schema.common.util;

import java.util.ArrayList;

public class CallStackUtils {
   public static synchronized String getCallStackHeadAsString() {
      StackTraceElement[] var0;
      return (var0 = Thread.currentThread().getStackTrace()).length > 2 ? getStackTraceElementAtIndexAsString(var0, 2) : "";
   }

   public static synchronized String getCallStackAsString() {
      StringBuilder var0 = new StringBuilder();
      String[] var1 = getCallStackAsStringArray(Thread.currentThread().getStackTrace());

      for(int var2 = 0; var2 < var1.length; ++var2) {
         var0.append(var1[var2] + "\n");
      }

      return var0.toString();
   }

   public static synchronized String[] getCallStackAsStringArray() {
      return getCallStackAsStringArray(Thread.currentThread().getStackTrace());
   }

   private static synchronized String[] getCallStackAsStringArray(StackTraceElement[] var0) {
      ArrayList var1 = new ArrayList();
      String[] var2 = new String[1];

      for(int var3 = 0; var3 < var0.length; ++var3) {
         var1.add(getStackTraceElementAtIndexAsString(var0, var3));
      }

      return (String[])var1.toArray(var2);
   }

   private static String getStackTraceElementAtIndexAsString(StackTraceElement[] var0, int var1) {
      StackTraceElement var3;
      String var5 = (var3 = var0[var1]).getClassName();
      String var2 = var3.getMethodName();
      int var4 = var3.getLineNumber();
      return var5 + "." + var2 + ":" + var4;
   }
}
