package org.schema.common.util;

public class QuickSort {
   public static void sort(int var0, int var1, ArraySortInterface var2) {
      while(true) {
         int var3 = var0;
         int var4 = var1;
         if (var1 - var0 <= 0) {
            return;
         }

         float var5 = var2.getValue(var0);

         while(var4 > var3) {
            while(var2.getValue(var3) <= var5 && var3 <= var1 && var4 > var3) {
               ++var3;
            }

            while(var2.getValue(var4) > var5 && var4 >= var0 && var4 >= var3) {
               --var4;
            }

            if (var4 > var3) {
               var2.swapValues(var3, var4);
            }
         }

         var2.swapValues(var0, var4);
         sort(var0, var4 - 1, var2);
         var0 = var4 + 1;
      }
   }

   public static void sort(int[] var0, int var1, int var2) {
      while(true) {
         int var3 = var1;
         int var4 = var2;
         if (var1 >= var2) {
            return;
         }

         int var5 = var0[(var1 + var2) / 2];

         while(var3 < var4) {
            while(var3 < var4 && var0[var3] < var5) {
               ++var3;
            }

            while(var3 < var4 && var0[var4] > var5) {
               --var4;
            }

            if (var3 < var4) {
               int var6 = var0[var3];
               var0[var3] = var0[var4];
               var0[var4] = var6;
            }
         }

         if (var4 < var3) {
            var3 = var4;
         }

         sort(var0, var1, var3);
         var1 = var3 == var1 ? var3 + 1 : var3;
         var0 = var0;
      }
   }
}
