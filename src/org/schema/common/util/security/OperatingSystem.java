package org.schema.common.util.security;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import org.schema.schine.resource.FileExt;

public enum OperatingSystem {
   LINUX(new String[]{"linux", "unix"}),
   SOLARIS(new String[]{"sunos", "solaris"}),
   WINDOWS(new String[]{"win"}),
   MAC(new String[]{"mac"}),
   UNKNOWN(new String[0]);

   public static OperatingSystem OS = null;
   public final String[] ids;
   public String serial;

   private OperatingSystem(String... var3) {
      this.ids = var3;
   }

   public static File getAppDir() throws IOException {
      return getAppDir("StarMade");
   }

   public static File getAppDir(String var0) throws IOException {
      String var1 = System.getProperty("user.home", ".");
      FileExt var3;
      switch(getOS()) {
      case WINDOWS:
         String var2;
         if ((var2 = System.getenv("APPDATA")) != null) {
            var3 = new FileExt(var2, "." + var0 + '/');
         } else {
            var3 = new FileExt(var1, "." + var0 + '/');
         }
         break;
      case MAC:
         var3 = new FileExt(var1, "Library/Application Support/" + var0);
         break;
      case LINUX:
      case SOLARIS:
         var3 = new FileExt(var1, "." + var0 + '/');
         break;
      default:
         var3 = new FileExt(var1, var0 + '/');
      }

      if (!var3.exists() && !var3.mkdirs()) {
         throw new IOException("Error: Failed to create working directory: " + var3);
      } else if (!var3.isDirectory()) {
         throw new IOException("Error: File is not a directory: " + var3);
      } else {
         return var3;
      }
   }

   public static OperatingSystem getOS() {
      if (OS == null) {
         String var0 = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);
         OS = UNKNOWN;
         OperatingSystem[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            OperatingSystem var4 = var1[var3];

            for(int var5 = 0; var5 < var4.ids.length; ++var5) {
               if (var0.contains(var4.ids[var5])) {
                  try {
                     var4.serial = "not retrieved";
                  } catch (Exception var6) {
                     var6.printStackTrace();
                  }

                  OS = var4;
                  return var4;
               }
            }
         }
      }

      return OS;
   }
}
