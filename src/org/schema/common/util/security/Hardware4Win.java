package org.schema.common.util.security;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Hardware4Win {
   private static String sn = null;

   public static final String getSerialNumber() {
      if (sn != null) {
         return sn;
      } else {
         Runtime var0 = Runtime.getRuntime();

         Process var1;
         try {
            var1 = var0.exec(new String[]{"wmic", "bios", "get", "serialnumber"});
         } catch (IOException var11) {
            throw new RuntimeException(var11);
         }

         OutputStream var13 = var1.getOutputStream();
         InputStream var15 = var1.getInputStream();

         try {
            var13.close();
         } catch (IOException var10) {
            throw new RuntimeException(var10);
         }

         Scanner var14 = new Scanner(var15);

         try {
            boolean var2 = false;

            while(var14.hasNext()) {
               String var3 = var14.next();
               if (var2) {
                  sn = sn + var3.trim() + " ";
               }

               if (!var2) {
                  var2 = "SerialNumber".equals(var3);
                  sn = "";
               }
            }

            if (var2) {
               sn = sn.trim();
            }
         } finally {
            try {
               var15.close();
               var14.close();
            } catch (IOException var9) {
               throw new RuntimeException(var9);
            }
         }

         if (sn == null) {
            throw new RuntimeException("Cannot find computer SN");
         } else {
            return sn;
         }
      }
   }
}
