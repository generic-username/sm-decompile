package org.schema.common.util;

public interface ArraySortInterface {
   float getValue(int var1);

   void swapValues(int var1, int var2);
}
