package org.schema.common.util;

import java.awt.Color;
import java.util.Arrays;

public class ColorTools {
   public static void main(String[] var0) {
      long var1 = System.currentTimeMillis();

      for(int var3 = 0; var3 < 1024; ++var3) {
         getColor(var3);
      }

      System.err.println(System.currentTimeMillis() - var1 + "ms");
   }

   public static Color getColor(int var0) {
      return new Color(getRGB(var0));
   }

   public static int getRGB(int var0) {
      int[] var1;
      return getElement((var1 = getPattern(var0))[0]) << 16 | getElement(var1[1]) << 8 | getElement(var1[2]);
   }

   public static int getElement(int var0) {
      --var0;
      int var1 = 0;

      for(int var2 = 0; var2 < 8; ++var2) {
         var1 = (var1 | var0 & 1) << 1;
         var0 >>= 1;
      }

      return var1 >> 1 & 255;
   }

   public static int[] getPattern(int var0) {
      int var1 = (int)Math.cbrt((double)var0);
      var0 -= var1 * var1 * var1;
      int[] var2;
      Arrays.fill(var2 = new int[3], var1);
      if (var0 == 0) {
         return var2;
      } else {
         --var0;
         int var3 = var0 % 3;
         if ((var0 /= 3) < var1) {
            var2[var3] = var0 % var1;
            return var2;
         } else {
            var0 -= var1;
            var2[var3] = var0 / var1;
            ++var3;
            var2[var3 % 3] = var0 % var1;
            return var2;
         }
      }
   }
}
