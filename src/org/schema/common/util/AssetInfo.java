package org.schema.common.util;

import java.io.InputStream;

public abstract class AssetInfo {
   public abstract AssetId getId();

   public abstract InputStream openStream();
}
