package org.schema.common.util;

public interface LogInterface {
   void log(String var1, LogInterface.LogLevel var2);

   public static enum LogLevel {
      FINE,
      NORMAL,
      ERROR,
      DEBUG;
   }

   public static enum LogMode {
      FILE,
      STDERR;
   }
}
