package org.schema.common.util.linAlg;

import java.io.Serializable;
import java.rmi.Remote;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Matrix16f implements Serializable, Cloneable, Remote {
   private static final long serialVersionUID = 65793L;
   public final float[] m;

   public strictfp Matrix16f() {
      this.m = new float[16];
   }

   public strictfp Matrix16f(float[] var1) throws RuntimeException {
      if (var1 != null && var1.length == 16) {
         this.m = var1;
      } else {
         throw new RuntimeException();
      }
   }

   public static strictfp double determinant(float[] var0) {
      return (double)(var0[3] * var0[6] * var0[9] * var0[12] - var0[2] * var0[7] * var0[9] * var0[12] - var0[3] * var0[5] * var0[10] * var0[12] + var0[1] * var0[7] * var0[10] * var0[12] + var0[2] * var0[5] * var0[11] * var0[12] - var0[1] * var0[6] * var0[11] * var0[12] - var0[3] * var0[6] * var0[8] * var0[13] + var0[2] * var0[7] * var0[8] * var0[13] + var0[3] * var0[4] * var0[10] * var0[13] - var0[0] * var0[7] * var0[10] * var0[13] - var0[2] * var0[4] * var0[11] * var0[13] + var0[0] * var0[6] * var0[11] * var0[13] + var0[3] * var0[5] * var0[8] * var0[14] - var0[1] * var0[7] * var0[8] * var0[14] - var0[3] * var0[4] * var0[9] * var0[14] + var0[0] * var0[7] * var0[9] * var0[14] + var0[1] * var0[4] * var0[11] * var0[14] - var0[0] * var0[5] * var0[11] * var0[14] - var0[2] * var0[5] * var0[8] * var0[15] + var0[1] * var0[6] * var0[8] * var0[15] + var0[2] * var0[4] * var0[9] * var0[15] - var0[0] * var0[6] * var0[9] * var0[15] - var0[1] * var0[4] * var0[10] * var0[15] + var0[0] * var0[5] * var0[10] * var0[15]);
   }

   public static strictfp Matrix16f identity() {
      Matrix16f var0;
      (var0 = new Matrix16f()).m[0] = 1.0F;
      var0.m[5] = 1.0F;
      var0.m[10] = 1.0F;
      var0.m[15] = 1.0F;
      return var0;
   }

   public static strictfp float[] invert(float[] var0) {
      float[] var1;
      (var1 = new float[16])[0] = var0[6] * var0[11] * var0[13] - var0[7] * var0[10] * var0[13] + var0[7] * var0[9] * var0[14] - var0[5] * var0[11] * var0[14] - var0[6] * var0[9] * var0[15] + var0[5] * var0[10] * var0[15];
      var1[1] = var0[3] * var0[10] * var0[13] - var0[2] * var0[11] * var0[13] - var0[3] * var0[9] * var0[14] + var0[1] * var0[11] * var0[14] + var0[2] * var0[9] * var0[15] - var0[1] * var0[10] * var0[15];
      var1[2] = var0[2] * var0[7] * var0[13] - var0[3] * var0[6] * var0[13] + var0[3] * var0[5] * var0[14] - var0[1] * var0[7] * var0[14] - var0[2] * var0[5] * var0[15] + var0[1] * var0[6] * var0[15];
      var1[3] = var0[3] * var0[6] * var0[9] - var0[2] * var0[7] * var0[9] - var0[3] * var0[5] * var0[10] + var0[1] * var0[7] * var0[10] + var0[2] * var0[5] * var0[11] - var0[1] * var0[6] * var0[11];
      var1[4] = var0[7] * var0[10] * var0[12] - var0[6] * var0[11] * var0[12] - var0[7] * var0[8] * var0[14] + var0[4] * var0[11] * var0[14] + var0[6] * var0[8] * var0[15] - var0[4] * var0[10] * var0[15];
      var1[5] = var0[2] * var0[11] * var0[12] - var0[3] * var0[10] * var0[12] + var0[3] * var0[8] * var0[14] - var0[0] * var0[11] * var0[14] - var0[2] * var0[8] * var0[15] + var0[0] * var0[10] * var0[15];
      var1[6] = var0[3] * var0[6] * var0[12] - var0[2] * var0[7] * var0[12] - var0[3] * var0[4] * var0[14] + var0[0] * var0[7] * var0[14] + var0[2] * var0[4] * var0[15] - var0[0] * var0[6] * var0[15];
      var1[7] = var0[2] * var0[7] * var0[8] - var0[3] * var0[6] * var0[8] + var0[3] * var0[4] * var0[10] - var0[0] * var0[7] * var0[10] - var0[2] * var0[4] * var0[11] + var0[0] * var0[6] * var0[11];
      var1[8] = var0[5] * var0[11] * var0[12] - var0[7] * var0[9] * var0[12] + var0[7] * var0[8] * var0[13] - var0[4] * var0[11] * var0[13] - var0[5] * var0[8] * var0[15] + var0[4] * var0[9] * var0[15];
      var1[9] = var0[3] * var0[9] * var0[12] - var0[1] * var0[11] * var0[12] - var0[3] * var0[8] * var0[13] + var0[0] * var0[11] * var0[13] + var0[1] * var0[8] * var0[15] - var0[0] * var0[9] * var0[15];
      var1[10] = var0[1] * var0[7] * var0[12] - var0[3] * var0[5] * var0[12] + var0[3] * var0[4] * var0[13] - var0[0] * var0[7] * var0[13] - var0[1] * var0[4] * var0[15] + var0[0] * var0[5] * var0[15];
      var1[11] = var0[3] * var0[5] * var0[8] - var0[1] * var0[7] * var0[8] - var0[3] * var0[4] * var0[9] + var0[0] * var0[7] * var0[9] + var0[1] * var0[4] * var0[11] - var0[0] * var0[5] * var0[11];
      var1[12] = var0[6] * var0[9] * var0[12] - var0[5] * var0[10] * var0[12] - var0[6] * var0[8] * var0[13] + var0[4] * var0[10] * var0[13] + var0[5] * var0[8] * var0[14] - var0[4] * var0[9] * var0[14];
      var1[13] = var0[1] * var0[10] * var0[12] - var0[2] * var0[9] * var0[12] + var0[2] * var0[8] * var0[13] - var0[0] * var0[10] * var0[13] - var0[1] * var0[8] * var0[14] + var0[0] * var0[9] * var0[14];
      var1[14] = var0[2] * var0[5] * var0[12] - var0[1] * var0[6] * var0[12] - var0[2] * var0[4] * var0[13] + var0[0] * var0[6] * var0[13] + var0[1] * var0[4] * var0[14] - var0[0] * var0[5] * var0[14];
      var1[15] = var0[1] * var0[6] * var0[8] - var0[2] * var0[5] * var0[8] + var0[2] * var0[4] * var0[9] - var0[0] * var0[6] * var0[9] - var0[1] * var0[4] * var0[10] + var0[0] * var0[5] * var0[10];
      double var2 = determinant(var0);

      for(int var4 = 0; var4 < var0.length; ++var4) {
         var1[var4] = (float)((double)var1[var4] / var2);
      }

      return var1;
   }

   public static strictfp Matrix16f lookAt(Vector3f var0, Vector3f var1, Vector3f var2) {
      var1 = Vector3fTools.sub(var1, var0);
      return lookAt(Vector3fTools.crossProduct(var2, var1), var2, var1, var0);
   }

   public static strictfp Matrix16f lookAt(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      Matrix16f var4;
      (var4 = new Matrix16f()).m[0] = -var0.x;
      var4.m[4] = -var0.y;
      var4.m[8] = -var0.z;
      var4.m[12] = 0.0F;
      var4.m[1] = var1.x;
      var4.m[5] = var1.y;
      var4.m[9] = var1.z;
      var4.m[13] = 0.0F;
      var4.m[2] = -var2.x;
      var4.m[6] = -var2.y;
      var4.m[10] = -var2.z;
      var4.m[14] = 0.0F;
      var4.m[3] = 0.0F;
      var4.m[7] = 0.0F;
      var4.m[11] = 0.0F;
      var4.m[15] = 1.0F;
      var4.translate(new Vector3f(-var3.x, -var3.y, -var3.z));
      return var4;
   }

   public static strictfp void printMatrix(float[] var0) {
      System.err.println(var0[0] + "   " + var0[4] + "   " + var0[8] + "   " + var0[12]);
      System.err.println(var0[1] + "   " + var0[5] + "   " + var0[9] + "   " + var0[13]);
      System.err.println(var0[2] + "   " + var0[6] + "   " + var0[10] + "   " + var0[14]);
      System.err.println(var0[3] + "   " + var0[7] + "   " + var0[11] + "   " + var0[15]);
   }

   public static strictfp Matrix16f rotationAroundX(float var0) {
      float var1 = FastMath.cos(var0);
      var0 = FastMath.sin(var0);
      Matrix16f var2;
      (var2 = new Matrix16f()).m[0] = 1.0F;
      var2.m[4] = 0.0F;
      var2.m[8] = 0.0F;
      var2.m[12] = 0.0F;
      var2.m[1] = 0.0F;
      var2.m[5] = var1;
      var2.m[9] = var0;
      var2.m[13] = 0.0F;
      var2.m[2] = 0.0F;
      var2.m[6] = -var0;
      var2.m[10] = var1;
      var2.m[14] = 0.0F;
      var2.m[3] = 0.0F;
      var2.m[7] = 0.0F;
      var2.m[11] = 0.0F;
      var2.m[15] = 1.0F;
      return var2;
   }

   public static strictfp Matrix16f rotationAroundY(float var0) {
      float var1 = FastMath.cos(var0);
      var0 = FastMath.sin(var0);
      Matrix16f var2;
      (var2 = new Matrix16f()).m[0] = var1;
      var2.m[4] = 0.0F;
      var2.m[8] = var0;
      var2.m[12] = 0.0F;
      var2.m[1] = 0.0F;
      var2.m[5] = 1.0F;
      var2.m[9] = 0.0F;
      var2.m[13] = 0.0F;
      var2.m[2] = -var0;
      var2.m[6] = 0.0F;
      var2.m[10] = var1;
      var2.m[14] = 0.0F;
      var2.m[3] = 0.0F;
      var2.m[7] = 0.0F;
      var2.m[11] = 0.0F;
      var2.m[15] = 1.0F;
      return var2;
   }

   public static strictfp Matrix16f rotationAroundZ(float var0) {
      float var1 = FastMath.cos(var0);
      var0 = FastMath.sin(var0);
      Matrix16f var2;
      (var2 = new Matrix16f()).m[0] = var1;
      var2.m[4] = var0;
      var2.m[8] = 0.0F;
      var2.m[12] = 0.0F;
      var2.m[1] = -var0;
      var2.m[5] = var1;
      var2.m[9] = 0.0F;
      var2.m[13] = 0.0F;
      var2.m[2] = 0.0F;
      var2.m[6] = 0.0F;
      var2.m[10] = 1.0F;
      var2.m[14] = 0.0F;
      var2.m[3] = 0.0F;
      var2.m[7] = 0.0F;
      var2.m[11] = 0.0F;
      var2.m[15] = 1.0F;
      return var2;
   }

   public static strictfp Matrix16f scaleMatrix(Vector3f var0) {
      float var1 = var0.x;
      float var2 = var0.y;
      float var4 = var0.z;
      Matrix16f var3;
      (var3 = new Matrix16f()).m[0] = var1;
      var3.m[4] = 0.0F;
      var3.m[8] = 0.0F;
      var3.m[12] = 0.0F;
      var3.m[1] = 0.0F;
      var3.m[5] = var2;
      var3.m[9] = 0.0F;
      var3.m[13] = 0.0F;
      var3.m[2] = 0.0F;
      var3.m[6] = 0.0F;
      var3.m[10] = var4;
      var3.m[14] = 0.0F;
      var3.m[3] = 0.0F;
      var3.m[7] = 0.0F;
      var3.m[11] = 0.0F;
      var3.m[15] = 1.0F;
      return var3;
   }

   public static strictfp Matrix16f translation(Vector3f var0) {
      Matrix16f var1;
      (var1 = identity()).m[12] = var0.x;
      var1.m[13] = var0.y;
      var1.m[14] = var0.z;
      return var1;
   }

   public strictfp Matrix16f clone() {
      Matrix16f var1 = new Matrix16f();
      System.arraycopy(this.m, 0, var1.m, 0, this.m.length);
      return var1;
   }

   public strictfp Matrix16f copyFrom(Matrix16f var1) {
      System.arraycopy(var1.m, 0, this.m, 0, this.m.length);
      return this;
   }

   public strictfp Matrix16f copyTo(Matrix16f var1) {
      System.arraycopy(this.m, 0, var1.m, 0, this.m.length);
      return this;
   }

   public strictfp float determinant() {
      return (this.m[0] * this.m[5] - this.m[1] * this.m[4]) * (this.m[10] * this.m[15] - this.m[11] * this.m[14]) - (this.m[0] * this.m[6] - this.m[2] * this.m[4]) * (this.m[9] * this.m[15] - this.m[11] * this.m[13]) + (this.m[0] * this.m[7] - this.m[3] * this.m[4]) * (this.m[9] * this.m[14] - this.m[10] * this.m[13]) + (this.m[1] * this.m[6] - this.m[2] * this.m[5]) * (this.m[8] * this.m[15] - this.m[11] * this.m[12]) - (this.m[1] * this.m[7] - this.m[3] * this.m[5]) * (this.m[8] * this.m[14] - this.m[10] * this.m[12]) + (this.m[2] * this.m[7] - this.m[3] * this.m[6]) * (this.m[8] * this.m[13] - this.m[9] * this.m[12]);
   }

   public strictfp Vector3f getForward() {
      return this.transformTransposed(new Vector3f(0.0F, 0.0F, -1.0F));
   }

   public strictfp Vector3f getLeft() {
      return this.transformTransposed(new Vector3f(-1.0F, 0.0F, 0.0F));
   }

   public strictfp Matrix16f getMatrix() {
      return this;
   }

   public strictfp Vector3f getPosition() {
      return this.transformTransposed(new Vector3f(-this.m[12], -this.m[13], -this.m[14]));
   }

   public strictfp Quaternion4f getQuaternion() {
      Matrix16f var1;
      (var1 = this.clone()).m[12] = 0.0F;
      var1.m[13] = 0.0F;
      var1.m[14] = 0.0F;
      return Quaternion4f.fromMatrix(var1);
   }

   public strictfp Vector3f getUp() {
      return this.transformTransposed(new Vector3f(0.0F, 1.0F, 0.0F));
   }

   public strictfp Matrix16f inverse() {
      float var1;
      if ((var1 = this.determinant()) == 0.0F) {
         throw new RuntimeException("Can'transformationArray calculate the inverse matrix, determinant is null.");
      } else {
         float var10002 = this.m[5] * (this.m[10] * this.m[15] - this.m[11] * this.m[14]) + this.m[6] * (this.m[11] * this.m[13] - this.m[9] * this.m[15]);
         float var10003 = this.m[7];
         float var10004 = this.m[9] * this.m[14];
         Matrix16f var2;
         (var2 = new Matrix16f()).m[0] = (var10002 + var10003 * (var10004 - this.m[10] * this.m[13])) / var1;
         var2.m[1] = (this.m[9] * (this.m[2] * this.m[15] - this.m[3] * this.m[14]) + this.m[10] * (this.m[3] * this.m[13] - this.m[1] * this.m[15]) + this.m[11] * (this.m[1] * this.m[14] - this.m[2] * this.m[13])) / var1;
         var2.m[2] = (this.m[13] * (this.m[2] * this.m[7] - this.m[3] * this.m[6]) + this.m[14] * (this.m[3] * this.m[5] - this.m[1] * this.m[7]) + this.m[15] * (this.m[1] * this.m[6] - this.m[2] * this.m[5])) / var1;
         var2.m[3] = (this.m[1] * (this.m[7] * this.m[10] - this.m[6] * this.m[11]) + this.m[2] * (this.m[5] * this.m[11] - this.m[7] * this.m[9]) + this.m[3] * (this.m[6] * this.m[9] - this.m[5] * this.m[10])) / var1;
         var2.m[4] = (this.m[6] * (this.m[8] * this.m[15] - this.m[11] * this.m[12]) + this.m[7] * (this.m[10] * this.m[12] - this.m[8] * this.m[14]) + this.m[4] * (this.m[11] * this.m[14] - this.m[10] * this.m[15])) / var1;
         var2.m[5] = (this.m[10] * (this.m[0] * this.m[15] - this.m[3] * this.m[12]) + this.m[11] * (this.m[2] * this.m[12] - this.m[0] * this.m[14]) + this.m[8] * (this.m[3] * this.m[14] - this.m[2] * this.m[15])) / var1;
         var2.m[6] = (this.m[14] * (this.m[0] * this.m[7] - this.m[3] * this.m[4]) + this.m[15] * (this.m[2] * this.m[4] - this.m[0] * this.m[6]) + this.m[12] * (this.m[3] * this.m[6] - this.m[2] * this.m[7])) / var1;
         var2.m[7] = (this.m[2] * (this.m[7] * this.m[8] - this.m[4] * this.m[11]) + this.m[3] * (this.m[4] * this.m[10] - this.m[6] * this.m[8]) + this.m[0] * (this.m[6] * this.m[11] - this.m[7] * this.m[10])) / var1;
         var2.m[8] = (this.m[7] * (this.m[8] * this.m[13] - this.m[9] * this.m[12]) + this.m[4] * (this.m[9] * this.m[15] - this.m[11] * this.m[13]) + this.m[5] * (this.m[11] * this.m[12] - this.m[8] * this.m[15])) / var1;
         var2.m[9] = (this.m[11] * (this.m[0] * this.m[13] - this.m[1] * this.m[12]) + this.m[8] * (this.m[1] * this.m[15] - this.m[3] * this.m[13]) + this.m[9] * (this.m[3] * this.m[12] - this.m[0] * this.m[15])) / var1;
         var2.m[10] = (this.m[15] * (this.m[0] * this.m[5] - this.m[1] * this.m[4]) + this.m[12] * (this.m[1] * this.m[7] - this.m[3] * this.m[5]) + this.m[13] * (this.m[3] * this.m[4] - this.m[0] * this.m[7])) / var1;
         var2.m[11] = (this.m[3] * (this.m[5] * this.m[8] - this.m[4] * this.m[9]) + this.m[0] * (this.m[7] * this.m[9] - this.m[5] * this.m[11]) + this.m[1] * (this.m[4] * this.m[11] - this.m[7] * this.m[8])) / var1;
         var2.m[12] = (this.m[4] * (this.m[10] * this.m[13] - this.m[9] * this.m[14]) + this.m[5] * (this.m[8] * this.m[14] - this.m[10] * this.m[12]) + this.m[6] * (this.m[9] * this.m[12] - this.m[8] * this.m[13])) / var1;
         var2.m[13] = (this.m[8] * (this.m[2] * this.m[13] - this.m[1] * this.m[14]) + this.m[9] * (this.m[0] * this.m[14] - this.m[2] * this.m[12]) + this.m[10] * (this.m[1] * this.m[12] - this.m[0] * this.m[13])) / var1;
         var2.m[14] = (this.m[12] * (this.m[2] * this.m[5] - this.m[1] * this.m[6]) + this.m[13] * (this.m[0] * this.m[6] - this.m[2] * this.m[4]) + this.m[14] * (this.m[1] * this.m[4] - this.m[0] * this.m[5])) / var1;
         var2.m[15] = (this.m[0] * (this.m[5] * this.m[10] - this.m[6] * this.m[9]) + this.m[1] * (this.m[6] * this.m[8] - this.m[4] * this.m[10]) + this.m[2] * (this.m[4] * this.m[9] - this.m[5] * this.m[8])) / var1;
         return var2;
      }
   }

   public strictfp Matrix16f inverseRotation() {
      this.transpose();
      Vector3f var1 = this.transform(new Vector3f(this.m[12], this.m[13], this.m[14]));
      this.m[12] = -var1.x;
      this.m[13] = -var1.y;
      this.m[14] = -var1.z;
      return this;
   }

   public strictfp Matrix16f multiply(Matrix16f var1) {
      Matrix16f var2 = this.clone();

      for(int var3 = 0; var3 < 4; ++var3) {
         for(int var4 = 0; var4 < 4; ++var4) {
            int var5 = 4 * var3;
            this.m[var4 + var5] = var2.m[var4] * var1.m[var5] + var2.m[var4 + 4] * var1.m[var5 + 1] + var2.m[var4 + 8] * var1.m[var5 + 2] + var2.m[var4 + 12] * var1.m[var5 + 3];
         }
      }

      return this;
   }

   public strictfp Vector3f multiply(Vector3f var1) {
      Vector3f var10000 = var1 = this.transform(var1);
      var10000.x += this.m[12];
      var1.y += this.m[13];
      var1.z += this.m[14];
      return var1;
   }

   public strictfp Matrix16f multiply3x3(Matrix16f var1) {
      Matrix16f var2 = this.clone();

      for(int var3 = 0; var3 < 3; ++var3) {
         for(int var4 = 0; var4 < 3; ++var4) {
            int var5 = 4 * var3;
            this.m[var4 + var5] = var2.m[var4] * var1.m[var5] + var2.m[var4 + 4] * var1.m[var5 + 1] + var2.m[var4 + 8] * var1.m[var5 + 2];
         }
      }

      return this;
   }

   public strictfp void print() {
      System.err.printf("%.4f\transformationArray%.4f\transformationArray%.4f\transformationArray%.4f\n%.4f\transformationArray%.4f\transformationArray%.4f\transformationArray%.4f\n%.4f\transformationArray%.4f\transformationArray%.4f\transformationArray%.4f\n%.4f\transformationArray%.4f\transformationArray%.4f\transformationArray%.4f\n", this.m[0], this.m[4], this.m[8], this.m[12], this.m[1], this.m[5], this.m[9], this.m[13], this.m[2], this.m[6], this.m[10], this.m[14], this.m[3], this.m[7], this.m[11], this.m[15]);
   }

   public strictfp Matrix16f rotate(Quaternion4f var1) {
      return this.multiply3x3(var1.createMatrix());
   }

   public strictfp Matrix16f scale(float var1) {
      float[] var10000 = this.m;
      var10000[0] *= var1;
      var10000 = this.m;
      var10000[4] *= var1;
      var10000 = this.m;
      var10000[8] *= var1;
      var10000 = this.m;
      var10000[1] *= var1;
      var10000 = this.m;
      var10000[5] *= var1;
      var10000 = this.m;
      var10000[9] *= var1;
      var10000 = this.m;
      var10000[2] *= var1;
      var10000 = this.m;
      var10000[6] *= var1;
      var10000 = this.m;
      var10000[10] *= var1;
      var10000 = this.m;
      var10000[3] *= var1;
      var10000 = this.m;
      var10000[7] *= var1;
      var10000 = this.m;
      var10000[11] *= var1;
      return this;
   }

   public strictfp Matrix16f scale(Vector3f var1) {
      float[] var10000 = this.m;
      var10000[0] *= var1.x;
      var10000 = this.m;
      var10000[4] *= var1.y;
      var10000 = this.m;
      var10000[8] *= var1.z;
      var10000 = this.m;
      var10000[1] *= var1.x;
      var10000 = this.m;
      var10000[5] *= var1.y;
      var10000 = this.m;
      var10000[9] *= var1.z;
      var10000 = this.m;
      var10000[2] *= var1.x;
      var10000 = this.m;
      var10000[6] *= var1.y;
      var10000 = this.m;
      var10000[10] *= var1.z;
      var10000 = this.m;
      var10000[3] *= var1.x;
      var10000 = this.m;
      var10000[7] *= var1.y;
      var10000 = this.m;
      var10000[11] *= var1.z;
      return this;
   }

   public strictfp Vector3f transform(Vector3f var1) {
      float var2 = this.m[0] * var1.x + this.m[4] * var1.y + this.m[8] * var1.z;
      float var3 = this.m[1] * var1.x + this.m[5] * var1.y + this.m[9] * var1.z;
      float var4 = this.m[2] * var1.x + this.m[6] * var1.y + this.m[10] * var1.z;
      return new Vector3f(var2, var3, var4);
   }

   public strictfp Vector3f transformTransposed(Vector3f var1) {
      float var2 = this.m[0] * var1.x + this.m[1] * var1.y + this.m[2] * var1.z;
      float var3 = this.m[4] * var1.x + this.m[5] * var1.y + this.m[6] * var1.z;
      float var4 = this.m[8] * var1.x + this.m[9] * var1.y + this.m[10] * var1.z;
      return new Vector3f(var2, var3, var4);
   }

   public strictfp Matrix16f translate(Vector3f var1) {
      var1 = this.transform(var1);
      float[] var10000 = this.m;
      var10000[12] += var1.x;
      var10000 = this.m;
      var10000[13] += var1.y;
      var10000 = this.m;
      var10000[14] += var1.z;
      return this;
   }

   public strictfp Matrix16f transpose() {
      for(int var1 = 0; var1 < 4; ++var1) {
         for(int var2 = var1 + 1; var2 < 4; ++var2) {
            int var3 = 4 * var1;
            int var4 = 4 * var2;
            float var5 = this.m[var2 + var3];
            this.m[var2 + var3] = this.m[var4 + var1];
            this.m[var4 + var1] = var5;
         }
      }

      return this;
   }

   public strictfp Matrix16f transpose3x3() {
      for(int var1 = 0; var1 < 3; ++var1) {
         for(int var2 = var1 + 1; var2 < 3; ++var2) {
            int var3 = 4 * var1;
            int var4 = 4 * var2;
            float var5 = this.m[var2 + var3];
            this.m[var2 + var3] = this.m[var4 + var1];
            this.m[var4 + var1] = var5;
         }
      }

      return this;
   }
}
