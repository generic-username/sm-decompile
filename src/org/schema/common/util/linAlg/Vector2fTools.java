package org.schema.common.util.linAlg;

import javax.vecmath.Vector2f;

public class Vector2fTools {
   public static Vector2f intersectsFromInside(float var0, float var1, float var2, float var3, Vector2f var4, Vector2f var5) {
      Vector2f var6 = new Vector2f(var0, var1);
      Vector2f var7 = new Vector2f(var0, var1 + var3);
      if ((var6 = intersects(var4, var5, var6, var7)) != null) {
         return var6;
      } else {
         var6 = new Vector2f(var0, var1);
         var7 = new Vector2f(var0 + var2, var1);
         if ((var6 = intersects(var4, var5, var6, var7)) != null) {
            return var6;
         } else {
            var6 = new Vector2f(var0 + var2, var1);
            var7 = new Vector2f(var0 + var2, var1 + var3);
            if ((var6 = intersects(var4, var5, var6, var7)) != null) {
               return var6;
            } else {
               var6 = new Vector2f(var0 + var2, var1 + var3);
               var7 = new Vector2f(var0, var1 + var3);
               return (var6 = intersects(var4, var5, var6, var7)) != null ? var6 : null;
            }
         }
      }
   }

   public static Vector2f intersects(Vector2f var0, Vector2f var1, Vector2f var2, Vector2f var3) {
      var1 = sub(var1, var0);
      var3 = sub(var3, var2);
      float var4;
      if ((var4 = var1.x * var3.y - var1.y * var3.x) == 0.0F) {
         return null;
      } else {
         float var6;
         if ((var6 = ((var2 = sub(var2, var0)).x * var3.y - var2.y * var3.x) / var4) >= 0.0F && var6 <= 1.0F) {
            float var5;
            if ((var5 = (var2.x * var1.y - var2.y * var1.x) / var4) >= 0.0F && var5 <= 1.0F) {
               var1.scale(var6);
               var0.add(var1);
               return var0;
            } else {
               return null;
            }
         } else {
            return null;
         }
      }
   }

   public static Vector2f sub(Vector2f var0, Vector2f var1) {
      Vector2f var2;
      (var2 = new Vector2f()).sub(var0, var1);
      return var2;
   }
}
