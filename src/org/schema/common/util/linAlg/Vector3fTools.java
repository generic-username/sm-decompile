package org.schema.common.util.linAlg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;

public class Vector3fTools {
   static DecimalFormat df2 = new DecimalFormat("#,###,###,##0.00");

   public static Vector3f add(Vector3f var0, Vector3f var1) {
      (var0 = new Vector3f(var0)).add(var1);
      return var0;
   }

   public static Vector3f lerp(Vector3f var0, Vector3f var1, float var2, Vector3f var3) {
      var3.set(var0.x + var2 * (var1.x - var0.x), var0.y + var2 * (var1.y - var0.y), var0.z + var2 * (var1.z - var0.z));
      return var3;
   }

   public static Vector3f slerp(Vector3f var0, Vector3f var1, float var2, Vector3f var3, Vector3f var4) {
      var3.set(var0);
      var4.set(var1);
      float var5;
      if ((double)(var5 = var0.dot(var1)) <= 0.99995D && (double)var5 >= 0.9995D) {
         if (var5 > 1.0F) {
            var5 = 1.0F;
         }

         if (var5 < -1.0F) {
            var5 = -1.0F;
         }

         var2 = FastMath.acos(var5) * var2;
         var4.x -= var0.x * var5;
         var4.y -= var0.y * var5;
         var4.z -= var0.z * var5;
         var3.set(var4);
         var3.normalize();
         var4.set(var3);
         var3.set(var0);
         var3.scale(FastMath.cos(var2));
         var4.scale(FastMath.sin(var2));
         var3.add(var4);
         var3.normalize();
         return var3;
      } else {
         var4.sub(var0);
         var3.scaleAdd(var2, var4);
         var3.normalize();
         return var3;
      }
   }

   static Quat4f fromtwovectors(Vector3f var0, Vector3f var1) {
      Vector3f var2 = crossProduct(var0, var1);
      Quat4f var3 = new Quat4f(var2.x, var2.y, var2.z, dot(var0, var1));
      float var4 = var2.x * var2.x + var2.y * var2.y + var2.z * var2.z;
      var3.w = (float)((double)var3.w + Math.sqrt((double)(var3.w * var3.w + var4)));
      var3.scale((float)(1.0D / Math.sqrt((double)(var3.w * var3.w + var4))));
      return var3;
   }

   public static Vector3f slerp(Vector3f var0, Vector3f var1, float var2) {
      float var3 = var0.angle(var1);
      float var4 = FastMath.sin(1.0F - var2) * var3 / FastMath.sin(var3);
      var2 = FastMath.sin(var3 * var2) / (float)Math.sinh((double)var3);
      Vector3f var5 = new Vector3f();
      var0 = new Vector3f(var0);
      var1 = new Vector3f(var1);
      var0.scale(var4);
      var1.scale(var2);
      var5.add(var0, var1);
      var5.normalize();
      return var5;
   }

   public static Vector3f slerp2(Vector3f var0, Vector3f var1, float var2) {
      Quat4f var4 = Quat4Util.fromEulerAngles(var0.x, var0.y, var0.z);
      Quat4f var5 = Quat4Util.fromEulerAngles(var1.x, var1.y, var1.z);
      Quat4f var3 = new Quat4f();
      Quat4Util.slerp(var4, var5, var2, var3);
      return new Vector3f(var3.x, var3.y, var3.z);
   }

   public static Vector3f checkLineBox(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      if (var3.x < var0.x && var2.x < var0.x) {
         return null;
      } else if (var3.x > var1.x && var2.x > var1.x) {
         return null;
      } else if (var3.y < var0.y && var2.y < var0.y) {
         return null;
      } else if (var3.y > var1.y && var2.y > var1.y) {
         return null;
      } else if (var3.z < var0.z && var2.z < var0.z) {
         return null;
      } else if (var3.z > var1.z && var2.z > var1.z) {
         return null;
      } else if (var2.x > var0.x && var2.x < var1.x && var2.y > var0.y && var2.y < var1.y && var2.z > var0.z && var2.z < var1.z) {
         return var2;
      } else {
         Vector3f var4;
         return ((var4 = getIntersection(var2.x - var0.x, var3.x - var0.x, var2, var3)) == null || !inBox(var4, var0, var1, 1)) && ((var4 = getIntersection(var2.y - var0.y, var3.y - var0.y, var2, var3)) == null || !inBox(var4, var0, var1, 2)) && ((var4 = getIntersection(var2.z - var0.z, var3.z - var0.z, var2, var3)) == null || !inBox(var4, var0, var1, 3)) && ((var4 = getIntersection(var2.x - var1.x, var3.x - var1.x, var2, var3)) == null || !inBox(var4, var0, var1, 1)) && ((var4 = getIntersection(var2.y - var1.y, var3.y - var1.y, var2, var3)) == null || !inBox(var4, var0, var1, 2)) && ((var4 = getIntersection(var2.z - var1.z, var3.z - var1.z, var2, var3)) == null || !inBox(var4, var0, var1, 3)) ? null : var4;
      }
   }

   public static void clamp(Vector3f var0, Vector3f var1, Vector3f var2) {
      var0.x = FastMath.clamp(var0.x, var1.x, var2.x);
      var0.y = FastMath.clamp(var0.y, var1.y, var2.y);
      var0.z = FastMath.clamp(var0.z, var1.z, var2.z);
   }

   public static Vector3f[] clone(Vector3f[] var0) {
      return (Vector3f[])Arrays.copyOf(var0, var0.length);
   }

   public static float getAngleSigned(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      var0.normalize();
      var1.normalize();
      float var4 = dot(var0, var1);
      float var5 = FastMath.atan2((var0 = crossProduct(var0, var1, var3)).length(), var4);
      if (dot(var2, var0) < 0.0F) {
         var5 = -var5;
      }

      return var5;
   }

   public static float getAngleSigned(Vector3f var0, Vector3f var1, Vector3f var2) {
      return getAngleSigned(var0, var1, var2, new Vector3f());
   }

   public static float cross(Vector3f var0, Vector3f var1) {
      float var2 = var1.z * var0.y - var1.y * var0.z;
      float var3 = var1.x * var0.z - var1.z * var0.x;
      float var4 = var1.x * var0.y - var1.y * var0.x;
      return var2 + var3 + var4;
   }

   public static Vector3f crossProduct(Vector3f var0, Vector3f var1) {
      return new Vector3f(var0.y * var1.z - var0.z * var1.y, var0.z * var1.x - var0.x * var1.z, var0.x * var1.y - var0.y * var1.x);
   }

   public static Vector3f crossProduct(Vector3f var0, Vector3f var1, Vector3f var2) {
      float var3 = var0.y * var1.z - var0.z * var1.y;
      float var4 = var0.z * var1.x - var0.x * var1.z;
      float var5 = var0.x * var1.y - var0.y * var1.x;
      var2.set(var3, var4, var5);
      return var2;
   }

   public static float getAngleX(Vector3f var0) {
      return (float)Math.acos((double)var0.x / Math.sqrt(Math.pow((double)(var0.x - 1.0F), 2.0D) + Math.pow((double)var0.y, 2.0D) + Math.pow((double)var0.z, 2.0D)));
   }

   public static float getAngleY(Vector3f var0) {
      return (float)Math.acos((double)var0.y / Math.sqrt(Math.pow((double)var0.x, 2.0D) + Math.pow((double)(var0.y - 1.0F), 2.0D) + Math.pow((double)var0.z, 2.0D)));
   }

   public static float getAngleZ(Vector3f var0) {
      return (float)Math.acos((double)var0.z / Math.sqrt(Math.pow((double)var0.x, 2.0D) + Math.pow((double)var0.y, 2.0D) + Math.pow((double)(var0.z - 1.0F), 2.0D)));
   }

   public static float getFullRange2DAngle(Vector3f var0, Vector3f var1) {
      float var2 = var0.x * var1.x + var0.y * var1.y;
      return FastMath.atan2(var0.x * var1.y - var0.y * var1.x, var2);
   }

   public static float getFullRange2DAngleFast(Vector3f var0, Vector3f var1) {
      float var2 = var0.x * var1.x + var0.y * var1.y;
      return FastMath.atan2Fast(var0.x * var1.y - var0.y * var1.x, var2);
   }

   public static Vector3f getIntersection(float var0, float var1, Vector3f var2, Vector3f var3) {
      if (var0 * var1 >= 0.0F) {
         return null;
      } else if (var0 == var1) {
         return null;
      } else {
         Vector3f var4;
         (var4 = new Vector3f(var2)).add(sub(var3, var2));
         var4.scale(-var0 / (var1 - var0));
         return var4;
      }
   }

   public static Vector3f intersectLinePLane(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      Vector3f var4 = new Vector3f();
      Vector3f var5 = new Vector3f();
      var4.sub(var1, var0);
      var5.sub(var0, var2);
      float var6;
      if (Math.abs(var6 = var3.dot(var4)) > 1.1920929E-7F) {
         if ((var6 = -var3.dot(var5) / var6) >= 0.0F && var6 <= 1.0F) {
            var4.scale(var6);
            var4.add(var0);
            return var4;
         } else {
            return null;
         }
      } else {
         return null;
      }
   }

   public static String getRoundedString(Vector3f var0) {
      return "(" + df2.format((double)var0.x) + ", " + df2.format((double)var0.y) + ", " + df2.format((double)var0.z) + ")";
   }

   public static boolean inBox(Vector3f var0, Vector3f var1, Vector3f var2, int var3) {
      if (var3 == 1 && var0.z > var1.z && var0.z < var2.z && var0.y > var1.y && var0.y < var2.y) {
         return true;
      } else if (var3 == 2 && var0.z > var1.z && var0.z < var2.z && var0.x > var1.x && var0.x < var2.x) {
         return true;
      } else {
         return var3 == 3 && var0.x > var1.x && var0.x < var2.x && var0.y > var1.y && var0.y < var2.y;
      }
   }

   public static Vector3f intersect_RayTriangle(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      var3 = sub(var3, var2);
      var4 = sub(var4, var2);
      Vector3f var5;
      if ((var5 = crossProduct(var3, var4)).length() == 0.0F) {
         return null;
      } else {
         Vector3f var6 = sub(var1, var0);
         Vector3f var7 = sub(var0, var2);
         Vector3f var8;
         (var8 = new Vector3f(var5)).scale(-1.0F);
         float var16 = var8.dot(var7);
         float var14;
         if (FastMath.abs(var14 = var5.dot(var6)) < 0.0F) {
            if (var16 == 0.0F) {
               System.err.println("lies in");
               return var1;
            } else {
               return null;
            }
         } else {
            float var10 = var16 / var14;
            if (var14 == 0.0F) {
               return null;
            } else if ((double)var10 < 0.0D) {
               return null;
            } else {
               var6.scale(var10);
               var1 = add(var0, var6);
               var14 = var3.dot(var3);
               float var15 = var3.dot(var4);
               var16 = var4.dot(var4);
               float var11 = (var0 = sub(var1, var2)).dot(var3);
               float var9 = var0.dot(var4);
               float var12 = var15 * var15 - var14 * var16;
               float var13;
               if ((double)(var13 = (var15 * var9 - var16 * var11) / var12) >= 0.0D && (double)var13 <= 1.0D) {
                  return (double)(var9 = (var15 * var11 - var14 * var9) / var12) >= 0.0D && (double)(var13 + var9) <= 1.0D ? var1 : null;
               } else {
                  return null;
               }
            }
         }
      }
   }

   public static Vector3f intersectsRayAABB(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, float var4, float var5) {
      Vector3f var6;
      boolean var7 = (var6 = new Vector3f(1.0F / var1.x, 1.0F / var1.y, 1.0F / var1.z)).x < 0.0F;
      boolean var8 = var6.y < 0.0F;
      boolean var9 = var6.z < 0.0F;
      float var10 = ((var7 ? var3 : var2).x - var0.x) * var6.x;
      float var13 = ((var7 ? var2 : var3).x - var0.x) * var6.x;
      float var11 = ((var8 ? var3 : var2).y - var0.y) * var6.y;
      float var14 = ((var8 ? var2 : var3).y - var0.y) * var6.y;
      if (var10 <= var14 && var11 <= var13) {
         if (var11 > var10) {
            var10 = var11;
         }

         if (var14 < var13) {
            var13 = var14;
         }

         var14 = ((var9 ? var3 : var2).z - var0.z) * var6.z;
         float var12 = ((var9 ? var2 : var3).z - var0.z) * var6.z;
         if (var10 <= var12 && var14 <= var13) {
            if (var14 > var10) {
               var10 = var14;
            }

            if (var12 < var13) {
               var13 = var12;
            }

            if (var10 < var5 && var13 > var4) {
               (var1 = new Vector3f(var1)).scale(var10);
               var1.add(var0);
               return var1;
            } else {
               return null;
            }
         } else {
            return null;
         }
      } else {
         return null;
      }
   }

   public static boolean isNan(Vector3f var0) {
      return Float.isNaN(var0.x) || Float.isNaN(var0.y) || Float.isNaN(var0.z);
   }

   public static float length(float var0, float var1, float var2) {
      return FastMath.carmackSqrt(var0 * var0 + var1 * var1 + var2 * var2);
   }

   public static float lengthSquared(float var0, float var1, float var2) {
      return var0 * var0 + var1 * var1 + var2 * var2;
   }

   public static float diffLengthSquared(Vector3f var0, Vector3f var1) {
      float var2 = var0.x - var1.x;
      float var3 = var0.y - var1.y;
      float var4 = var0.z - var1.z;
      return lengthSquared(var2, var3, var4);
   }

   public static float diffLength(Vector3f var0, Vector3f var1) {
      float var2 = var0.x - var1.x;
      float var3 = var0.y - var1.y;
      float var4 = var0.z - var1.z;
      return length(var2, var3, var4);
   }

   public static void makeCeil(Vector3f var0, Vector3f var1) {
      if (var1.x > var0.x) {
         var0.x = var1.x;
      }

      if (var1.y > var0.y) {
         var0.y = var1.y;
      }

      if (var1.z > var0.z) {
         var0.z = var1.z;
      }

   }

   public static void makeFloor(Vector3f var0, Vector3f var1) {
      if (var1.x < var0.x) {
         var0.x = var1.x;
      }

      if (var1.y < var0.y) {
         var0.y = var1.y;
      }

      if (var1.z < var0.z) {
         var0.z = var1.z;
      }

   }

   public static void max(Vector3f var0, Vector3f var1, Vector3f var2) {
      var2.x = Math.max(var0.x, var1.x);
      var2.y = Math.max(var0.y, var1.y);
      var2.z = Math.max(var0.z, var1.z);
   }

   public static void min(Vector3f var0, Vector3f var1, Vector3f var2) {
      var2.x = Math.min(var0.x, var1.x);
      var2.y = Math.min(var0.y, var1.y);
      var2.z = Math.min(var0.z, var1.z);
   }

   public static void pointMult(Vector3f var0, Vector3f var1) {
      var0.set(var0.x * var1.x, var0.y * var1.y, var0.z * var1.z);
   }

   public static Vector3f pointMultInstance(Vector3f var0, Vector3f var1) {
      return new Vector3f(var0.x * var1.x, var0.y * var1.y, var0.z * var1.z);
   }

   public static Vector3f predictPoint(Vector3f var0, Vector3f var1, float var2, Vector3f var3) {
      Vector3f var4;
      (var4 = new Vector3f()).sub(var0, var3);
      if (var1.lengthSquared() == 0.0F) {
         return var4;
      } else {
         double var5 = (double)(var1.dot(var1) - var2 * var2);
         double var7 = (double)(2.0F * var4.dot(var1));
         double var9 = (double)var4.dot(var4);
         double var11 = (double)((float)Math.sqrt(Math.abs(var7 * var7 - var5 * 4.0D * var9)));
         double var13 = (-var7 - var11) / (var5 * 2.0D);
         double var15 = (-var7 + var11) / (var5 * 2.0D);
         double var17;
         if (var13 > var15 && var15 > 0.0D) {
            var17 = var15;
         } else {
            var17 = var13;
         }

         Vector3f var19 = new Vector3f();
         (var1 = new Vector3f(var1)).scale((float)var17);
         var19.add(var0, var1);
         (var0 = new Vector3f()).sub(var19, var3);
         return var0;
      }
   }

   public static void quaternionToDegree(Vector4f var0) {
      if (var0.x == 0.0F && var0.y == 0.0F && var0.z == 0.0F) {
         var0.w = 0.0F;
      } else {
         float var1 = FastMath.carmackSqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z);
         var0.x /= var1;
         var0.y /= var1;
         var0.z /= var1;
         var0.w = (float)(2.0D * FastMath.acosFast((double)var0.w));
      }
   }

   public static Vector3f sub(Vector3f var0, Vector3f var1) {
      (var0 = new Vector3f(var0)).sub(var1);
      return var0;
   }

   public static Vector3f projectOnPlaneUnnormalized(Vector3f var0, Vector3f var1) {
      Vector3f var2 = new Vector3f(var0);
      (var1 = new Vector3f(var1)).scale(var0.dot(var1));
      var2.sub(var1);
      return var2;
   }

   public static Vector3f projectOnPlane(Vector3f var0, Vector3f var1) {
      Vector3f var2 = new Vector3f(var0);
      var1 = new Vector3f(var1);
      var2.normalize();
      var1.normalize();
      var1.scale(var0.dot(var1));
      var2.sub(var1);
      return var2;
   }

   public static float dot(Vector3f var0, Vector3f var1) {
      return var0.x * var1.x + var0.y * var1.y + var0.z * var1.z;
   }

   public static float length(Vector3i var0, Vector3i var1) {
      return length((float)(var0.x - var1.x), (float)(var0.y - var1.y), (float)(var0.z - var1.z));
   }

   public static float lengthSquared(Vector3i var0, Vector3i var1) {
      return lengthSquared((float)(var0.x - var1.x), (float)(var0.y - var1.y), (float)(var0.z - var1.z));
   }

   public static float lengthSquared(Vector3f var0, Vector3f var1) {
      return lengthSquared(var0.x - var1.x, var0.y - var1.y, var0.z - var1.z);
   }

   public static float length(Vector3f var0, Vector3f var1) {
      return length(var0.x - var1.x, var0.y - var1.y, var0.z - var1.z);
   }

   public static float projectScalar(Vector3f var0, Vector3f var1) {
      float var2 = FastMath.carmackLength(var0);
      float var3 = var0.x * var2;
      float var4 = var0.y * var2;
      float var5 = var0.z * var2;
      return var1.x * var3 + var1.y * var4 + var1.z * var5;
   }

   public static void serialize(Vector3f var0, DataOutput var1) throws IOException {
      var1.writeFloat(var0.x);
      var1.writeFloat(var0.y);
      var1.writeFloat(var0.z);
   }

   public static Vector3f deserialize(Vector3f var0, DataInput var1) throws IOException {
      var0.x = var1.readFloat();
      var0.y = var1.readFloat();
      var0.z = var1.readFloat();
      return var0;
   }

   public static Vector3f deserialize(DataInput var0) throws IOException {
      return deserialize(new Vector3f(), var0);
   }

   public static float dot2(Vector3f var0) {
      return dot(var0, var0);
   }

   public static Vector3f mult(Vector3f var0, float var1) {
      (var0 = new Vector3f(var0)).scale(var1);
      return var0;
   }

   public static Vector3f read(String var0) {
      String[] var1 = var0.split(",");
      return new Vector3f(Float.parseFloat(var1[0].trim()), Float.parseFloat(var1[1].trim()), Float.parseFloat(var1[2].trim()));
   }

   public static String toStringRaw(Vector3f var0) {
      return var0.x + ", " + var0.y + ", " + var0.z;
   }

   public static Vector3f getCenterOfTriangle(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      var3.x = (var0.x + var1.x + var2.x) / 3.0F;
      var3.y = (var0.y + var1.y + var2.y) / 3.0F;
      var3.z = (var0.z + var1.z + var2.z) / 3.0F;
      return var3;
   }

   public static float distance(float var0, float var1, float var2, float var3, float var4, float var5) {
      return length(var0 - var3, var1 - var4, var2 - var5);
   }

   public static void addScaled(Vector3f var0, float var1, Vector3f var2) {
      var0.x += var1 * var2.x;
      var0.y += var1 * var2.y;
      var0.z += var1 * var2.z;
   }
}
