package org.schema.common.util.linAlg;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Quat4Util {
   public static float dot(Quat4f var0, Quat4f var1) {
      return var0.w * var1.w + var0.x * var1.x + var0.y * var1.y + var0.z * var1.z;
   }

   public static Quat4f fromAngleAxis(float var0, Vector3f var1, Quat4f var2) {
      (var1 = new Vector3f(var1)).normalize();
      fromAngleNormalAxis(var0, var1, var2);
      return var2;
   }

   public static Quat4f fromAngleNormalAxis(float var0, Vector3f var1, Quat4f var2) {
      if (var1.x == 0.0F && var1.y == 0.0F && var1.z == 0.0F) {
         var2.set(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         float var3 = FastMath.sin(var0 = 0.5F * var0);
         var2.w = FastMath.cos(var0);
         var2.x = var3 * var1.x;
         var2.y = var3 * var1.y;
         var2.z = var3 * var1.z;
      }

      return var2;
   }

   public static Quat4f fromEulerAngles(float var0, float var1, float var2) {
      Quat4f var3 = new Quat4f(1.0F, 0.0F, 0.0F, var0);
      Quat4f var4 = new Quat4f(0.0F, 1.0F, 0.0F, var1);
      Quat4f var5 = new Quat4f(0.0F, 0.0F, 1.0F, var2);
      var4.mul(var5);
      var4.mul(var3);
      return var4;
   }

   public static Quat4f mult(Quat4f var0, Quat4f var1, Quat4f var2) {
      float var3 = var0.x;
      float var4 = var0.y;
      float var5 = var0.z;
      float var9 = var0.w;
      if (var2 == null) {
         var2 = new Quat4f();
      }

      float var6 = var1.w;
      float var7 = var1.x;
      float var8 = var1.y;
      float var10 = var1.z;
      var2.x = var3 * var6 + var4 * var10 - var5 * var8 + var9 * var7;
      var2.y = -var3 * var10 + var4 * var6 + var5 * var7 + var9 * var8;
      var2.z = var3 * var8 - var4 * var7 + var5 * var6 + var9 * var10;
      var2.w = -var3 * var7 - var4 * var8 - var5 * var10 + var9 * var6;
      return var2;
   }

   public static Vector3f mult(Quat4f var0, Vector3f var1, Vector3f var2) {
      float var3 = var0.x;
      float var4 = var0.y;
      float var5 = var0.z;
      float var8 = var0.w;
      if (var2 == null) {
         var2 = new Vector3f();
      }

      if (var1.x == 0.0F && var1.y == 0.0F && var1.z == 0.0F) {
         var2.set(0.0F, 0.0F, 0.0F);
      } else {
         float var6 = var1.x;
         float var7 = var1.y;
         float var9 = var1.z;
         var2.x = var8 * var8 * var6 + var4 * 2.0F * var8 * var9 - var5 * 2.0F * var8 * var7 + var3 * var3 * var6 + var4 * 2.0F * var3 * var7 + var5 * 2.0F * var3 * var9 - var5 * var5 * var6 - var4 * var4 * var6;
         var2.y = var3 * 2.0F * var4 * var6 + var4 * var4 * var7 + var5 * 2.0F * var4 * var9 + var8 * 2.0F * var5 * var6 - var5 * var5 * var7 + var8 * var8 * var7 - var3 * 2.0F * var8 * var9 - var3 * var3 * var7;
         var2.z = var3 * 2.0F * var5 * var6 + var4 * 2.0F * var5 * var7 + var5 * var5 * var9 - var8 * 2.0F * var4 * var6 - var4 * var4 * var9 + var8 * 2.0F * var3 * var7 - var3 * var3 * var9 + var8 * var8 * var9;
      }

      return var2;
   }

   public static void nlerp(Quat4f var0, float var1, Quat4f var2) {
      float var3 = dot(var2, var0);
      float var4 = 1.0F - var1;
      if (var3 < 0.0F) {
         var2.x = var4 * var2.x - var1 * var0.x;
         var2.y = var4 * var2.y - var1 * var0.y;
         var2.z = var4 * var2.z - var1 * var0.z;
         var2.w = var4 * var2.w - var1 * var0.w;
      } else {
         var2.x = var4 * var2.x + var1 * var0.x;
         var2.y = var4 * var2.y + var1 * var0.y;
         var2.z = var4 * var2.z + var1 * var0.z;
         var2.w = var4 * var2.w + var1 * var0.w;
      }

      var2.normalize();
   }

   public static void slerp(Quat4f var0, float var1, Quat4f var2) {
      if (var2.x != var0.x || var2.y != var0.y || var2.z != var0.z || var2.w != var0.w) {
         float var3;
         if ((var3 = var2.x * var0.x + var2.y * var0.y + var2.z * var0.z + var2.w * var0.w) < 0.0F) {
            var0.x = -var0.x;
            var0.y = -var0.y;
            var0.z = -var0.z;
            var0.w = -var0.w;
            var3 = -var3;
         }

         float var4 = 1.0F - var1;
         float var5 = var1;
         if (1.0F - var3 > 0.1F) {
            var3 = FastMath.acos(var3);
            var5 = 1.0F / FastMath.sin(var3);
            var4 = FastMath.sin(var4 * var3) * var5;
            var5 = FastMath.sin(var1 * var3) * var5;
         }

         var2.x = var4 * var2.x + var5 * var0.x;
         var2.y = var4 * var2.y + var5 * var0.y;
         var2.z = var4 * var2.z + var5 * var0.z;
         var2.w = var4 * var2.w + var5 * var0.w;
      }
   }

   public static final Quat4f slerp(Quat4f var0, Quat4f var1, float var2, boolean var3, Quat4f var4) {
      float var5 = dot(var0, var1);
      float var6;
      if (1.0F - Math.abs(var5) < 0.01F) {
         var6 = 1.0F - var2;
      } else {
         float var7;
         float var8 = FastMath.sin(var7 = FastMath.acos(FastMath.abs(var5)));
         var6 = FastMath.sin(var7 * (1.0F - var2)) / var8;
         var2 = FastMath.sin(var7 * var2) / var8;
      }

      if (var3 && (double)var5 < 0.0D) {
         var6 = -var6;
      }

      var4.set(var6 * var0.x + var2 * var1.x, var6 * var0.y + var2 * var1.y, var6 * var0.z + var2 * var1.z, var6 * var0.w + var2 * var1.w);
      return var4;
   }

   public static Quat4f slerp(Quat4f var0, Quat4f var1, float var2, Quat4f var3) {
      if (var0.x == var1.x && var0.y == var1.y && var0.z == var1.z && var0.w == var1.w) {
         var3.set(var0);
         return var3;
      } else {
         float var4;
         if ((var4 = var0.x * var1.x + var0.y * var1.y + var0.z * var1.z + var0.w * var1.w) < 0.0F) {
            var1.x = -var1.x;
            var1.y = -var1.y;
            var1.z = -var1.z;
            var1.w = -var1.w;
            var4 = -var4;
         }

         float var5 = 1.0F - var2;
         float var6 = var2;
         if (1.0F - var4 > 0.1F) {
            var4 = FastMath.acos(var4);
            var6 = 1.0F / FastMath.sin(var4);
            var5 = FastMath.sin(var5 * var4) * var6;
            var6 = FastMath.sin(var2 * var4) * var6;
         }

         var3.x = var5 * var0.x + var6 * var1.x;
         var3.y = var5 * var0.y + var6 * var1.y;
         var3.z = var5 * var0.z + var6 * var1.z;
         var3.w = var5 * var0.w + var6 * var1.w;
         return var3;
      }
   }
}
