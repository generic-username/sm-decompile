package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class PolygonTools {
   public static float distanceInst(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      Vector3f var4 = Vector3fTools.sub(var1, var0);
      Vector3f var5 = Vector3fTools.sub(var3, var0);
      Vector3f var6 = Vector3fTools.sub(var2, var1);
      var1 = Vector3fTools.sub(var3, var1);
      var0 = Vector3fTools.sub(var0, var2);
      var2 = Vector3fTools.sub(var3, var2);
      var3 = Vector3fTools.crossProduct(var4, var0);
      return FastMath.carmackSqrt((double)(FastMath.sign(Vector3fTools.dot(Vector3fTools.crossProduct(var4, var3), var5)) + FastMath.sign(Vector3fTools.dot(Vector3fTools.crossProduct(var6, var3), var1)) + FastMath.sign(Vector3fTools.dot(Vector3fTools.crossProduct(var0, var3), var2))) < 2.0D ? Math.min(Math.min(Vector3fTools.dot2(Vector3fTools.sub(Vector3fTools.mult(var4, FastMath.clamp(Vector3fTools.dot(var4, var5) / Vector3fTools.dot2(var4), 0.0F, 1.0F)), var5)), Vector3fTools.dot2(Vector3fTools.sub(Vector3fTools.mult(var6, FastMath.clamp(Vector3fTools.dot(var6, var1) / Vector3fTools.dot2(var6), 0.0F, 1.0F)), var1))), Vector3fTools.dot2(Vector3fTools.sub(Vector3fTools.mult(var0, FastMath.clamp(Vector3fTools.dot(var0, var2) / Vector3fTools.dot2(var0), 0.0F, 1.0F)), var2))) : Vector3fTools.dot(var3, var5) * Vector3fTools.dot(var3, var5) / Vector3fTools.dot2(var3));
   }

   public static float distance(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, PolygonToolsVars var4) {
      var4.v21.sub(var1, var0);
      var4.p1.sub(var3, var0);
      var4.v32.sub(var2, var1);
      var4.p2.sub(var3, var1);
      var4.v13.sub(var0, var2);
      var4.p3.sub(var3, var2);
      var4.nor.cross(var4.v21, var4.v13);
      if (var4.v13.lengthSquared() < 64.0F && var4.v32.lengthSquared() < 64.0F && var4.v21.lengthSquared() < 64.0F && Vector3fTools.lengthSquared(var0, var3) > 100.0F) {
         return FastMath.carmackSqrt(Math.min(Math.min(Vector3fTools.lengthSquared(var0, var3), Vector3fTools.lengthSquared(var1, var3)), Vector3fTools.lengthSquared(var2, var3)));
      } else {
         var4.v21crossNor.cross(var4.v21, var4.nor);
         var4.v32crossNor.cross(var4.v32, var4.nor);
         var4.v13crossNor.cross(var4.v13, var4.nor);
         if ((double)(FastMath.sign(Vector3fTools.dot(var4.v21crossNor, var4.p1)) + FastMath.sign(Vector3fTools.dot(var4.v32crossNor, var4.p2)) + FastMath.sign(Vector3fTools.dot(var4.v13crossNor, var4.p3))) < 2.0D) {
            var4.mult21.scale(FastMath.clamp(Vector3fTools.dot(var4.v21, var4.p1) / Vector3fTools.dot2(var4.v21), 0.0F, 1.0F), var4.v21);
            var4.mult32.scale(FastMath.clamp(Vector3fTools.dot(var4.v32, var4.p2) / Vector3fTools.dot2(var4.v32), 0.0F, 1.0F), var4.v32);
            var4.mult13.scale(FastMath.clamp(Vector3fTools.dot(var4.v13, var4.p3) / Vector3fTools.dot2(var4.v13), 0.0F, 1.0F), var4.v13);
            return FastMath.carmackSqrt(Math.min(Math.min(Vector3fTools.dot2(Vector3fTools.sub(var4.mult21, var4.p1)), Vector3fTools.dot2(Vector3fTools.sub(var4.mult32, var4.p2))), Vector3fTools.dot2(Vector3fTools.sub(var4.mult13, var4.p3))));
         } else {
            return FastMath.carmackSqrt(Vector3fTools.dot(var4.nor, var4.p1) * Vector3fTools.dot(var4.nor, var4.p1) / Vector3fTools.dot2(var4.nor));
         }
      }
   }

   public static float distance(Triangle[] var0, int var1, Vector3f var2, PolygonToolsVars var3) {
      float var4 = Float.POSITIVE_INFINITY;

      for(int var5 = 0; var5 < var1; ++var5) {
         Triangle var6;
         float var7;
         if ((var7 = distance((var6 = var0[var5]).v1, var6.v2, var6.v3, var2, var3)) < var4) {
            var4 = var7;
            var3.outFrom.set(var6.v1);
            Vector3fTools.getCenterOfTriangle(var6.v1, var6.v2, var6.v3, var3.outFrom);
         }
      }

      return var4;
   }

   public static float distanceBad(Triangle[] var0, int var1, Vector3f var2) {
      float var3 = Float.POSITIVE_INFINITY;

      for(int var4 = 0; var4 < var1; ++var4) {
         float var5 = var0[var4].v2.x - var0[var4].v1.x;
         float var6 = var0[var4].v2.y - var0[var4].v1.y;
         float var7 = var0[var4].v2.z - var0[var4].v1.z;
         float var8 = var0[var4].v3.x - var0[var4].v1.x;
         float var9 = var0[var4].v3.y - var0[var4].v1.y;
         float var10 = var0[var4].v3.z - var0[var4].v1.z;
         float var11 = var6 * var10 - var7 * var9;
         var10 = var7 * var8 - var5 * var10;
         var8 = var5 * var9 - var6 * var8;
         var5 = FastMath.carmackSqrt(var11 * var11 + var10 * var10 + var8 * var8);
         var11 /= var5;
         var10 /= var5;
         var8 /= var5;
         var5 = var2.x - var0[var4].v1.x;
         var6 = var2.y - var0[var4].v1.y;
         var7 = var2.z - var0[var4].v1.z;
         var5 = var5 * var11 + var6 * var10 + var7 * var8;
         var6 = -var11 * var5;
         var7 = -var10 * var5;
         var5 = -var8 * var5;
         var3 = Math.min(FastMath.carmackSqrt(var6 * var6 + var7 * var7 + var5 * var5), var3);
      }

      return var3;
   }
}
