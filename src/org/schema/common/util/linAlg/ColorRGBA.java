package org.schema.common.util.linAlg;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import org.schema.common.FastMath;

public class ColorRGBA implements Externalizable, Cloneable {
   public static final ColorRGBA black = new ColorRGBA(0.0F, 0.0F, 0.0F, 1.0F);
   public static final ColorRGBA white = new ColorRGBA(1.0F, 1.0F, 1.0F, 1.0F);
   public static final ColorRGBA darkGray = new ColorRGBA(0.2F, 0.2F, 0.2F, 1.0F);
   public static final ColorRGBA gray = new ColorRGBA(0.5F, 0.5F, 0.5F, 1.0F);
   public static final ColorRGBA lightGray = new ColorRGBA(0.8F, 0.8F, 0.8F, 1.0F);
   public static final ColorRGBA red = new ColorRGBA(1.0F, 0.0F, 0.0F, 1.0F);
   public static final ColorRGBA green = new ColorRGBA(0.0F, 1.0F, 0.0F, 1.0F);
   public static final ColorRGBA blue = new ColorRGBA(0.0F, 0.0F, 1.0F, 1.0F);
   public static final ColorRGBA yellow = new ColorRGBA(1.0F, 1.0F, 0.0F, 1.0F);
   public static final ColorRGBA magenta = new ColorRGBA(1.0F, 0.0F, 1.0F, 1.0F);
   public static final ColorRGBA cyan = new ColorRGBA(0.0F, 1.0F, 1.0F, 1.0F);
   public static final ColorRGBA orange = new ColorRGBA(0.9843137F, 0.50980395F, 0.0F, 1.0F);
   public static final ColorRGBA brown = new ColorRGBA(0.25490198F, 0.15686275F, 0.09803922F, 1.0F);
   public static final ColorRGBA pink = new ColorRGBA(1.0F, 0.68F, 0.68F, 1.0F);
   public float r;
   public float g;
   public float b;
   public float a;

   public ColorRGBA() {
      this.r = this.g = this.b = this.a = 1.0F;
   }

   public ColorRGBA(ColorRGBA var1) {
      this.a = var1.a;
      this.r = var1.r;
      this.g = var1.g;
      this.b = var1.b;
   }

   public ColorRGBA(float var1, float var2, float var3, float var4) {
      this.r = var1;
      this.g = var2;
      this.b = var3;
      this.a = var4;
   }

   public static ColorRGBA randomColor() {
      ColorRGBA var0;
      (var0 = new ColorRGBA(0.0F, 0.0F, 0.0F, 1.0F)).r = FastMath.nextRandomFloat();
      var0.g = FastMath.nextRandomFloat();
      var0.b = FastMath.nextRandomFloat();
      var0.clamp();
      return var0;
   }

   public ColorRGBA add(ColorRGBA var1) {
      return new ColorRGBA(var1.r + this.r, var1.g + this.g, var1.b + this.b, var1.a + this.a);
   }

   public ColorRGBA addLocal(ColorRGBA var1) {
      this.set(var1.r + this.r, var1.g + this.g, var1.b + this.b, var1.a + this.a);
      return this;
   }

   public int asIntARGB() {
      return ((int)(this.a * 255.0F) & 255) << 24 | ((int)(this.r * 255.0F) & 255) << 16 | ((int)(this.g * 255.0F) & 255) << 8 | (int)(this.b * 255.0F) & 255;
   }

   public int asIntRGBA() {
      return ((int)(this.r * 255.0F) & 255) << 24 | ((int)(this.g * 255.0F) & 255) << 16 | ((int)(this.b * 255.0F) & 255) << 8 | (int)(this.a * 255.0F) & 255;
   }

   public void clamp() {
      if (this.r < 0.0F) {
         this.r = 0.0F;
      } else if (this.r > 1.0F) {
         this.r = 1.0F;
      }

      if (this.g < 0.0F) {
         this.g = 0.0F;
      } else if (this.g > 1.0F) {
         this.g = 1.0F;
      }

      if (this.b < 0.0F) {
         this.b = 0.0F;
      } else if (this.b > 1.0F) {
         this.b = 1.0F;
      }

      if (this.a < 0.0F) {
         this.a = 0.0F;
      } else {
         if (this.a > 1.0F) {
            this.a = 1.0F;
         }

      }
   }

   public void fromIntARGB(int var1) {
      this.a = (float)(var1 >>> 24) / 255.0F;
      this.r = (float)((byte)(var1 >> 16) & 255) / 255.0F;
      this.g = (float)((byte)(var1 >> 8) & 255) / 255.0F;
      this.b = (float)((byte)var1 & 255) / 255.0F;
   }

   public void fromIntRGBA(int var1) {
      this.r = (float)(var1 >>> 24) / 255.0F;
      this.g = (float)((byte)(var1 >> 16) & 255) / 255.0F;
      this.b = (float)((byte)(var1 >> 8) & 255) / 255.0F;
      this.a = (float)((byte)var1 & 255) / 255.0F;
   }

   public Class getClassTag() {
      return this.getClass();
   }

   public float[] getColorArray() {
      return new float[]{this.r, this.g, this.b, this.a};
   }

   public float[] getColorArray(float[] var1) {
      var1[0] = this.r;
      var1[1] = this.g;
      var1[2] = this.b;
      var1[3] = this.a;
      return var1;
   }

   public int hashCode() {
      int var10000 = 37 + 1369 + Float.floatToIntBits(this.r);
      var10000 += var10000 * 37 + Float.floatToIntBits(this.g);
      var10000 += var10000 * 37 + Float.floatToIntBits(this.b);
      return var10000 + var10000 * 37 + Float.floatToIntBits(this.a);
   }

   public boolean equals(Object var1) {
      if (!(var1 instanceof ColorRGBA)) {
         return false;
      } else if (this == var1) {
         return true;
      } else {
         ColorRGBA var2 = (ColorRGBA)var1;
         if (Float.compare(this.r, var2.r) != 0) {
            return false;
         } else if (Float.compare(this.g, var2.g) != 0) {
            return false;
         } else if (Float.compare(this.b, var2.b) != 0) {
            return false;
         } else {
            return Float.compare(this.a, var2.a) == 0;
         }
      }
   }

   public ColorRGBA clone() {
      try {
         return (ColorRGBA)super.clone();
      } catch (CloneNotSupportedException var1) {
         throw new AssertionError();
      }
   }

   public String toString() {
      return "com.jme.renderer.ColorRGBA: [R=" + this.r + ", G=" + this.g + ", B=" + this.b + ", A=" + this.a + "]";
   }

   public void interpolate(ColorRGBA var1, ColorRGBA var2, float var3) {
      this.r = (1.0F - var3) * var1.r + var3 * var2.r;
      this.g = (1.0F - var3) * var1.g + var3 * var2.g;
      this.b = (1.0F - var3) * var1.b + var3 * var2.b;
      this.a = (1.0F - var3) * var1.a + var3 * var2.a;
   }

   public void interpolate(ColorRGBA var1, float var2) {
      this.r = (1.0F - var2) * this.r + var2 * var1.r;
      this.g = (1.0F - var2) * this.g + var2 * var1.g;
      this.b = (1.0F - var2) * this.b + var2 * var1.b;
      this.a = (1.0F - var2) * this.a + var2 * var1.a;
   }

   public ColorRGBA mult(ColorRGBA var1) {
      return new ColorRGBA(var1.r * this.r, var1.g * this.g, var1.b * this.b, var1.a * this.a);
   }

   public ColorRGBA multLocal(float var1) {
      this.r *= var1;
      this.g *= var1;
      this.b *= var1;
      this.a *= var1;
      return this;
   }

   public ColorRGBA set(ColorRGBA var1) {
      if (var1 == null) {
         this.r = 0.0F;
         this.g = 0.0F;
         this.b = 0.0F;
         this.a = 0.0F;
      } else {
         this.r = var1.r;
         this.g = var1.g;
         this.b = var1.b;
         this.a = var1.a;
      }

      return this;
   }

   public void set(float var1, float var2, float var3, float var4) {
      this.r = var1;
      this.g = var2;
      this.b = var3;
      this.a = var4;
   }

   public float[] toArray(float[] var1) {
      if (var1 == null) {
         var1 = new float[4];
      }

      var1[0] = this.r;
      var1[1] = this.g;
      var1[2] = this.b;
      var1[3] = this.a;
      return var1;
   }

   public void writeExternal(ObjectOutput var1) throws IOException {
      var1.writeFloat(this.r);
      var1.writeFloat(this.g);
      var1.writeFloat(this.b);
      var1.writeFloat(this.a);
   }

   public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
      this.r = var1.readFloat();
      this.g = var1.readFloat();
      this.b = var1.readFloat();
      this.a = var1.readFloat();
   }
}
