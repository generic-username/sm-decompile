package org.schema.common.util.linAlg;

import java.io.Serializable;
import org.schema.common.FastMath;

public class Vector implements Serializable {
   private static final long serialVersionUID = 1L;
   public float x;
   public float y;
   private Vector basis;

   public Vector(float var1, float var2) {
      this.x = var1;
      this.y = var2;
   }

   public Vector add(Vector var1) {
      return new Vector(this.x + var1.getX(), this.y + var1.getY());
   }

   public float crossMult(Vector var1) {
      return this.x * var1.getY() - this.y * var1.getX();
   }

   public float dotMult(Vector var1) {
      return this.x * var1.getX() + this.y * var1.getY();
   }

   public float getAngle(Vector var1) {
      return FastMath.acos(this.dotMult(var1) / (this.getLength() * var1.getLength()));
   }

   public Vector getBasis() {
      return this.basis;
   }

   public void setBasis(Vector var1) {
      this.basis = var1;
   }

   public Vector getDirVec(Vector var1) {
      return var1.sub(this);
   }

   public float getLength() {
      return FastMath.sqrt(this.x * this.x + this.y * this.y);
   }

   public Vector getNormalized() {
      return this.mult(1.0F / this.getLength());
   }

   public Vector getNormalVector() {
      return new Vector(-this.y, this.x);
   }

   public float getTotalAngle(Vector var1) {
      float var2 = this.getAngle(var1);
      if (this.x > var1.getX()) {
         var2 = 6.2831855F - var2;
      }

      return var2;
   }

   public float getX() {
      return this.x;
   }

   public void setX(float var1) {
      this.x = var1;
   }

   public float getY() {
      return this.y;
   }

   public void setY(float var1) {
      this.y = var1;
   }

   public Vector mult(float var1) {
      return new Vector(this.x * var1, this.y * var1);
   }

   public Vector rotate(float var1) {
      float var2 = FastMath.sin(var1);
      var1 = FastMath.cos(var1);
      return new Vector(this.x * var1 + this.y * var2, this.x * -var2 + this.y * var1);
   }

   public Vector sub(Vector var1) {
      return new Vector(this.x - var1.getX(), this.y - var1.getY());
   }

   public String toString() {
      return this.x + ", " + this.y;
   }
}
