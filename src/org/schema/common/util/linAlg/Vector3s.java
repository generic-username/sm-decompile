package org.schema.common.util.linAlg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Vector3s implements Comparable {
   public static final long SHORT_MAX = 32767L;
   public static final long SHORT_MAX2 = 65534L;
   public static final long SHORT_MAX2x2 = 4294705156L;
   private static final Pattern parameterRegex = Pattern.compile("(\\-?\\s*[0-9]+)[\\s\\,\\.]+(\\-?\\s*[0-9]+)[\\s\\,\\.]+(\\-?\\s*[0-9]+)");
   public short x;
   public short y;
   public short z;

   public Vector3s() {
   }

   public Vector3s(float var1, float var2, float var3) {
      this.x = (short)((int)var1);
      this.y = (short)((int)var2);
      this.z = (short)((int)var3);
   }

   public Vector3s(short var1, short var2, short var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public Vector3s(Vector3f var1) {
      this.x = (short)((int)var1.x);
      this.y = (short)((int)var1.y);
      this.z = (short)((int)var1.z);
   }

   public Vector3s(Vector3s var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public static void main(String[] var0) {
      try {
         parseVector3sFree("");
      } catch (Exception var11) {
         var11.printStackTrace();
      }

      try {
         parseVector3sFree("asdf asdf asfd");
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      try {
         parseVector3sFree("1 2 3");
      } catch (Exception var9) {
         var9.printStackTrace();
      }

      try {
         parseVector3sFree("-12 2 3");
      } catch (Exception var8) {
         var8.printStackTrace();
      }

      try {
         parseVector3sFree("-12 2 -343");
      } catch (Exception var7) {
         var7.printStackTrace();
      }

      try {
         parseVector3sFree("-012 2 -343");
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      try {
         parseVector3sFree("012 2 -343");
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      try {
         parseVector3sFree("012, 2, -343");
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      try {
         parseVector3sFree("012,   2,   -0343");
      } catch (Exception var3) {
         var3.printStackTrace();
      }

      try {
         parseVector3sFree("012,  . - 2,  . -343");
      } catch (Exception var2) {
         var2.printStackTrace();
      }

      try {
         parseVector3sFree("1.-2.33");
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public static Vector3s parseVector3i(String var0) {
      String[] var1;
      if ((var1 = var0.split(",")).length != 3) {
         throw new NumberFormatException("Wrong number of arguments");
      } else {
         return new Vector3s((float)Integer.parseInt(var1[0].trim()), (float)Integer.parseInt(var1[1].trim()), (float)Integer.parseInt(var1[2].trim()));
      }
   }

   public static Vector3s parseVector3sFree(String var0) {
      Vector3s var1 = new Vector3s();
      Matcher var2 = parameterRegex.matcher(var0.trim());

      short var3;
      for(var3 = 0; var2.find(); ++var3) {
         try {
            var1.x = Short.parseShort(var2.group(1).replaceAll("\\s", ""));
            var1.y = Short.parseShort(var2.group(2).replaceAll("\\s", ""));
            var1.z = Short.parseShort(var2.group(3).replaceAll("\\s", ""));
         } catch (Exception var4) {
            var4.printStackTrace();
            throw new NumberFormatException("Exception in string: " + var0);
         }
      }

      if (var3 == 0) {
         throw new NumberFormatException("No pattern found in " + var0);
      } else {
         System.err.println("[VECTOR3i] PARSED: FROM \"" + var0 + "\" -> " + var1);
         return var1;
      }
   }

   public static float getDisatance(Vector3s var0, Vector3s var1) {
      int var2 = var1.x - var0.x;
      int var3 = var1.y - var0.y;
      int var4 = var1.z - var0.z;
      return FastMath.sqrt((float)(var2 * var2 + var3 * var3 + var4 * var4));
   }

   public void absolute() {
      this.x = (short)Math.abs(this.x);
      this.y = (short)Math.abs(this.y);
      this.z = (short)Math.abs(this.z);
   }

   public void add(short var1, short var2, short var3) {
      this.x += var1;
      this.y += var2;
      this.z += var3;
   }

   public void add(Vector3s var1) {
      this.x += var1.x;
      this.y += var1.y;
      this.z += var1.z;
   }

   public void add(Vector3s var1, Vector3s var2) {
      this.x = (short)(var1.x + var2.x);
      this.y = (short)(var1.y + var2.y);
      this.z = (short)(var1.z + var2.z);
   }

   public int compareTo(Vector3s var1) {
      return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z) - (Math.abs(var1.x) + Math.abs(var1.y) + Math.abs(var1.z));
   }

   public void coordAdd(short var1, short var2) {
      switch(var1) {
      case 0:
         this.x += var2;
      case 1:
         this.y += var2;
      case 2:
         this.z += var2;
      default:
         assert false : var1;

         throw new NullPointerException(var1 + " coord");
      }
   }

   public void div(short var1) {
      this.x /= var1;
      this.y /= var1;
      this.z /= var1;
   }

   public boolean equals(short var1, short var2, short var3) {
      return this.x == var1 && this.y == var2 && this.z == var3;
   }

   public short getCoord(short var1) {
      switch(var1) {
      case 0:
         return this.x;
      case 1:
         return this.y;
      case 2:
         return this.z;
      default:
         assert false : var1;

         throw new NullPointerException(var1 + " coord");
      }
   }

   public int hashCode() {
      return ((this.x ^ this.x >>> 8) * 15 + (this.y ^ this.y >>> 8)) * 15 + (this.z ^ this.z >>> 8);
   }

   public boolean equals(Object var1) {
      try {
         Vector3s var4 = (Vector3s)var1;
         return this.x == var4.x && this.y == var4.y && this.z == var4.z;
      } catch (NullPointerException var2) {
         return false;
      } catch (ClassCastException var3) {
         return false;
      }
   }

   public String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + ")";
   }

   public long code() {
      short var10000 = this.x;
      long var1 = (long)this.y + 32767L;
      return (((long)this.z + 32767L) * 4294705156L + var1 * 65534L + (long)this.x) * 232323L;
   }

   public final float length() {
      return FastMath.sqrt((float)(this.x * this.x + this.y * this.y + this.z * this.z));
   }

   public void negate() {
      this.x = (short)(-this.x);
      this.y = (short)(-this.y);
      this.z = (short)(-this.z);
   }

   public void scale(short var1) {
      this.x *= var1;
      this.y *= var1;
      this.z *= var1;
   }

   public void scale(short var1, short var2, short var3) {
      this.x *= var1;
      this.y *= var2;
      this.z *= var3;
   }

   public void scaleFloat(float var1) {
      this.x = (short)((int)((float)this.x * var1));
      this.y = (short)((int)((float)this.y * var1));
      this.z = (short)((int)((float)this.z * var1));
   }

   public void set(short var1, short var2, short var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public void set(Vector3s var1) {
      this.set(var1.x, var1.y, var1.z);
   }

   public void sub(short var1, short var2, short var3) {
      this.x -= var1;
      this.y -= var2;
      this.z -= var3;
   }

   public void sub(Vector3s var1) {
      this.x -= var1.x;
      this.y -= var1.y;
      this.z -= var1.z;
   }

   public void sub(Vector3s var1, Vector3s var2) {
      this.x = (short)(var1.x - var2.x);
      this.y = (short)(var1.y - var2.y);
      this.z = (short)(var1.z - var2.z);
   }

   public Vector3f toVector3f() {
      return new Vector3f((float)this.x, (float)this.y, (float)this.z);
   }

   public void set(Vector3b var1) {
      this.x = (short)var1.x;
      this.y = (short)var1.y;
      this.z = (short)var1.z;
   }

   public float lengthSquared() {
      return (float)(this.x * this.x + this.y * this.y + this.z * this.z);
   }

   public boolean betweenIncl(Vector3s var1, Vector3s var2) {
      return this.x >= var1.x && this.y >= var1.y && this.z >= var1.z && this.x <= var2.x && this.y <= var2.y && this.z <= var2.z;
   }

   public boolean betweenExcl(Vector3s var1, Vector3s var2) {
      return this.x > var1.x && this.y > var1.y && this.z > var1.z && this.x < var2.x && this.y < var2.y && this.z < var2.z;
   }

   public boolean betweenIncExcl(Vector3s var1, Vector3s var2) {
      return this.x >= var1.x && this.y >= var1.y && this.z >= var1.z && this.x < var2.x && this.y < var2.y && this.z < var2.z;
   }

   public String toStringPure() {
      return this.x + ", " + this.y + ", " + this.z;
   }
}
