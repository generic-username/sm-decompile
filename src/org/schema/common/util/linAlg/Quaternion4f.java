package org.schema.common.util.linAlg;

import java.io.Serializable;
import java.rmi.Remote;
import org.schema.common.FastMath;

public class Quaternion4f implements Serializable, Cloneable, Remote {
   private static final long serialVersionUID = 65793L;
   private float x;
   private float y;
   private float z;
   private float w;

   public strictfp Quaternion4f() {
      this(0.0F, 0.0F, 0.0F, 1.0F);
   }

   public strictfp Quaternion4f(float var1, float var2, float var3, float var4) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.w = var4;
   }

   public static strictfp Quaternion4f fromAxisAngle(float var0, float var1, float var2, float var3) {
      float var4;
      if ((var4 = FastMath.sqrt(var0 * var0 + var1 * var1 + var2 * var2)) == 0.0F) {
         return new Quaternion4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         float var5 = FastMath.cos(var3 /= 2.0F);
         var3 = FastMath.sin(var3) / var4;
         return new Quaternion4f(var0 * var3, var1 * var3, var2 * var3, var5);
      }
   }

   public static strictfp Quaternion4f fromEulerAngles(float var0, float var1, float var2) {
      var0 *= 0.5F;
      var1 *= 0.5F;
      var2 *= 0.5F;
      float var3 = FastMath.cos(var0);
      float var4 = FastMath.cos(var1);
      float var5 = FastMath.cos(var2);
      var0 = FastMath.sin(var0);
      var1 = FastMath.sin(var1);
      var2 = FastMath.sin(var2);
      float var6 = var4 * var5;
      float var7 = var1 * var2;
      return new Quaternion4f(var0 * var6 - var3 * var7, var3 * var1 * var5 + var0 * var4 * var2, var3 * var4 * var2 - var0 * var1 * var5, var3 * var6 + var0 * var7);
   }

   public static strictfp Quaternion4f fromMatrix(Matrix16f var0) {
      float var1;
      float var2;
      float var3;
      float var4;
      if ((var1 = var0.m[0] + var0.m[5] + var0.m[10] + 1.0F) > 0.0F) {
         var3 = FastMath.sqrt(var1);
         var4 = 0.5F * var3;
         var3 = 0.5F / var3;
         var1 = (var0.m[6] - var0.m[9]) * var3;
         var2 = (var0.m[8] - var0.m[2]) * var3;
         var3 = (var0.m[1] - var0.m[4]) * var3;
      } else if ((var3 = Math.max(Math.max(var0.m[0], var0.m[5]), var0.m[10])) == var0.m[0]) {
         var4 = FastMath.sqrt(1.0F + var0.m[0] - var0.m[5] - var0.m[10]);
         var1 = 0.5F * var4;
         var4 = 0.5F / var4;
         var2 = (var0.m[1] + var0.m[4]) * var4;
         var3 = (var0.m[2] + var0.m[8]) * var4;
         var4 = (var0.m[6] - var0.m[9]) * var4;
      } else if (var3 == var0.m[5]) {
         var4 = FastMath.sqrt(1.0F + var0.m[5] - var0.m[0] - var0.m[10]);
         var2 = 0.5F * var4;
         var4 = 0.5F / var4;
         var1 = (var0.m[1] + var0.m[4]) * var4;
         var3 = (var0.m[6] + var0.m[9]) * var4;
         var4 = (var0.m[8] - var0.m[2]) * var4;
      } else {
         var4 = FastMath.sqrt(1.0F + var0.m[10] - var0.m[0] - var0.m[5]);
         var3 = 0.5F * var4;
         var4 = 0.5F / var4;
         var1 = (var0.m[2] + var0.m[8]) * var4;
         var2 = (var0.m[6] + var0.m[9]) * var4;
         var4 = (var0.m[1] - var0.m[4]) * var4;
      }

      return new Quaternion4f(var1, var2, var3, var4);
   }

   public strictfp Quaternion4f clone() {
      return new Quaternion4f(this.x, this.y, this.z, this.w);
   }

   public strictfp String toString() {
      return "[Quaternion] " + this.x + ", " + this.y + ", " + this.z + ", " + this.w;
   }

   public strictfp Quaternion4f conjugate() {
      this.x = -this.x;
      this.y = -this.y;
      this.z = -this.z;
      return this;
   }

   public strictfp void copyFrom(Quaternion4f var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
      this.w = var1.w;
   }

   public strictfp Vector4D createAxisAngles() {
      float var1 = FastMath.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
      float var2 = 0.0F;
      float var3 = 0.0F;
      float var4 = 0.0F;
      if (var1 != 0.0F) {
         var2 = this.x / var1;
         var3 = this.y / var1;
         var4 = this.z / var1;
      }

      var1 = 2.0F * FastMath.acos(this.w);
      return new Vector4D(var2, var3, var4, var1);
   }

   public strictfp Vector3D createEulerAngles() {
      float var1;
      float var2;
      float var3;
      if (FastMath.abs(var1 = FastMath.cos(this.y)) > 0.001F) {
         var1 = 1.0F / var1;
         var2 = FastMath.atan2(2.0F * (this.w * this.x - this.y * this.z) * var1, (1.0F - 2.0F * (this.x * this.x + this.y * this.y)) * var1);
         var3 = FastMath.asin(2.0F * (this.x * this.z + this.w * this.y));
         var1 = FastMath.atan2(2.0F * (this.w * this.z - this.x * this.y) * var1, (1.0F - 2.0F * (this.y * this.y + this.z * this.z)) * var1);
         return new Vector3D(var2, var3, var1);
      } else {
         var2 = FastMath.asin(2.0F * (this.x * this.z + this.w * this.y));
         var3 = FastMath.atan2(2.0F * (this.x * this.y + this.w * this.z), 1.0F - 2.0F * (this.x * this.x + this.z * this.z));
         return new Vector3D(0.0F, var2, var3);
      }
   }

   public strictfp Matrix16f createMatrix() {
      float[] var1;
      (var1 = new float[16])[0] = 1.0F - 2.0F * this.y * this.y - 2.0F * this.z * this.z;
      var1[1] = 2.0F * this.x * this.y + 2.0F * this.z * this.w;
      var1[2] = 2.0F * this.x * this.z - 2.0F * this.y * this.w;
      var1[3] = 0.0F;
      var1[4] = 2.0F * this.x * this.y - 2.0F * this.z * this.w;
      var1[5] = 1.0F - 2.0F * this.x * this.x - 2.0F * this.z * this.z;
      var1[6] = 2.0F * this.y * this.z + 2.0F * this.x * this.w;
      var1[7] = 0.0F;
      var1[8] = 2.0F * this.x * this.z + 2.0F * this.y * this.w;
      var1[9] = 2.0F * this.y * this.z - 2.0F * this.x * this.w;
      var1[10] = 1.0F - 2.0F * this.x * this.x - 2.0F * this.y * this.y;
      var1[11] = 0.0F;
      var1[12] = 0.0F;
      var1[13] = 0.0F;
      var1[14] = 0.0F;
      var1[15] = 1.0F;
      return new Matrix16f(var1);
   }

   public strictfp float getW() {
      return this.w;
   }

   public strictfp float getX() {
      return this.x;
   }

   public strictfp float getY() {
      return this.y;
   }

   public strictfp float getZ() {
      return this.z;
   }

   public strictfp Quaternion4f invert() {
      float var1 = this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
      this.x /= -var1;
      this.y /= -var1;
      this.z /= -var1;
      this.w /= var1;
      return this;
   }

   public strictfp float length() {
      return FastMath.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
   }

   public strictfp Quaternion4f multiply(Quaternion4f var1) {
      float var2 = this.x * var1.w + this.y * var1.z - this.z * var1.y + this.w * var1.x;
      float var3 = -this.x * var1.z + this.y * var1.w + this.z * var1.x + this.w * var1.y;
      float var4 = this.x * var1.y - this.y * var1.x + this.z * var1.w + this.w * var1.z;
      float var5 = -this.x * var1.x - this.y * var1.y - this.z * var1.z + this.w * var1.w;
      this.x = var2;
      this.y = var3;
      this.z = var4;
      this.w = var5;
      return this;
   }

   public strictfp Quaternion4f multiply(Vector3D var1) {
      float var2 = this.w * var1.getX() - this.z * var1.getY() + this.y * var1.getZ();
      float var3 = this.z * var1.getX() + this.w * var1.getY() - this.x * var1.getZ();
      float var4 = -this.y * var1.getX() + this.x * var1.getY() + this.w * var1.getZ();
      float var5 = -this.x * var1.getX() - this.y * var1.getY() - this.z * var1.getZ();
      this.x = var2;
      this.y = var3;
      this.z = var4;
      this.w = var5;
      return this;
   }

   public strictfp Quaternion4f multiplyInvert(Quaternion4f var1) {
      return this.invert().multiply(var1);
   }

   public strictfp Quaternion4f normalize() {
      float var1;
      if ((var1 = this.length()) > 0.0F) {
         this.x /= var1;
         this.y /= var1;
         this.z /= var1;
         this.w /= var1;
      }

      return this;
   }

   public strictfp Vector3D rotate(Vector3D var1) {
      Quaternion4f var2 = this.clone().conjugate().normalize();
      var2 = this.clone().multiply(var1).multiply(var2);
      return new Vector3D(var2.x, var2.y, var2.z);
   }

   public strictfp Quaternion4f scale(float var1) {
      this.x *= var1;
      this.y *= var1;
      this.z *= var1;
      this.w *= var1;
      return this;
   }

   public strictfp Quaternion4f slerp(Quaternion4f var1, float var2) {
      if (var1 == null) {
         return this.clone();
      } else {
         float var3;
         if ((var3 = this.x * var1.x + this.y * var1.y + this.z * var1.z + this.w * var1.w) < 0.0F) {
            var3 = -var3;
            var1 = new Quaternion4f(-var1.x, -var1.y, -var1.z, -var1.w);
         } else {
            var1 = new Quaternion4f(var1.x, var1.y, var1.z, var1.w);
         }

         float var4 = 1.0F - var2;
         float var5 = var2;
         if (1.0F - var3 > 1.0E-4F) {
            if (var3 > 1.0F) {
               var3 /= var3;
            }

            float var6;
            if (FastMath.abs(var6 = FastMath.sin(var3 = FastMath.acos(var3))) > 1.0E-4F) {
               var4 = FastMath.sin(var4 * var3) / var6;
               var5 = FastMath.sin(var2 * var3) / var6;
            }
         }

         var1.x = var4 * this.x + var5 * var1.x;
         var1.y = var4 * this.y + var5 * var1.y;
         var1.z = var4 * this.z + var5 * var1.z;
         var1.w = var4 * this.w + var5 * var1.w;
         return var1;
      }
   }

   public strictfp Quaternion4f slerp2(Quaternion4f var1, float var2) {
      if (var1 == null) {
         return this.clone();
      } else {
         float var3 = this.x * var1.x + this.y * var1.y + this.z * var1.z + this.w * var1.w;
         float var4 = 1.0F - var2;
         float var5 = var2;
         if (var3 + 1.0F > 0.001F) {
            var1 = new Quaternion4f(var1.x, var1.y, var1.z, var1.w);
            if (FastMath.abs(var3) > 1.0F) {
               var3 /= FastMath.abs(var3);
            }

            float var6;
            if (FastMath.abs(var6 = FastMath.sin(var3 = FastMath.acos(var3))) > 0.001F) {
               var4 = FastMath.sin(var4 * var3) / var6;
               var5 = FastMath.sin(var2 * var3) / var6;
            }
         } else {
            var1 = new Quaternion4f(-this.y, this.x, -this.w, this.z);
            var4 = FastMath.sin(var4 * 3.1415927F / 2.0F);
            var5 = FastMath.sin(var2 * 3.1415927F / 2.0F);
         }

         var1.x = var4 * this.x + var5 * var1.x;
         var1.y = var4 * this.y + var5 * var1.y;
         var1.z = var4 * this.z + var5 * var1.z;
         var1.w = var4 * this.w + var5 * var1.w;
         return var1;
      }
   }
}
