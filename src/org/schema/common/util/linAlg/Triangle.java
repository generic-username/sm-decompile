package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;

public class Triangle {
   public Vector3f v1;
   public Vector3f v2;
   public Vector3f v3;
   private Vector3f normal;

   public Triangle(Vector3f var1, Vector3f var2, Vector3f var3) {
      this.v1 = var1;
      this.v2 = var2;
      this.v3 = var3;
   }

   public String toString() {
      return "[" + this.v1 + ", " + this.v2 + ", " + this.v3 + "]";
   }

   public Vector3f getNormal() {
      if (this.normal == null) {
         this.normal = this.calculateNormal(this.v1, this.v2, this.v3);
      }

      return this.normal;
   }

   public Vector3f calculateNormal(Vector3f var1, Vector3f var2, Vector3f var3) {
      return calculateNormal(var1.x, var1.y, var1.z, var2.x, var2.y, var2.z, var3.x, var3.y, var3.z);
   }

   public static Vector3f calculateNormal(float var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
      Vector3f var9 = new Vector3f(var0 - var3, var1 - var4, var2 - var5);
      Vector3f var10 = new Vector3f(var3 - var6, var4 - var7, var5 - var8);
      Vector3f var11;
      (var11 = new Vector3f()).cross(var9, var10);
      var11.normalize();
      return var11;
   }
}
