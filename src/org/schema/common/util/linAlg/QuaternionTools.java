package org.schema.common.util.linAlg;

import javax.vecmath.Quat4f;
import org.schema.common.FastMath;

public class QuaternionTools {
   public static float getAngle(Quat4f var0) {
      return 2.0F * (float)FastMath.acosFast((double)var0.w);
   }
}
