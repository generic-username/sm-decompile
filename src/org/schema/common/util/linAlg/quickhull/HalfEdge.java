package org.schema.common.util.linAlg.quickhull;

class HalfEdge {
   Vertex vertex;
   Face face;
   HalfEdge next;
   HalfEdge prev;
   HalfEdge opposite;

   public HalfEdge(Vertex var1, Face var2) {
      this.vertex = var1;
      this.face = var2;
   }

   public HalfEdge() {
   }

   public void setNext(HalfEdge var1) {
      this.next = var1;
   }

   public HalfEdge getNext() {
      return this.next;
   }

   public void setPrev(HalfEdge var1) {
      this.prev = var1;
   }

   public HalfEdge getPrev() {
      return this.prev;
   }

   public Face getFace() {
      return this.face;
   }

   public HalfEdge getOpposite() {
      return this.opposite;
   }

   public void setOpposite(HalfEdge var1) {
      this.opposite = var1;
      var1.opposite = this;
   }

   public Vertex head() {
      return this.vertex;
   }

   public Vertex tail() {
      return this.prev != null ? this.prev.vertex : null;
   }

   public Face oppositeFace() {
      return this.opposite != null ? this.opposite.face : null;
   }

   public String getVertexString() {
      return this.tail() != null ? this.tail().index + "-" + this.head().index : "?-" + this.head().index;
   }

   public double length() {
      return this.tail() != null ? this.head().pnt.distance(this.tail().pnt) : -1.0D;
   }

   public double lengthSquared() {
      return this.tail() != null ? this.head().pnt.distanceSquared(this.tail().pnt) : -1.0D;
   }
}
