package org.schema.common.util.linAlg.quickhull;

class VertexList {
   private Vertex head;
   private Vertex tail;

   public void clear() {
      this.head = this.tail = null;
   }

   public void add(Vertex var1) {
      if (this.head == null) {
         this.head = var1;
      } else {
         this.tail.next = var1;
      }

      var1.prev = this.tail;
      var1.next = null;
      this.tail = var1;
   }

   public void addAll(Vertex var1) {
      if (this.head == null) {
         this.head = var1;
      } else {
         this.tail.next = var1;
      }

      for(var1.prev = this.tail; var1.next != null; var1 = var1.next) {
      }

      this.tail = var1;
   }

   public void delete(Vertex var1) {
      if (var1.prev == null) {
         this.head = var1.next;
      } else {
         var1.prev.next = var1.next;
      }

      if (var1.next == null) {
         this.tail = var1.prev;
      } else {
         var1.next.prev = var1.prev;
      }
   }

   public void delete(Vertex var1, Vertex var2) {
      if (var1.prev == null) {
         this.head = var2.next;
      } else {
         var1.prev.next = var2.next;
      }

      if (var2.next == null) {
         this.tail = var1.prev;
      } else {
         var2.next.prev = var1.prev;
      }
   }

   public void insertBefore(Vertex var1, Vertex var2) {
      var1.prev = var2.prev;
      if (var2.prev == null) {
         this.head = var1;
      } else {
         var2.prev.next = var1;
      }

      var1.next = var2;
      var2.prev = var1;
   }

   public Vertex first() {
      return this.head;
   }

   public boolean isEmpty() {
      return this.head == null;
   }
}
