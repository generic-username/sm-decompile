package org.schema.common.util.linAlg.quickhull;

public class InternalErrorException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public InternalErrorException(String var1) {
      super(var1);
   }
}
