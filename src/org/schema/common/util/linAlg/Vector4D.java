package org.schema.common.util.linAlg;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;

public class Vector4D {
   public float x;
   public float y;
   public float z;
   public float a;

   public Vector4D() {
      this.x = 0.0F;
      this.y = 0.0F;
      this.z = 0.0F;
      this.a = 0.0F;
   }

   public Vector4D(float var1, float var2, float var3, float var4) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.a = var4;
   }

   public static Vector4D add(Vector4D var0, Vector4D var1) {
      return new Vector4D(var0.x + var1.x, var0.y + var1.y, var0.z + var1.z, var0.a + var1.a);
   }

   public void add(Vector4D var1) {
      this.x += var1.x;
      this.y += var1.y;
      this.z += var1.z;
      this.a += var1.a;
   }

   public Vector3D as3D() {
      return new Vector3D(this.x, this.y, this.z);
   }

   public Vector4D clone() {
      return new Vector4D(this.x, this.y, this.z, this.a);
   }

   public String toString() {
      return "[a" + this.a + ", x" + this.x + ", y" + this.y + ", z" + this.z + "]";
   }

   public Quat4f getQuat4f() {
      return new Quat4f(this.x, this.y, this.z, this.a);
   }

   public Vector4f getVector4f() {
      return new Vector4f(this.x, this.y, this.z, this.a);
   }

   public float length() {
      return FastMath.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.a * this.a);
   }

   public void mult(Vector4D var1) {
      this.x *= var1.x;
      this.y *= var1.y;
      this.z *= var1.z;
      this.a *= var1.a;
   }

   public boolean nearly(Vector4D var1, float var2) {
      return (new Vector4D(this.x - var1.x, this.y - var1.y, this.z - var1.z, this.a - var1.a)).length() <= var2;
   }

   public void normalize() {
      this.scalarDiv(this.length());
   }

   public void quaternionToDegree() {
      if (this.x == 0.0F && this.y == 0.0F && this.z == 0.0F) {
         this.a = 0.0F;
      } else {
         float var1 = FastMath.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
         this.x /= var1;
         this.y /= var1;
         this.z /= var1;
         this.a = 2.0F * FastMath.acos(this.a);
      }
   }

   public void scalarDiv(float var1) {
      this.x /= var1;
      this.y /= var1;
      this.z /= var1;
      this.a /= var1;
   }

   public void scalarMult(float var1) {
      this.x *= var1;
      this.y *= var1;
      this.z *= var1;
      this.a *= var1;
   }

   public void set(float var1, float var2, float var3, float var4) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.a = var4;
   }

   public void sub(Vector4D var1) {
      this.x -= var1.x;
      this.y -= var1.y;
      this.z -= var1.z;
      this.a -= var1.a;
   }
}
