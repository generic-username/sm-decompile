package org.schema.common.util.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataUtil {
   public static final int[] primes = firstNPrimes(512);
   public static final String dataPath;
   public static String configPath;

   public static void copy(File var0, File var1) throws IOException {
      FileInputStream var4 = new FileInputStream(var0);
      FileOutputStream var5 = new FileOutputStream(var1);
      byte[] var2 = new byte[1024];

      int var3;
      while((var3 = var4.read(var2)) > 0) {
         var5.write(var2, 0, var3);
      }

      var4.close();
      var5.close();
   }

   public static int[] firstNPrimes(int var0) {
      System.currentTimeMillis();
      int[] var1 = new int[var0];
      int var2 = 0;

      for(int var3 = 2; var2 < var0; ++var3) {
         boolean var4 = true;

         for(int var5 = 2; var5 < var3; ++var5) {
            if (var3 % var5 == 0) {
               var4 = false;
               break;
            }
         }

         if (var4) {
            var1[var2] = var3;
            ++var2;
         }
      }

      return var1;
   }

   static {
      dataPath = "data" + File.separator;
      configPath = "config/mainConfig.xml";
   }
}
