package org.schema.common.util.image;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import org.schema.common.FastMath;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.resource.FileExt;

public class ImageUtil {
   public static BufferedImage createBufferedImage(String var0) throws IOException {
      FileExt var1;
      if (!(var1 = new FileExt(DataUtil.dataPath + var0)).exists()) {
         throw new FileNotFoundException("-- ERROR(ImageUtil): Picture not found: " + var1.getAbsolutePath());
      } else {
         try {
            BufferedImage var5 = ImageIO.read(var1);
            return var5;
         } catch (Exception var3) {
            ImageIcon var2 = createImageIcon(var0);
            Graphics2D var4;
            (var4 = (Graphics2D)(new BufferedImage(var2.getIconWidth(), var2.getIconHeight(), 1)).getGraphics()).drawImage(var2.getImage(), 0, 0, (ImageObserver)null);
            var4.dispose();
            throw new FileNotFoundException("-- Warning: ImageIO threw exception while reading " + var0 + ", using alterantive version succeeded");
         }
      }
   }

   public static ImageIcon createImageIcon(String var0) throws IOException {
      FileExt var1;
      if (!(var1 = new FileExt(DataUtil.dataPath + var0)).exists()) {
         throw new FileNotFoundException("-- ERROR(ImageUtil): Picture not found: " + var1.getAbsolutePath());
      } else {
         return new ImageIcon(var1.getAbsolutePath());
      }
   }

   public static BufferedImage[] getBufferedImagesFromDir(String var0, String var1, int var2, String var3) {
      BufferedImage[] var4 = new BufferedImage[var2 + 1];
      DecimalFormat var5 = new DecimalFormat("000");

      for(int var6 = 0; var6 <= var2; ++var6) {
         try {
            var4[var6] = createBufferedImage(var1 + var0 + " " + var5.format((long)var6) + "." + var3);
         } catch (IOException var7) {
            var7.printStackTrace();
         }

         if (var4[var6] == null) {
            throw new IndexOutOfBoundsException("!! ERROR(ImageUtil) while requesting picture #" + var6 + " of maximal " + var2);
         }
      }

      return var4;
   }

   public static Dimension getImageDimension(File var0) throws IOException {
      return getImageDimension((InputStream)(new FileInputStream(var0)));
   }

   public static Dimension getImageDimension(InputStream var0) throws IOException {
      DataInputStream var5 = new DataInputStream(var0);

      try {
         int var1;
         Dimension var6;
         if ((var1 = var5.readUnsignedShort()) == 35152) {
            var5.readFully(new byte[14]);
            var6 = new Dimension(var5.readInt(), var5.readInt());
            return var6;
         } else {
            int var2;
            if (var1 != 65496) {
               if (var1 == 16973) {
                  var5.readFully(new byte[16]);
                  var1 = var5.read() | var5.read() << 8 | var5.read() << 16 | var5.read() << 24;
                  var2 = var5.read() | var5.read() << 8 | var5.read() << 16 | var5.read() << 24;
                  var6 = new Dimension(var1, var2);
                  return var6;
               } else if (var1 == 18249) {
                  var5.readFully(new byte[4]);
                  var1 = var5.read() | var5.read() << 8;
                  var2 = var5.read() | var5.read() << 8;
                  var6 = new Dimension(var1, var2);
                  return var6;
               } else {
                  throw new IllegalStateException("unexpected header: " + Integer.toHexString(var1));
               }
            } else {
               while(true) {
                  switch(var1 = var5.readUnsignedShort()) {
                  case 65472:
                  case 65474:
                     var5.readUnsignedShort();
                     var5.readByte();
                     var2 = var5.readUnsignedShort();
                     var1 = var5.readUnsignedShort();
                     var6 = new Dimension(var1, var2);
                     return var6;
                  case 65473:
                  case 65475:
                  case 65477:
                  case 65478:
                  case 65479:
                  case 65480:
                  case 65481:
                  case 65482:
                  case 65483:
                  case 65484:
                  case 65485:
                  case 65486:
                  case 65487:
                  case 65500:
                  case 65502:
                  case 65503:
                  case 65520:
                  case 65521:
                  case 65522:
                  case 65523:
                  case 65524:
                  case 65525:
                  case 65526:
                  case 65527:
                  case 65528:
                  case 65529:
                  case 65530:
                  case 65531:
                  case 65532:
                  case 65533:
                  default:
                     throw new IllegalStateException("invalid jpg marker: " + Integer.toHexString(var1));
                  case 65476:
                  case 65498:
                  case 65499:
                  case 65504:
                  case 65505:
                  case 65506:
                  case 65507:
                  case 65508:
                  case 65509:
                  case 65510:
                  case 65511:
                  case 65512:
                  case 65513:
                  case 65514:
                  case 65515:
                  case 65516:
                  case 65517:
                  case 65518:
                  case 65519:
                  case 65534:
                     var5.readFully(new byte[var5.readUnsignedShort() - 2]);
                  case 65488:
                  case 65489:
                  case 65490:
                  case 65491:
                  case 65492:
                  case 65493:
                  case 65494:
                  case 65495:
                  case 65496:
                  case 65497:
                     break;
                  case 65501:
                     var5.readUnsignedShort();
                  }
               }
            }
         }
      } finally {
         var5.close();
      }
   }

   public static ImageIcon[] getImageIconsFromDir(String var0, String var1, int var2, String var3) throws IOException {
      ImageIcon[] var4 = new ImageIcon[var2 + 1];
      DecimalFormat var5 = new DecimalFormat("000");

      for(int var6 = 0; var6 <= var2; ++var6) {
         var4[var6] = createImageIcon(var1 + var0 + " " + var5.format((long)var6) + "." + var3);
         if (var4[var6] == null) {
            throw new IndexOutOfBoundsException("!! ERROR(ImageUtil) while requesting picture #" + var6 + " of maximal " + var2);
         }
      }

      return var4;
   }

   public static ImageAnalysisResult analyzeImage(File var0) throws NoSuchAlgorithmException, IOException {
      ImageAnalysisResult var1 = new ImageAnalysisResult();
      BufferedInputStream var2 = new BufferedInputStream(new FileInputStream(var0));

      try {
         ImageInputStream var3;
         try {
            Iterator var4;
            if (!(var4 = ImageIO.getImageReaders(var3 = ImageIO.createImageInputStream(var2))).hasNext()) {
               System.err.println("ERROR: NO IMAGE READERS");

               assert false;

               var1.setImage(false);
               ImageAnalysisResult var16 = var1;
               return var16;
            }

            ImageReader var15;
            (var15 = (ImageReader)var4.next()).setInput(var3);
            BufferedImage var5;
            if ((var5 = var15.read(0)) == null) {
               assert false : var15 + "; " + var0.getAbsolutePath();

               ImageAnalysisResult var14 = var1;
               return var14;
            }

            var5.flush();
            if (var15.getFormatName().equals("JPEG")) {
               var3.seek(var3.getStreamPosition() - 2L);
               byte[] var12 = new byte[2];
               var3.read(var12);
               if (var12[0] != 255 && var12[1] != 217) {
                  var1.setTruncated(true);
               } else {
                  var1.setTruncated(false);

                  assert false;
               }
            }

            boolean var13 = FastMath.isPowerOfTwo(var5.getWidth()) && FastMath.isPowerOfTwo(var5.getHeight());
            var1.setPowerOfTwo(var13);
            var1.setImage(true);
         } catch (IndexOutOfBoundsException var9) {
            var1.setTruncated(true);
         } catch (IOException var10) {
            var3 = null;
            if (var10.getCause() instanceof EOFException) {
               var1.setTruncated(true);
            }
         }
      } finally {
         var2.close();
      }

      return var1;
   }
}
