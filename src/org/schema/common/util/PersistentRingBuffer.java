package org.schema.common.util;

import javax.vecmath.Vector3f;

public abstract class PersistentRingBuffer {
   private int size;
   private final int capacity;
   private final Object[] e;
   private int start = 0;

   public PersistentRingBuffer(int var1) {
      this.capacity = var1;
      this.e = this.create(var1);
   }

   public abstract Object[] create(int var1);

   public int size() {
      return this.size;
   }

   public Object add() {
      if (this.size < this.capacity) {
         ++this.size;
      } else {
         this.start = (this.start + 1) % this.capacity;
      }

      return this.get(this.size - 1);
   }

   public Object get(int var1) {
      if (var1 >= 0 && var1 < this.size) {
         var1 = (this.start + var1) % this.capacity;
         return this.e[var1];
      } else {
         throw new ArrayIndexOutOfBoundsException(var1);
      }
   }

   public Object pop() {
      Object var1 = this.get(this.size - 1);
      --this.size;
      return var1;
   }

   public static void main(String[] var0) {
      PersistentRingBuffer var1;
      ((Vector3f)(var1 = new PersistentRingBuffer(4) {
         public final Vector3f[] create(int var1) {
            Vector3f[] var2 = new Vector3f[var1];

            for(int var3 = 0; var3 < var1; ++var3) {
               var2[var3] = new Vector3f();
            }

            return var2;
         }
      }).add()).set(1.0F, 1.0F, 1.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(2.0F, 2.0F, 2.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(3.0F, 3.0F, 3.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(4.0F, 4.0F, 4.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(5.0F, 5.0F, 5.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(6.0F, 6.0F, 6.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(7.0F, 7.0F, 7.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(8.0F, 8.0F, 8.0F);
      System.err.println(":::: " + var1);
      ((Vector3f)var1.add()).set(9.0F, 9.0F, 9.0F);
   }

   public String toString() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("{");

      for(int var2 = 0; var2 < this.size; ++var2) {
         var1.append(this.get(var2) + (var2 < this.size - 1 ? ", " : ""));
      }

      var1.append("}");
      return var1.toString();
   }
}
