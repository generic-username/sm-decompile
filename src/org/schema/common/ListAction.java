package org.schema.common;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Action;
import javax.swing.JList;
import javax.swing.KeyStroke;

public class ListAction implements MouseListener {
   private static final KeyStroke ENTER = KeyStroke.getKeyStroke(10, 0);
   private JList list;
   private KeyStroke keyStroke;

   public ListAction(JList var1, Action var2) {
      this(var1, var2, ENTER);
   }

   public ListAction(JList var1, Action var2, KeyStroke var3) {
      this.list = var1;
      this.keyStroke = var3;
      var1.getInputMap().put(var3, var3);
      this.setAction(var2);
      var1.addMouseListener(this);
   }

   public void mouseClicked(MouseEvent var1) {
      Action var3;
      if (var1.getClickCount() == 2 && (var3 = this.list.getActionMap().get(this.keyStroke)) != null) {
         ActionEvent var2 = new ActionEvent(this.list, 1001, "");
         var3.actionPerformed(var2);
      }

   }

   public void mousePressed(MouseEvent var1) {
   }

   public void mouseReleased(MouseEvent var1) {
   }

   public void mouseEntered(MouseEvent var1) {
   }

   public void mouseExited(MouseEvent var1) {
   }

   public void setAction(Action var1) {
      this.list.getActionMap().put(this.keyStroke, var1);
   }
}
