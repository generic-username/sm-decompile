package org.schema.common;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.util.logging.Level;

public class StdOutErrLevel extends Level {
   private static final long serialVersionUID = 1L;
   public static Level STDOUT;
   public static Level STDERR;

   private StdOutErrLevel(String var1, int var2) {
      super(var1, var2);
   }

   protected Object readResolve() throws ObjectStreamException {
      if (this.intValue() == STDOUT.intValue()) {
         return STDOUT;
      } else if (this.intValue() == STDERR.intValue()) {
         return STDERR;
      } else {
         throw new InvalidObjectException("Unknown instance :" + this);
      }
   }

   static {
      STDOUT = new StdOutErrLevel("STDOUT", Level.INFO.intValue() + 53);
      STDERR = new StdOutErrLevel("STDERR", Level.INFO.intValue() + 54);
   }
}
