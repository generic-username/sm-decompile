package org.schema.common.system.thread;

public interface ForRunnable {
   ForRunnable copy();

   void run(int var1);
}
