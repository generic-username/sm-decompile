package org.schema.common.system.thread;

import java.util.concurrent.Semaphore;

public final class MultiThreadUtil {
   public static final int PROCESSORS_COUNT = Runtime.getRuntime().availableProcessors();
   public static boolean DISABLE_MULTICORE = false;

   private MultiThreadUtil() {
   }

   public static final void multiFor(int var0, int var1, ForRunnable var2) {
      int var3;
      if (PROCESSORS_COUNT > 1 && !DISABLE_MULTICORE) {
         var0 = (var3 = (var1 - var0) / PROCESSORS_COUNT) + (var1 - var0) % PROCESSORS_COUNT;
         final Semaphore var8 = new Semaphore(PROCESSORS_COUNT - 1);

         int var4;
         final int var5;
         final int var6;
         for(var4 = 0; var4 < PROCESSORS_COUNT - 1; ++var4) {
            var8.acquireUninterruptibly();
            var5 = var4 * var3;
            var6 = var4 * var3 + var3;
            final ForRunnable var7 = var2.copy();
            (new Thread(new Runnable() {
               public final void run() {
                  for(int var1 = var5; var1 < var6; ++var1) {
                     var7.run(var1);
                  }

                  var8.release();
               }
            })).start();
            Thread.yield();
         }

         var4 = (PROCESSORS_COUNT - 1) * var3;
         var5 = (PROCESSORS_COUNT - 1) * var3 + var0;

         for(var6 = var4; var6 < var5; ++var6) {
            var2.run(var6);
         }

         var8.acquireUninterruptibly(PROCESSORS_COUNT - 1);
      } else {
         for(var3 = var0; var3 < var1; ++var3) {
            var2.run(var3);
         }

      }
   }
}
