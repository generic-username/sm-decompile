package org.schema.common.config;

public class ConfigParserException extends Exception {
   private static final long serialVersionUID = 1L;

   public ConfigParserException(String var1, Throwable var2) {
      super(var1 + "\n(FixSuggestion: check your xml config files)", var2);
   }

   public ConfigParserException(String var1) {
      super(var1 + "\n(FixSuggestion: check your xml config files)");
   }
}
