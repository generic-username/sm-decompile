package org.schema.common.config;

public interface FloatMultiConfigField extends MultiConfigField {
   float get(int var1);

   void set(int var1, float var2);
}
