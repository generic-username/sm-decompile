package org.schema.common;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.HashMap;
import java.util.Map;

public class ObjectPoolAdvanced {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final Map initialValue() {
         return new HashMap();
      }
   };
   private Class cls;
   private ObjectArrayList list = new ObjectArrayList();
   private int limit;

   public ObjectPoolAdvanced(Class var1, int var2) {
      this.cls = var1;
      this.limit = var2;
   }

   public static void cleanCurrentThread() {
      threadLocal.remove();
   }

   public static ObjectPoolAdvanced get(Class var0, int var1) {
      Map var2;
      ObjectPoolAdvanced var3;
      if ((var3 = (ObjectPoolAdvanced)(var2 = (Map)threadLocal.get()).get(var0)) == null) {
         var3 = new ObjectPoolAdvanced(var0, var1);
         var2.put(var0, var3);
      }

      return var3;
   }

   private Object create() {
      try {
         return this.cls.newInstance();
      } catch (InstantiationException var2) {
         throw new IllegalStateException(var2);
      } catch (IllegalAccessException var3) {
         throw new IllegalStateException(var3);
      }
   }

   public Object get() {
      return this.list.size() > 0 ? this.list.remove(this.list.size() - 1) : this.create();
   }

   public void release(Object var1) {
      if (this.list.size() < this.limit) {
         this.list.add(var1);
      }

   }
}
