package org.schema.common;

import java.util.ArrayList;
import java.util.Collections;

public class SortedList extends ArrayList {
   private static final long serialVersionUID = 1L;
   private int lastIndexAdded = -1;

   public SortedList() {
   }

   public SortedList(int var1) {
      super(var1);
   }

   public boolean add(Comparable var1) {
      if (var1 == null) {
         throw new NullPointerException("tried to add null " + var1);
      } else if (this.size() == 0) {
         this.setLastIndexAdded(0);
         return super.add(var1);
      } else {
         int var2;
         if ((var2 = Collections.binarySearch(this, var1)) >= 0) {
            this.setLastIndexAdded(var2 + 1);
            super.add(var2 + 1, var1);
            return true;
         } else {
            this.setLastIndexAdded(-var2 - 1);
            super.add(-var2 - 1, var1);
            return true;
         }
      }
   }

   public int getLastIndexAdded() {
      return this.lastIndexAdded;
   }

   public void setLastIndexAdded(int var1) {
      this.lastIndexAdded = var1;
   }
}
