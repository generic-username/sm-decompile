package org.schema.common;

import java.util.HashMap;

public class TimeStatistics {
   public static HashMap timer = new HashMap();

   public static long get(String var0) {
      Long var1;
      return (var1 = (Long)timer.get(var0)) != null ? var1 : -1L;
   }

   public static void reset(String var0) {
      timer.put(var0, System.currentTimeMillis());
   }

   public static void set(String var0) {
      timer.put(var0, System.currentTimeMillis() - (Long)timer.get(var0));
   }
}
