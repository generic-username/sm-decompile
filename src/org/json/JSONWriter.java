package org.json;

import java.io.IOException;
import java.io.Writer;

public class JSONWriter {
   private static final int maxdepth = 200;
   private boolean comma = false;
   protected char mode = 'i';
   private final JSONObject[] stack = new JSONObject[200];
   private int top = 0;
   protected Writer writer;

   public JSONWriter(Writer var1) {
      this.writer = var1;
   }

   private JSONWriter append(String var1) throws JSONException {
      if (var1 == null) {
         throw new JSONException("Null pointer");
      } else if (this.mode != 'o' && this.mode != 'a') {
         throw new JSONException("Value out of sequence.");
      } else {
         try {
            if (this.comma && this.mode == 'a') {
               this.writer.write(44);
            }

            this.writer.write(var1);
         } catch (IOException var2) {
            throw new JSONException(var2);
         }

         if (this.mode == 'o') {
            this.mode = 'k';
         }

         this.comma = true;
         return this;
      }
   }

   public JSONWriter array() throws JSONException {
      if (this.mode != 'i' && this.mode != 'o' && this.mode != 'a') {
         throw new JSONException("Misplaced array.");
      } else {
         this.push((JSONObject)null);
         this.append("[");
         this.comma = false;
         return this;
      }
   }

   private JSONWriter end(char var1, char var2) throws JSONException {
      if (this.mode != var1) {
         throw new JSONException(var1 == 'a' ? "Misplaced endArray." : "Misplaced endObject.");
      } else {
         this.pop(var1);

         try {
            this.writer.write(var2);
         } catch (IOException var3) {
            throw new JSONException(var3);
         }

         this.comma = true;
         return this;
      }
   }

   public JSONWriter endArray() throws JSONException {
      return this.end('a', ']');
   }

   public JSONWriter endObject() throws JSONException {
      return this.end('k', '}');
   }

   public JSONWriter key(String var1) throws JSONException {
      if (var1 == null) {
         throw new JSONException("Null key.");
      } else if (this.mode == 'k') {
         try {
            this.stack[this.top - 1].putOnce(var1, Boolean.TRUE);
            if (this.comma) {
               this.writer.write(44);
            }

            this.writer.write(JSONObject.quote(var1));
            this.writer.write(58);
            this.comma = false;
            this.mode = 'o';
            return this;
         } catch (IOException var2) {
            throw new JSONException(var2);
         }
      } else {
         throw new JSONException("Misplaced key.");
      }
   }

   public JSONWriter object() throws JSONException {
      if (this.mode == 'i') {
         this.mode = 'o';
      }

      if (this.mode != 'o' && this.mode != 'a') {
         throw new JSONException("Misplaced object.");
      } else {
         this.append("{");
         this.push(new JSONObject());
         this.comma = false;
         return this;
      }
   }

   private void pop(char var1) throws JSONException {
      if (this.top <= 0) {
         throw new JSONException("Nesting error.");
      } else if ((this.stack[this.top - 1] == null ? 97 : 107) != var1) {
         throw new JSONException("Nesting error.");
      } else {
         --this.top;
         this.mode = (char)(this.top == 0 ? 100 : (this.stack[this.top - 1] == null ? 97 : 107));
      }
   }

   private void push(JSONObject var1) throws JSONException {
      if (this.top >= 200) {
         throw new JSONException("Nesting too deep.");
      } else {
         this.stack[this.top] = var1;
         this.mode = (char)(var1 == null ? 97 : 107);
         ++this.top;
      }
   }

   public JSONWriter value(boolean var1) throws JSONException {
      return this.append(var1 ? "true" : "false");
   }

   public JSONWriter value(double var1) throws JSONException {
      return this.value(new Double(var1));
   }

   public JSONWriter value(long var1) throws JSONException {
      return this.append(Long.toString(var1));
   }

   public JSONWriter value(Object var1) throws JSONException {
      return this.append(JSONObject.valueToString(var1));
   }
}
