package org.json;

import java.util.Iterator;

public class CookieList {
   public static JSONObject toJSONObject(String var0) throws JSONException {
      JSONObject var1 = new JSONObject();
      JSONTokener var3 = new JSONTokener(var0);

      while(var3.more()) {
         String var2 = Cookie.unescape(var3.nextTo('='));
         var3.next('=');
         var1.put(var2, (Object)Cookie.unescape(var3.nextTo(';')));
         var3.next();
      }

      return var1;
   }

   public static String toString(JSONObject var0) throws JSONException {
      boolean var1 = false;
      Iterator var2 = var0.keys();
      StringBuffer var4 = new StringBuffer();

      while(var2.hasNext()) {
         String var3 = var2.next().toString();
         if (!var0.isNull(var3)) {
            if (var1) {
               var4.append(';');
            }

            var4.append(Cookie.escape(var3));
            var4.append("=");
            var4.append(Cookie.escape(var0.getString(var3)));
            var1 = true;
         }
      }

      return var4.toString();
   }
}
