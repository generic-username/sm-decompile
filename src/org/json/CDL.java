package org.json;

public class CDL {
   private static String getValue(JSONTokener var0) throws JSONException {
      char var1;
      while((var1 = var0.next()) == ' ' || var1 == '\t') {
      }

      switch(var1) {
      case '\u0000':
         return null;
      case '"':
      case '\'':
         char var2 = var1;
         StringBuffer var3 = new StringBuffer();

         while((var1 = var0.next()) != var2) {
            if (var1 == 0 || var1 == '\n' || var1 == '\r') {
               throw var0.syntaxError("Missing close quote '" + var2 + "'.");
            }

            var3.append(var1);
         }

         return var3.toString();
      case ',':
         var0.back();
         return "";
      default:
         var0.back();
         return var0.nextTo(',');
      }
   }

   public static JSONArray rowToJSONArray(JSONTokener var0) throws JSONException {
      JSONArray var1 = new JSONArray();

      while(true) {
         String var2 = getValue(var0);
         char var3 = var0.next();
         if (var2 == null || var1.length() == 0 && var2.length() == 0 && var3 != ',') {
            return null;
         }

         var1.put((Object)var2);

         while(var3 != ',') {
            if (var3 != ' ') {
               if (var3 != '\n' && var3 != '\r' && var3 != 0) {
                  throw var0.syntaxError("Bad character '" + var3 + "' (" + var3 + ").");
               }

               return var1;
            }

            var3 = var0.next();
         }
      }
   }

   public static JSONObject rowToJSONObject(JSONArray var0, JSONTokener var1) throws JSONException {
      JSONArray var2;
      return (var2 = rowToJSONArray(var1)) != null ? var2.toJSONObject(var0) : null;
   }

   public static String rowToString(JSONArray var0) {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < var0.length(); ++var2) {
         if (var2 > 0) {
            var1.append(',');
         }

         Object var3;
         if ((var3 = var0.opt(var2)) != null) {
            String var7;
            if ((var7 = var3.toString()).length() > 0 && (var7.indexOf(44) >= 0 || var7.indexOf(10) >= 0 || var7.indexOf(13) >= 0 || var7.indexOf(0) >= 0 || var7.charAt(0) == '"')) {
               var1.append('"');
               int var4 = var7.length();

               for(int var5 = 0; var5 < var4; ++var5) {
                  char var6;
                  if ((var6 = var7.charAt(var5)) >= ' ' && var6 != '"') {
                     var1.append(var6);
                  }
               }

               var1.append('"');
            } else {
               var1.append(var7);
            }
         }
      }

      var1.append('\n');
      return var1.toString();
   }

   public static JSONArray toJSONArray(String var0) throws JSONException {
      return toJSONArray(new JSONTokener(var0));
   }

   public static JSONArray toJSONArray(JSONTokener var0) throws JSONException {
      return toJSONArray(rowToJSONArray(var0), var0);
   }

   public static JSONArray toJSONArray(JSONArray var0, String var1) throws JSONException {
      return toJSONArray(var0, new JSONTokener(var1));
   }

   public static JSONArray toJSONArray(JSONArray var0, JSONTokener var1) throws JSONException {
      if (var0 != null && var0.length() != 0) {
         JSONArray var2 = new JSONArray();

         JSONObject var3;
         while((var3 = rowToJSONObject(var0, var1)) != null) {
            var2.put((Object)var3);
         }

         return var2.length() == 0 ? null : var2;
      } else {
         return null;
      }
   }

   public static String toString(JSONArray var0) throws JSONException {
      JSONObject var1;
      JSONArray var2;
      return (var1 = var0.optJSONObject(0)) != null && (var2 = var1.names()) != null ? rowToString(var2) + toString(var2, var0) : null;
   }

   public static String toString(JSONArray var0, JSONArray var1) throws JSONException {
      if (var0 != null && var0.length() != 0) {
         StringBuffer var2 = new StringBuffer();

         for(int var3 = 0; var3 < var1.length(); ++var3) {
            JSONObject var4;
            if ((var4 = var1.optJSONObject(var3)) != null) {
               var2.append(rowToString(var4.toJSONArray(var0)));
            }
         }

         return var2.toString();
      } else {
         return null;
      }
   }
}
