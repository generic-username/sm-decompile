package org.json.zip;

import org.json.JSONException;

public class Huff implements None, PostMortem {
   private final int domain;
   private final Huff.Symbol[] symbols;
   private Huff.Symbol table;
   private boolean upToDate = false;
   private int width;

   public Huff(int var1) {
      this.domain = var1;
      int var2 = (var1 << 1) - 1;
      this.symbols = new Huff.Symbol[var2];

      int var3;
      for(var3 = 0; var3 < var1; ++var3) {
         this.symbols[var3] = new Huff.Symbol(var3);
      }

      for(var3 = var1; var3 < var2; ++var3) {
         this.symbols[var3] = new Huff.Symbol(-1);
      }

   }

   public void generate() {
      if (!this.upToDate) {
         Huff.Symbol var1;
         Huff.Symbol var3 = var1 = this.symbols[0];
         this.table = null;
         var1.next = null;

         Huff.Symbol var2;
         Huff.Symbol var4;
         int var5;
         for(var5 = 1; var5 < this.domain; ++var5) {
            if ((var4 = this.symbols[var5]).weight < var1.weight) {
               var4.next = var1;
               var1 = var4;
            } else {
               if (var4.weight < var3.weight) {
                  var3 = var1;
               }

               while((var2 = var3.next) != null && var4.weight >= var2.weight) {
                  var3 = var2;
               }

               var4.next = var2;
               var3.next = var4;
               var3 = var4;
            }
         }

         var5 = this.domain;
         var3 = var1;

         while(true) {
            while(true) {
               var2 = var1;
               Huff.Symbol var6;
               var1 = (var6 = var1.next).next;
               var4 = this.symbols[var5];
               ++var5;
               var4.weight = var2.weight + var6.weight;
               var4.zero = var2;
               var4.one = var6;
               var4.back = null;
               var2.back = var4;
               var6.back = var4;
               if (var1 == null) {
                  this.table = var4;
                  this.upToDate = true;
                  return;
               }

               if (var4.weight < var1.weight) {
                  var4.next = var1;
                  var1 = var4;
                  var3 = var4;
               } else {
                  while((var2 = var3.next) != null && var4.weight >= var2.weight) {
                     var3 = var2;
                  }

                  var4.next = var2;
                  var3.next = var4;
                  var3 = var4;
               }
            }
         }
      }
   }

   private boolean postMortem(int var1) {
      int[] var2 = new int[this.domain];
      Huff.Symbol var3;
      if ((var3 = this.symbols[var1]).integer != var1) {
         return false;
      } else {
         int var4;
         Huff.Symbol var5;
         for(var4 = 0; (var5 = var3.back) != null; var3 = var5) {
            if (var5.zero == var3) {
               var2[var4] = 0;
            } else {
               if (var5.one != var3) {
                  return false;
               }

               var2[var4] = 1;
            }

            ++var4;
         }

         if (var3 != this.table) {
            return false;
         } else {
            this.width = 0;

            for(var3 = this.table; var3.integer == -1; var3 = var2[var4] != 0 ? var3.one : var3.zero) {
               --var4;
            }

            if (var3.integer == var1 && var4 == 0) {
               return true;
            } else {
               return false;
            }
         }
      }
   }

   public boolean postMortem(PostMortem var1) {
      for(int var2 = 0; var2 < this.domain; ++var2) {
         if (!this.postMortem(var2)) {
            JSONzip.log("\nBad huff ");
            JSONzip.logchar(var2, var2);
            return false;
         }
      }

      return this.table.postMortem(((Huff)var1).table);
   }

   public int read(BitReader var1) throws JSONException {
      try {
         this.width = 0;

         Huff.Symbol var2;
         for(var2 = this.table; var2.integer == -1; var2 = var1.bit() ? var2.one : var2.zero) {
            ++this.width;
         }

         this.tick(var2.integer);
         return var2.integer;
      } catch (Throwable var3) {
         throw new JSONException(var3);
      }
   }

   public void tick(int var1) {
      ++this.symbols[var1].weight;
      this.upToDate = false;
   }

   public void tick(int var1, int var2) {
      while(var1 <= var2) {
         this.tick(var1);
         ++var1;
      }

   }

   private void write(Huff.Symbol var1, BitWriter var2) throws JSONException {
      try {
         Huff.Symbol var3;
         if ((var3 = var1.back) != null) {
            ++this.width;
            this.write(var3, var2);
            if (var3.zero == var1) {
               var2.zero();
               return;
            }

            var2.one();
         }

      } catch (Throwable var4) {
         throw new JSONException(var4);
      }
   }

   public void write(int var1, BitWriter var2) throws JSONException {
      this.width = 0;
      this.write(this.symbols[var1], var2);
      this.tick(var1);
   }

   static class Symbol implements PostMortem {
      public Huff.Symbol back;
      public Huff.Symbol next;
      public Huff.Symbol zero;
      public Huff.Symbol one;
      public final int integer;
      public long weight;

      public Symbol(int var1) {
         this.integer = var1;
         this.weight = 0L;
         this.next = null;
         this.back = null;
         this.one = null;
         this.zero = null;
      }

      public boolean postMortem(PostMortem var1) {
         boolean var2 = true;
         Huff.Symbol var5 = (Huff.Symbol)var1;
         if (this.integer == var5.integer && this.weight == var5.weight) {
            if (this.back != null != (var5.back != null)) {
               return false;
            } else {
               Huff.Symbol var3 = this.zero;
               Huff.Symbol var4 = this.one;
               if (var3 == null) {
                  if (var5.zero != null) {
                     return false;
                  }
               } else {
                  var2 = var3.postMortem(var5.zero);
               }

               if (var4 == null) {
                  if (var5.one != null) {
                     return false;
                  }
               } else {
                  var2 = var4.postMortem(var5.one);
               }

               return var2;
            }
         } else {
            return false;
         }
      }
   }
}
