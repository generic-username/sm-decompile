package org.json.zip;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.Kim;

public class Compressor extends JSONzip {
   final BitWriter bitwriter;

   public Compressor(BitWriter var1) {
      this.bitwriter = var1;
   }

   private static int bcd(char var0) {
      if (var0 >= '0' && var0 <= '9') {
         return var0 - 48;
      } else {
         switch(var0) {
         case '+':
            return 12;
         case ',':
         default:
            return 13;
         case '-':
            return 11;
         case '.':
            return 10;
         }
      }
   }

   public void flush() throws JSONException {
      this.pad(8);
   }

   private void one() throws JSONException {
      this.write(1, 1);
   }

   public void pad(int var1) throws JSONException {
      try {
         this.bitwriter.pad(var1);
      } catch (Throwable var2) {
         throw new JSONException(var2);
      }
   }

   private void write(int var1, int var2) throws JSONException {
      try {
         this.bitwriter.write(var1, var2);
      } catch (Throwable var3) {
         throw new JSONException(var3);
      }
   }

   private void write(int var1, Huff var2) throws JSONException {
      var2.write(var1, this.bitwriter);
   }

   private void write(Kim var1, Huff var2) throws JSONException {
      this.write(var1, 0, var1.length, var2);
   }

   private void write(Kim var1, int var2, int var3, Huff var4) throws JSONException {
      while(var2 < var3) {
         this.write(var1.get(var2), var4);
         ++var2;
      }

   }

   private void writeAndTick(int var1, Keep var2) throws JSONException {
      int var3 = var2.bitsize();
      var2.tick(var1);
      this.write(var1, var3);
   }

   private void writeArray(JSONArray var1) throws JSONException {
      boolean var2 = false;
      int var3;
      if ((var3 = var1.length()) == 0) {
         this.write(1, 3);
      } else {
         Object var4;
         if ((var4 = var1.get(0)) == null) {
            var4 = JSONObject.NULL;
         }

         if (var4 instanceof String) {
            var2 = true;
            this.write(6, 3);
            this.writeString((String)var4);
         } else {
            this.write(7, 3);
            this.writeValue(var4);
         }

         for(int var5 = 1; var5 < var3; ++var5) {
            if ((var4 = var1.get(var5)) == null) {
               var4 = JSONObject.NULL;
            }

            if (var4 instanceof String != var2) {
               this.write(0, 1);
            }

            this.write(1, 1);
            if (var4 instanceof String) {
               this.writeString((String)var4);
            } else {
               this.writeValue(var4);
            }
         }

         this.write(0, 1);
         this.write(0, 1);
      }
   }

   private void writeJSON(Object var1) throws JSONException {
      if (JSONObject.NULL.equals(var1)) {
         this.write(4, 3);
      } else if (Boolean.FALSE.equals(var1)) {
         this.write(3, 3);
      } else if (Boolean.TRUE.equals(var1)) {
         this.write(2, 3);
      } else {
         if (var1 instanceof Map) {
            var1 = new JSONObject((Map)var1);
         } else if (var1 instanceof Collection) {
            var1 = new JSONArray((Collection)var1);
         } else if (var1.getClass().isArray()) {
            var1 = new JSONArray(var1);
         }

         if (var1 instanceof JSONObject) {
            this.writeObject((JSONObject)var1);
         } else if (var1 instanceof JSONArray) {
            this.writeArray((JSONArray)var1);
         } else {
            throw new JSONException("Unrecognized object");
         }
      }
   }

   private void writeName(String var1) throws JSONException {
      Kim var3 = new Kim(var1);
      int var2;
      if ((var2 = this.namekeep.find(var3)) != -1) {
         this.write(1, 1);
         this.writeAndTick(var2, this.namekeep);
      } else {
         this.write(0, 1);
         this.write(var3, this.namehuff);
         this.write(256, this.namehuff);
         this.namekeep.register(var3);
      }
   }

   private void writeObject(JSONObject var1) throws JSONException {
      boolean var2 = true;
      Iterator var3 = var1.keys();

      while(var3.hasNext()) {
         Object var4;
         if ((var4 = var3.next()) instanceof String) {
            if (var2) {
               var2 = false;
               this.write(5, 3);
            } else {
               this.write(1, 1);
            }

            this.writeName((String)var4);
            if ((var4 = var1.get((String)var4)) instanceof String) {
               this.write(0, 1);
               this.writeString((String)var4);
            } else {
               this.write(1, 1);
               this.writeValue(var4);
            }
         }
      }

      if (var2) {
         this.write(0, 3);
      } else {
         this.write(0, 1);
      }
   }

   private void writeString(String var1) throws JSONException {
      if (var1.length() == 0) {
         this.write(0, 1);
         this.write(0, 1);
         this.write(256, this.substringhuff);
         this.write(0, 1);
      } else {
         Kim var3 = new Kim(var1);
         int var2;
         if ((var2 = this.stringkeep.find(var3)) != -1) {
            this.write(1, 1);
            this.writeAndTick(var2, this.stringkeep);
         } else {
            this.writeSubstring(var3);
            this.stringkeep.register(var3);
         }
      }
   }

   private void writeSubstring(Kim var1) throws JSONException {
      this.substringkeep.reserve();
      this.write(0, 1);
      int var2 = 0;
      int var3;
      int var4 = (var3 = var1.length) - 3;
      int var5 = -1;
      int var6 = 0;

      while(true) {
         int var8 = -1;

         int var7;
         for(var7 = var2; var7 <= var4 && (var8 = this.substringkeep.match(var1, var7, var3)) == -1; ++var7) {
         }

         if (var8 == -1) {
            this.write(0, 1);
            if (var2 < var3) {
               this.write(var1, var2, var3, this.substringhuff);
               if (var5 != -1) {
                  this.substringkeep.registerOne(var1, var5, var6);
               }
            }

            this.write(256, this.substringhuff);
            this.write(0, 1);
            this.substringkeep.registerMany(var1);
            return;
         }

         if (var2 != var7) {
            this.write(0, 1);
            this.write(var1, var2, var7, this.substringhuff);
            this.write(256, this.substringhuff);
            if (var5 != -1) {
               this.substringkeep.registerOne(var1, var5, var6);
               var5 = -1;
            }
         }

         this.write(1, 1);
         this.writeAndTick(var8, this.substringkeep);
         var2 = var7 + this.substringkeep.length(var8);
         if (var5 != -1) {
            this.substringkeep.registerOne(var1, var5, var6);
         }

         var5 = var7;
         var6 = var2 + 1;
      }
   }

   private void writeValue(Object var1) throws JSONException {
      if (var1 instanceof Number) {
         String var2 = JSONObject.numberToString((Number)var1);
         int var3;
         if ((var3 = this.values.find(var2)) != -1) {
            this.write(2, 2);
            this.writeAndTick(var3, this.values);
         } else {
            long var4;
            if ((var1 instanceof Integer || var1 instanceof Long) && (var4 = ((Number)var1).longValue()) >= 0L && var4 < 16384L) {
               this.write(0, 2);
               if (var4 < 16L) {
                  this.write(0, 1);
                  this.write((int)var4, 4);
               } else {
                  this.write(1, 1);
                  if (var4 < 128L) {
                     this.write(0, 1);
                     this.write((int)var4, 7);
                  } else {
                     this.write(1, 1);
                     this.write((int)var4, 14);
                  }
               }
            } else {
               this.write(1, 2);

               for(int var6 = 0; var6 < var2.length(); ++var6) {
                  this.write(bcd(var2.charAt(var6)), 4);
               }

               this.write(endOfNumber, 4);
               this.values.register(var2);
            }
         }
      } else {
         this.write(3, 2);
         this.writeJSON(var1);
      }
   }

   private void zero() throws JSONException {
      this.write(0, 1);
   }

   public void zip(JSONObject var1) throws JSONException {
      this.begin();
      this.writeJSON(var1);
   }

   public void zip(JSONArray var1) throws JSONException {
      this.begin();
      this.writeJSON(var1);
   }
}
