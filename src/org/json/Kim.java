package org.json;

import java.util.Arrays;

public class Kim {
   private byte[] bytes;
   private int hashcode;
   public int length;
   private String string;

   public Kim(byte[] var1, int var2, int var3) {
      this.bytes = null;
      this.hashcode = 0;
      this.length = 0;
      this.string = null;
      int var4 = 1;
      this.hashcode = 0;
      this.length = var3 - var2;
      if (this.length > 0) {
         this.bytes = new byte[this.length];

         for(int var5 = 0; var5 < this.length; ++var5) {
            var3 = var1[var5 + var2] & 255;
            var4 += var3;
            this.hashcode += var4;
            this.bytes[var5] = (byte)var3;
         }

         this.hashcode += var4 << 16;
      }

   }

   public Kim(byte[] var1, int var2) {
      this((byte[])var1, 0, var2);
   }

   public Kim(Kim var1, int var2, int var3) {
      this(var1.bytes, var2, var3);
   }

   public Kim(String var1) throws JSONException {
      this.bytes = null;
      this.hashcode = 0;
      this.length = 0;
      this.string = null;
      int var2 = var1.length();
      this.hashcode = 0;
      this.length = 0;
      if (var2 > 0) {
         int var3 = 0;

         while(true) {
            if (var3 >= var2) {
               this.bytes = new byte[this.length];
               var3 = 0;
               int var9 = 1;

               for(int var6 = 0; var6 < var2; ++var6) {
                  int var7;
                  if ((var7 = var1.charAt(var6)) <= 127) {
                     this.bytes[var3] = (byte)var7;
                     var9 += var7;
                     this.hashcode += var9;
                     ++var3;
                  } else {
                     int var8;
                     if (var7 <= 16383) {
                        var8 = 128 | var7 >>> 7;
                        this.bytes[var3] = (byte)var8;
                        var9 += var8;
                        this.hashcode += var9;
                        ++var3;
                        var8 = var7 & 127;
                        this.bytes[var3] = (byte)var8;
                        var9 += var8;
                        this.hashcode += var9;
                        ++var3;
                     } else {
                        if (var7 >= 55296 && var7 <= 56319) {
                           ++var6;
                           var7 = ((var7 & 1023) << 10 | var1.charAt(var6) & 1023) + 65536;
                        }

                        var8 = 128 | var7 >>> 14;
                        this.bytes[var3] = (byte)var8;
                        var9 += var8;
                        this.hashcode += var9;
                        ++var3;
                        var8 = 128 | var7 >>> 7 & 255;
                        this.bytes[var3] = (byte)var8;
                        var9 += var8;
                        this.hashcode += var9;
                        ++var3;
                        var8 = var7 & 127;
                        this.bytes[var3] = (byte)var8;
                        var9 += var8;
                        this.hashcode += var9;
                        ++var3;
                     }
                  }
               }

               this.hashcode += var9 << 16;
               break;
            }

            char var4;
            if ((var4 = var1.charAt(var3)) <= 127) {
               ++this.length;
            } else if (var4 <= 16383) {
               this.length += 2;
            } else {
               if (var4 >= '\ud800' && var4 <= '\udfff') {
                  ++var3;
                  char var5 = var1.charAt(var3);
                  if (var4 > '\udbff' || var5 < '\udc00' || var5 > '\udfff') {
                     throw new JSONException("Bad UTF16");
                  }
               }

               this.length += 3;
            }

            ++var3;
         }
      }

   }

   public int characterAt(int var1) throws JSONException {
      int var2;
      if (((var2 = this.get(var1)) & 128) == 0) {
         return var2;
      } else {
         int var3;
         if (((var3 = this.get(var1 + 1)) & 128) == 0) {
            if ((var2 = (var2 & 127) << 7 | var3) > 127) {
               return var2;
            }
         } else {
            int var4 = this.get(var1 + 2);
            var2 = (var2 & 127) << 14 | (var3 & 127) << 7 | var4;
            if ((var4 & 128) == 0 && var2 > 16383 && var2 <= 1114111 && (var2 < 55296 || var2 > 57343)) {
               return var2;
            }
         }

         throw new JSONException("Bad character at " + var1);
      }
   }

   public static int characterSize(int var0) throws JSONException {
      if (var0 >= 0 && var0 <= 1114111) {
         if (var0 <= 127) {
            return 1;
         } else {
            return var0 <= 16383 ? 2 : 3;
         }
      } else {
         throw new JSONException("Bad character " + var0);
      }
   }

   public int copy(byte[] var1, int var2) {
      System.arraycopy(this.bytes, 0, var1, var2, this.length);
      return var2 + this.length;
   }

   public boolean equals(Object var1) {
      if (!(var1 instanceof Kim)) {
         return false;
      } else {
         Kim var2 = (Kim)var1;
         if (this == var2) {
            return true;
         } else {
            return this.hashcode != var2.hashcode ? false : Arrays.equals(this.bytes, var2.bytes);
         }
      }
   }

   public int get(int var1) throws JSONException {
      if (var1 >= 0 && var1 <= this.length) {
         return this.bytes[var1] & 255;
      } else {
         throw new JSONException("Bad character at " + var1);
      }
   }

   public int hashCode() {
      return this.hashcode;
   }

   public String toString() throws JSONException {
      if (this.string == null) {
         int var2 = 0;
         char[] var3 = new char[this.length];

         int var1;
         for(int var4 = 0; var4 < this.length; var4 += characterSize(var1)) {
            if ((var1 = this.characterAt(var4)) < 65536) {
               var3[var2] = (char)var1;
               ++var2;
            } else {
               var3[var2] = (char)('\ud800' | var1 - 65536 >>> 10);
               ++var2;
               var3[var2] = (char)('\udc00' | var1 & 1023);
               ++var2;
            }
         }

         this.string = new String(var3, 0, var2);
      }

      return this.string;
   }
}
