package obfuscated;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.Element;

public final class ac extends X {
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();
   private int c = 3;
   private Vector3i f = new Vector3i();
   private Vector3i g;
   private short a;

   public ac(X[] var1, Vector3i var2, Vector3i var3, Vector3i var4, short var5) {
      super(var1, var2, var3, 5, 0);
      this.g = var4;
      this.a = var5;
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      a(this.e, this.d);
      this.f.set(this.g);
      Vector3i var2 = Element.DIRECTIONSi[this.c];
      this.f.add(var2);
      if (var2.x > 0 && var1.x > this.g.x || var2.x < 0 && var1.x < this.g.x || var2.y > 0 && var1.y > this.g.y || var2.y < 0 && var1.y < this.g.y || var2.z > 0 && var1.z > this.g.z || var2.z < 0 && var1.z < this.g.z) {
         return 32767;
      } else {
         this.f.set(this.g);
         this.f.sub(var2);
         if (this.c.x == (this.d.x - this.e.x) / 2 || this.c.y == (this.d.y - this.e.y) / 2) {
            if (this.c.z > this.d.z - 3 && this.c.z < this.d.z - 1) {
               return 282;
            }

            if (this.c.z < this.d.z - 1) {
               return 286;
            }
         }

         return this.c.z % 2 == 0 && this.c.z < this.d.z - 4 ? this.a : 32767;
      }
   }
}
