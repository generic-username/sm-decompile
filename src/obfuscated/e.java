package obfuscated;

import java.util.Random;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.controller.GenerationElementMap;

public class e extends d {
   private static final int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)140));
   private static final int b = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)155));

   public e(long var1) {
      super(var1);
   }

   protected final int a(float var1, Random var2) {
      return var1 < 0.07F ? a : b;
   }

   public final void a() {
      this.a = new aO[2];
      this.a[0] = new aJ((short)485, (short)155);
      this.a[1] = new aJ((short)493, (short)155);
   }

   protected final void a(byte var1, byte var2, byte var3, A var4, Random var5) {
      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)485, (short)155, a);
      }

      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)493, (short)155, a);
      }

   }
}
