package obfuscated;

import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.GenerationElementMap;
import org.schema.game.server.controller.RequestDataStructureGen;

public class y extends w {
   private static final int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)89));
   // $FF: synthetic field
   private static boolean a = !y.class.desiredAssertionStatus();

   public y() {
      this.a.set(-1, 1, -1);
      this.b.set(1, 16, 1);
   }

   public final void a(Segment var1, RequestDataStructureGen var2, int var3, int var4, int var5, short var6, short var7, short var8) throws SegmentDataWriteException {
      ++var4;
      if (!a && var6 < 0) {
         throw new AssertionError();
      } else {
         SegmentData var10 = var1.getSegmentData();
         int var11 = 4 + (var6 & 3);
         if (var3 >= 0 && var3 < 32 && var5 >= 0 && var5 < 32) {
            byte var12 = (byte)Math.min(32, var4 + var11);

            for(byte var9 = (byte)Math.max(0, var4); var9 < var12; ++var9) {
               if (var10.getType((byte)var3, var9, (byte)var5) == 0) {
                  var2.currentChunkCache.placeBlock(a, (byte)var3, var9, (byte)var5, var10);
               }
            }
         }

         int var13;
         if ((var13 = var6 >> 2 & 7) < 4) {
            a(var10, var2, var3, var4, var5, var13, var11, var6 >> 6);
            if ((var6 >> 5 & 1) == 1) {
               a(var10, var2, var3, var4, var5, var13 + 1 & 3, var11, var6 >> 8);
            }
         }

      }
   }

   private static void a(SegmentData var0, RequestDataStructureGen var1, int var2, int var3, int var4, int var5, int var6, int var7) throws SegmentDataWriteException {
      int var8;
      if ((var8 = var7 & 3) > 0) {
         ++var8;
      }

      var6 = (var7 >> 2) % (var6 - 2) + 1;
      switch(var5) {
      case 0:
         ++var2;
         break;
      case 1:
         ++var4;
         break;
      case 2:
         --var2;
         break;
      case 3:
         --var4;
         break;
      default:
         return;
      }

      var3 += var6;
      if (var2 >= 0 && var2 < 32 && var3 >= 0 && var3 < 32 && var4 >= 0 && var4 < 32 && var0.getType((byte)var2, (byte)var3, (byte)var4) == 0) {
         var1.currentChunkCache.placeBlock(a, (byte)var2, (byte)var3, (byte)var4, var0);
      }

      switch(var5) {
      case 0:
         ++var2;
         break;
      case 1:
         ++var4;
         break;
      case 2:
         --var2;
         break;
      case 3:
         --var4;
         break;
      default:
         return;
      }

      if (var2 >= 0 && var2 < 32 && var4 >= 0 && var4 < 32) {
         byte var10 = (byte)Math.min(var3 + var8, 32);

         for(byte var9 = (byte)Math.max(0, var3); var9 < var10; ++var9) {
            if (var0.getType((byte)var2, var9, (byte)var4) == 0) {
               var1.currentChunkCache.placeBlock(a, (byte)var2, var9, (byte)var4, var0);
            }
         }
      }

   }
}
