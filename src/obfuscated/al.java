package obfuscated;

import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;

public final class al extends X {
   private static int c = 32;
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();
   private Random a;

   public al(an var1, Vector3i var2, Vector3i var3, Random var4) {
      super((X[])null, var2, var3, 8, 0);
      var2.y += c;
      var3.y += c;
      var2.x -= var1.a;
      var2.z -= var1.a;
      var3.x -= var1.a;
      var3.z -= var1.a;
      this.a = var4;
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      if (var1.y % 8 == 0) {
         int var2 = this.a.nextInt(48);
         if (this.b.y - var1.y > 5 && var2 < 9 && this.c.x > 0 && this.c.z > 0 && var1.x <= this.d.x - 2 && this.c.z <= this.d.z - 2 && this.c.y != this.d.y - 1) {
            switch(var2) {
            case 0:
               return 285;
            case 1:
               return 55;
            case 2:
               return 282;
            case 3:
               return 285;
            case 4:
               return 285;
            case 5:
               return 282;
            case 6:
               return 285;
            case 7:
               return 283;
            case 8:
               return 285;
            }
         }

         return 5;
      } else if (this.c.x > 0 && this.c.z > 0 && this.c.x <= this.d.x - 2 && this.c.z <= this.d.z - 2 && this.c.y != this.d.y - 1) {
         return 32767;
      } else {
         return (short)(var1.y % 8 <= 3 || this.b.y - var1.y <= 5 || var1.x % 8 <= 2 && var1.z % 8 <= 2 ? 75 : 63);
      }
   }
}
