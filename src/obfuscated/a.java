package obfuscated;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;

public final class a extends c {
   private Vector3i a = new Vector3i();
   private Vector3i b = new Vector3i();
   private Vector3f a = new Vector3f();
   private Vector3f b = new Vector3f();
   private Vector3f c = new Vector3f();
   private int a;
   private int b;
   private int c;
   private Vector3i c;
   private Vector3i d;

   public a(Vector3i var1) {
      new Transform();
      this.a = 16;
      this.b = 48;
      this.c = 4;
      this.c = var1;
      this.d = new Vector3i(this.b, this.b, this.b);
   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      try {
         var2 = var2;
         a var11 = this;

         for(byte var4 = 0; var4 < 32; ++var4) {
            for(byte var5 = 0; var5 < 32; ++var5) {
               for(byte var6 = 0; var6 < 32; ++var6) {
                  var11.a.set(var6 + var2.pos.x, var5 + var2.pos.y, var4 + var2.pos.z);
                  var11.b.sub(var11.c, var11.a);
                  var11.b.set((float)var11.d.x, (float)var11.d.y, (float)var11.d.z);
                  var11.a.set((float)var11.c.x, (float)var11.c.y, (float)var11.c.z);
                  Vector3f var10000 = var11.a;
                  var10000.x -= (float)var11.d.x;
                  var10000 = var11.a;
                  var10000.y -= (float)var11.d.y;
                  var10000 = var11.a;
                  var10000.z -= (float)var11.d.z;
                  float var7 = var11.a.length();
                  var11.a.normalize();
                  float var8 = 0.0F;

                  boolean var9;
                  for(var9 = false; var8 < var7; ++var8) {
                     var11.b.add(var11.a);
                     var11.c.set(var11.b);
                     var10000 = var11.c;
                     var10000.x -= (float)var11.a.x;
                     var10000 = var11.c;
                     var10000.y -= (float)var11.a.y;
                     var10000 = var11.c;
                     var10000.z -= (float)var11.a.z;
                     if (var11.c.length() < (float)var11.c) {
                        var9 = true;
                        break;
                     }
                  }

                  if (var11.a.equals(var11.c)) {
                     System.err.println("PLACED DEATH START CENTER!");
                     var11.a(var6, var5, var4, var2, (short)65);
                  }

                  if (!var9) {
                     if (var11.b.length() > (float)(var11.a + 1) && var11.b.length() < (float)var11.b) {
                        var11.a(var6, var5, var4, var2, (short)5);
                     } else if (var11.b.length() == (float)(var11.a + 1)) {
                        var11.a(var6, var5, var4, var2, (short)80);
                     }
                  }
               }
            }
         }

         var1.getSegmentBuffer().updateBB(var2);
      } catch (SegmentDataWriteException var10) {
         var10.printStackTrace();
      }
   }
}
