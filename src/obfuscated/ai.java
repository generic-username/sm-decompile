package obfuscated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.forms.Mesh;

public final class ai {
   private Random a;
   private ArrayList a = new ArrayList();
   private Collection a = new ArrayList();
   private int a;
   private int b;
   private int c;
   private int d;
   private int e;
   private Vector4f a;
   private boolean a;
   private boolean b;
   private int f;
   private Vector4f b;
   private an a;

   public ai(an var1, Random var2, Collection var3, int var4, int var5, int var6, int var7, int var8, int var9, Vector4f var10) {
      this.a = var1;
      this.a = var2;
      this.a = var3;
      this.a = var5;
      this.b = var6;
      this.c = var8;
      this.d = var9;
      this.e = var7;
      new Vector3f((float)(this.a + var8 / 2), 0.0F, (float)(this.b + var9 / 2));
      var2.nextInt();
      this.a = var10;
      this.a.w = 0.1F;
      this.a = false;
      this.b = false;
      this.f = 0;
      this.b = new Vector4f();
      new Mesh();
      new Mesh();
      switch(var4) {
      case 0:
         float var19 = (float)(1 + this.a.nextInt(4));
         float var20 = (float)this.a.nextInt(10) / 30.0F;
         float var12 = (float)this.a;
         float var14 = (float)(this.a + this.c);
         float var18 = (float)this.e;
         float var17 = (float)this.b;
         float var15 = (float)(this.b + this.d);
         this.a.add(new al(this.a, new Vector3i((int)var12, 0, (int)var15), new Vector3i((int)var14, (int)var18, (int)var17), this.a));
         this.a.nextInt(16);
         this.a.nextInt(32);
         float var10001 = var12 - var20;
         float var10002 = var14 + var20;
         float var10003 = var17 - var20;
         float var10004 = var15 + var20;
         float var10005 = (float)this.e;
         var19 += (float)this.e;
         var18 = var10005;
         var17 = var10004;
         var15 = var10003;
         var14 = var10002;
         var12 = var10001;
         this.a.add(new al(this.a, new Vector3i((int)var12, (int)var18, (int)var17), new Vector3i((int)var14, (int)var19, (int)var15), this.a));
         return;
      case 1:
         new ArrayList();
         new Vector3f();
         Vector3f var11 = new Vector3f();
         new Vector3f();
         new Vector3f();
         this.a.nextInt(5);
         this.a.nextInt(8);
         this.a.nextInt(2);
         if (this.e > 48) {
            this.a.nextInt(3);
         }

         var7 = this.d / 2;
         var8 = this.c / 2;
         Vector3f var13 = new Vector3f((float)(this.a + var8), 0.0F, (float)(this.b + var7));
         Vector2f var16 = new Vector2f((float)var8, (float)var7);
         var11.x = 0.0F;
         System.err.println("MODERN " + var13 + "; " + var16 + "; " + this.b + "; " + this.e);
         this.a.add(new aj(this.a, new Vector3i((int)(var13.x - var16.x), 0, (int)(var13.z - var16.y)), new Vector3i((int)(var13.x + var16.x), this.e, (int)(var13.z - var16.y))));
         return;
      case 2:
         this.b();
         return;
      case 3:
         this.a();
      default:
      }
   }

   private static boolean a() {
      return Math.random() > 0.5D;
   }

   private void a(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.a.add(new ak(this.a, new Vector3i((int)var1, (int)var5, (int)var4), new Vector3i((int)var2, (int)var6, (int)var3)));
   }

   private void a(float var1, float var2, float var3, float var4, float var5) {
      while(true) {
         int var7 = 0;
         ++this.f;
         int var10 = this.e / 13;
         int var8 = (int)(var2 - var1);
         int var9 = (int)(var4 - var3);
         int var6 = this.e / 13 - this.f;
         if (var5 > 35.0F) {
            var7 = this.a.nextInt(5);
         }

         System.err.println("cube height " + var6);
         this.a(var1, var2, var3, var4, var5, var5 + (float)var6);
         ao var11;
         if (var7 == 0 && !this.b) {
            var11 = new ao(this.a);
            if (var8 > var9) {
               var7 = a() ? 0 : 1;
            } else {
               var7 = a() ? 2 : 3;
            }

            switch(var7) {
            case 0:
               new Vector2f(var1, var4 + 0.2F);
               new Vector2f(var2, var4 + 0.2F);
               break;
            case 1:
               new Vector2f(var2, var3 - 0.2F);
               new Vector2f(var1, var3 - 0.2F);
               break;
            case 2:
               new Vector2f(var2 + 0.2F, var4);
               new Vector2f(var2 + 0.2F, var3);
               break;
            case 3:
            default:
               new Vector2f(var1 - 0.2F, var3);
               new Vector2f(var1 - 0.2F, var4);
            }

            Vector4f var10000 = this.b;
            this.b = true;
            this.a.add(var11);
         } else if (var7 == 0) {
            var11 = new ao(this.a);
            this.a.add(new T((X[])null, new Vector3i((int)var1, (int)var5, (int)var4), new Vector3i((int)var2, (int)var5 + 10, (int)var3), 8));
            this.a.add(var11);
         } else if (var7 == 0 && !this.a) {
            new Vector3f(var1, var5 + 2.0F, var3);
            Vector4f var10001 = this.b;
            new ap();
            new Vector3f(var2, var5 + 2.0F, var3);
            var10001 = this.b;
            new ap();
            new Vector3f(var2, var5 + 2.0F, var4);
            var10001 = this.b;
            new ap();
            new Vector3f(var1, var5 + 2.0F, var4);
            var10001 = this.b;
            new ap();
            this.a = true;
         }

         var5 += (float)var6;
         if (var8 <= this.c / 5 || var9 <= this.d - 5 || this.f >= var10) {
            var6 = this.e / 15;

            for(var7 = 0; var7 < var6; ++var7) {
               float var12 = (float)(10 + this.a.nextInt(30)) / 10.0F;
               float var13 = (float)this.a.nextInt(20) / 10.0F + 1.0F;
               if (var8 <= 0 && var8 == 0) {
                  var8 = 1;
               }

               float var16 = var1 + (float)this.a.nextInt(Math.abs(var8)) * Math.signum((float)var8);
               if (var9 <= 0 && var9 == 0) {
                  var9 = 1;
               }

               float var17 = var3 + (float)this.a.nextInt(Math.abs(var9)) * Math.signum((float)var9);
               if (var16 + var12 > var2) {
                  var16 = var2 - var12;
               }

               if (var17 + var12 > var4) {
                  var17 = var4 - var12;
               }

               this.a(var16, var16 + var12, var17, var17 + var12, var5, var5 + var13);
            }

            if (this.e > 45) {
               var11 = new ao(this.a);
               this.a.nextInt(8);
               Collection var19 = this.a;
               Vector3f var15 = new Vector3f((var1 + var2) / 2.0F, var5, (var3 + var4) / 2.0F);
               Collection var14 = var19;
               var11.a = var15;
               var14.add(new as(var11.a, new Vector3i(var11.a.x, var11.a.y, var11.a.z), new Vector3i(var11.a.x + 1.0F, var11.a.y + 15.0F, var11.a.z + 1.0F)));
               new Vector3f(var11.a.x, var11.a.y + 15.0F + 1.0F, var11.a.z);
               new Vector4f(255.0F, 192.0F, 160.0F, 1.0F);
               new ap();
               this.a.add(var11);
            }

            return;
         }

         float var18 = var1 + 1.0F;
         float var10002 = var2 - 1.0F;
         float var10003 = var3 + 1.0F;
         --var4;
         var3 = var10003;
         var2 = var10002;
         var1 = var18;
         this = this;
      }
   }

   private float a(int var1, int var2, int var3, int var4, int var5, int var6, float var7) {
      new ArrayList();
      byte var8 = 1;
      byte var9 = 0;
      Vector3f var10 = new Vector3f();
      switch(var4) {
      case 0:
         var9 = 1;
         var8 = 0;
         break;
      case 1:
         var9 = -1;
         var8 = 0;
         break;
      case 2:
         var9 = 0;
         var8 = 1;
         break;
      case 3:
         var9 = 0;
         var8 = -1;
      }

      var10.x = (float)(var1 + var3) / 32.0F;
      var10.x = var7;
      this.a.add(new am(this.a, new Vector3i(var1, var2, var3), new Vector3i(var1 + (var8 == 0 ? 1 : 0) + var8 * var5, var2 + var6, var3 + (var9 == 0 ? 1 : 0) + var9 * var5)));
      return var10.x;
   }

   private void a() {
      a();
      float var17 = (float)this.a.nextInt(32) / 32.0F;
      this.a.nextInt(4);
      float var16 = (float)(this.a.nextInt(3) + 1);
      int var10 = this.a + this.c / 2;
      int var11 = this.b + this.d / 2;
      int var8 = 1;
      int var7 = 1;
      int var6 = 1;
      int var5 = 1;
      int var9 = this.e;
      int var10000 = this.e;
      int var12 = this.d / 2;
      int var13 = this.c / 2;
      int var14 = 0;
      byte var15;
      if (this.e > 40) {
         var15 = 15;
      } else if (this.e > 30) {
         var15 = 10;
      } else if (this.e > 20) {
         var15 = 5;
      } else if (this.e > 10) {
         var15 = 2;
      } else {
         var15 = 1;
      }

      for(; var9 >= 3 && var14 < var15; --var9) {
         int var1 = this.a.nextInt() % var13 + 1;
         int var2 = this.a.nextInt() % var13 + 1;
         int var3 = this.a.nextInt() % var12 + 1;
         int var4 = this.a.nextInt() % var12 + 1;
         boolean var18 = false;
         if (var1 <= var5 && var2 <= var6 && var3 <= var7 && var4 <= var8) {
            var18 = true;
         }

         if (var1 == var5 || var2 == var6 || var3 == var7 || var4 == var8) {
            var18 = true;
         }

         if (!var18) {
            var5 = Math.max(var1, var5);
            var6 = Math.max(var2, var6);
            var7 = Math.max(var3, var7);
            var8 = Math.max(var4, var8);
            var17 = this.a(var10 - var1, 0, var11 + var4, 1, var3 + var4, var9, var17) - 0.03125F;
            var17 = this.a(var10 - var1, 0, var11 - var3, 2, var2 + var1, var9, var17) - 0.03125F;
            var17 = this.a(var10 + var2, 0, var11 - var3, 0, var3 + var4, var9, var17) - 0.03125F;
            var17 = this.a(var10 + var2, 0, var11 + var4, 3, var2 + var1, var9, var17) - 0.03125F;
            if (var14 == 0) {
               this.a((float)(var10 - var1), (float)(var10 + var2), (float)(var11 - var3), (float)(var11 + var4), (float)var9);
            } else {
               this.a((float)(var10 - var1), (float)(var10 + var2), (float)(var11 - var3), (float)(var11 + var4), (float)var9, (float)var9 + var16);
            }

            var9 -= this.a.nextInt() % 10 + 1;
            ++var14;
         }
      }

      this.a((float)(var10 - var13), (float)(var10 + var13), (float)(var11 - var12), (float)(var11 + var12), 0.0F, 2.0F);
   }

   private void b() {
      float var14 = (float)this.a.nextInt(3) * 0.25F;
      int var10 = this.a.nextInt(4) + 1;
      this.a.nextInt(3);
      this.a.nextInt(4);
      this.a.nextInt(3);
      int var11 = 2 + this.a.nextInt(4);
      int var12 = 1 + this.a.nextInt(10);
      int var5 = 2 + this.a.nextInt(3);
      if (this.a.nextInt(5) != 0) {
         int var10000 = this.e;
      }

      int var1 = this.a;
      int var2 = this.a + this.c;
      int var3 = this.b;
      int var4 = this.b + this.d;
      int var13 = 0;
      this.a((float)var1 - var14, (float)var2 + var14, (float)var3 - var14, (float)var4 + var14, 0.0F, (float)var5);

      while(true) {
         int var9 = this.e - var5;
         int var8 = var4 - var3;
         int var7 = var2 - var1;
         int var6 = Math.max(var9 / var11, 2);
         if (var9 < 10) {
            var6 = var9;
         }

         float var15 = (float)this.a.nextInt(32) / 32.0F;
         var15 = this.a(var1, var5, var4, 1, var8, var6, var15) - 0.03125F;
         var15 = this.a(var1, var5, var3, 2, var7, var6, var15) - 0.03125F;
         var15 = this.a(var2, var5, var3, 0, var8, var6, var15) - 0.03125F;
         this.a(var2, var5, var4, 3, var7, var6, var15);
         if ((var5 += var6) + var10 > this.e) {
            break;
         }

         this.a((float)var1 - var14, (float)var2 + var14, (float)var3 - var14, (float)var4 + var14, (float)var5, (float)(var5 + var10));
         if ((var5 += var10) > this.e) {
            break;
         }

         ++var13;
         if (var13 % var12 == 0) {
            if (var7 > 7) {
               ++var1;
               --var2;
            }

            if (var8 > 7) {
               ++var3;
               --var4;
            }
         }
      }

      this.a((float)var1, (float)var2, (float)var3, (float)var4, (float)var5);
   }
}
