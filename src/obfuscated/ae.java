package obfuscated;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public class ae extends X {
   private static float a = 2.0F;
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();
   private Vector3f a = new Vector3f();
   private Vector3f b = new Vector3f();

   public ae(X[] var1, Vector3i var2, Vector3i var3, int var4) {
      super(var1, var2, var3, 5, var4);
   }

   public short a() {
      return 75;
   }

   public short b() {
      return 76;
   }

   public short c() {
      return 78;
   }

   public short d() {
      return 5;
   }

   public short e() {
      return 55;
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      a(this.e, this.d);
      if ((float)this.c.y < (float)(this.d.y / 2) + a) {
         short var3 = this.d();
         if (this.c.y == this.d.y / 2) {
            var3 = this.a();
         } else if (this.c.y != this.d.y / 2 + 1 && this.c.y != this.d.y / 2 - 1) {
            if (this.c.z % 8 == 0) {
               if (this.c.y == 2) {
                  var3 = this.e();
               } else {
                  var3 = this.c();
               }
            }
         } else {
            var3 = this.b();
         }

         if (this.c.y != 0 && this.c.y != this.d.y - 1) {
            return this.c.x != this.d.x - 1 && this.c.x != 0 ? 0 : var3;
         } else {
            return this.c.y == 0 && this.c.x > 0 && this.c.x % 10 == 0 && this.c.z % 5 == 0 ? this.e() : this.d();
         }
      } else {
         this.a.set((float)this.c.x + 0.5F, (float)this.c.y * 1.3F, 0.0F);
         this.b.set((float)this.d.x / 2.0F, (float)this.d.y / 2.0F + a, 0.0F);
         this.a.sub(this.b);
         float var2;
         return (var2 = this.a.length()) >= (float)this.d.x / 2.0F + 0.5F || var2 <= (float)this.d.x / 2.0F - 1.5F && this.c.y != this.d.y - 1 && this.c.y != this.d.y - 2 && this.c.z != this.d.z - 1 ? 0 : this.d();
      }
   }
}
