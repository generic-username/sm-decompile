package obfuscated;

import javax.vecmath.Vector2f;
import org.schema.common.util.linAlg.Vector3i;

public final class aa extends X {
   private Vector2f a = new Vector2f();
   private Vector2f b = new Vector2f();
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();

   public aa(X[] var1, Vector3i var2, Vector3i var3) {
      super(var1, var2, var3, 5, 0);
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      a(this.e, this.d);
      this.a.set((float)this.c.x + 0.5F, (float)this.c.z + 0.5F);
      this.b.set((float)(this.d.x / 2), (float)(this.d.z / 2));
      this.b.sub(this.a);
      if (this.b.length() <= (float)(this.d.x / 2)) {
         if (this.c.y % 9 < 3) {
            return 63;
         } else if (this.c.y % 9 == 6) {
            return 5;
         } else if (this.c.y % 9 == 7) {
            return 55;
         } else {
            return (short)(this.c.y % 9 == 8 ? 5 : 75);
         }
      } else {
         return 0;
      }
   }
}
