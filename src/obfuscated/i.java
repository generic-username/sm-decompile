package obfuscated;

import java.util.Random;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.controller.GenerationElementMap;

public class i extends d {
   private static final int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)151));
   private static final int b = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)64));

   public i(long var1) {
      super(var1);
   }

   protected final int a(float var1, Random var2) {
      return var1 < 0.01F && var2.nextFloat() > 0.8F ? b : a;
   }

   public final void a() {
      this.a = new aO[2];
      this.a[0] = new aJ((short)486, (short)151);
      this.a[1] = new aJ((short)494, (short)151);
   }

   protected final void a(byte var1, byte var2, byte var3, A var4, Random var5) {
      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)486, (short)151, a);
      }

      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)494, (short)151, a);
      }

   }
}
