package obfuscated;

import java.util.Random;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;

public abstract class c {
   public static short a(Random var0) {
      int var1 = var0.nextInt(16) + 1;
      return ElementKeyMap.orientationToResIDMapping[var1];
   }

   public boolean a(int var1, int var2, int var3, Segment var4, short var5) throws SegmentDataWriteException {
      if (var5 == 0) {
         return false;
      } else if (!var4.getSegmentData().containsUnsave((byte)var1, (byte)var2, (byte)var3)) {
         var4.getSegmentData().setInfoElementForcedAddUnsynched((byte)var1, (byte)var2, (byte)var3, var5, false);
         return true;
      } else {
         return false;
      }
   }

   public boolean a(int var1, int var2, Segment var3, byte var4, boolean var5) throws SegmentDataWriteException {
      if (!var3.getSegmentData().containsUnsave((byte)((byte)var1), (byte)16, (byte)((byte)var2))) {
         if (var4 == 5) {
            var4 = 4;
         } else if (var4 == 4) {
            var4 = 5;
         }

         var3.getSegmentData().setInfoElementForcedAddUnsynched((byte)var1, (byte)16, (byte)var2, (short)662, var4, (byte)(var5 ? 1 : 0), false);
         return true;
      } else {
         return false;
      }
   }

   public abstract void a(SegmentController var1, Segment var2, RequestData var3);
}
