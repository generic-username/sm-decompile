package obfuscated;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public final class ab extends X {
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();
   private Vector3f a = new Vector3f();
   private Vector3f b = new Vector3f();

   public ab(X[] var1, Vector3i var2, Vector3i var3) {
      super(var1, var2, var3, 5, 0);
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      a(this.e, this.d);
      float var2;
      if (this.c.y < this.d.y / 2) {
         this.a.set((float)this.c.x + 0.5F, 0.0F, (float)this.c.z + 0.5F);
         this.b.set((float)this.d.x / 2.0F, 0.0F, (float)this.d.z / 2.0F);
         this.a.sub(this.b);
         if ((var2 = this.a.length()) < 1.0F && this.c.y < this.d.y - 15) {
            return 2;
         } else if (var2 < (float)this.d.x / 2.0F + 0.5F && (var2 > (float)this.d.x / 2.0F - 1.5F || this.c.y <= 1)) {
            return (short)(this.c.y > 0 && this.c.y % 20 == 0 ? 77 : 75);
         } else {
            return 0;
         }
      } else {
         this.a.set((float)this.c.x + 0.5F, 0.0F, (float)this.c.z + 0.5F);
         this.b.set((float)this.d.x / 2.0F, 0.0F, (float)this.d.z / 2.0F);
         this.a.sub(this.b);
         if ((var2 = this.a.length()) < 1.0F && this.c.y < this.d.y - 15) {
            return 3;
         } else if (var2 >= (float)this.d.x / 2.0F + 0.5F || var2 <= (float)this.d.x / 2.0F - 1.5F && (float)this.c.y < (float)this.d.y - 1.0F) {
            return 0;
         } else {
            return (short)(this.c.y > 0 && this.c.y % 20 == 0 ? 77 : 75);
         }
      }
   }
}
