package obfuscated;

import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.data.GameServerState;

public final class ay extends c {
   private static short[] a = new short[16384];
   private Vector3i a = new Vector3i();
   private X[] a;
   private aB a;
   private short a = 69;
   private Random a;
   private ShortArrayList a = new ShortArrayList(6);
   private int a;
   private Vector3i b = new Vector3i(-5, 0, -5);
   private Vector3i c = new Vector3i(5, 60, 5);

   public ay(long var1) {
      if (this.a == null) {
         this.a = new Random(var1);
      }

      if (this.a == null) {
         this.a = new aE(this.a);
      }

      this.a = this.a.nextInt(15);
      this.a = new X[40];
      X[] var10000 = this.a;
      Vector3i var2 = new Vector3i(0, 1, 0);
      Vector3i var3;
      (var3 = new Vector3i(-5, 0, -5)).add(var2);
      Vector3i var4;
      (var4 = new Vector3i(5, 52, 5)).add(var2);
      var10000[0] = new aa(this.a, var3, var4);
      var10000 = this.a;
      var2 = new Vector3i(0, 1, 0);
      (var3 = new Vector3i(-4, 0, -4)).add(var2);
      (var4 = new Vector3i(4, 52, 4)).add(var2);
      var10000[1] = new Z(this.a, var3, var4);
      this.a[2] = this.a(new Vector3i(0, 50, 0), 4, 0);
      this.a[3] = this.a(new Vector3i(0, 50, 0), 4, 1);
      this.a[4] = this.b(new Vector3i(0, 50, 0), 4, 0);
      this.a[5] = this.b(new Vector3i(0, 50, 0), 4, 1);
      this.a[6] = this.a(new Vector3i(0, 50, -55), 4, 0);
      this.a[7] = this.a(new Vector3i(0, 50, -55), 4, 1);
      this.a[8] = this.b(new Vector3i(0, 50, -55), 4, 0);
      this.a[9] = this.b(new Vector3i(0, 50, -55), 4, 1);
      this.a[10] = this.a(new Vector3i(0, 50, 0), 3);
      this.a[11] = this.a(new Vector3i(0, 5, 0), 5, 0);
      this.a[12] = this.a(new Vector3i(0, 5, 0), 5, 1);
      this.a[13] = this.b(new Vector3i(0, 5, 0), 5, 0);
      this.a[14] = this.b(new Vector3i(0, 5, 0), 5, 1);
      this.a[15] = this.a(new Vector3i(0, 5, -55), 5, 0);
      this.a[16] = this.a(new Vector3i(0, 5, -55), 5, 1);
      this.a[17] = this.b(new Vector3i(0, 5, -55), 5, 0);
      this.a[18] = this.b(new Vector3i(0, 5, -55), 5, 1);
      this.a[19] = this.a(new Vector3i(0, 4, 0), 4);
      this.a[20] = this.a(new Vector3i(30, 0, 30));
      this.a[21] = this.a(new Vector3i(-30, 0, 30));
      this.a[22] = this.a(new Vector3i(-30, 0, -30));
      this.a[23] = this.a(new Vector3i(30, 0, -30));
      this.a[24] = this.b(new Vector3i(0, 48, -62), 1);
      this.a[25] = this.b(new Vector3i(0, 48, 62), 0);
      this.a[26] = this.b(new Vector3i(62, 48, 0), 4);
      this.a[27] = this.b(new Vector3i(-62, 48, 0), 5);
      this.a[28] = this.b(new Vector3i(0, 6, -62), 1);
      this.a[29] = this.b(new Vector3i(0, 6, 62), 0);
      this.a[30] = this.b(new Vector3i(62, 6, 0), 4);
      this.a[31] = this.b(new Vector3i(-62, 6, 0), 5);
      this.a[32] = this.b(new Vector3i(0, 54, -10), 2);
      this.a[33] = this.b(new Vector3i(15, 0, 15), 3);
      this.a[34] = new ad(new Vector3i(2, 1, -2), this.a, new Vector3i(-3, 0, -3), new Vector3i(3, 2, 3), 20);
      this.a[35] = new ad(new Vector3i(2, 1, 2), this.a, new Vector3i(-3, 0, -3), new Vector3i(3, 2, 3), 20);
      this.a[36] = new ad(new Vector3i(-2, 1, 2), this.a, new Vector3i(-3, 0, -3), new Vector3i(3, 2, 3), 20);
      this.a[37] = new ad(new Vector3i(-2, 1, -2), this.a, new Vector3i(-3, 0, -3), new Vector3i(3, 2, 3), 20);
      this.a[38] = this.b(new Vector3i(0, 0, 35));
      this.a[39] = this.b(new Vector3i(0, 0, -35));

      for(int var5 = 0; var5 < this.a.length; ++var5) {
         this.a[var5].a();
      }

   }

   private X a(Vector3i var1) {
      Vector3i var2;
      (var2 = new Vector3i(this.b)).add(var1);
      Vector3i var3;
      (var3 = new Vector3i(this.c)).add(var1);
      return new V(this.a, var2, var3);
   }

   private X a(Vector3i var1, int var2) {
      Vector3i var3;
      (var3 = new Vector3i(-57.0F, (float)(-var2), -57.0F)).add(var1);
      Vector3i var4;
      (var4 = new Vector3i(57.0F, (float)var2, 57.0F)).add(var1);
      return new Y(this.a, var3, var4);
   }

   private X b(Vector3i var1) {
      Vector3i var2;
      (var2 = new Vector3i(this.b)).add(var1);
      Vector3i var3;
      (var3 = new Vector3i(this.c)).add(var1);
      return new ab(this.a, var2, var3);
   }

   private X a(Vector3i var1, int var2, int var3) {
      Vector3i var4;
      (var4 = new Vector3i(-5, -var2, 0)).add(var1);
      Vector3i var5;
      (var5 = new Vector3i(5, var2, 55)).add(var1);
      this.a.add((short)75);
      this.a.add((short)77);
      this.a.add((short)69);
      this.a.add((short)79);
      this.a.add((short)76);
      this.a.add((short)5);
      this.a.add((short)63);
      Collections.shuffle(this.a, this.a);
      return new ae(this.a, var4, var5, var3) {
         public final short a() {
            return ay.this.a.getShort(1);
         }

         public final short b() {
            return ay.this.a.getShort(2);
         }

         public final short c() {
            return ay.this.a.getShort(3);
         }

         public final short d() {
            return ay.this.a.getShort(0);
         }

         public final short e() {
            return 282;
         }
      };
   }

   private X b(Vector3i var1, int var2, int var3) {
      Vector3i var4;
      (var4 = new Vector3i(-4, -(var2 - 1), 0)).add(var1);
      Vector3i var5;
      (var5 = new Vector3i(4, var2 - 1, 55)).add(var1);
      return new af(this.a, var4, var5, var3);
   }

   private X b(Vector3i var1, int var2) {
      Vector3i var3;
      (var3 = new Vector3i(-20, -20, -20)).add(var1);
      Vector3i var4;
      (var4 = new Vector3i(20, 20, 20)).add(var1);
      return new S(var1, this.a, var3, var4, (byte)var2, this.a);
   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      for(int var27 = 0; var27 < this.a.length; ++var27) {
         if (this.a[var27] instanceof ag) {
            ((ag)this.a[var27]).a = var2.getSegmentController().getControlElementMap();
         }
      }

      this.a.setSeed(var1.getSeed() + (long)(var2.pos.x << 4) + (long)(var2.pos.y << 8) + (long)var2.pos.z);
      int var4;
      if (var2.pos.y >= 0 && FastMath.sqrt((float)(var2.pos.x * var2.pos.x + var2.pos.y * var2.pos.y)) <= 96.0F) {
         Arrays.fill(a, (short)0);
         aB var10000 = this.a;
         long var10001 = var1.getSeed();
         int var10002 = ByteUtil.divUSeg(var2.pos.x);
         ByteUtil.divUSeg(Math.abs(var2.pos.y));
         var10000.a(var10001, var10002, ByteUtil.divUSeg(var2.pos.z), a, this.a);

         try {
            int var32 = var2.pos.x;
            var10002 = var2.pos.y;
            var10002 = var2.pos.z;
            short[] var10003 = a;
            SegmentData var7 = var2.getSegmentData();
            short[] var6 = var10003;
            int var5 = var10002;
            var4 = var32;
            ay var28 = this;

            for(int var8 = 0; var8 < 16; ++var8) {
               for(int var9 = 0; var9 < 16; ++var9) {
                  int var10 = var4 + var9;
                  int var11 = var5 + var8;
                  if (FastMath.sqrt((float)(var10 * var10 + var11 * var11)) < 80.0F) {
                     SegmentData var14 = var7;
                     short[] var13 = var6;
                     int var12 = var8;
                     var11 = var9;
                     ay var30 = var28;
                     int var15;
                     int var16 = (var15 = Math.abs(var7.getSegment().pos.y)) + 16;

                     for(int var17 = 63; var17 >= 0; --var17) {
                        if (var17 < 64) {
                           int var18 = ((var11 << 4) + var12 << 7) + var17;
                           if (var17 >= var15 && var17 < var16) {
                              Segment var19 = var14.getSegment();
                              var30.a.set(var19.pos.x + var11, var17, var19.pos.z + var12);
                              boolean var20 = false;
                              X[] var21;
                              int var22 = (var21 = var30.a).length;

                              int var23;
                              for(var23 = 0; var23 < var22; ++var23) {
                                 X var24;
                                 if ((var24 = var21[var23]).a(var30.a)) {
                                    short var37 = var24.b(var30.a);
                                    if ((var24.a >= 6 || var13[var18] == 0) && var37 != 32767) {
                                       byte var25 = var24.a(var30.a);
                                       byte var31 = (byte)var11;
                                       byte var33 = (byte)(var17 % 16);
                                       byte var35 = (byte)var12;
                                       byte var44 = var25;
                                       byte var38 = var35;
                                       byte var36 = var33;
                                       byte var39 = var31;
                                       if (var37 != 0 && !var19.getSegmentData().containsUnsave((byte)var39, (byte)var36, (byte)var38)) {
                                          if (var25 == 5) {
                                             var44 = 4;
                                          } else if (var25 == 4) {
                                             var44 = 5;
                                          }

                                          var19.getSegmentData().setInfoElementForcedAddUnsynched((byte)var39, (byte)var36, (byte)var38, var37, var44, ElementInformation.activateOnPlacement(var37), false);
                                       }
                                    }

                                    var20 = true;
                                    break;
                                 }
                              }

                              if (!var20 && var13[var18] == 0 && var11 >= 0 && var17 >= 0 && var12 >= 0 && var11 < 16 && var17 < 128 && var12 < 16) {
                                 int var40 = ((var11 - 1 << 4) + var12 << 7) + var17;
                                 var22 = ((var11 + 1 << 4) + var12 << 7) + var17;
                                 var23 = ((var11 << 4) + var12 << 7) + var17 + 1;
                                 int var46 = ((var11 << 4) + var12 << 7) + (var17 - 1);
                                 int var41 = ((var11 << 4) + var12 + 1 << 7) + var17;
                                 int var45 = ((var11 << 4) + (var12 - 1) << 7) + var17;
                                 short var34;
                                 if (var11 > 0 && (var34 = var13[var40]) > 0 || var11 < 15 && (var34 = var13[var22]) > 0 || var17 < 127 && (var34 = var13[var23]) > 0 || var17 > 0 && (var34 = var13[var46]) > 0 || var12 < 15 && (var34 = var13[var41]) > 0 || var12 > 0 && (var34 = var13[var45]) > 0) {
                                    if (var17 >= 62 && var30.a.nextInt(16) == 0) {
                                       var14.setInfoElementForcedAddUnsynched((byte)var11, (byte)(var17 % 16), (byte)var12, (short)55, false);
                                    } else {
                                       var14.setInfoElementForcedAddUnsynched((byte)var11, (byte)(var17 % 16), (byte)var12, var34, false);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }

            if (var2.pos.equals(0, 0, 0)) {
               var2.getSegmentData().setInfoElementForcedAddUnsynched((byte)8, (byte)8, (byte)8, (short)70, false);
               var2.getSegmentData().setInfoElementForcedAddUnsynched((byte)8, (byte)9, (byte)8, (short)291, false);
            }
         } catch (SegmentDataWriteException var26) {
            var26.printStackTrace();
         }
      }

      GameServerState var29 = (GameServerState)var2.getSegmentController().getState();

      for(var4 = 0; var4 < this.a.length; ++var4) {
         if (this.a[var4] instanceof ag && ((ag)this.a[var4]).a()) {
            ((ag)this.a[var4]).a(var29.getCreatorHooks(), var2);
         }
      }

      var1.getSegmentBuffer().updateBB(var2);
   }
}
