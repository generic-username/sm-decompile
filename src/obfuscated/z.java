package obfuscated;

import com.bulletphysics.linearmath.MatrixUtil;
import java.util.Random;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.GenerationElementMap;
import org.schema.game.server.controller.RequestDataStructureGen;

public class z extends w {
   private static int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)64));
   private static int b = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)286));
   private static v a;
   private static Matrix3f[] a;

   public final void a(Segment var1, RequestDataStructureGen var2, int var3, int var4, int var5, short var6, short var7, short var8) throws SegmentDataWriteException {
      float var16 = (float)var6;
      float var17 = (float)var7;
      float var9 = var16 / var17;
      int var10 = FastMath.fastCeil(var16) + 1;
      int var18 = FastMath.fastCeil(var17);
      var18 = Math.max(var10, var18);
      SegmentData var20 = var1.getSegmentData();
      Matrix3f var19 = a[var8 & 63];
      Vector3f var11 = new Vector3f();

      for(byte var13 = (byte)Math.max(0, var3 - var18); var13 < Math.min(32, var3 + var18 + 1); ++var13) {
         for(byte var14 = (byte)Math.max(0, var4 - var18); var14 < Math.min(32, var4 + var18 + 1); ++var14) {
            for(byte var15 = (byte)Math.max(0, var5 - var18); var15 < Math.min(32, var5 + var18 + 1); ++var15) {
               var11.set((float)(var13 - var3), (float)(var14 - var4), (float)(var15 - var5));
               var19.transform(var11);
               float var12;
               if ((var12 = FastMath.carmackInvSqrt(var12 = var11.x * var11.x + var11.z * var11.z) * var12 + a.d((float)(var13 + var1.pos.x), (float)(var14 + var1.pos.y), (float)(var15 + var1.pos.z))) * 2.0F < var16 * 0.5F - Math.abs(var11.y) * var9) {
                  var2.currentChunkCache.placeBlock(b, var13, var14, var15, var20);
               } else if (var12 < var16 - Math.abs(var11.y) * var9) {
                  var2.currentChunkCache.placeBlock(a, var13, var14, var15, var20);
               }
            }
         }
      }

   }

   static {
      (a = new v()).a(0.03F);
      a.a(v.e.c);
      a.a();
      a = new Matrix3f[64];
      Random var0 = new Random(1337L);

      for(int var1 = 0; var1 < a.length; ++var1) {
         a[var1] = new Matrix3f();
         MatrixUtil.setEulerZYX(a[var1], (var0.nextFloat() - 0.5F) * 1.2F, var0.nextFloat() * 6.2831855F, 0.0F);
      }

   }
}
