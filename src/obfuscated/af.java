package obfuscated;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public final class af extends X {
   private static float a = 2.0F;
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();
   private Vector3f a = new Vector3f();
   private Vector3f b = new Vector3f();

   public af(X[] var1, Vector3i var2, Vector3i var3, int var4) {
      super(var1, var2, var3, 6, var4);
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      a(this.e, this.d);
      if ((float)this.c.y < (float)(this.d.y / 2) + a) {
         return 0;
      } else {
         this.a.set((float)this.c.x + 0.5F, (float)this.c.y * 1.3F, 0.0F);
         this.b.set((float)this.d.x / 2.0F, (float)this.d.y / 2.0F + a, 0.0F);
         this.a.sub(this.b);
         float var2;
         return (short)((var2 = this.a.length()) >= (float)this.d.x / 2.0F + 0.5F || var2 <= (float)this.d.x / 2.0F - 1.5F && this.c.y != this.d.y - 1 && this.c.y != this.d.y - 2 && this.c.z != this.d.z - 1 ? 5 : 0);
      }
   }
}
