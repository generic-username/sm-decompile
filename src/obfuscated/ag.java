package obfuscated;

import java.util.Collection;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ControlElementMap;
import org.schema.game.common.data.world.Segment;

public abstract class ag extends X {
   public final Vector3i c;
   public ControlElementMap a;
   private byte a;

   public ag(Vector3i var1, X[] var2, Vector3i var3, Vector3i var4, int var5, byte var6) {
      super(var2, var3, var4, var5, 0);
      this.c = var1;
      this.a = var6;
   }

   public final void a(Collection var1, Segment var2) {
      av var5 = this.a(var2);
      synchronized(var1) {
         var1.add(var5);
      }
   }

   public abstract av a(Segment var1);

   public final byte a(Vector3i var1) {
      return var1.equals(this.c) ? this.a : -1;
   }

   public abstract boolean a();
}
