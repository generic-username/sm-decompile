package obfuscated;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.Logbook;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

public final class au extends av {
   public final void a() {
      float var1 = (Float)ServerConfig.CHEST_LOOT_COUNT_MULTIPLIER.getCurrentState();
      float var2 = (Float)ServerConfig.CHEST_LOOT_STACK_MULTIPLIER.getCurrentState();
      System.err.println("[SERVER] EXECUTING REGION HOOK: " + this + ": " + ((ad)this.a).c);
      SegmentController var3;
      GameServerState var4 = (GameServerState)(var3 = this.a.getSegmentController()).getState();
      Inventory var12;
      if ((var12 = ((ManagedSegmentController)var3).getManagerContainer().getInventory(((ad)this.a).c)) == null) {
         System.err.println("[REGIONHOOK] Exception: Inventory not found at: " + ((ad)this.a).c);
      } else {
         try {
            IntOpenHashSet var5 = new IntOpenHashSet();
            int var6;
            if (var1 > 0.0F && var2 > 0.0F) {
               int var10 = Math.min((int)((float)Universe.getRandom().nextInt(45) * var1), 45);

               for(var6 = 0; var6 < var10; ++var6) {
                  ElementKeyMap.keySet.iterator();
                  int var7 = Universe.getRandom().nextInt(ElementKeyMap.typeList().length);
                  short var13;
                  if (ElementKeyMap.getInfo(var13 = ElementKeyMap.typeList()[var7]).isShoppable()) {
                     int var8 = (int)((float)(Universe.getRandom().nextInt(1000) + 100) * var2);
                     System.err.println("putting into CHEST: " + ElementKeyMap.getInfo(var13).getName() + " #" + var8);
                     var5.add(var12.putNextFreeSlot(var13, var8, -1));
                  }
               }
            }

            if (Universe.getRandom().nextInt(3) == 0) {
               MetaObject var11;
               ((Logbook)(var11 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.LOG_BOOK, (short)-1, true))).setTxt(Logbook.getRandomEntry(var4));
               var6 = var12.getFreeSlot();
               var12.put(var6, var11);
               var5.add(var6);
            }

            if (var5.size() > 0) {
               var12.sendInventoryModification(var5);
            }

         } catch (NoSlotFreeException var9) {
         }
      }
   }
}
