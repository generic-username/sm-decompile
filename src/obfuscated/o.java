package obfuscated;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Map;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public class o extends p {
   private aO[] a;
   // $FF: synthetic field
   private static boolean b = !o.class.desiredAssertionStatus();

   public o(long var1, Vector3f[] var3, float var4) {
      super(var1, var3, var4);
      Random var5 = new Random(var1);
      this.a = new aO[7];
      this.a[0] = new aK(a(var5), (short)73);
      this.a[1] = new aK(a(var5), (short)73);
      this.a[2] = new aQ();
      this.a[3] = new aI((short)93, new short[]{82, 74}, (byte)0);
      this.a[4] = new aI((short)98, new short[]{82, 74}, (byte)0);
      this.a[5] = new aI((short)102, new short[]{82, 74}, (byte)0);
      this.a[6] = new aI((short)106, new short[]{82, 74}, (byte)0);
   }

   public final void a(Random var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      Vector3i var3 = new Vector3i(8, ak.c + 6, 8);
      ad var6 = new ad(var3, (X[])null, new Vector3i(var3.x - 1, var3.y, var3.z - 1), new Vector3i(var3.x + 1, var3.y + 1, var3.z + 1), 40);
      var2.add(var6);
      (new an(var1, var2)).a();
      Object[] var5 = var2.elements();
      X[] var7 = new X[var2.size()];
      System.err.println("[EARTH] created extra regions: " + var2.size());

      int var4;
      for(var4 = 0; var4 < var2.size(); ++var4) {
         if (!b && var5[var4] == null) {
            throw new AssertionError();
         }

         var7[var4] = (X)var5[var4];
      }

      for(var4 = 0; var4 < var7.length; ++var4) {
         var7[var4].a = var7;
      }

      this.a.a = new Object2ObjectOpenHashMap();

      for(var4 = 0; var4 < var7.length; ++var4) {
         var7[var4].a((Map)this.a.a);
      }

      this.a.a(var7);
   }

   public final short a() {
      return 74;
   }

   public final aO[] a() {
      return this.a;
   }

   public final short b() {
      return 73;
   }

   public final short c() {
      return 82;
   }
}
