package obfuscated;

import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.controller.world.factory.terrain.TerrainGenerator;
import org.schema.game.server.data.ServerConfig;

public abstract class p extends c {
   public final Vector3f[] a;
   public float a;
   private long a;
   protected boolean a = false;
   public TerrainGenerator a;

   public p(long var1, Vector3f[] var3, float var4) {
      this.a = var1;
      this.a = var3;
      this.a = var4;
   }

   public abstract void a(Random var1);

   public void a(SegmentController var1, Segment var2, RequestData var3) {
      synchronized(this) {
         if (!this.a) {
            this.a(var1);
         }
      }

      try {
         this.a(var2, (RequestDataPlanet)var3);
      } catch (SegmentDataWriteException var5) {
         var5.printStackTrace();
      }
   }

   public final void a(SegmentController var1) {
      this.a = new TerrainGenerator(((Planet)var1).getSeed());
      this.a.a(this);
      this.b(var1);
      this.a = true;
   }

   protected final void a(Segment var1, RequestDataPlanet var2) throws SegmentDataWriteException {
      if (!var2.done) {
         var2.initWith(var1);

         for(int var3 = 0; var3 < var2.segs.length; ++var3) {
            var2.index = var3;

            for(int var4 = 0; var4 < var2.getR().segmentData.length; ++var4) {
               Chunk16SegmentData var5 = var2.getR().segmentData[var4];
               TerrainGenerator var10000 = this.a;
               Vector3i var10002 = var5.getSegmentPos();
               int var10003 = 64 + var5.getSegmentPos().x / TerrainGenerator.a;
               int var10004 = Math.abs(var5.getSegmentPos().y) / TerrainGenerator.a;
               int var10005 = 64 + var5.getSegmentPos().z / TerrainGenerator.a;
               int var10006 = var5.getSegmentPos().y;
               var10000.a(var5, var10002, var10003, var10004, var10005, var2);
               this.a.a(var1);
            }
         }

         var2.done = true;
      }

      var2.applyTo(var1);
   }

   public short d() {
      return 80;
   }

   public abstract short a();

   public abstract aO[] a();

   public abstract short b();

   public abstract short c();

   protected final void b(SegmentController var1) {
      Random var2;
      if ((var2 = new Random(this.a)).nextInt((Integer)ServerConfig.PLANET_SPECIAL_REGION_PROBABILITY.getCurrentState()) == 0 || var1.forceSpecialRegion) {
         this.a(var2);
      }

   }
}
