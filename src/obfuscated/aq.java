package obfuscated;

import org.schema.common.util.linAlg.Vector3i;

public final class aq extends X {
   public aq(an var1, Vector3i var2, Vector3i var3) {
      super((X[])null, var2, var3, 7, 0);
      var2.y += ak.c;
      var3.y += ak.c;
      var2.x -= var1.a;
      var2.z -= var1.a;
      var3.x -= var1.a;
      var3.z -= var1.a;
   }

   protected final short a(Vector3i var1) {
      return (short)(var1.y - this.a.y == 0 ? 75 : 0);
   }
}
