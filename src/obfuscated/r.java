package obfuscated;

import java.util.Random;
import javax.vecmath.Vector3f;

public final class r extends p {
   private aO[] a;

   public r(long var1, Vector3f[] var3, float var4) {
      super(var1, var3, var4);
      Random var5 = new Random(var1);
      this.a = new aO[6];
      this.a[0] = new aK(a(var5), (short)139);
      this.a[1] = new aK(a(var5), (short)139);
      this.a[2] = new aI((short)96, new short[]{138, 140}, (byte)0);
      this.a[3] = new aI((short)104, new short[]{138, 140}, (byte)0);
      this.a[4] = new aI((short)100, new short[]{138, 140}, (byte)0);
      this.a[5] = new aI((short)108, new short[]{138, 140}, (byte)0);
   }

   public final void a(Random var1) {
   }

   public final short a() {
      return 140;
   }

   public final aO[] a() {
      return this.a;
   }

   public final short b() {
      return 139;
   }

   public final short c() {
      return 138;
   }
}
