package obfuscated;

import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataAsteroid;
import org.schema.game.server.controller.TerrainChunkCacheElement;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;
import org.schema.game.server.data.ServerConfig;

public abstract class d extends c {
   private long a;
   protected aO[] a;
   private boolean a = false;
   private boolean b = false;
   private FastNoiseSIMD a;
   private FastNoiseSIMD b;
   private Vector3i a;
   private Vector3i b;
   private int a;
   private int b;
   private int c;
   protected static short a;
   protected static float a;

   public d(long var1) {
      this.a = var1;
   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      RequestDataAsteroid var21;
      Random var4 = (var21 = (RequestDataAsteroid)var3).random;
      float[] var5 = var21.noiseSetBase;
      float[] var6 = var21.noiseSetVeins;
      if (!this.a) {
         synchronized(this) {
            if (!this.a) {
               new Random(this.a);
               this.a();
               this.a = new Vector3i(((FloatingRock)var1).getLoadedOrGeneratedMinPos());
               this.b = new Vector3i(((FloatingRock)var1).getLoadedOrGeneratedMaxPos());
               this.a = ((FloatingRock)var1).getLoadedOrGeneratedSizeGen().x;
               this.b = ((FloatingRock)var1).getLoadedOrGeneratedSizeGen().y;
               this.c = ((FloatingRock)var1).getLoadedOrGeneratedSizeGen().z;
               this.a = new FastNoiseSIMD((int)this.a);
               this.a.a(FastNoiseSIMD.d.b);
               this.a.a(0.008F);
               this.a.a(4);
               this.a.b(0.5F);
               this.a.a();
               this.a.a(FastNoiseSIMD.c.a);
               if (this.b = this.a() != -1) {
                  this.b = new FastNoiseSIMD((int)this.a);
                  this.b.a(FastNoiseSIMD.d.a);
                  this.b.a(0.05F);
               }

               this.a = true;
            }
         }
      }

      int var9 = var2.pos.z;
      int var8 = var2.pos.y;
      int var7 = var2.pos.x;
      var7 = ByteUtil.divSeg(var7);
      var8 = ByteUtil.divSeg(var8);
      var9 = ByteUtil.divSeg(var9);
      if (var7 <= this.b.x && var8 <= this.b.y && var9 <= this.b.z && var7 >= this.a.x && var8 >= this.a.y && var9 >= this.a.z) {
         var4.setSeed(this.a + (long)var2.pos.hashCode());
         Vector3i var22 = new Vector3i();
         Segment.getSegmentIndexFromSegmentElement(var2.pos.x, var2.pos.y, var2.pos.z, var22);
         var22.x <<= 5;
         var22.y <<= 5;
         var22.z <<= 5;
         A var20 = new A();
         TerrainChunkCacheElement var23 = var21.currentChunkCache;
         SegmentData var24 = var2.getSegmentData();
         this.a.a(var5, var22.x + 1337, var22.y - 268, var22.z + 1282);
         boolean var10 = false;
         int var11 = 0;
         Vector3f var12 = new Vector3f(2.0F / (float)this.a, 2.0F / (float)this.b, 2.0F / (float)this.c);
         float var13 = FastMath.carmackInvSqrt((float)(this.a * this.a + this.b * this.b + this.c * this.c)) * 120.0F;

         try {
            for(byte var14 = 0; var14 < 32; ++var14) {
               for(byte var15 = 0; var15 < 32; ++var15) {
                  for(byte var16 = 0; var16 < 32; ++var16) {
                     float var17;
                     if ((var17 = -FastMath.carmackLength((float)(var14 + var22.x) * var12.x, (float)(var15 + var22.y) * var12.y, (float)(var16 + var22.z) * var12.z) + 1.1F - (var5[var11] + 1.0F) * var13) > 0.0F) {
                        int var25;
                        if (this.b) {
                           if (!var10) {
                              this.b.a(var6, var22.x + 1336, var22.y - 268, var22.z + 12398);
                              var10 = true;
                           }

                           if (var6[var11] > 0.4F) {
                              var25 = this.a();
                           } else {
                              var25 = this.a(var17, var4);
                           }
                        } else {
                           var25 = this.a(var17, var4);
                        }

                        var23.placeBlock(var25, var14, var15, var16, var24);
                        this.a(var14, var15, var16, var20, var4);
                     } else {
                        var23.placeAir();
                     }

                     ++var11;
                  }
               }
            }

            var20.a(var2, var21);
         } catch (SegmentDataWriteException var19) {
            var19.printStackTrace();
         }

         var2.getSegmentController().getSegmentBuffer().updateBB(var2);
      }
   }

   protected abstract void a(byte var1, byte var2, byte var3, A var4, Random var5);

   protected abstract int a(float var1, Random var2);

   public abstract void a();

   protected int a() {
      return -1;
   }

   static {
      a = w.a((Float)ServerConfig.ASTEROID_RESOURCE_SIZE.getCurrentState());
      a = (Float)ServerConfig.ASTEROID_RESOURCE_CHANCE.getCurrentState();
   }
}
