package obfuscated;

import com.bulletphysics.linearmath.Transform;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.data.ServerConfig;

public final class s extends c {
   private final long a;

   public s(long var1) {
      this.a = var1;
   }

   public final boolean a(int var1, int var2, int var3, Segment var4, short var5) throws SegmentDataWriteException {
      if (var5 == 0) {
         return false;
      } else if (!var4.getSegmentData().containsUnsave((byte)var1 + 8, (byte)var2 + 8, (byte)var3 + 8)) {
         var4.getSegmentData().setInfoElementForcedAddUnsynched((byte)(var1 + 8), (byte)(var2 + 8), (byte)(var3 + 8), var5, false);
         return true;
      } else {
         return false;
      }
   }

   public final boolean a(int var1, int var2, Segment var3, byte var4, boolean var5) throws SegmentDataWriteException {
      if (!var3.getSegmentData().containsUnsave((int)((byte)var1 + 8), (int)24, (int)((byte)var2 + 8))) {
         if (var4 == 5) {
            var4 = 4;
         } else if (var4 == 4) {
            var4 = 5;
         }

         var3.getSegmentData().setInfoElementForcedAddUnsynched((byte)(var1 + 8), (byte)24, (byte)(var2 + 8), (short)662, var4, (byte)(var5 ? 1 : 0), false);
         return true;
      } else {
         return false;
      }
   }

   private void a(Segment var1, SegmentController var2, Random var3, Vector3f var4) throws SegmentDataWriteException {
      new Vector3f();
      Transform var5 = new Transform();

      byte var7;
      for(byte var6 = 5; var6 < 11; ++var6) {
         for(var7 = -8; var7 < 24; ++var7) {
            for(byte var8 = 5; var8 < 11; ++var8) {
               if ((var8 > 5 || var6 > 5) && (var8 < 10 || var6 < 10) && (var8 > 5 || var6 < 10) && (var8 < 10 || var6 > 5)) {
                  this.a(var8, var7, var6, var1, (short)5);
               }
            }
         }

         var2.getSegmentBuffer().updateBB(var1);
      }

      for(var7 = (byte)((byte)(var1.pos.equals(0, 0, 0) ? 1 : 0) - 8); var7 < 24; ++var7) {
         if (var3.nextFloat() < 0.1F) {
            this.a(var7, var1, var5, var3, var4);
         }
      }

      if (var1.pos.equals(0, 0, 0)) {
         this.a((byte)0, var1, var5, var3, var4);
         this.a(0, 0, 8, var1, (short)94);
         this.a(15, 0, 8, var1, (short)94);
         this.a(8, 0, 0, var1, (short)94);
         this.a(8, 0, 15, var1, (short)94);
      }

      if (ServerConfig.SHOP_RAILS_ON_NORMAL.isOn() && var1.pos.equals(0, 0, 0)) {
         this.a((byte)16, var1, var5, var3, var4);
         this.a(0, 8, var1, (byte)1, false);
         this.a(15, 8, var1, (byte)4, false);
         this.a(8, 0, var1, (byte)5, true);
         this.a(8, 15, var1, (byte)0, true);
      }

   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      Random var6 = new Random(this.a + ElementCollection.getIndex((short)var2.pos.x, (short)var2.pos.y, (short)var2.pos.z));
      Vector3f var4 = new Vector3f(1.0F, 0.0F, 0.0F);
      if (var2.pos.y < 240 && var2.pos.y > -240 && var2.pos.x == 0 && var2.pos.z == 0) {
         try {
            this.a(var2, var1, var6, var4);
            return;
         } catch (SegmentDataWriteException var5) {
            var5.printStackTrace();
         }
      }

   }

   private void a(byte var1, Segment var2, Transform var3, Random var4, Vector3f var5) throws SegmentDataWriteException {
      int var9 = var4.nextFloat() < 0.5F ? 5 : 75;
      var3.setIdentity();

      for(float var6 = 0.0F; var6 < 6.2831855F; var6 += 0.05F) {
         var5.set(1.0F, 0.0F, 0.0F);
         var3.setIdentity();
         var3.basis.rotY(var6);
         var3.transform(var5);
         var5.scale(7.0F);
         int var7 = 8 + (int)(var5.x - 0.5F);
         int var8 = 8 + (int)(var5.z - 0.5F);
         this.a(var7, var1, var8, var2, (short)var9);
      }

      var2.getSegmentController().getSegmentBuffer().updateBB(var2);
   }
}
